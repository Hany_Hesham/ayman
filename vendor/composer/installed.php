<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '309d2dfbb90ba9b9bc673ab07c7240dde09d22bf',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '309d2dfbb90ba9b9bc673ab07c7240dde09d22bf',
    ),
    'cocur/slugify' => 
    array (
      'pretty_version' => 'v3.2',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd41701efe58ba2df9cae029c3d21e1518cc6780e',
    ),
    'facebook/graph-sdk' => 
    array (
      'pretty_version' => '5.x-dev',
      'version' => '5.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '2d8250638b33d73e7a87add65f47fabf91f8ad9b',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '7.4.x-dev',
      ),
      'reference' => '868b3571a039f0ebc11ac8f344f4080babe2cb94',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.5.x-dev',
      ),
      'reference' => 'fe752aedc9fd8fcca3fe7ad05d419d32998a06da',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.1.x-dev',
      ),
      'reference' => 'c55d23ab371b472342bee943a8bdb9c3cf963a18',
    ),
    'league/oauth2-client' => 
    array (
      'pretty_version' => '2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'badb01e62383430706433191b82506b6df24ad98',
    ),
    'league/oauth2-facebook' => 
    array (
      'pretty_version' => '2.0.5',
      'version' => '2.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '14cead7580cab8caace67f5a61ea5d2a8ff213eb',
    ),
    'mikechip/chatapi' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd883ffb2df76167d0a266dd507d9baa82d9cd379',
    ),
    'neitanod/forceutf8' => 
    array (
      'pretty_version' => 'v2.x-dev',
      'version' => '2.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'c1fbe70bfb5ad41b8ec5785056b0e308b40d4831',
    ),
    'nuovo/spreadsheet-reader' => 
    array (
      'pretty_version' => '0.5.11',
      'version' => '0.5.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6bd49d101042eaa71b0fbd82e7a57e5a276dc3d',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v5.2.28',
      'version' => '5.2.28.0',
      'aliases' => 
      array (
      ),
      'reference' => 'acba50393dd03da69a50226c139722af8b153b11',
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '22b2ef5687f43679481615605d7a15c557ce85b1',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '36fa03d50ff82abcae81860bdaf4ed9a1510c7cd',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => 'efd67d1dc14a7ef4fc4e518e7dee91c271d524e4',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'stripe/stripe-php' => 
    array (
      'pretty_version' => 'v6.43.1',
      'version' => '6.43.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '42fcdaf99c44bb26937223f8eae1f263491d5ab8',
    ),
    'studio-42/elfinder' => 
    array (
      'pretty_version' => '2.1.38',
      'version' => '2.1.38.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5d1221cd876e8cca146886c590a08d721e00b4f',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => '2.5.x-dev',
      'version' => '2.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '6f981ee24cf69ee7ce9736146d1c57c2780598a8',
    ),
    'tecnickcom/tcpdf' => 
    array (
      'pretty_version' => '6.4.2',
      'version' => '6.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '172540dcbfdf8dc983bc2fe78feff48ff7ec1c76',
    ),
    'twilio/sdk' => 
    array (
      'pretty_version' => '5.42.2',
      'version' => '5.42.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '0cfcb871b18a9c427dd9e8f0ed7458d43009b48a',
    ),
    'twisted1919/mailwizz-php-sdk' => 
    array (
      'pretty_version' => 'v1.0.11',
      'version' => '1.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '37444bbfd4cef0ccb496a38f83077505a8b24660',
    ),
  ),
);
