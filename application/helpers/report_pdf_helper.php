<?php
if (!function_exists('report_pdf_generator')){
    function report_pdf_generator($data){ 
    	 $ci=get_instance();
			ob_start();
			ini_set("memory_limit","256M");
			ini_set("pcre.backtrack_limit", "5000000");
			$html='';  
			//--------------------------------------- start of Reservation Report -----------------------------------------------
			    if ($data['report_name']=='Reservations Report') {	     
				          $html .='<tr>';
					      $html .='  
					            <td width="20%"><strong>Reservation Data</strong></td>
					            <td width="3%"><strong>Rooms</strong></td>
					            <td width="10%"><strong>Pax</strong></td>
					            <td width="3%"><strong>Disc</strong></td>
					            <td width="7%"><strong>Type</strong></td>
					            <td width="10%"><strong>Board Type</strong></td>
					            <td width="10%"><strong>Room Type</strong></td>
					            <td width="5%"><strong>Rate</strong></td>
					            <td width="10%"><strong>Req. by</strong></td>
					          </tr>
					        </thead>
					       <tbody>';     
						    foreach ($data['tableData'] as $row) {
						      $html .='<tr>';
				              $html .='    <td>'.$row['res_data'].'</td>
							               <td>'.$row['rooms'].'</td>
							               <td>'.$row['pax'].'</td>
							               <td>'.$row['disc'].'</td>
							               <td>'.$row['type'].'</td>
							               <td>'.$row['board_type'].'</td>
							               <td>'.$row['room_type'].'</td>
							               <td>'.$row['rate'].'</td>
							               <td>'.$row['req_by'].'</td>
						               </tr>';
						            }
			       
			        }
			//------------------------------------- end of eservation Report --------------------------------------------	

			//--------------------------------------- start of Reservation Report -----------------------------------------------
			    if ($data['report_name']==' Report') {	       
				          $html .='<tr>';
					      if ($data['hotels_count']>1) {
					          $width_arr=array('hotel'=>'17%','poSer'=>'12%','item'=>'18%','del'=>'12%','major'=>'10%','total'=>'11%');
					      	  $html .='<td width="'.$width_arr['hotel'].'"><strong>Hotel Name</strong></td>';
					        }elseif ($data['hotels_count']==1) {
					      	 $width_arr=array('poSer'=>'14%','item'=>'20%','del'=>'14%','major'=>'12%','total'=>'15%');
					       }
					      $html .='  
					            <td width="'.$width_arr['pser'].'"><strong>Reservation Data</strong></td>
					            <td width="'.$width_arr['item'].'"><strong>Rooms</strong></td>
					            <td width="'.$width_arr['del'].'"><strong>Pax</strong></td>
					            <td width="'.$width_arr['major'].'"><strong>Disc</strong></td>
					            <td width="'.$width_arr['major'].'"><strong>Type</strong></td>
					            <td width="'.$width_arr['major'].'"><strong>Board Type</strong></td>
					            <td width="'.$width_arr['total'].'"><strong>Room Type</strong></td>
					            <td width="'.$width_arr['major'].'"><strong>Rate</strong></td>
					            <td width="'.$width_arr['total'].'"><strong>Req. by</strong></td>
					          </tr>
					        </thead>
					       <tbody>';     
						    foreach ($data['tableData'] as $row) {
						      $html .='<tr class="'.($row['po_serial']=='Department:' || $row['po_serial']=='Item Group:' 
						      	                     || strstr( $row['item_name'],'Total Report') || strstr( $row['item_name'],'Total Departments') 
						      	                     || strstr( $row['item_name'],'Total Tax') ?'department-row':'').'
						                              '.(strstr( $row['item_name'],'Total Item Group')?'total-department-row':'').'">';
				              $html .=' '.($data['hotels_count']>1?'<td>'.$row['hname'].'</td>':'').'   
							               <td>'.$row['po_serial'].'</td>
							               <td>'.$row['item_name'].'</td>
							               <td>'.$row['del_serial'].'</td>
							               <td>'.$row['unit_name'].'</td>
							               <td>'.$row['quantity'].'</td>
							               <td>'.$row['price'].'</td>
							               <td>'.$row['net_amount'].'</td>
						               </tr>';
						            }
			       
			        }
			//------------------------------------- end of eservation Report --------------------------------------------		        


					    $html .= ' </tbody></table></body></html>';   
					   	ob_end_clean();
						return($html);
						 
             }
         }


?>