<?php

if (!function_exists('pdfStyling')) {
	function pdfStyling(){
		$html ='<html>
					<head>
						<style>
							body {font-family: sans-serif;
							  font-size: 10pt;
							 }
							p { margin: 0pt; }
							table.items {
							  border: 0.1mm solid #000000;
							 }
							td { vertical-align: top; }
							.items td {
							  border-left: 0.1mm solid #000000;
							  border-right: 0.1mm solid #000000;
							 }
							table thead td { 
							  background-color: #343a40;
							  color:#FFFFFF;
							  text-align: center;
							  border: 0.1mm solid #000000;
							  border-bottom: 0.1mm solid #000000;
							 }
							.items td.blanktotal {
							  background-color: #EEEEEE;
							  border: 0.1mm solid #000000;
							  background-color: #FFFFFF;
							  border: 0mm none #000000;
							  border-top: 0.1mm solid #000000;
							  border-right: 0.1mm solid #000000;
							 }
							.items td.totals {
							  text-align: right;
							  border: 0.1mm solid #000000;
							 }
							.items td.cost{
							  text-align: "." center;
							 }
							.department-row{
							  background-color :#EEEEEE;
							  border: 0 !important;
							 }
							.total-department-row{
							  background-color :#ccc;
							  color:#FFF !important;
							  border: 0 !important;
							  }
							a{ 
							  color: inherit;
							  text-decoration: none; 
							  } 

						</style>
					</head>';
		return $html;			
	  }
 }


if (!function_exists('report_pdf_generator')){
    function report_pdf_generator($data){ 
    	 $ci=get_instance();
			ob_start();
			ini_set("memory_limit","1024M");
			ini_set("pcre.backtrack_limit", "50000000000000");
			// header("Content-type: application/octet-stream"); 
			// header("Content-Disposition:attachment;filename=".$data['report_name'].'.pdf'."");
			// header("Cache-control: private");
			// header('Content-Length: ' . filesize($data['report_name'].'.pdf'));
			// readfile($data['report_name'].'.pdf');
			$html = pdfStyling();
			$html.='
					<body>
					<!--mpdf
						<htmlpagefooter name="myfooter">
					    <div style="border-top: 1px solid #000000; font-size: 9pt;padding-top: 3mm; ">
						  <table width="100%">
							 <tr>
				               <td width="35%">'.date("Y-m-d h:i:sa").'</td>
				               <td width="25%">'.$data['username'].'</td>
				               <td width="45%" style="text-align: right;">Page {PAGENO} of {nb}</td>
					         </tr>
						  </table>
					    </div>
						</htmlpagefooter>
						<sethtmlpagefooter name="myfooter" value="on" />
					mpdf-->
					<table width="100%">
						<tr>
							<td width="50%"><br><br>
							  <h2 style="color:#198bbe;">'.$data['hotel']['hotel_name'].'</h2><br>
                              <h3><strong>'.$data['report_name'].'</strong></h3>
					          <h4><strong>From: '.$data['from_date'].' to: '.$data['to_date'].'</strong></h4>
							</td>
							<td width="50%" style="text-align: right;"> <img style="width:200px;height:130px;" src="'.base_url('assets/uploads/logos/'.$data['hotel']['logo']).'" alt="user"><br></td>
						</tr>
					</table>
					<br />
					<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
				       <thead>';  
		//--------------------------------------- start of Reservation Report -----------------------------------------------
			    if ($data['report_name']=='Reservations Report') {	     
				          $html .='<tr>';
					      $html .='  
					            <td width="5%"><strong>#</strong></td>
					            <td width="30%"><strong>Reservation Data</strong></td>
					            <td width="8%"><strong>Rooms</strong></td>
					            <td width="15%"><strong>Pax</strong></td>
					            <td width="7%"><strong>Disc</strong></td>
					            <td width="10%"><strong>Type</strong></td>
					            <td width="10%"><strong>Board Type</strong></td>
					            <td width="10%"><strong>Room Type</strong></td>
					            <td width="7%"><strong>Rate</strong></td>
					            <td width="15%"><strong>Req. by</strong></td>
					          </tr>
					        </thead>
					       <tbody>';
					        $i = 1;     
						    foreach ($data['tableData'] as $row) {
						        $html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
				                $html .='   <td>'.$i.'.</td>
				                            <td>'.$row['res_data'].'</td>
							                <td>'.$row['rooms'].'</td>
							                <td>'.$row['pax'].'</td>
							                <td>'.$row['disc'].'</td>
							                <td>'.$row['type'].'</td>
							                <td>'.$row['board_type'].'</td>
							                <td>'.$row['room_type'].'</td>
							                <td>'.$row['rate'].'</td>
							                <td>'.$row['req_by'].'</td>
						                </tr>';
                                $i++;
						     }
			       
			        }
			//------------------------------------- end of Reservation Report --------------------------------------------	

//---------------------------------------financial reports

			//--------------------------------------- start of Out Going Item Report -------------------------------------
			    if ($data['report_name']=='Out Going Item Report') {	     
				          $html .='<tr>';
					      $html .='  
					            <td width="5%"><strong>#</strong></td>
					            <td width="20%"><strong>Out Going Data</strong></td>
					            <td width="14%"><strong>Description</strong></td>
					            <td width="14%"><strong>Remarks</strong></td>
					            <td width="7%"><strong>Qty</strong></td>
					            <td width="8%"><strong>Recieved</strong></td>
					            <td width="10%"><strong>Date</strong></td>
					            <td width="10%"><strong>Return Date</strong></td>
					            <td width="10%"><strong>Actual Date</strong></td>
					            <td width="8%"><strong>Delayed For</strong></td>
					          </tr>
					        </thead>
					       <tbody>';
					        $i = 1;     
						    foreach ($data['tableData'] as $row) {
						        $html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
				                $html .='   <td>'.$i.'.</td>
				                            <td>'.$row['out_data'].'</td>
							                <td>'.$row['desc'].'</td>
							                <td>'.$row['remarks'].'</td>
							                <td>'.$row['qty'].'</td>
							                <td>'.$row['recieved'].'</td>
							                <td>'.$row['date'].'</td>
							                <td>'.$row['return_date'].'</td>
							                <td>'.$row['actual_date'].'</td>
							                <td>'.$row['delayed'].'</td>
						                </tr>';
                                $i++;
						     }
			       
			        }
			//------------------------------------- end of Out Going Item Report -------------------------------

		    //----------------------------------   start of Out Go changed return date Report -----------------
			    if ($data['report_name']=='Out Going Changed Return Date Report') {	     
				          $html .='<tr>';
					      $html .='  
					            <td width="5%"><strong>#</strong></td>
					            <td width="25%"><strong>Out Going Data</strong></td>
					            <td width="14%"><strong>Creation</strong></td>
					            <td width="10%"><strong>Date</strong></td>
					            <td width="10%"><strong>Return Date</strong></td>
					            <td width="12%"><strong>Out With</strong></td>
					            <td width="10%"><strong>Changed Dates</strong></td>
					            <td width="20%"><strong>Reasons</strong></td>
					          </tr>
					        </thead>
					       <tbody>';
					        $i = 1;     
						    foreach ($data['tableData'] as $row) {
						        $html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
				                $html .='   <td>'.$i.'.</td>
				                            <td>'.$row['out_data'].'</td>
							                <td>'.$row['timestamp'].'</td>
							                <td>'.$row['date'].'</td>
							                <td>'.$row['re_date'].'</td>
							                <td>'.$row['out_with'].'</td>
							                <td>'.$row['changed_dates'].'</td>
							                <td>'.$row['reasons'].'</td>
						                </tr>';
                                $i++;
						     }
			       
			        }
			  //----------------------------------   end of Out Go changed return date Report -----------------


//---------------------------------------Front Office reports


        //----------------------------------   start of amenity summary Report -----------------
			    if ($data['report_name']=='Amenity Summary Report') {	     
				          $html .='<tr>';
					      $html .='  
					            <td width="25%" style="text-align:left !important;"><strong>Treatement Name</strong></td>
					            <td width="8%"><strong>No. of Treatement</strong></td>
					            <td width="8%"><strong>Adult pax</strong></td>
					            <td width="8%"><strong>Child Pax</strong></td>
					          </tr>
					        </thead>
					       <tbody>';
					        $i = 1;     
						    foreach ($data['tableData'] as $row) {
						        $html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
				                $html .='   <td>'.$row['treatment'].'</td>
							                <td style="text-align:center;">'.$row['no_treatment'].'</td>
							                <td style="text-align:center;">'.$row['adults'].'</td>
							                <td style="text-align:center;">'.$row['childs'].'</td>
						                </tr>';
                                $i++;
						     }
			       
			        }
	    //----------------------------------   end of amenity summary Report ----------------

	    //----------------------------------   start of amenity summary Report -----------------
			    if ($data['report_name']=='Amenity Report') {	     
				          $html .='<tr>';
					      $html .='  
					            <td width="5%"><strong>#</strong></td>
					            <td width="20%"><strong>Form Data</strong></td>
					            <td width="8%"><strong>Room</strong></td>
					            <td width="10%"><strong>Pax</strong></td>
					            <td width="8%"><strong>Treatment</strong></td>
					            <td width="5%"><strong>Qty</strong></td>
					            <td width="15%"><strong>Amenities</strong></td>
					            <td width="10%"><strong>Creator</strong></td>
					          </tr>
					        </thead>
					       <tbody>';
					        $i = 1;     
						    foreach ($data['tableData'] as $row) {
						        $html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
				                $html .='   <td>'.$i.'.</td>
				                            <td>'.$row['amen_data'].'</td>
							                <td>'.$row['room'].'</td>
							                <td>'.$row['pax'].'</td>
							                <td>'.$row['treatment'].'</td>
							                <td>'.$row['qty'].'</td>
							                <td>'.$row['amenities'].'</td>
							                <td>'.$row['creator'].'</td>
						                </tr>';
                                $i++;
						     }
			       
			        }
	    //----------------------------------   end of amenity summary Report ----------------


//----------------------------------   end of Front office Report -----------------


//---------------------------------------workshop reports


        //----------------------------------   start of workshop detailed Report -----------------

			        if ($data['report_name']=='Workshop Detailed Report') {	     
				          $html .='<tr>';
					      $html .='  
					            <td width="5%"><strong>#</strong></td>
					            <td width="25%"><strong>Form Data</strong></td>
					            <td width="25%"><strong>Request Items</strong></td>
					            <td width="25%"><strong>Workshop Order</strong></td>
					            <td width="12%"><strong>Order Price</strong></td>
					            <td width="12%"><strong>Actual Price</strong></td>
					            <td width="12%"><strong>Ready At</strong></td>
					            <td width="12%"><strong>Recieved At</strong></td>
					          </tr>
					        </thead>
					       <tbody>';
					        $i = 1;     
						    foreach ($data['tableData'] as $row) {
						        $html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
				                $html .='   <td>'.$i.'.</td>
				                            <td>'.$row['workshop_data'].'</td>
							                <td>'.$row['req_items'].'</td>
							                <td>'.$row['order_items'].'</td>
							                <td>'.$row['order_price'].'</td>
							                <td>'.$row['actual_price'].'</td>
							                <td>'.$row['readt_at'].'</td>
							                <td>'.$row['recieved_at'].'</td>
						                </tr>';
                                $i++;
						     }
			       
			        }

        //----------------------------------   end of workshop detailed Report ----------------


//----------------------------------   end of workshop Report -----------------


//---------------------------------------projects reports


        //----------------------------------   start of project detailed Report -----------------

			        if ($data['report_name']=='Projects Detailed Report') {	     
				          $html .='<tr>';
					      $html .='  
					            <td width="3%"><strong>#</strong></td>
					            <td width="17%"><strong>Project Data</strong></td>
					            <td width="7%"><strong>Budget</strong></td>
					            <td width="7%"><strong>Total Budget</strong></td>
					            <td width="7%"><strong>Cost</strong></td>
					            <td width="7%"><strong>Total Cost</strong></td>
					            <td width="7%"><strong>Actual Cost</strong></td>
					            <td width="7%"><strong>Total Actual Cost</strong></td>
					            <td width="7%"><strong>Total Difference</strong></td>
					            <td width="7%"><strong>Status</strong></td>
					            <td width="7%"><strong>Progress</strong></td>
					            <td width="17%"><strong>Dates</strong></td>
					          </tr>
					        </thead>
					       <tbody>';
					        $i = 1;     
						    foreach ($data['tableData'] as $row) {
						        $html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
				                $html .='   <td>'.$i.'.</td>
				                            <td>'.$row['project_data'].'</td>
							                <td>'.$row['budget'].'</td>
							                <td>'.$row['total_budget'].'</td>
							                <td>'.$row['cost'].'</td>
							                <td>'.$row['total_cost'].'</td>
							                <td>'.$row['actual_cost'].'</td>
							                <td>'.$row['total_actual_cost'].'</td>
							                <td>'.$row['total_difference'].'</td>
							                <td>'.$row['status'].'</td>
							                <td>'.$row['progress'].'</td>
							                <td>'.$row['dates'].'</td>
						                </tr>';
                                $i++;
						     }
			       
			        }

        //----------------------------------   end of project detailed Report ----------------

        //----------------------------------   start of project detailed Report -----------------

			        if ($data['report_name']=='Projects Summary Report') {	     
				          $html .='<tr>';
					      $html .='  
					            <td width="5%"><strong>#</strong></td>
					            <td width="25%"><strong>Hotel Name</strong></td>
					            <td width="15%"><strong>Budgets</strong></td>
					            <td width="15%"><strong>Total Budget</strong></td>
					            <td width="15%"><strong>Costs</strong></td>
					            <td width="15%"><strong>Total cost</strong></td>
					          </tr>
					        </thead>
					       <tbody>';
					        $i = 1;     
						    foreach ($data['tableData'] as $row) {
						        $html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
				                $html .='   <td>'.$i.'.</td>
				                            <td>'.$row['hotel'].'</td>
							                <td>'.$row['budget'].'</td>
							                <td>'.$row['total_budget'].'</td>
							                <td>'.$row['cost'].'</td>
							                <td>'.$row['total_cost'].'</td>
						                </tr>';
                                $i++;
						     }
			       
			        }

        //----------------------------------   end of project detailed Report ----------------











	
		   //--------------------------------------------- start of SRP Summary Report ----------------------------
					    if ($data['report_name']=='SRP Summary Report') {	       
						      $html .='</tbody></table><br><strong>Purchasing Requests Summary</strong><br>
						                 <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; "cellpadding="8">
						                   <thead><tr>
							            <td width="17%"><strong>Purchasing Amend</strong></td>
							            <td width="17%"><strong>Purchasing Approve</strong></td>
							            <td width="15%"><strong>In Signing</strong></td>
							            <td width="12%"><strong>Approved</strong></td>
							            <td width="12%"><strong>Rejected</strong></td>
							            <td width="17%"><strong>Purchasing Reject</strong></td>
							            <td width="17%"><strong>Partial Ordered</strong></td>
							            <td width="12%"><strong>Ordered</strong></td>
							            <td width="12%"><strong>Total</strong></td>
							          </tr>
							        </thead>
							       <tbody>';     
								    foreach ($data['tableData']['pr_stats'] as $row) {
								      $html .='<tr>  
									               <td style="text-align:center;">'.$row['purchasing_amend'].'</td>
									               <td style="text-align:center;">'.$row['purchasing_approve'].'</td>
									               <td style="text-align:center;">'.$row['in_signing'].'</td>
									               <td style="text-align:center;">'.$row['approved'].'</td>
									               <td style="text-align:center;">'.$row['rejected'].'</td>
									               <td style="text-align:center;">'.$row['purchasing_reject'].'</td>
									               <td style="text-align:center;">'.$row['partial_ordered'].'</td>
									               <td style="text-align:center;">'.$row['ordered'].'</td>
									               <td style="text-align:center;">'.$row['all_prs'].'</td>
								               </tr>';
								            }
								   $html .= ' </tbody></table><br><br>';  
								   $html .= '<br><strong>Purchasing Orders Summary</strong><table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; "cellpadding="8">
						                   <thead><tr>
										            <td width="17%"><strong>Ordered</strong></td>
										            <td width="17%"><strong>Partial Recieved</strong></td>
										            <td width="15%"><strong>Recieved</strong></td>
										            <td width="12%"><strong>Locked</strong></td>
										            <td width="12%"><strong>Reopened</strong></td>
										            <td width="17%"><strong>Total</strong></td>
										          </tr>
										        </thead>
										       <tbody>';
								   foreach ($data['tableData']['po_stats'] as $row) {
								      $html .='<tr>  
									               <td style="text-align:center;">'.$row['ordered'].'</td>
									               <td style="text-align:center;">'.$row['partial_recieved'].'</td>
									               <td style="text-align:center;">'.$row['recieved'].'</td>
									               <td style="text-align:center;">'.$row['locked'].'</td>
									               <td style="text-align:center;">'.$row['reopened'].'</td>
									               <td style="text-align:center;">'.$row['all_pos'].'</td>
								               </tr>';
								            }		       
								   $html .= ' </tbody></table><br><br>';  
								   $html .= '<br><strong>Delivery Notes Summary</strong><table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; "cellpadding="8">
						                   <thead><tr>
										            <td width="25%"><strong>Opened</strong></td>
										            <td width="25%"><strong>locked</strong></td>
										            <td width="25%"><strong>Reopened</strong></td>
										            <td width="25%"><strong>Total</strong></td>
										          </tr>
										        </thead>
										       <tbody>';
								   foreach ($data['tableData']['delivery_stats'] as $row) {
								      $html .='<tr>  
									               <td style="text-align:center;">'.$row['opened'].'</td>
									               <td style="text-align:center;">'.$row['locked'].'</td>
									               <td style="text-align:center;">'.$row['reopened'].'</td>
									               <td style="text-align:center;">'.$row['all_delivery'].'</td>
								               </tr>';
								            }		                         
					       
					           }
		//--------------------------------------------- end of SRP Summary Report -----------------------------------				        

					    $html .= ' </tbody></table></body></html>';   
					   	ob_end_clean();
						return($html);
						 
             }
         }

              


?>