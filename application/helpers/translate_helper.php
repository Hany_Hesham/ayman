<?php 

  if (!function_exists('translate')){
    function translate($phrase,$lang){
      $phrase = strtolower($phrase);
      $ci=get_instance();
      $query = 'SELECT languages.* FROM languages 
        WHERE english = "'.$phrase.'" OR german = "'.$phrase.'" OR french = "'.$phrase.'" OR arabic = "'.$phrase.'" ';
      $row   = $ci->db->query($query)->row_array();
      if (isset($row[$lang]) && $row[$lang]) {
        return $row[$lang];
      }else{
        return $phrase;
      }
    }
  }

?>