<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if (!function_exists('get_roles')){
   
   function get_roles(){
   
    $ci=get_instance();
    return $ci->db->get('user_groups')->result_array();
  }

}

if (!function_exists('get_ubranches')){
   
   function get_ubranches($user_id){
   
    $ci=get_instance();

    $ci->db->select('user_branches.branch_id');
    
    $ci->db->where('user_branches.user_id',$user_id);
    
    $user_hotels= $ci->db->get('user_branches')->result_array();
    
    $uhotels = array();
        
        foreach ($user_hotels as $hotel) {
        
          $uhotels[] = $hotel['branch_id'];
        
        }

     return $uhotels;   
  }

}

if (!function_exists('get_departments')){
   
   function get_departments(){
   
    $ci=get_instance();
  return $ci->db->get('departments')->result_array();
  }

}

if (!function_exists('hotel_name')){
  function hotel_name($hid){
    $ci=get_instance();
    $ci->db->select('hotels.hotel_name');
    if ($hid) {
      $ci->db->where('hotels.id',$hid);
    }
    $hotel = $ci->db->get('hotels')->row_array();
    return $hotel['hotel_name'];
  }
}

if (!function_exists('get_statuss')){
  function get_statuss($module_id=''){
    $ci=get_instance();
    $ci->db->select('status.*');
    if ($module_id) {
      $ci->db->where_in('status.module_id',$module_id);
    }
    return $ci->db->get('status')->result_array();
  }
}

if (!function_exists('get_hotelServer')){
  function get_hotelServer($hid_id){
    $ci=get_instance();
    $ci->db->select('hotels.*');
    if ($hid_id) {
      $ci->db->where_in('hotels.id',$hid_id);
    }
    $server = $ci->db->get('hotels')->row_array();
    return $server;
  }
}

if (!function_exists('get_hotelServer_from_interfaces')){
  function get_hotelServer_from_interfaces($hid_id,$interface){
    $ci=get_instance();
    $ci->db->select('hotel_interfaces.*');
    if ($hid_id) {
      $ci->db->where(array('hotel_interfaces.hid'=>$hid_id,'hotel_interfaces.interface'=>$interface));
    }
    $server = $ci->db->get('hotel_interfaces')->row_array();
    return $server;
  }
}

if (!function_exists('get_menuModules')){
  function get_menuModules($group){
    $ci=get_instance();
    $ci->db->select('modules.*');
    if ($group) {
      $ci->db->where_in('modules.menu_group',$group);
    }
    $ci->db->order_by('modules.rank','ASC');
    return $ci->db->get('modules')->result_array();
  }
}

if (!function_exists('get_devisions')){
   
   function get_devisions(){  
    $ci=get_instance();
    return $ci->db->get('devisions')->result_array();
  }

}

if (!function_exists('status_changer')){
   
   function status_changer($table,$column,$value,$id){
    
    $ci=get_instance();
  
  $ci->db->update($table, array($column => $value), "id = ".$id);
  }
}

 if (!function_exists('get_uhotels')){
   
   function get_uhotels($user_id){
   
    $ci=get_instance();

    $ci->db->select('user_hotels.hotel_id');
    
    $ci->db->where('user_hotels.user_id',$user_id);
    
    $user_hotels= $ci->db->get('user_hotels')->result_array();
    
    $uhotels = array();
        
        foreach ($user_hotels as $hotel) {
        
          $uhotels[] = $hotel['hotel_id'];
        
        }

     return $uhotels;   
  }

}

if (!function_exists('get_udepartments')){
  function get_udepartments($dep_id){
    $ci=get_instance();
    $ci->db->select('departments.id, departments.dep_name, departments.code');
    $ci->db->where_in('departments.id',$dep_id);
    $user_departments= $ci->db->get('departments')->result_array();
    $udepartments = array();
    foreach ($user_departments as $department) {
      $udepartments[] = $department['code'];
    }
    return $udepartments;   
  }
}


if (!function_exists('get_currency')){
   
   function get_currency(){
   
    $ci=get_instance();
    return $ci->db->get('currencies')->result_array();
  }

}


if (!function_exists('get_hotels_groups')){
   
   function get_hotels_groups(){
   
    $ci=get_instance();
    return $ci->db->get('hotels_group')->result_array();
  }

}

if (!function_exists('get_code')){
  function get_code($start, $id = FALSE){
    $ci=get_instance();
    $ci->db->order_by('timestamp','DESC');
    $ci->db->select_max(''.$start.'.id');
    $row = $ci->db->get(''.$start.'')->row_array();
    if ($id) {
      $endid = $id;
    }else{
      $endid = $row['id']+1;
    }
      if (strlen($start)>=3){
        return (strtoupper($start[0].$start[1].$start[2]).(date('Y').($endid)));
      }elseif (strlen($start)==2) {
        return (strtoupper($start[0].$start[1]).(date('Y').($endid)));
      }
  }
}

if (!function_exists('get_file_code')){
   
   function get_file_code($table,$start){
   
    $ci=get_instance();
    $ci->db->order_by('timestamp','DESC');
    $ci->db->select('files.id');
    $row = $ci->db->get('files')->row_array();
    if (isset($row['id'])) {
        $row['id'] +=1;
        $row['id'] = $row['id'].strtoupper(str_pad(dechex( mt_rand( 0, 1048575 ) ), 8, '0', STR_PAD_LEFT));
      }else{
        $row = array('id' => 1);
     }
    return ($start.(date('Y').($row['id'])));
    
    
  }

}

 if (!function_exists('getUserRoles')){
   
   function getUserRoles(){
      $ci=get_instance();
      $user_id = $_SESSION['user_id'];
      $uhotels =  get_uhotels($user_id);
      $query = 'SELECT user_hotels.*
                FROM user_hotels 
                WHERE user_hotels.user_id = '.$user_id.' AND user_hotels.hotel_id IN ('.implode(',',$uhotels).')';
      $rows = $ci->db->query($query)->result_array();
      $roles = array();
       foreach ($rows as $row) {
       $selected_roles = \json_decode($row['role_id']);
       if (isset($selected_roles)) {
           foreach ($selected_roles as $key => $role) {
             if (!in_array($selected_roles[$key], $roles)) {          
                array_push($roles,$selected_roles[$key]);
              }
            }
          }
        }
      
       return $roles;   
      
      }

    }

  if (!function_exists('getUserdepartments')){
    function getUserdepartments(){
      $ci=get_instance();
      $user_id = $_SESSION['user_id'];
      $query = 'SELECT users.*
                FROM users 
                WHERE users.id = '.$user_id;
      $row = $ci->db->query($query)->row_array();
      $departments = \json_decode($row['department'], true);
      return $departments;   
    }
  }


 if (!function_exists('custom_search_query')){
  
    function custom_search_query($custom_search, $extra_filters='', $table='', $module_id='', $special=''){
      $query='';
        if ((isset($custom_search['hotel_id']) && $custom_search['hotel_id'] !='')) {
            if ($special) {
              $query .=' AND '.$table.'.from_hid IN('.implode(',',$custom_search['hotel_id']).')';
            }else{
              $query .=' AND '.$table.'.hid IN('.implode(',',$custom_search['hotel_id']).')';
            }
        }
        if ((isset($custom_search['to_hotel_id']) && $custom_search['to_hotel_id'] !='')) {
            $query .=' AND '.$table.'.to_hid IN('.implode(',',$custom_search['to_hotel_id']).')';
        }
        if (isset($custom_search['dep_code']) && $custom_search['dep_code'] !='') {
          if ($special) {
            $query .=' AND '.$table.'.from_dep_code IN("'.implode('","',$custom_search['dep_code']).'")';
            $query .=' OR '.$table.'.to_dep_code IN("'.implode('","',$custom_search['dep_code']).'")';
          }else{
            $query .=' AND '.$table.'.dep_code IN("'.implode('","',$custom_search['dep_code']).'")';
          }
        }  
        if (isset($custom_search['toDate']) && $custom_search['fromDate'] && $custom_search['toDate']) {
          $query .=' AND DATE_FORMAT('.$table.'.timestamp,GET_FORMAT(DATE,"JIS")) BETWEEN ("'.$custom_search['fromDate'].'") AND ("'.$custom_search['toDate'].'")';
        }
        if (isset($custom_search['toDate']) && $custom_search['fromDate'] && !$custom_search['toDate']) {
          $query .=' AND DATE_FORMAT('.$table.'.timestamp,GET_FORMAT(DATE,"JIS")) >= "'.$custom_search['fromDate'].'"';
        }
        if (isset($custom_search['toDate']) && $custom_search['toDate'] && !$custom_search['fromDate']) {
          $query .=' AND DATE_FORMAT('.$table.'.timestamp,GET_FORMAT(DATE,"JIS")) <= "'.$custom_search['toDate'].'"';
        } 
        if (isset($custom_search['status_id']) && $custom_search['status_id'] !='') {
          $query .=' AND '.$table.'.status IN('.implode(',',$custom_search['status_id']).')';
        }
        if ($extra_filters !='') {
          if (isset($custom_search['role_id']) && $custom_search['role_id'] !='') {
            if (in_array('reject',$custom_search['role_id']) || in_array('approve',$custom_search['role_id'])) {
              $dataIds = signature_state($custom_search['role_id'], $module_id); 
              $prdid=array();
              foreach ($dataIds as $key => $dataId) {
                $prdid[]=$dataId['form_id'];
              }
              if (count($prdid) >=1) {
                $query .=' AND '.$table.'.id IN('.implode(',',$prdid).')';
              }
              foreach ($custom_search['role_id'] as $key => $role_id) {
                if ($role_id =='reject' || $role_id =='approve') {
                  unset($custom_search['role_id'][$key]);
                }
              }
              if (count($custom_search['role_id']) >=1) {
                $query .=' AND '.$table.'.role_id IN('.implode(',',$custom_search['role_id']).')';
              }
            }else{
              $query .=' AND '.$table.'.role_id IN('.implode(',',$custom_search['role_id']).')';
            }
          }
          if (isset($custom_search['status_id']) && $custom_search['status_id'] !='') {
            $query .=' AND '.$table.'.status IN('.implode(',',$custom_search['status_id']).')';
          }   
        }         
        return $query;      
      }

    }

  if (!function_exists('signature_state')){
        function signature_state($states,$module_id){
          $ci=get_instance();
          $user_id = $_SESSION['user_id'];
          $ci->db->select('signatures.form_id');
          if (in_array('reject',$states) && !in_array('approve',$states)) {
            $ci->db->where(array('signatures.module_id'=>$module_id,
                                 'signatures.user_id'=>$user_id,
                                 'signatures.reject'=>1
                                )
                           );
          }elseif (in_array('reject',$states) && in_array('approve',$states)) {
            $ci->db->where(array('signatures.module_id'=>$module_id,'signatures.user_id'=>$user_id));   
          }elseif (!in_array('reject',$states) && in_array('approve',$states)) {
             $ci->db->where(array('signatures.module_id'=>$module_id,
                                 'signatures.user_id'=>$user_id,
                                 'signatures.reject'=>0
                                )
                           );   
          }elseif (!in_array('reject',$states) && !in_array('approve',$states)) {
             $ci->db->where(array('signatures.module_id'=>$module_id,
                                 'signatures.user_id'=>$user_id
                                )
                           );   
           }
          $ci->db->where('signatures.deleted !=', 1);   
          $ci->db->order_by('signatures.form_id', 'ASC');
          return $ci->db->get('signatures')->result_array();   
        }
    }

  function switch_oracle_db($hid) {
    $CI =& get_instance();
    $hotel = get_hotelServer($hid);
    $oracle_db['hostname'] = $hotel['server_ip'];
    $oracle_db['username'] = $hotel['username'];
    $oracle_db['password'] = $hotel['password'];
    $oracle_db['database'] = $hotel['description'];
    $oracle_db['dbdriver'] = 'oci8';
    $oracle_db['dbprefix'] = '';
    $oracle_db['pconnect'] = FALSE;
    $oracle_db['autoinit'] = FALSE; 
    $oracle_db['db_debug'] = FALSE;
    $oracle_db['cache_on'] = FALSE;
    $oracle_db['cachedir'] = '';
    $oracle_db['char_set'] = 'utf8';
    $oracle_db['dbcollat'] = 'utf8_general_ci';
    $oracle_db['swap_pre'] = '';
    $oracle_db['encrypt' ] = FALSE;
    $oracle_db['compress'] = FALSE;
    $oracle_db['stricton'] = FALSE;
    $oracle_db['failover'] = array();
    $oracle_db['save_queries'] = TRUE;
    return $oracle_db;
  }

  function switch_mssql_db($hid) {
    $CI =& get_instance();
    $hotel = get_hotelServer_from_interfaces($hid,'solution');
    $mssql_db['hostname'] =  $hotel['server_ip'];
    $mssql_db['username'] =  $hotel['username'];
    $mssql_db['password'] =  $hotel['pass'];
    $mssql_db['database'] =  $hotel['db_name'];
    $mssql_db['dbdriver'] =  'mssql';
    $mssql_db['dbprefix'] = '';
    $mssql_db['pconnect'] = FALSE;
    $mssql_db['autoinit'] = FALSE; 
    $mssql_db['db_debug'] = FALSE;
    $mssql_db['cache_on'] = FALSE;
    $mssql_db['cachedir'] = '';
    $mssql_db['char_set'] = 'utf8';
    $mssql_db['dbcollat'] = 'utf8_general_ci';
    $mssql_db['swap_pre'] = '';
    $mssql_db['encrypt' ] = FALSE;
    $mssql_db['compress'] = FALSE;
    $mssql_db['stricton'] = FALSE;
    $mssql_db['failover'] = array();
    $mssql_db['save_quer]ies'] = TRUE;
    return $mssql_db;
  }

/**
 * Count total rows on table based on params
 * @param  string $table Table from where to count
 * @param  array  $where
 * @return mixed  Total rows
 */
  function total_rows($table,$module_id) {
    $CI =& get_instance();
    if ($CI->data['uroles']) {
        $query = 'SELECT '.$table.'.*
                  FROM '.$table.'
                  WHERE '.$table.'.deleted = 0 AND '.$table.'.status = 1';
                  if ($module_id == 64 || $module_id == 70 || $module_id == 72 || $module_id == 73 || $module_id == 74) {
                    $query .= ' AND '.$table.'.role_id IN('.implode(',',$CI->data['uroles']).')';
                  }elseif ($module_id == 61 || $module_id == 76) {
                    $query .= ' AND (('.$table.'.from_hid IN('.implode(',',$CI->data['uhotels']).') AND '.$table.'.role_id IN('.implode(',',$CI->data['uroles']).')) OR ('.$table.'.to_hid IN('.implode(',',$CI->data['uhotels']).')  AND '.$table.'.role_id IN('.implode(',',$CI->data['uroles']).')))';
                  }else{
                    $query .= ' AND '.$table.'.hid IN('.implode(',',$CI->data['uhotels']).') AND '.$table.'.role_id IN('.implode(',',$CI->data['uroles']).')';

                  }
        return $CI->db->query($query)->num_rows();

      // $CI->db->where_in('role_id', $CI->data['uroles']);
      // $CI->db->where_in('hid', $CI->data['uhotels']);
      // $CI->db->where('deleted', '0');
      // return $CI->db->count_all_results($table);
    } else{
      return FALSE;
    }
  }

  function totalempty_rows($table,$module_id) {
    $CI =& get_instance();
    if ($CI->data['uroles']) {
        $query = 'SELECT '.$table.'.*
                  FROM '.$table.'
                  WHERE '.$table.'.deleted = 0 AND '.$table.'.status = 1 
                  AND '.$table.'.role_id IN('.implode(',',$CI->data['uroles']).')';
        return $CI->db->query($query)->num_rows();

      // $CI->db->where_in('role_id', $CI->data['uroles']);
      // $CI->db->where_in('hid', $CI->data['uhotels']);
      // $CI->db->where('deleted', '0');
      // return $CI->db->count_all_results($table);
    } else{
      return FALSE;
    }
  }

  function fromto_total_rows($table,$module_id) {
    $CI =& get_instance();
    if ($CI->data['uroles']) {
        $query = 'SELECT '.$table.'.*
                  FROM '.$table.'
                  WHERE '.$table.'.deleted = 0 AND '.$table.'.status = 1 
                  AND (('.$table.'.from_hid IN('.implode(',',$CI->data['uhotels']).') AND '.$table.'.role_id IN('.implode(',',$CI->data['uroles']).')) OR ('.$table.'.to_hid IN('.implode(',',$CI->data['uhotels']).')  AND '.$table.'.role_id IN('.implode(',',$CI->data['uroles']).')))';
        return $CI->db->query($query)->num_rows();

      // $CI->db->where_in('role_id', $CI->data['uroles']);
      // $CI->db->where_in('hid', $CI->data['uhotels']);
      // $CI->db->where('deleted', '0');
      // return $CI->db->count_all_results($table);
    } else{
      return FALSE;
    }
  }

  function total_module_data($table) {
    $CI =& get_instance();
    if ($CI->data['uroles']) {
      $CI->db->select(''.$table.'.*,(SELECT modules.view_link FROM modules WHERE modules.tdatabase ="'.$table.'" LIMIT 1) AS view_link');
      $CI->db->where_in('role_id', $CI->data['uroles']);
      $CI->db->where_in('hid', $CI->data['uhotels']);
      $CI->db->where('deleted', '0');
      return $CI->db->get($table)->result_array();
    } else{
      return FALSE;
    }
  }

  function totalempty_module_data($table) {
    $CI =& get_instance();
    if ($CI->data['uroles']) {
      $CI->db->select(''.$table.'.*,(SELECT modules.view_link FROM modules WHERE modules.tdatabase ="'.$table.'" LIMIT 1) AS view_link');
      $CI->db->where_in('role_id', $CI->data['uroles']);
      $CI->db->where('deleted', '0');
      return $CI->db->get($table)->result_array();
    } else{
      return FALSE;
    }
  }

  function fromto_total_module_data($table) {
    $CI =& get_instance();
    if ($CI->data['uroles']) {
      $CI->db->select(''.$table.'.*,(SELECT modules.view_link FROM modules WHERE modules.tdatabase ="'.$table.'" LIMIT 1) AS view_link');
      $CI->db->where(''.$table.'.from_hid IN('.implode(',',$CI->data['uhotels']).') AND '.$table.'.role_id IN('.implode(',',(array)$CI->data['uroles']).')');
      $CI->db->or_where(''.$table.'.to_hid IN('.implode(',',$CI->data['uhotels']).') AND '.$table.'.role_id IN('.implode(',',(array)$CI->data['uroles']).')');
      $CI->db->where('deleted', '0');
      return $CI->db->get($table)->result_array();
    } else{
      return FALSE;
    }
  }

if (!function_exists('form_modules')){
  function form_modules(){
    $CI =& get_instance();
    
    $form_module = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 
        25, 26, 27, 28, 29, 30, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57 , 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 79, 80, 81];
    
    $CI->db->select('*');
    $CI->db->where_in('id', $form_module);
    return $CI->db->get('modules')->result_array();
    
    return $modules;

   }
 }


function form_to_sign($module, $t_attr='',$filter_count=''){
    $CI =& get_instance();
    if ($CI->data['uroles']) {
        $query = 'SELECT '.$module['tdatabase'].'.*, hotels.hotel_name, user_groups.name as role_name,
                 (SELECT signatures.id FROM signatures WHERE signatures.module_id='.$module['id'].' AND signatures.form_id='.$module['tdatabase'].'.id AND signatures.user_id IS NULL AND signatures.role_id ='.$module['tdatabase'].'.role_id LIMIT 1 )AS sign_id
                  FROM '.$module['tdatabase'].'';
                  if ($module['id'] == 64 || $module['id'] == 70 || $module['id'] == 72 || $module['id'] == 73 || $module['id'] == 74) {
                    $query .= ' LEFT JOIN hotels ON '.$module['tdatabase'].'.timestamp = hotels.id';
                  }elseif ($module['id'] == 61 || $module['id'] == 76) {
                    $query .= ' LEFT JOIN hotels ON '.$module['tdatabase'].'.from_hid = hotels.id';
                  }else{
                    $query .= ' LEFT JOIN hotels ON '.$module['tdatabase'].'.hid = hotels.id';
                  }
                  $query .= ' LEFT JOIN user_groups ON '.$module['tdatabase'].'.role_id = user_groups.id
                  WHERE '.$module['tdatabase'].'.deleted = 0 AND '.$module['tdatabase'].'.status = 1';

                  if ($module['id'] == 64 || $module['id'] == 70 || $module['id'] == 72 || $module['id'] == 73 || $module['id'] == 74) {
                    $query .= ' AND '.$module['tdatabase'].'.role_id IN('.implode(',',$CI->data['uroles']).')';
                  }elseif ($module['id'] == 61 || $module['id'] == 76) {
                    $query .= ' AND (('.$module['tdatabase'].'.from_hid IN('.implode(',',$CI->data['uhotels']).') AND '.$module['tdatabase'].'.role_id IN('.implode(',',$CI->data['uroles']).')) OR ('.$module['tdatabase'].'.to_hid IN('.implode(',',$CI->data['uhotels']).')  AND '.$module['tdatabase'].'.role_id IN('.implode(',',$CI->data['uroles']).')))';
                  }else{
                    $query .= ' AND '.$module['tdatabase'].'.hid IN('.implode(',',$CI->data['uhotels']).') AND '.$module['tdatabase'].'.role_id IN('.implode(',',$CI->data['uroles']).')';

                  }


          if ($t_attr['search']) {
              $query .= '  AND ('.$module['tdatabase'].'.id like "%'.$t_attr['search'].'%"  OR hotels.hotel_name like "%'.$t_attr['search'].'%")';
            }
           if ($filter_count == 'count') {
              return $CI->db->query($query)->num_rows();
            }else{
               $query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'
                          LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
              return $CI->db->query($query)->result_array();
            }
      }else{
         return false;
     }
  }

function get_current_module($link){
    $CI =& get_instance();
        $query = 'SELECT modules.* FROM modules
                  WHERE modules.view_link LIKE "%'.$link.'%" AND 
                  modules.id IN(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 
        25, 26, 27, 28, 29, 30, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57 , 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 79, 80, 81)';
        return $CI->db->query($query)->row_array();
  }

function get_next_form_to_sign($module, $form_id=''){
    $CI =& get_instance();
    if ($CI->data['uroles'] && $module) {
        $query = 'SELECT '.$module['tdatabase'].'.* FROM '.$module['tdatabase'].'
                  WHERE '.$module['tdatabase'].'.deleted = 0 AND '.$module['tdatabase'].'.status = 1 
                  AND '.$module['tdatabase'].'.hid IN('.implode(',',$CI->data['uhotels']).') AND '.$module['tdatabase'].'.role_id IN('.implode(',',$CI->data['uroles']).')';
        if ($form_id) {
            $query .= '  AND '.$module['tdatabase'].'.id != '.$form_id.'';
          }
        $query .= '  ORDER BY RAND()';
        return $CI->db->query($query)->row_array();
      }else{
         return false;
     }
  }

function get_nextempty_form_to_sign($module, $form_id=''){
    $CI =& get_instance();
    if ($CI->data['uroles'] && $module) {
        $query = 'SELECT '.$module['tdatabase'].'.* FROM '.$module['tdatabase'].'
                  WHERE '.$module['tdatabase'].'.deleted = 0 AND '.$module['tdatabase'].'.status = 1 
                  AND '.$module['tdatabase'].'.role_id IN('.implode(',',$CI->data['uroles']).')';
        if ($form_id) {
            $query .= '  AND '.$module['tdatabase'].'.id != '.$form_id.'';
          }
        $query .= '  ORDER BY RAND()';
        return $CI->db->query($query)->row_array();
      }else{
         return false;
     }
  }

function fromto_get_next_form_to_sign($module, $form_id=''){
    $CI =& get_instance();
    if ($CI->data['uroles'] && $module) {
        $query = 'SELECT '.$module['tdatabase'].'.* FROM '.$module['tdatabase'].'
                  WHERE '.$module['tdatabase'].'.deleted = 0 AND '.$module['tdatabase'].'.status = 1 
                  AND (('.$module['tdatabase'].'.from_hid IN('.implode(',',$CI->data['uhotels']).') AND '.$module['tdatabase'].'.role_id IN('.implode(',',$CI->data['uroles']).')) OR ('.$module['tdatabase'].'.to_hid IN('.implode(',',$CI->data['uhotels']).')  AND '.$module['tdatabase'].'.role_id IN('.implode(',',$CI->data['uroles']).')))';
        if ($form_id) {
            $query .= '  AND '.$module['tdatabase'].'.id != '.$form_id.'';
          }
        $query .= '  ORDER BY RAND()';
        return $CI->db->query($query)->row_array();
      }else{
         return false;
     }
  }

if (!function_exists('get_userBy_email')){
  function get_userBy_email($email){
    $ci=get_instance();
    $ci->db->select('users.mobile_no');
    $ci->db->where('users.email',$email);
    $user_data =  $ci->db->get('users')->row();
    return $user_data->mobile_no;
  }
}

  if (!function_exists('get_serial')){
    function get_serial($table){
      $ci=get_instance();
      $ci->db->select_max('serial');
      $ci->db->like('timestamp', date('Y'));
      $row = $ci->db->get($table)->row_array();
      return ($row['serial'] + 1);
    }
  }

  if (!function_exists('learn_translate')){
  function learn_translate($phrase){
    $ci=get_instance();
    $query = 'SELECT languages.* FROM languages 
              WHERE english = "'.$phrase.'" ';
    $row   = $ci->db->query($query)->row_array();
    $data  = ['english'=>$phrase];
    if (!$row) {
        $ci->db->insert('languages', $data);
    }
   }
}
if (!function_exists('translate')){
  function translate($phrase,$lang){
    $ci=get_instance();
    $phrase = str_replace('"',"'",$phrase);
    $query = 'SELECT languages.* FROM languages 
              WHERE english = "'.$phrase.'" OR german = "'.$phrase.'" OR french = "'.$phrase.'" OR arabic = "'.$phrase.'" ';
    $row   = $ci->db->query($query)->row_array();
    if (isset($row[$lang]) && $row[$lang]) {
        return $row[$lang];
    }else{
         return $phrase;
    }
   
   }
}

// if (!function_exists('get_code_with_random')){
//   function get_code_with_random($start){
//     $ci = get_instance();
//     $ci->db->order_by('timestamp','DESC');
//     $ci->db->select_max('projects.id');
//     $row = $ci->db->get('projects')->row_array();
//     $random_str  = strtoupper(str_pad(dechex( mt_rand( 2, 1648575 ) ), 5, '0', STR_PAD_LEFT));
//       if (strlen($start)>=3){
//         return (strtoupper($start[0].$start[1].$start[2]).($random_str));
//       }elseif (strlen($start)==2) {
//         return (strtoupper($start[0].$start[1]).($random_str));
//       }
//   }
// }

if (!function_exists('get_code_with_random')){
   
  function get_code_with_random($start){
    $ci=get_instance();
    $ci->db->order_by('timestamp','DESC');
    $ci->db->select_max('projects.id');
    $row = $ci->db->get('projects')->row_array();
    $id = isset($row['id']) ? $row['id'] : 0;
    return (strtoupper($start[0].$start[1].$start[2]).(date('Y').($id+1)));

 }

}

if (!function_exists('get_competition_code')){
   
  function get_competition_code($start = 'competition'){
    $ci=get_instance();
    $ci->db->order_by('timestamp','DESC');
    $ci->db->select_max('competitions.id');
    $row = $ci->db->get('competitions')->row_array();
    $id = isset($row['id']) ? $row['id'] : 0;
    return (strtoupper($start[0].$start[1].$start[2]).(date('Y').($id+1)));

 }

}

           

 ?>