
    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                         <span class="page-title-toSave hidden">Supplier Profile</span>
                         <?php if($this->data['permission']['edit'] == 1){?>
                                <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-primary btn-sm">
                                <strong style="font-size:20px;"><i class="fa fa-cloud"></i>&nbsp;<span class="page-info-data"><?php echo $supplier['supp_name']?></span></strong> </button>
                          <?php }else{?>
                                <h4 class="page-title"><span class="page-info-data"><?php echo $supplier['supp_name']?></span></h4>
                          <?php }?>
                          <a class="btn btn-light  btn-circle savedFormsButton" id="<?php echo $this->data['module']['id'].','.$supplier['id']?>"
                           data-toggle="tooltip" data-placement="top" data-original-title="Save Link And see it later" 
                           href="javascript: void(0);" >
                           <span class="text-danger"><i class="fas fa-database"></i></span>
                          </a>        
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/suppliers');?>">Suppliers</a></li>
                              <li class="breadcrumb-item active" aria-current="page"><?php echo $supplier['supp_name']?></li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
          <div class="row">
              <div class="col-12">
                 <div class="card">
                    <div class="card-body">
                     <div class="row">
                         <div class="col-lg-8" style="margin-top: 50px;"> 
                             <div> <strong style="font-size:16px;">Supplier Name: </strong><?php echo $supplier['supp_name']?></div>
                              <div> <strong style="font-size:16px;">Supplier Interface: </strong><?php echo $supplier['interface']?></div>
                             <div> <strong style="font-size:16px;">Supplier Group: </strong><?php echo $supplier['hotel_group']?></div>
                             <div> <strong style="font-size:16px;">Phone: </strong><?php echo $supplier['phone']?></div>
                             <div> <strong style="font-size:16px;">Address: </strong><?php echo $supplier['address']?></div>
                             <div> <strong style="font-size:16px;">Contacted Person: </strong><?php echo $supplier['contact']?></div>
                             <div> <strong style="font-size:16px;">Contact Email: </strong><?php echo $supplier['cont_email']?></div>
                         </div>
                         <div class="col-lg-4">
                               <div class="el-card-item"> <a class="btn default btn-outline image-popup-vertical-fit el-link" href="<?php echo $image_link;?>"><img  style="width:90%;height:200px;" src="<?php echo $image_link;?>" alt="user"></a>
                                    <div class="el-card-content centered">
                                        <h4 class="m-b-0 "><?php echo $supplier['supp_name']?> Files</h4>
                                    </div>
                                    <div>
                                      <div class="accordion" id="accordionExample">
                                        <div class="card m-b-0">
                                          <?php foreach($uploads as $upload){?>
                                            <div class="card-header" id="headingOne">
                                              <h5 class="mb-0">
                                                <a href="javascript: void(0);" data-toggle="collapse" data-target="#collapse<?php echo $upload['id']?>" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                    <i class="m-r-5 fas fa-image" aria-hidden="true"></i>
                                                    <span><?php echo $upload['file_name']?></span>
                                                </a>
                                              </h5>
                                            </div>
                                            <div id="collapse<?php echo $upload['id']?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample" style="">
                                              <div class="card-body">
                                                <a class="btn default btn-outline image-popup-vertical-fit     el-link" href="<?php echo base_url('assets/uploads/suppliers/'.$upload['file_name']);?>"><img  style="width:150px;height:80px;" src="<?php echo $image_link;?>" alt="user"></a>
                                              </div>
                                            </div>
                                         <?php }?>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                            </div> 
                        </div>
                     </div>
                  </div>
                 <div class="card">
                    <div class="card-body">
                   <h4 class="page-title">Transactions With <span style="color:red;"><?php echo $supplier['supp_name']?></span></h4><br><br>
                          <table id="supplier_transactions-list-table" class="table table-hover" style="width:100% !important;">
                              <thead>
                                  <tr>
                                      <th class="tableCol" id="po_serial"><strong>Delivery Serial</strong></th>
                                      <th class="tableCol" id="hid"><strong>Hotel Name</strong></th>
                                      <th class="tableCol" id="dep_code"><strong>Department</strong></th>
                                      <th class="tableCol" id="item_name"><strong>Item Name</strong></th>
                                      <th class="tableCol" id="serial"><strong>Item Serial</strong></th>
                                      <th class="tableCol" id="description"><strong>Description</strong></th>
                                      <th class="tableCol" id="quantity"><strong>Qty REC</strong></th>
                                      <th class="tableCol" id="rated_price"><strong>Total Price</strong></th>
                                  </tr>
                              </thead>
                              <tbody>
                              </tbody>
                          </table>
                    </div>
                 </div>
              </div>
            
            </div>                 
      </div>

  <?php 
           $select = '<select id="group_id" name="group_id" class="selectpicker form-control custom-select"  data-container="body" data-live-search="true" title="Select Hotel Group" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="width: 100%; height:36px;" required>';
           $select .= '<option value="">Select Item Unit</option>';    
                foreach ($groups as $group) {
                    $select .= '<option value="'.$group['id'].'" '.($supplier['group_id'] == $group['id'] ? 'selected':'').'>'.$group['hotel_group'].'</option>';
                  }
           $select .= '</select><br>';
           initiate_modal(
                         'lg','Add New Supplier','admin/suppliers/supplier_process',
                          '<input id="supp_id" type="hidden" name="id" value="'.$supplier['id'].'">
                           <input  id="suppName" onkeyup="fieldChecker(\'suppName\',\'admin/suppliers/check_field/suppliers/supp_name/'.$supplier['supp_name'].'\')" type="text" name="supp_name" class="form-control " placeholder="Item Supplier" value="'.$supplier['supp_name'].'" required><br>
                            <input  id="interface" onkeyup="fieldChecker(\'interface\',\'admin/suppliers/check_field/suppliers/interface/'.$supplier['interface'].'\')" type="text" name="interface" class="form-control " placeholder="Supplier interface" value="'.$supplier['interface'].'" required><br>
                             '. $select.'<br>
                           <input id="phone" type="number" name="phone" class="form-control" placeholder="Phone" value="'.$supplier['phone'].'"><br>
                           <input id="address" type="text" name="address" class="form-control" placeholder="Address" value="'.$supplier['address'].'"><br>
                           <input id="contact" type="text" name="contact" class="form-control" placeholder="Contacted Person"
                           value="'.$supplier['contact'].'"><br>
                           <input id="cont_email" type="email" name="cont_email" class="form-control" placeholder="Contact Email" value="'.$supplier['cont_email'].'"><br>
                            <input type="hidden" name="supp_id" value="'.$supplier['id'].'" />
                                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />         
                            <br>'
                         )
  ?>

<?php upfiles_js('admin/suppliers',$uploads,4,$supplier['id'],'suppliers');?>
<script type="text/javascript">
   initDTable(
                 "supplier_transactions-list-table",
                    [{"name":"delivery_serial"},
                     {"name":"hid"},
                     {"name":"dep_code"},
                     {"name":"item_name"},
                     {"name":"serial"},
                     {"name":"description"},
                     {"name":"recieved"},
                     {"name":"net_amount"},],
                  "<?php echo base_url("admin/suppliers/supplier_transactions/".$supplier['id']) ?>"
           );
</script>
