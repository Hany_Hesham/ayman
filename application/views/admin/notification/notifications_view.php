<div class="page-breadcrumb">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="col-sm-4">
            <div class="float-left">
              <h4 class="page-title">Notification List</h4>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="ml-auto float-right">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Notifications</li>
                  <li class="breadcrumb-item"><a href="<?php echo($this->agent->referrer());?>">Back</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <table id="emails-list-table" class="table table-hover" style="width:100% !important;">
            <thead >
              <tr>
                <th scope="col" width="5%"><strong>#</strong></th>
                <th scope="col" width="10%"><strong>Head</strong></th>
                <th scope="col" width="15%"><strong>Notification Time</strong></th>
                <th scope="col" width="10%"><strong>Made On</strong></th>
                <th scope="col" width="10%"><strong>Form Id</strong></th>
                <th scope="col" width="7%"><strong>To User</strong></th>
                <th scope="col" width="7%"><strong>To Department</strong></th>
                <th scope="col" width="7%"><strong>To Hotel</strong></th>
                <th scope="col" width="7%"><strong>UserName</strong></th>
                <th scope="col" width="10%"><strong>Message</strong></th>
                <th scope="col" width="10%"><strong>Status</strong></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

  initDTable(
    "emails-list-table",
    [{"name":"id"},
    {"name":"head"},
    {"name":"timestamp"},
    {"name":"module_id"},
    {"name":"link"},
    {"name":"to_uid"},
    {"name":"to_dep_id"},
    {"name":"to_hid"},
    {"name":"fullname"},
    {"name":"message"},
    {"name":"status"},],
    "<?php echo base_url("admin/notification/notification_ajax/".$module_id) ?>"
  );

  function upRead(notification_id, statue){
    var url="<?php echo base_url('admin/notification/read/');?>";      
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{notification_id:notification_id, statue:statue},
      success: function(data){
        return true;
      }
    });
  }

</script>