
<?php
   $slider_permission = user_access(1);
   $competitions_permission = user_access(2);
   $projects_permission = user_access(5);
   $rules_permission = user_access(15);
   $about_permission = user_access(14);
   $language_permission = user_access(25);
   $competitors_permission = user_access(18);
   $voters_permission = user_access(26);
   $winners_permission = user_access(24);
   $partners_permission = user_access(3);

  //  $gallary_permission          = user_access(4);
  //  $event_permission            = user_access(6);
  //  $setting_permission          = user_access(11);
  //  $products_permission         = user_access(16);
  //  $portfolio_permission        = user_access(17);
  //  $contact_permission          = user_access(19);
  //  $footer_permission           = user_access(20);
  //  $clients_req_permission      = user_access(21);
  //  $team_permission             = user_access(23);
?>

<aside class="left-sidebar no-print  float-right" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" >
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> 
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager');?>" aria-expanded="false">
                               <i class="fas fa-bars"></i><span class="hide-menu"><strong>Dashboard</strong></span>
                           </a>
                        </li>
                        <?php if($projects_permission['view']==1){?>
                          <li class="sidebar-item"> 
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/projects_manager');?>" aria-expanded="false"><i class="fas fa-tasks"></i><span class="hide-menu"><strong>Projects</strong></span>
                            </a>
                          </li>
                        <?php }?>    
                        <?php if($competitions_permission['view']==1){?>
                          <li class="sidebar-item">
                            <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/competitions_manager');?>" aria-expanded="false"><i class="fas fa-location-arrow"></i><span class="hide-menu"><strong>Competitions Manager</strong></span></a>
                          </li>
                        <?php }?> 
                        <?php if($rules_permission['view']==1){?> 
                          <li class="sidebar-item">
                            <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/rules_manager');?>" aria-expanded="false"><i class="mdi mdi-book"></i><span class="hide-menu"><strong>Rules</strong></span></a>
                          </li>
                         <?php }?>
                         <?php if($competitors_permission['view']==1){?> 
                          <li class="sidebar-item">
                            <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/competitors_manager');?>" aria-expanded="false"><i class="mdi mdi-account-card-details"></i><span class="hide-menu"><strong>Competitors Manager</strong></span></a>
                          </li>
                         <?php }?>
                         <?php if($voters_permission['view']==1){?> 
                          <li class="sidebar-item">
                            <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/voters_manager');?>" aria-expanded="false"><i class="far fa-thumbs-up"></i><span class="hide-menu"><strong>Voters Manager</strong></span></a>
                          </li>
                         <?php }?>
                         <?php if($winners_permission['view']==1){?> 
                          <li class="sidebar-item">
                            <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/winners_manager');?>" aria-expanded="false"><i class="fas fa-trophy"></i><span class="hide-menu"><strong>Winners Manager</strong></span></a>
                          </li>
                         <?php }?>
                         <?php if($about_permission['view']==1){?> 
                          <li class="sidebar-item">
                            <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/about_us_manager');?>" aria-expanded="false"><i class="mdi mdi-angularjs"></i><span class="hide-menu"><strong>About US & Info Manager</strong></span></a>
                          </li>
                         <?php }?>
                         <?php if($slider_permission['view']==1){?>
                          <li class="sidebar-item">
                             <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/slider_manager');?>" aria-expanded="false"><i class="fas fa-sliders-h"></i><span class="hide-menu"><strong>Sliders</strong></span></a>
                          </li>
                        <?php }?>
                        <?php if($partners_permission['view']==1){?>
                          <li class="sidebar-item">
                             <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/partners_manager');?>" aria-expanded="false"><i class="mdi mdi-animation"></i><span class="hide-menu"><strong>Partners Manager</strong></span></a>
                          </li>
                        <?php }?>
                        <?php if($language_permission['view']==1){?>
                          <li class="sidebar-item"> 
                              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/lang_manager');?>" aria-expanded="false"><i class="fas fa-globe"></i><span class="hide-menu"><strong>Languages Translator</strong></span></a>
                          </li>
                         <?php }?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>