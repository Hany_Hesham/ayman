<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')?>">
    <script src="<?php echo base_url('assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script> 
</head>
<body>
    <div>
           <style type="text/css"> 
            .modal-lg {
                    max-width: 95% !important;
                    zoom:90%;
                }
           </style>
           <?php $this->load->view($view);?>
    </div>

    <script src="<?php echo base_url('assets/extra-libs/selectpicker/dist/js/bootstrap-select.min.js')?>"src="/path/to/datepicker.js"></script>
    <script src="<?php echo base_url('assets/extra-libs/selectpicker/dist/js/i18n/defaults-en_US.min.js')?>"src="/path/to/datepicker.js"></script>
</body>
</html>
<script type="text/javascript"> 
    function signatureController(){
        $(".signature-buttons-container").hasClass("signature-button");
        let button_visiblity= $('.signature-button').is(":visible"); 
        if (button_visiblity==false) {
            $('.signature-button-from-summary-view').remove();
         }
        $('.signature-button').hide();
     }

    setTimeout(function(){
         signatureController();
      }, 0);
  
</script>