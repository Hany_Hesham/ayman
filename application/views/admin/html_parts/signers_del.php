<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row">
    <div class="col-lg-3">
      <div class="card">
        <div class="card-header badge-dark after-hover">
          <h6 class="card-title float-left">Receiving</h6>
          <ul class="navbar-nav float-right mr-auto">
            <li class="nav-item dropdown">
              <span class="wait-hover">
                <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
              </span>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#"></a>
                <div class="dropdown-divider"></div>
              </div>
            </li>
          </ul>
        </div>
        <div class="card-body centered">
          <h5 class="card-title centered"></h5>
          <div>
            <img src="" alt="" style="width: 200px; height: 55px;">
          </div>
          <h5></h5>
          <h6></h6>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="card">
        <div class="card-header badge-dark after-hover">
          <h6 class="card-title float-left">Costcontrol</h6>
          <ul class="navbar-nav float-right mr-auto">
            <li class="nav-item dropdown">
              <span class="wait-hover">
                <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
              </span>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#"></a>
                <div class="dropdown-divider"></div>
              </div>
            </li>
          </ul>
        </div>
        <div class="card-body centered">
          <h5 class="card-title centered"></h5>
          <div>
            <img src="" alt="" style="width: 200px; height: 55px;">
          </div>
          <h5></h5>
          <h6></h6>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="card">
        <div class="card-header badge-dark after-hover">
          <h6 class="card-title float-left">Payable</h6>
          <ul class="navbar-nav float-right mr-auto">
            <li class="nav-item dropdown">
              <span class="wait-hover">
                <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
              </span>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#"></a>
                <div class="dropdown-divider"></div>
              </div>
            </li>
          </ul>
        </div>
        <div class="card-body centered">
          <h5 class="card-title centered"></h5>
          <div>
            <img src="" alt="" style="width: 200px; height: 55px;">
          </div>
          <h5></h5>
          <h6></h6>
        </div>
      </div>
    </div>
  </div>
</div>