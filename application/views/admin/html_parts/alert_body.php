 <?php if($this->session->flashdata('msg') != ''): ?>
      <div class="row">
       <div class="col-6" style="float:right">
         <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
              <span class="badge badge-pill badge-danger" style="font-size:16px;"><?= $this->session->flashdata('alert'); ?></span>
                <?= $this->session->flashdata('msg'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
       </div> 
      </div>
  <?php endif; ?>

  