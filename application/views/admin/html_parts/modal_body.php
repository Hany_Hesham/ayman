<div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-<?php echo $size?>" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url($form_link), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel"><?php echo $head?></h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <p>
                      <?php echo $body?>
                  </p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">إغلاق </button>
                  <button id="modalSubmit" type="submit" type="submit" name="submit" class="btn btn-primary">إضافة</button>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
