<?php initiate_alert();?> 
<div class="card">
  <div class="card-body">
    <?php if($this->data['permission']['creat'] == 1){?>
      <button id="add-type" class="btn btn-outline-dark">
        <i class="fa fa-cloud"></i>&nbsp; <strong>Add Type</strong>
      </button> <br> 
      <div class="dropdown-divider"></div><br><br>
    <?php }?>
    <h4 class="page-title">Sign Types</h4>
    <table id="sign-list-table" class="table table-hover"  style="width:100% !important;">
      <thead  class="thead-light">
        <tr>
          <th scope="col" width="10%"><strong>#</strong></th>
          <th scope="col" width="80%"><strong>Unit Name</strong></th>
          <th scope="col" width="10%"><strong>Deleted</strong></th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
<?php $this->load->view('admin/modules/sign.js.php');?>