<?php $select = '<option value="">Choose a Condition</option>';
  $select .= '<option value="<=">Less Than Or Equal</option>';
  $select .= '<option value=">=">More Than Or Equal</option>';
  $select .= '<option value=">">More Than</option>';
  $select .= '<option value="<">Less Than</option>';
  $select .= '<option value="!=">Not Equal</option>';
  $select .= '<option value="==">Equal</option>';
  $select .= '<option value="is_not_null">Not Equal NULL</option>';
  $select .= '<option value="is_nulls">Equal NULL</option>';
?>
<?php $select1 = '<option value="">Choose a Hotel</option>';
  foreach ($hotels as $hotel) {
    $select1 .= '<option value="'.$hotel['id'].'">'.$hotel['hotel_name'].'</option>';
  }
  $select1 .= '</select>';
?>
<?php $select2 = '<option value="">Choose a Condition</option>';
  $select2 .= '<option value="!=">Not Equal</option>';
  $select2 .= '<option value="==">Equal</option>';
?>
<?php $select3 = '<option value="">Choose a Department</option>';
  foreach ($departments as $department) {
    $select3 .= '<option value="'.$department['id'].'">'.$department['dep_name'].'</option>';
  }
  $select3 .= '</select>';
?>
<?php $select5 = '<option value="">Choose a Role</option>';
  foreach ($roles as $role) {
    $select5 .= '<option value="'.$role['id'].'">'.$role['name'].'</option>';
  }
  $select5 .= '</select>';
?>
<script type="text/javascript">
  let select = '<?php echo $select?>';
  let select1 = '<?php echo $select1?>';
  let select2 = '<?php echo $select2?>';
  let select3 = '<?php echo $select3?>';
  let select5 = '<?php echo $select5?>';
  let module_id = '<?php echo $module_id?>';
  var select4 =  moduleCol(module_id);
  initDTable(
    "sign-list-table",
    [{"name":"id"},
    {"name":"name"},
    {"name":"deleted"},],
    "<?php echo base_url("admin/modules/sign_ajax/".$module_id) ?>"
  );
  function moduleCol(module_id){
    if (module_id != '') {
      let temp ='';
      var url = "<?php echo base_url('admin/modules/get_modulecol');?>";
      $.ajax({
        url: url,
        dataType: "json",
        type : 'POST',
        data:{module_id:module_id},
        success: function(res){
          temp = res;
        }
      });
      return temp;
    }
  }
  $(document).ready(function(){  
    $("#add-type").click(function() {
      $('#smallmodal').modal('show');
      $('form').attr('action', ''+getUrl()+'admin/modules/sign_process');
      $('#smallmodal .modal-body').empty();
      $('#smallmodal .modal-header .modal-title').empty();
      $('#smallmodal .modal-header .modal-title').append('Add Sign Type');
      $('#smallmodal .modal-body').append('<div class="row"><div class="col-sm-4"><select id="hotel_id" name="hotel_id" class="form-control selectpiker" style="width: 100%; height:36px;">'+select1+'</select></div><div class="col-sm-4"><select id="h_condition" name="hotel_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select2+'</select></div><div class="col-sm-4"><input id="sign_id" type="hidden" name="id"><input id="module_id" type="hidden" name="module_id" value="'+module_id+'"><input  id="sign_name" type="text" name="name" class="form-control" placeholder="Name" required></div></div><br><br><div class="row"><div class="col-sm-4"><select id="did" name="dep_code" class="form-control selectpiker" style="width: 100%; height:36px;">'+select3+'</select></div><div class="col-sm-4"><select id="d_condition" name="department_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select2+'</select></div><div class="col-sm-4"><input id="priority" type="text" name="priority" class="form-control" placeholder="Priority" required></div></div><br><br><div class="row"><div class="col-sm-4"><select id="special" name="special" class="form-control selectpiker" style="width: 100%; height:36px;">'+select4+'</select></div><div class="col-sm-4"><select id="special_condition" name="special_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select+'</select></div><div class="col-sm-4"><input id="special_value" type="text" name="special_value" class="form-control" placeholder="Special Value" required></div></div><br><br><div class="row"><div class="col-sm-4"><select id="special1" name="special1" class="form-control selectpiker" style="width: 100%; height:36px;">'+select4+'</select></div><div class="col-sm-4"><select id="special_condition1" name="special1_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select+'</select></div><div class="col-sm-4"><input id="special_value1" type="text" name="special1_value" class="form-control" placeholder="Special Value" required></div></div><br><br><div class="row"><div class="col-sm-4"><select id="special2" name="special2" class="form-control selectpiker" style="width: 100%; height:36px;">'+select4+'</select></div><div class="col-sm-4"><select id="special_condition2" name="special2_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select+'</select></div><div class="col-sm-4"><input id="special_value2" type="text" name="special2_value" class="form-control" placeholder="Special Value" required></div></div><br><br><div class="row"><div class="col-sm-4"><select id="special3" name="special3" class="form-control selectpiker" style="width: 100%; height:36px;">'+select4+'</select></div><div class="col-sm-4"><select id="special_condition3" name="special3_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select+'</select></div><div class="col-sm-4"><input id="special_value3" type="text" name="special3_value" class="form-control" placeholder="Special Value" required></div></div><br>');
      $('#hotel_id').selectpicker('refresh');
      $('#h_condition').selectpicker('refresh');
      $('#did').selectpicker('refresh');
      $('#d_condition').selectpicker('refresh');
      $('#special').selectpicker('refresh');
      $('#special_condition').selectpicker('refresh');
      $('#special1').selectpicker('refresh');
      $('#special_condition1').selectpicker('refresh');
      $('#special2').selectpicker('refresh');
      $('#special_condition2').selectpicker('refresh');
      $('#special3').selectpicker('refresh');
      $('#special_condition3').selectpicker('refresh');
    });    
    $(".edit-type").click(function() {
      $('form').attr('action', ''+getUrl()+'admin/modules/sign_process');
      var $row                    = $(this).closest("tr");
      var sign_id                 = $row.find("#sign_id").text();
      var modules_id                 = $row.find("#modules_id").text();
      var sign_name               = $row.find("#sign_name").text();
      var hid                     = $row.find("#hid").text();
      var h_condition             = $row.find("#h_condition").text();
      var did                     = $row.find("#did").text();
      var d_condition             = $row.find("#d_condition").text();
      var priority                = $row.find("#priority").text();
      var special                 = $row.find("#special").text();
      var special_condition       = $row.find("#special_condition").text();
      var special_value           = $row.find("#special_value").text();
      var special1                = $row.find("#special1").text();
      var special_condition1      = $row.find("#special_condition1").text();
      var special_value1          = $row.find("#special_value1").text();
      var special2                = $row.find("#special2").text();
      var special_condition2      = $row.find("#special_condition2").text();
      var special_value2          = $row.find("#special_value2").text();
      var special3                = $row.find("#special3").text();
      var special_condition3      = $row.find("#special_condition3").text();
      var special_value3          = $row.find("#special_value3").text();
      $('#smallmodal .modal-body').empty();
      $('#smallmodal .modal-header .modal-title').empty();
      $('#smallmodal .modal-header .modal-title').append('Edit Sign Type');
      $('#smallmodal .modal-body').append('<div class="row"><div class="col-sm-4"><select id="hotel_id'+sign_id+'" name="hotel_id" class="form-control selectpiker" style="width: 100%; height:36px;">'+select1+'</select></div><div class="col-sm-4"><select id="h_condition'+sign_id+'" name="hotel_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select2+'</select></div><div class="col-sm-4"><input id="sign_id'+sign_id+'" type="hidden" value="'+sign_id+'" name="id"><input id="modules_id'+sign_id+'" type="hidden" value="'+modules_id+'" name="module_id"><input  id="sign_name'+sign_id+'" type="text" name="name" class="form-control" placeholder="Name" value="'+sign_name+'" required></div></div><br><br><div class="row"><div class="col-sm-4"><select id="did'+sign_id+'" name="dep_code" class="form-control selectpiker" style="width: 100%; height:36px;">'+select3+'</select></div><div class="col-sm-4"><select id="d_condition'+sign_id+'" name="department_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select2+'</select></div><div class="col-sm-4"><input id="priority'+sign_id+'" type="text" name="priority" class="form-control" placeholder="Priority" value="'+priority+'" required></div></div><br><br><div class="row"><div class="col-sm-4"><select id="special'+sign_id+'" name="special" class="form-control selectpiker" style="width: 100%; height:36px;">'+select4+'</select></div><div class="col-sm-4"><select id="special_condition'+sign_id+'" name="special_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select+'</select></div><div class="col-sm-4"><input id="special_value'+sign_id+'" type="text" name="special_value" class="form-control" placeholder="Special Value" value="'+special_value+'" required></div></div><br><br><div class="row"><div class="col-sm-4"><select id="special1'+sign_id+'" name="special1" class="form-control selectpiker" style="width: 100%; height:36px;">'+select4+'</select></div><div class="col-sm-4"><select id="special_condition1'+sign_id+'" name="special1_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select+'</select></div><div class="col-sm-4"><input id="special_value1'+sign_id+'" type="text" name="special1_value" class="form-control" placeholder="Special Value" value="'+special_value1+'" required></div></div><br><br><div class="row"><div class="col-sm-4"><select id="special2'+sign_id+'" name="special2" class="form-control selectpiker" style="width: 100%; height:36px;">'+select4+'</select></div><div class="col-sm-4"><select id="special_condition2'+sign_id+'" name="special2_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select+'</select></div><div class="col-sm-4"><input id="special_value2'+sign_id+'" type="text" name="special2_value" class="form-control" placeholder="Special Value" value="'+special_value2+'" required></div></div><br><br><div class="row"><div class="col-sm-4"><select id="special3'+sign_id+'" name="special3" class="form-control selectpiker" style="width: 100%; height:36px;">'+select4+'</select></div><div class="col-sm-4"><select id="special_condition3'+sign_id+'" name="special3_condition" class="form-control selectpiker" style="width: 100%; height:36px;">'+select+'</select></div><div class="col-sm-4"><input id="special_value3'+sign_id+'" type="text" name="special3_value" class="form-control" placeholder="Special Value" value="'+special_value3+'" required></div></div><br>');  
      $('#hotel_id'+sign_id+'').selectpicker('val', hid);
      $('#hotel_id'+sign_id+'').selectpicker('refresh');
      $('#h_condition'+sign_id+'').selectpicker('val', h_condition);
      $('#h_condition'+sign_id+'').selectpicker('refresh');
      $('#did'+sign_id+'').selectpicker('val', did);
      $('#did'+sign_id+'').selectpicker('refresh');
      $('#d_condition'+sign_id+'').selectpicker('val', d_condition);
      $('#d_condition'+sign_id+'').selectpicker('refresh');
      $('#special'+sign_id+'').selectpicker('val', special);
      $('#special'+sign_id+'').selectpicker('refresh');
      $('#special_condition'+sign_id+'').selectpicker('val', special_condition);
      $('#special_condition'+sign_id+'').selectpicker('refresh');
      $('#special1'+sign_id+'').selectpicker('val', special1);
      $('#special1'+sign_id+'').selectpicker('refresh');
      $('#special_condition1'+sign_id+'').selectpicker('val', special_condition1);
      $('#special_condition1'+sign_id+'').selectpicker('refresh');
      $('#special2'+sign_id+'').selectpicker('val', special2);
      $('#special2'+sign_id+'').selectpicker('refresh');
      $('#special_condition2'+sign_id+'').selectpicker('val', special_condition2);
      $('#special_condition2'+sign_id+'').selectpicker('refresh');
      $('#special3'+sign_id+'').selectpicker('val', special3);
      $('#special3'+sign_id+'').selectpicker('refresh');
      $('#special_condition3'+sign_id+'').selectpicker('val', special_condition3);
      $('#special_condition3'+sign_id+'').selectpicker('refresh');
    });     
  })
  function getsignroles(id){
    $('#smallmodal').modal('show');
    $('#smallmodal .modal-body').empty();
    var url="<?php echo base_url('admin/modules/get_submenu/Modules_model/get_roles/');?>";      
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{search:id},
      success: function(data){
        $('form').attr('action', ''+getUrl()+'admin/modules/role_process/'+id);
        $('#smallmodal .modal-header .modal-title').empty();
        $('#smallmodal .modal-header .modal-title').append('Edit Roles');
        $('#smallmodal .modal-body').append('<button id="addnewrole" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Role" class="btn btn-light btn-lg btn-circle"><i class="mdi mdi-database-plus"></i></button><br><br>'); 
        let temp ='';
        $.each(data, function(i,item){
          temp = data[i].id+','+temp;
          $('#smallmodal .modal-body').append('<div class="row"><div class="col-sm-6"><input id="role_id'+data[i].id+'" type="hidden" name="items['+data[i].id+'][id]" value="'+data[i].id+'"><select id="role_name'+data[i].id+'" name="items['+data[i].id+'][role]" class="form-control selectpiker" style="width: 100%; height:36px;">'+select5+'</select></div><div class="col-sm-6"><input id="role_rank'+data[i].id+'" type="text" name="items['+data[i].id+'][rank]" class="form-control" value="'+data[i].rank+'" required></div></div><br>');  
          $('#role_name'+data[i].id+'').selectpicker('val', data[i].role);
          $('#role_name'+data[i].id+'').selectpicker('refresh');
        });
        $('#smallmodal .modal-body').append('<input class="form-control " type="hidden" id="allstoredIds"  value="'+temp+'">');
        $(document).ready(function(){
          $('.selectpicker').selectpicker('render');
          $("#addnewrole").click(function() {
            let  allstoredIds   = $('#smallmodal .modal-body #allstoredIds').val().split(',');
            let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
            allstoredIds.push(rowId);
            $('#smallmodal .modal-body #allstoredIds').val(allstoredIds);
            $('#smallmodal .modal-body').append('<div class="row"><div class="col-sm-6"><select id="role_name'+rowId+'" name="items['+rowId+'][role]" class="form-control selectpiker" style="width: 100%; height:36px;">'+select5+'</select></div><div class="col-sm-6"><input id="role_rank'+rowId+'" type="text" name="items['+rowId+'][rank]" class="form-control" required></div></div><br>'); 
          }); 
        });
      }
    });
  }
</script> 