
    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                        <h4 class="page-title">Departments</h4>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item active" aria-current="page">Departments</li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <?php if($this->data['permission']['creat'] == 1){?>
                         <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-primary btn-lg">
                                 <i class="fa fa-cloud"></i>&nbsp; <strong>Add Department</strong></button> <br> 
                         <div class="dropdown-divider"></div><br><br>
                       <?php }?>
                         <table id="departments-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th scope="col" width="5%"><strong>#</strong></th>
                                  <th scope="col" width="15%"><strong>Department Name</strong></th>
                                  <th scope="col" width="15%"><strong>Code</strong></th>
                                  <th scope="col" width="10%"><strong>Rank</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <?php 
         initiate_modal(
                         '','Add New Department','admin/departments/dep_process',
                          '<input id="dep_id" type="hidden" name="id">
                           <input  id="depName" type="text" name="dep_name" class="form-control " placeholder="Department Name" required><br>
                           <input id="code" type="text" name="code" class="form-control" placeholder="Code" required><br> 
                           <input id="rank" type="text" name="rank" class="form-control" placeholder="rank" required><br>
                           <br>'
                         )
  ?>

<script type="text/javascript">
      initDTable(
                 "departments-list-table",
                    [{"name":"id"},
                     {"name":"dep_name"},
                     {"name":"code"},
                     {"name":"rank"},],
                  "<?php echo base_url("admin/departments/deps_ajax/") ?>"
           );

      $(document).ready(function(){
          
          $(".edit-dep").click(function() {
              var $row       = $(this).closest("tr");
              var dep_id     = $row.find("#depId").text();
              var dep_name   = $row.find("#depName").text();
              var code       = $row.find("#depCode").text();
              var rank       = $row.find("#depRank").text();
                 $(".modal-body #dep_id").val(dep_id );
                 $(".modal-body #depName").val( dep_name );
                 $(".modal-body #code").val( code );
                 $(".modal-body #rank").val( rank );

              });

          $("#add-new").click(function() {
                 $(".modal-body #dep_id").val('');
                 $(".modal-body #depName").val('');
                 $(".modal-body #code").val('');
                 $(".modal-body #rank").val('');
            });

       });
      
</script>