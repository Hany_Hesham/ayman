
    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                        <h4 class="page-title">الشركات  </h4>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">الرئيسية</a></li>
                              <li class="breadcrumb-item active" aria-current="page">الشركات   </li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
          <div class="col-lg-6"></div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <?php if($this->data['permission']['creat'] == 1){?>
                         <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-primary btn-lg float-right">
                                 <i class="fa fa-cloud"></i>&nbsp; <strong>إضافة شركة </strong></button> <br> 
                         <div class="dropdown-divider"></div><br><br>
                       <?php }?>
                         <table id="companies-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th scope="col" width="10%" class="text-right"><strong>الترتيب </strong></th>
                                  <th scope="col" width="15%" class="text-right"><strong>إسم الشركة </strong></th>
                                  <th scope="col" width="5%"  class="text-right"><strong>#</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <?php 
         initiate_modal(
                         '','إضافة شركة جديدة ','admin/companies/company_process',
                          '<input id="company_id" type="hidden" name="id">
                           <input  id="companyName" type="text" name="company_name" class="form-control " placeholder="إسم الشركة " dir="rtl" required><br>
                           <input id="rank" type="text" name="rank" class="form-control" placeholder="الترتيب " dir="rtl" required><br>
                           <br>'
                         )
  ?>

<script type="text/javascript">
      initDTable(
                 "companies-list-table",
                    [{"name":"id"},
                     {"name":"company_name"},
                     {"name":"rank"},],
                  "<?php echo base_url("admin/companies/companies_ajax/") ?>"
           );

      $(document).ready(function(){
          
          $(".edit-dep").click(function() {
              var $row           = $(this).closest("tr");
              var company_id     = $row.find("#companyId").text();
              var company_name   = $row.find("#companyName").text();
              var rank           = $row.find("#companyRank").text();
                 $(".modal-body #company_id").val(company_id );
                 $(".modal-body #companyName").val( company_name );
                 $(".modal-body #rank").val( rank );

              });

          $("#add-new").click(function() {
                 $(".modal-body #company_id").val('');
                 $(".modal-body #companyName").val('');
                 $(".modal-body #rank").val('');
            });

       });
      
</script>