<div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                         <h5 class="page-title">Products Manager</h5>      
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager/category_manager');?>">Category Manager</a></li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                          <?php if($category['id']){?>
                             <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-cyan btn-lg">
                                     <i class="fa fa-cloud"></i>&nbsp; <strong>Add Product</strong></button> <br> 
                             <div class="dropdown-divider"></div><br><br>
                          <?php }?>   
                         <table id="items-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th width="5%"><strong>#</strong></th>
                                  <th width="15%"><strong>Product Name</strong></th>
                                  <th width="10%"><strong>Serial</strong></th>
                                  <th width="30%"><strong>Description</strong></th>
                                  <th width="15%"><strong>company</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <?php initiate_modal(
                         'lg','Add New Product','admin/site_manager/product_process',
                          '<input id="group_id" type="hidden" name="group_id" value="'.$category['id'].'">
                           <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*Product Name</label>
                           <input  id="itemName" onkeyup="fieldChecker(\'itemName\',\'admin/site_manager/check_field/site_group_items/item_name\')" type="text" name="item_name" class="form-control " placeholder="Item Name" required><div class="invalid-feedback">Please change the name it is already exist</div>
                            <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*Company</label>
                            <input type="text" name="company" class="form-control " placeholder="Company Name">
                           <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*Description</label>
                           <textarea id="description" name="description" class="form-control" placeholder="Description" ></textarea>
                           <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*Rank</label>
                           <input type="text" name="rank" class="form-control " placeholder="Rank">
                           <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*Image</label>
                           <input type="file" name="img" class="form-control" accept="image/*"/>
                           <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*File</label>
                           <input type="file" name="file" class="form-control"/>
                            <br>'
                         )?>

<script type="text/javascript">
      initDTable(
                 "items-list-table",
                    [{"name":"id"},
                     {"name":"item_name"},
                     {"name":"serial"},
                     {"name":"company"},
                     {"name":"description"},],
                  "<?php echo base_url("admin/site_manager/products_ajax/".$category['id']) ?>"
           );
      
    </script>