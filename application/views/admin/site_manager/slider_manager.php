<div class="page-breadcrumb">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="col-sm-4">
            <div class="float-left">
              <h4 class="page-title">Slider Manager</h4>      
            </div>
          </div>
          <div class="col-sm-12">
            <div class="ml-auto float-right">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager');?>">Site Manager</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <?php  echo form_open(base_url('admin/site_manager/slider_manager'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <div style="width:100%;">
                    <table id="Items-list-table" class="table table-hover">
                      <thead class="thead-dark">
                        <tr>
                          <th width="1%"  class="hidden">#</th>
                          <th width="12%" style="font-size:13px;">Slider</th>
                          <th width="12%" style="font-size:13px;">Header</th>
                          <th width="25%" style="font-size:13px;">Text</th>
                          <th width="12%" style="font-size:13px;">Image</th>
                          <th width="12%" style="font-size:13px;">Current Image</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if (isset($sliders)) {$i=1;$ids = '';foreach($sliders as $row){ $ids= $i.','.$ids;?>
                          <tr class="rowIds" id="row<?php echo $i?>">
                            <td class="hidden">
                                <?php echo $i?>
                                <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>" id="items-<?php echo $i?>-id"/>
                            </td>
                            <td><?php echo $row['thumb']?></td>
                            <td>
                              <?php if($row['id'] == 1){?>
                                <input type="text" name="items[<?php echo $i?>][first_title]" class="form-control" value="<?php echo $row['first_title']?>" required id="items-<?php echo $i?>-first_title"/>
                              <?php }?>
                            </td>
                            <td>
                              <?php if($row['id'] == 1){?>
                                <input type="text" name="items[<?php echo $i?>][second_title]" class="form-control" value="<?php echo $row['second_title']?>" required id="items-<?php echo $i?>-second_title"/>
                              <?php }?>
                            </td>
                            <td>
                              <input type="file"  name="items-<?php echo $i?>-img" class="form-control" accept="image/*" id="items-<?php echo $i?>-img"/>
                              <input type="hidden" name="items[<?php echo $i?>][old_img]" value="<?php echo $row['img']?>">
                            </td>
                            <td class="text-center">
                              <a class="btn default btn-outline image-popup-vertical-fit el-link" href="<?php if($row['img']!='0'){echo base_url('site_assets/images/slider/'.$row['img']);}elseif($row['img']=='0'){echo base_url('site_assets/images/auth-bg.jpg');}?>">
                                <img style="width:100%;height:60px;" src="<?php if($row['img']!='0'){echo base_url('site_assets/images/slider/'.$row['img']);}elseif($row['img']=='0'){echo base_url('site_assets/images/auth-bg.jpg');}?>" alt="No Attach">
                              </a>
                            </td>
                          </tr>
                        <?php $i++;} }?>  
                      </tbody>
                    </table>
                    <input class="form-control " type="hidden" id="allstoredIds"  value="<?php echo (isset($sliders)) ? $ids :''?>">
                  </div>
                  <br>
                </div>
              </div>  
            </div> 
          </div>
        </div>
      </div>
    </div> 
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <input type="submit" name="submit" class="btn btn-dark btn-lg" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div> 
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>       
<?php $this->load->view('admin/site_manager/site_manager.js.php');?> 