<div class="page-breadcrumb">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="col-sm-4">
            <div class="float-left">
              <h4 class="page-title">Gallery Manager</h4>      
            </div>
          </div>
          <div class="col-sm-12">
            <div class="ml-auto float-right">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager');?>">Site Manager</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <?php  echo form_open(base_url('admin/site_manager/gallary_manager'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <div style="width:100%;">
                    <table id="Items-list-table" class="table table-hover">
                      <thead class="thead-dark">
                        <tr>
                          <th width="1%"  class="hidden">#</th>
                          <th width="12%" style="font-size:13px;">Title</th>
                          <th width="12%" style="font-size:13px;">Image</th>
                          <th width="14%" style="font-size:13px;">Old Image</th>
                          <th width="12%" style="font-size:13px;">Thumb</th>
                          <th width="14%" style="font-size:13px;">Old Thumb</th>
                          <th width="10%" style="font-size:13px;">Rank</th>
                          <th class="text-center" width="1%">
                            <a href="<?php echo base_url('admin/site_manager/add_gallary')?>" name="add" id="addRow" class="btn btn-light btn-sm btn-circle btn_add">
                              <i class="mdi mdi-database-plus"></i>
                            </a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if (isset($gallarys)) {$i=1;$ids = '';foreach($gallarys as $row){ $ids= $i.','.$ids;?>
                          <tr class="rowIds" id="row<?php echo $i?>">
                            <td class="hidden"><?php echo $i?></td>
                            <td>
                              <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>" id="items-<?php echo $i?>-id"/>
                              <input type="text" name="items[<?php echo $i?>][title]" class="form-control" value="<?php echo $row['title']?>" required id="items-<?php echo $i?>-title"/>
                            </td>
                            <td>
                              <input type="file"  name="items-<?php echo $i?>-img" class="form-control" accept="image/*" id="items-<?php echo $i?>-img"/>
                              <input type="hidden" name="items[<?php echo $i?>][old_img]" value="<?php echo $row['img']?>">
                            </td>
                            <td class="text-center">
                              <a class="btn default btn-outline image-popup-vertical-fit el-link" href="<?php if($row['img']!='0'){echo base_url('site_assets/images/portfolio/full/'.$row['img']);}elseif($row['img']=='0'){echo base_url('site_assets/images/auth-bg.jpg');}?>">
                                <img style="width:100%;height:60px;" src="<?php if($row['img']!='0'){echo base_url('site_assets/images/portfolio/full/'.$row['img']);}elseif($row['img']=='0'){echo base_url('site_assets/images/auth-bg.jpg');}?>" alt="No Attach">
                              </a>
                            </td>
                            <td>
                              <input type="file"  name="items-<?php echo $i?>-thumb" class="form-control" accept="image/*" id="items-<?php echo $i?>-thumb"/>
                              <input type="hidden" name="items[<?php echo $i?>][old_thumb]" value="<?php echo $row['thumb']?>">
                            </td>
                            <td class="text-center">
                              <a class="btn default btn-outline image-popup-vertical-fit el-link" href="<?php if($row['thumb']!='0'){echo base_url('site_assets/images/portfolio/4/'.$row['thumb']);}elseif($row['thumb']=='0'){echo base_url('site_assets/images/auth-bg.jpg');}?>">
                                <img style="width:100%;height:60px;" src="<?php if($row['thumb']!='0'){echo base_url('site_assets/images/portfolio/4/'.$row['thumb']);}elseif($row['thumb']=='0'){echo base_url('site_assets/images/auth-bg.jpg');}?>" alt="No Attach">
                              </a>
                            </td>
                            <td>
                              <input type="number" name="items[<?php echo $i?>][rank]" class="form-control" value="<?php echo $row['rank']?>" required id="items-<?php echo $i?>-rank"/>
                            </td>
                            <td class="text-center">
                              <button type="button" name="remove" onclick="removeRow('admin/site_manager/delete_gallary','<?php echo $i?>','<?php echo $row['id']?>')" class="btn btn-light btn-sm btn-circle btn_remove">
                                <i class="fas fa-trash-alt text-danger"></i>
                              </button>
                            </td>
                          </tr>
                        <?php $i++;} }?>  
                      </tbody>
                    </table>
                    <input class="form-control " type="hidden" id="allstoredIds"  value="<?php echo (isset($gallarys)) ? $ids :''?>">
                  </div>
                  <br>
                </div>
              </div>  
            </div> 
          </div>
        </div>
      </div>
    </div> 
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <input type="submit" name="submit" class="btn btn-dark btn-lg" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div> 
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>       
<?php $this->load->view('admin/site_manager/site_manager.js.php');?> 