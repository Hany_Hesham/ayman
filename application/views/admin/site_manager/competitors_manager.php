<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                <div class="col-sm-4">
                  <div class="float-left">
                      <h5 class="page-title">Competitors Manager</h5>      
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="ml-auto float-right">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                          <li class="breadcrumb-item">Competitors Manager</li>
                        </ol>
                      </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br>  
                      <table id="competitors-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="25%"><strong>Competitor Info</strong></th>
                              <th width="15%"><strong>City</strong></th>
                              <th width="10%"><strong>School</strong></th>
                              <th width="12%"><strong>Birth Date</strong></th>
                              <th width="12%"><strong>Age</strong></th>
                              <th width="20%"><strong>Active</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                  
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
  <div class="modal fade" id="changeCompetitorPassword" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/change_competitor_password'), 'class="form-horizontal" id="changeCompetitorPasswordForm" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">Change Competitor Password</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <input id="competitor_id" type="hidden" name="competitor_id" class="form-control">
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Email</label>
                      <input  id="email" type="text" name="email" class="form-control " disabled>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*New Password</label>
                      <input  id="password" type="password" name="password" onkeyup="passwordConfirm()" class="form-control " required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Confirm New Password</label>
                      <input  id="confirm_password" type="password" name="confirm_password" onkeyup="passwordConfirm()" class="form-control " required>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <input type="submit" name="submit" value="Save" id="changePasswordSaveBtn" class="btn btn-primary"/>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
    initDTable(
        "competitors-list-table",
            [{"name":"id"},
            {"name":"name"},
            {"name":"city"},
            {"name":"school"},
            {"name":"birth_date"},
            {"name":"age"},
            {"name":"disabled"},],
        "<?php echo base_url("admin/site_manager/competitors_ajax/") ?>"
    );
    function changeCompetitorPassword(competitor) {
      $('#changeCompetitorPassword').modal('show');
      if(competitor != undefined){
        let {id, email} = competitor;
        $('#changeCompetitorPassword .modal-body #competitor_id').val(id);
        $('#changeCompetitorPassword .modal-body #email').val(email);
      }else{
        $('#changeCompetitorPasswordForm').trigger("reset");
      }
     }

     function passwordConfirm(){
        let password = $('#password').val();
        let confirmedPassword = $('#confirm_password').val();
        if(password != '' || confirmedPassword != ''){
          if(password === confirmedPassword){
            $('#changePasswordSaveBtn').attr('disabled',false);
          }else{
            $('#changePasswordSaveBtn').attr('disabled',true);
          }
        }else{
          $('#changePasswordSaveBtn').attr('disabled',false);
        }
      }
   
  </script>