<div class="page-breadcrumb">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="col-sm-4">
            <div class="float-left">
              <h4 class="page-title">Offers Manager</h4>      
            </div>
          </div>
          <div class="col-sm-12">
            <div class="ml-auto float-right">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager');?>">Site Manager</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <?php  echo form_open(base_url('admin/site_manager/offer_manager'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <div style="width:100%;">
                    <table id="Items-list-table" class="table table-hover">
                      <thead class="thead-dark">
                        <tr>
                          <th width="1%"  class="hidden">#</th>
                          <th width="0%" style="font-size:13px;" class="hidden">Header</th>
                          <th width="10%" style="font-size:13px;">Activity</th>
                          <th width="10%" style="font-size:13px;">Hotel</th>
                          <th width="10%" style="font-size:13px;">Sub Activity</th>
                          <th width="13%" style="font-size:13px;">Name</th>
                          <th width="20%" style="font-size:13px;">Data</th>
                          <th width="10%" style="font-size:13px;">Image</th>
                          <th width="10%" style="font-size:13px;">Price</th>
                          <th width="5%" style="font-size:13px;">Currency</th>
                          <th width="10%" style="font-size:13px;">Special</th>
                          <th class="text-center" width="1%">
                            <button type="button" name="remove" id="addRow" class="btn btn-light btn-sm btn-circle btn_remove" onclick="addService()">
                              <i class="mdi mdi-database-plus"></i>
                            </button>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if (isset($offers)) {$i=1;$ids = '';foreach($offers as $row){ $ids= $i.','.$ids;?>
                          <tr class="rowIds" id="row<?php echo $i?>">
                            <td class="hidden"><?php echo $i?></td>
                            <td class="hidden">
                              <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                            </td>
                            <td>
                              <select name="items[<?php echo $i?>][activity_id]" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Activity" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                                <option value=""></option>
                                <?php foreach($activity as $active){?>
                                  <option value="<?php echo $active['id']?>" <?php if($row['activity_id']==$active['id']){echo 'selected';}?>>
                                    <?php echo $active['name']?>
                                  </option>
                                <?php }?>
                              </select>
                            </td>
                            <td>
                              <select name="items[<?php echo $i?>][hotel_id]" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                                <option value=""></option>
                                <?php foreach($hotels as $hotel){?>
                                  <option value="<?php echo $hotel['id']?>" <?php if($row['hotel_id']==$hotel['id']){echo 'selected';}?>>
                                    <?php echo $hotel['name']?>
                                  </option>
                                <?php }?>
                              </select>
                            </td>
                            <td>
                              <select name="items[<?php echo $i?>][sub_id]" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Sub Activity" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                                <option value=""></option>
                                <?php foreach($sub_activity as $sub){?>
                                  <option value="<?php echo $sub['id']?>" <?php if($row['sub_id']==$sub['id']){echo 'selected';}?>>
                                    <?php echo $sub['name']?>
                                  </option>
                                <?php }?>
                              </select>
                            </td>
                            <td>
                              <input type="text" name="items[<?php echo $i?>][name]" class="form-control" value="<?php echo $row['name']?>" required/>
                            </td>
                            <td>
                              <textarea name="items[<?php echo $i?>][data]" class="form-control" required><?php echo $row['data']?></textarea>
                            </td>
                            <td>
                              <input type="file" name="items-<?php echo $i?>-image" placeholder="Image" class="form-control"/>
                            </td>
                            <td>
                              <input type="number" name="items[<?php echo $i?>][price]" class="form-control" value="<?php echo $row['price']?>" required/>
                            </td>
                            <td>
                              <select name="items[<?php echo $i?>][currency]" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Currency" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                                <option value=""></option>
                                <option value="EGP" <?php if($row['currency']=='EGP'){echo 'selected';}?>>EGP</option>
                                <option value="$" <?php if($row['currency']=='$'){echo 'selected';}?>>$</option>
                                <option value="Euro" <?php if($row['currency']=='Euro'){echo 'selected';}?>>Euro</option>
                              </select>
                            </td>
                            <td>
                              <select name="items[<?php echo $i?>][special]" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Special" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                                <option value=""></option>
                                <option value="1" <?php if($row['special']==1){echo 'selected';}?>>Special Offer</option>
                                <option value="0" <?php if($row['special']==0){echo 'selected';}?>>Normal Offer</option>
                              </select>
                            </td>
                            <td class="text-center">
                              <button type="button" name="remove" onclick="removeRow('admin/site_manager/delete_offer','<?php echo $i?>','<?php echo $row['id']?>')" class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt text-danger"></i></button>
                            </td>
                          </tr>
                        <?php $i++;} }?>  
                      </tbody>
                    </table>
                    <input class="form-control " type="hidden" id="allstoredIds"  value="<?php echo (isset($offers)) ? $ids :''?>">
                  </div>
                  <br>
                </div>
              </div>  
            </div> 
          </div>
        </div>
      </div>
    </div> 
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <input type="submit" name="submit" class="btn btn-dark btn-lg" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div> 
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>       
<?php $this->load->view('admin/site_manager/site_manager.js.php');?> 