<div class="page-breadcrumb">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body">
                <div class="col-sm-4">
                  <div class="float-left">
                      <h5 class="page-title">Voters Manager</h5>      
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="ml-auto float-right">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                          <li class="breadcrumb-item">Voters Manager</li>
                        </ol>
                      </nav>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid">
    <?php initiate_alert();?> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><br>  
                      <table id="voters-list-table" class="table table-hover" style="width:100% !important;">
                          <thead >
                            <tr>
                              <th width="5%"><strong>#</strong></th>
                              <th width="15%"><strong>First Name</strong></th>
                              <th width="15%"><strong>Last Name</strong></th>
                              <th width="15%"><strong>Email</strong></th>
                              <th width="15%"><strong>Provider</strong></th>
                              <th width="15%"><strong>Created At</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                  
                          </tbody>
                      </table>
              </div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
    initDTable(
        "voters-list-table",
            [{"name":"id"},
            {"name":"first_name"},
            {"name":"last_name"},
            {"name":"email"},
            {"name":"oauth_provider"},
            {"name":"created_at"},],
        "<?php echo base_url("admin/site_manager/voters_ajax/") ?>"
    );
   
  </script>