
    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                         <h4 class="page-title">Footer Manager</h4>      
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager');?>">Site Manager</a></li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
       <?php  echo form_open(base_url('admin/site_manager/footer_manager'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
       <div class="row">
        <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                 <div class="row">
                  <div class="col-sm-4">
                    <label class="text-right control-label col-form-label" style="font-size:12px;">*Tel</label>
                     <input type="hidden" name="items[<?php echo $footer_tel['id']?>][id]" value="<?php if (isset($footer_tel)){echo $footer_tel['id'];}?>">
                     <input type="hidden" name="items[<?php echo $footer_tel['id']?>][meta]" value="<?php if (isset($footer_tel)){echo $footer_tel['meta'];}?>">
                    <input type="text" id="tel" name="items[<?php echo $footer_tel['id']?>][title]" class="form-control" value="<?php if (isset($footer_tel)){echo $footer_tel['title'];}?>" required>
                  </div>
                  <div class="col-sm-4">
                    <label class="text-right control-label col-form-label" style="font-size:12px;">*Email</label>
                     <input type="hidden" name="items[<?php echo $footer_email['id']?>][id]" value="<?php if (isset($footer_email)){echo $footer_email['id'];}?>">
                      <input type="hidden" name="items[<?php echo $footer_email['id']?>][meta]" value="<?php if (isset($footer_email)){echo $footer_email['meta'];}?>">
                    <input type="text" id="email" name="items[<?php echo $footer_email['id']?>][title]" class="form-control" value="<?php if (isset($footer_email)){echo $footer_email['title'];}?>" required>
                  </div>
                  <div class="col-sm-4">
                    <label class="text-right control-label col-form-label" style="font-size:12px;">*Working Hours</label>
                    <input type="hidden" name="items[<?php echo $footer_working['id']?>][id]" value="<?php if (isset($footer_working)){echo $footer_working['id'];}?>">
                      <input type="hidden" name="items[<?php echo $footer_working['id']?>][meta]" value="<?php if (isset($footer_working)){echo $footer_working['meta'];}?>">
                    <input type="text" id="work" name="items[<?php echo $footer_working['id']?>][title]" class="form-control" value="<?php if (isset($footer_working)){echo $footer_working['title'];}?>" required>
                  </div>
                </div><br>
                <div class="row">
                  <div class="col-sm-8">
                    <label class="text-right control-label col-form-label" style="font-size:12px;">*About</label>
                     <input type="hidden" name="items[<?php echo $footer_about['id']?>][id]" value="<?php if (isset($footer_about)){echo $footer_about['id'];}?>">
                      <input type="hidden" name="items[<?php echo $footer_about['id']?>][meta]" value="<?php if (isset($footer_about)){echo $footer_about['meta'];}?>">
                    <textarea id="about" name="items[<?php echo $footer_about['id']?>][paragraph]" class="form-control" rows="3" required><?php if (isset($footer_about)){echo $footer_about['paragraph'];}?></textarea>
                  </div>
                  <div class="col-sm-4">
                    <label class="text-right control-label col-form-label" style="font-size:12px;">*Contact Us Info</label>
                    <input type="hidden" name="items[<?php echo $footer_info['id']?>][id]" value="<?php if (isset($footer_info)){echo $footer_info['id'];}?>">
                      <input type="hidden" name="items[<?php echo $footer_info['id']?>][meta]" value="<?php if (isset($footer_info)){echo $footer_info['meta'];}?>">
                    <textarea id="info" name="items[<?php echo $footer_info['id']?>][paragraph]" class="form-control" rows="3" required><?php if (isset($footer_info)){echo $footer_info['paragraph'];}?></textarea>
                  </div> 
                </div>
               </div> 
              </div>
           </div>
        </div>
        <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-lg-12"> 
                  <input type="submit" name="submit" class="btn btn-dark btn-lg" value="Save" style="float:right;width:12%;">
                </div>
              </div>
            </div>
          </div> 
      </div>
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
    <?php echo form_close( ); ?>
    <?php $this->load->view('admin/html_parts/loader_div');?>       
    <?php $this->load->view('admin/site_manager/site_manager.js.php');?> 
   