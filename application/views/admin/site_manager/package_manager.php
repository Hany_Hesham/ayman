<div class="page-breadcrumb">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="col-sm-4">
            <div class="float-left">
              <h4 class="page-title">Package Manager</h4>      
            </div>
          </div>
          <div class="col-sm-12">
            <div class="ml-auto float-right">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager');?>">Site Manager</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <?php  echo form_open(base_url('admin/site_manager/package_manager'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <div style="width:100%;">
                    <table id="Items-list-table" class="table table-hover">
                      <thead class="thead-dark">
                        <tr>
                          <th width="1%"  class="hidden">#</th>
                          <th width="12%" style="font-size:13px;">Title</th>
                          <th width="14%" style="font-size:13px;">Remarks</th>
                          <th width="12%" style="font-size:13px;">Image</th>
                          <th width="14%" style="font-size:13px;">Old Image</th>
                          <th width="14%" style="font-size:13px;">Date</th>
                          <th width="12%" style="font-size:13px;">Time</th>
                          <th width="12%" style="font-size:13px;">Location</th>
                          <th width="12%" style="font-size:13px;">Status</th>
                          <th width="10%" style="font-size:13px;">Rank</th>
                          <th class="text-center" width="1%">
                            <a href="<?php echo base_url('admin/site_manager/add_package')?>" name="add" id="addRow" class="btn btn-light btn-sm btn-circle btn_add">
                              <i class="mdi mdi-database-plus"></i>
                            </a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if (isset($packages)) {$i=1;$ids = '';foreach($packages as $row){ $ids= $i.','.$ids;?>
                          <tr class="rowIds" id="row<?php echo $i?>">
                            <td class="hidden"><?php echo $i?></td>
                            <td>
                              <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>" id="items-<?php echo $i?>-id"/>
                              <input type="text" name="items[<?php echo $i?>][title]" class="form-control" value="<?php echo $row['title']?>" required id="items-<?php echo $i?>-title"/>
                            </td>
                            <td>
                              <textarea  class="form-control summernote" name="items[<?php echo $i?>][remarks]" id="items-<?php echo $i?>-remarks"><?php echo $row['remarks']?></textarea>
                            </td>
                            <td>
                              <input type="file"  name="items-<?php echo $i?>-image" class="form-control" accept="image/*" id="items-<?php echo $i?>-image"/>
                              <input type="hidden" name="items[<?php echo $i?>][old_image]" value="<?php echo $row['image']?>">
                            </td>
                            <td class="text-center">
                              <a class="btn default btn-outline image-popup-vertical-fit el-link" href="<?php if($row['image']!='0'){echo base_url('site_assets/images/portfolio/4/'.$row['image']);}elseif($row['image']=='0'){echo base_url('site_assets/images/auth-bg.jpg');}?>">
                                <img style="width:100%;height:60px;" src="<?php if($row['image']!='0'){echo base_url('site_assets/images/portfolio/4/'.$row['image']);}elseif($row['image']=='0'){echo base_url('site_assets/images/auth-bg.jpg');}?>" alt="No Attach">
                              </a>
                            </td>
                            <td>
                              <input type="date" name="items[<?php echo $i?>][date]" class="form-control" value="<?php echo $row['date']?>" id="items-<?php echo $i?>-date"/>
                            </td>
                            <td>
                              <input type="time" name="items[<?php echo $i?>][time]" class="form-control" value="<?php echo $row['time']?>" id="items-<?php echo $i?>-time"/>
                            </td>
                            <td>
                              <input type="text" name="items[<?php echo $i?>][location]" class="form-control" value="<?php echo $row['location']?>" id="items-<?php echo $i?>-location"/>
                            </td>
                            <td>
                              <input type="number" name="items[<?php echo $i?>][status]" class="form-control" value="<?php echo $row['status']?>" required id="items-<?php echo $i?>-status"/>
                            </td>
                            <td>
                              <input type="number" name="items[<?php echo $i?>][rank]" class="form-control" value="<?php echo $row['rank']?>" required id="items-<?php echo $i?>-rank"/>
                            </td>
                            <td class="text-center">
                              <button type="button" name="remove" onclick="removeRow('admin/site_manager/delete_package','<?php echo $i?>','<?php echo $row['id']?>')" class="btn btn-light btn-sm btn-circle btn_remove">
                                <i class="fas fa-trash-alt text-danger"></i>
                              </button>
                            </td>
                          </tr>
                        <?php $i++;} }?>  
                      </tbody>
                    </table>
                    <input class="form-control " type="hidden" id="allstoredIds"  value="<?php echo (isset($packages)) ? $ids :''?>">
                  </div>
                  <br>
                </div>
              </div>  
            </div> 
          </div>
        </div>
      </div>
    </div> 
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <input type="submit" name="submit" class="btn btn-dark btn-lg" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div> 
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>       
<?php $this->load->view('admin/site_manager/site_manager.js.php');?> 