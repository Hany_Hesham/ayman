 <link href="<?php echo base_url('assets/css/summernote-bs4.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/summernote.css')?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/summernote.js')?>"></script>
<script src="<?php echo base_url('assets/js/summernote-bs4.js')?>"></script>
<div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                         <h5 class="page-title">Clients Requests Manager</h5>      
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item">Clients Requests Manager</li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body"><br>  
                         <table id="client_emails-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th width="5%"><strong>#</strong></th>
                                  <th width="12%"><strong>Client Name</strong></th>
                                  <th width="12%"><strong>Email</strong></th>
                                  <th width="20%"><strong>Subject</strong></th>
                                  <th width="35%"><strong>Message</strong></th>
                                  <th width="12%"><strong>Status</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="modal fade" id="mailermodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <?php echo form_open(base_url('admin/site_manager/client_mailer'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
              <div class="modal-header badge-dark">
                  <h5 class="modal-title" id="smallmodalLabel">New Mail</h5>
                  <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <label class="text-right control-label col-form-label" style="font-size:12px;">*Email</label>
                  <input id="client_id" type="hidden" name="client_id" class="form-control" required>
                  <input id="email" type="text" name="email" class="form-control " placeholder="Email" required readonly>
                  <label class="text-right control-label col-form-label" style="font-size:12px;">*subject</label>
                  <input  id="subject" type="text" name="subject" class="form-control " placeholder="Subject" required>
                  <label class="text-right control-label col-form-label" style="font-size:12px;">*email Body</label>
                  <textarea  class="form-control" name="message" id="modal-message" rows="4"></textarea>
                  <br>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <button id="mailer-modalSubmit" type="submit" type="submit" name="submit" class="btn btn-primary">Send</button>
              </div>
              <?php echo form_close( ); ?>
          </div>
      </div>
  </div>
  <div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document ">
            <div class="modal-content">
                <div class="modal-body">
                  <h5 class="text-center" id="loading-part">
                    <div class="" id="process-loader">
                       <img src="<?php echo base_url();?>assets/images/spinner.gif"
                           style = " width: 30%;height:30%;">
                    </div>
                    Please Wait
                  </h5>
                  <div id="success-part">
                    <h1 class="text-center">
                      <strong style="font-size:100px;color:#3EC1D5"><i class="fa fa-check-circle"></i></strong>
                    </h1>
                    <h6 class="text-center">Success</h6>
                  </div>
                </div>
            </div>
        </div>
    </div>

  <script type="text/javascript">
      // $(document).ready(function() {
      //  $('#summernote').summernote({
      //       tabsize:1,
      //       height: 50,
      //       width: 800
      //     });
      // });
      initDTable(
                 "client_emails-list-table",
                    [{"name":"id"},
                     {"name":"client_name"},
                     {"name":"email"},
                     {"name":"subject"},
                     {"name":"message"},
                     {"name":"mail_status"},],
                  "<?php echo base_url("admin/site_manager/clients_req_ajax/") ?>"
           );

      function get_mailerView(id,email) {
       $('#mailermodal').modal('show');
       $('#mailermodal .modal-body #modal-message,#mailermodal .modal-body #subject').val('');
       $('#mailermodal .modal-body #email').val(email);
       $('#mailermodal .modal-body #client_id').val(id);
     }

     $(function () {
      $('form').on('submit', function (e) { 
      $('#mailermodal').modal('hide');
      $('#success-modal').modal('show');
      $('#success-modal .modal-body #success-part').hide();
      $('#success-modal .modal-body #loading-part').show();
      let clientsEmailsTable = $('#client_emails-list-table').DataTable();
         e.preventDefault();      
         var formdata = $('form').serialize();
         $.ajax({
           type: 'post',
           url: '<?php echo base_url("admin/site_manager/client_mailer") ?>',
           data: {formdata:formdata},
           success: function(data) { 
             setTimeout(function(){
              $('#success-modal .modal-body #loading-part').hide();
              $('#success-modal .modal-body #success-part').show();
              }, 1000);
              clientsEmailsTable.ajax.reload();
               
               }
          });
      });
   });
      
    </script>