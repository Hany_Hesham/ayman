<?php foreach($departments as $row){ ?>
  <div class="card departmentsDivs">
    <div class="card-header">
      <h5 class="mb-0" style="color:#0f597a !important" id="departmentName<?php echo $row['id']?>">
        <strong><?php echo $row['type_name']?></strong>
      </h5>
    </div>
    <div class="card-body">
      <?php $types = $this->General_model->get_meta_keys($row['id']);
      foreach ($types as $type) { ?>
        <?php $signature_item = $this->signature_model->get_signature_items($signature_id, $type['id']);?>
        <div class="row" style="zoom:70%;">
          <div id="typeView<?php echo $type['id']?>" class="table-responsive m-t-40" style="clear: both;">
            <table id="Items-list-table<?php echo $type['id']?>" class="table table-hover">
              <thead style="background-color:#0f597a;color:#FFF">
                <tr id="headTable<?php echo $type['id']?>">
                  <th  style="width:150px !important;">
                    <strong>Type</strong>
                    <!-- <button type="button" name="remove" id="addRow" class="btn btn-light btn-sm btn-circle btn_remove" onclick="storeSelected(<?php echo $type['id']?>)">
                      <i class="mdi mdi-database-plus"></i>
                    </button> -->
                  </th>
                  <th>First Signature</th>
                  <th>Second Signature</th>
                  <th>Third Signature</th>
                  <th>Fourth Signature</th>
                  <th>Fifth Signature</th>
                  <th>Sixth Signature</th>
                  <th>Seventh Signature</th>
                  <th>Eighth Signature</th>
                  <th>Ninth Signature</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr id="typeNameView<?php echo $type['id']?>">   
                  <td width="10%" style="font-size: 12px"><strong><?php echo $type['type_name']?></strong></td>
                  <td width="10%"><span id="first<?php echo $signature_item['id']?>"><?php echo $signature_item['role_first'] ?></span></td>
                  <td width="10%"><span id="second<?php echo $signature_item['id']?>"><?php echo $signature_item['role_second'] ?></span></td>
                  <td width="10%"><span id="third<?php echo $signature_item['id']?>"><?php echo $signature_item['role_third'] ?></span></td>
                  <td width="10%"><span id="fourth<?php echo $signature_item['id']?>"><?php echo $signature_item['role_fourth'] ?></span></td>
                  <td width="10%"><span id="fifth<?php echo $signature_item['id']?>"><?php echo $signature_item['role_fifth'] ?></span></td>
                  <td width="10%"><span id="sixth<?php echo $signature_item['id']?>"><?php echo $signature_item['role_sixth'] ?></span></td>
                  <td width="10%"><span id="seventh<?php echo $signature_item['id']?>"><?php echo $signature_item['role_seventh'] ?></span></td>
                  <td width="10%"><span id="eighth<?php echo $signature_item['id']?>"><?php echo $signature_item['role_eighth'] ?></span></td>
                  <td width="10%"><span id="ninth<?php echo $signature_item['id']?>"><?php echo $signature_item['role_ninth'] ?></span></td>
                  <td width="10%"><button type="button" onclick="editRow(<?php echo $type['id']?>)" class="btn btn-light btn-sm btn-circle"><span class="text-info">&#9998;</span></button></th>
                </tr>
                <tr id="typeNameEdit<?php echo $type['id']?>" class="hidden">   
                  <td width="10%" style="font-size: 12px">
                    <strong><?php echo $type['type_name']?></strong>
                    <input type="hidden" name="items-<?php echo $type['id']?>-id" value="<?php echo $signature_item['id']?>"/>
                    <input type="hidden" name="items-<?php echo $type['id']?>-type_id" value="<?php echo $type['id']?>"/>
                    <input type="hidden" id="items-<?php echo $type['id']?>-type_name" name="items-<?php echo $type['id']?>-type_name" value="<?php echo $type['type_name']?>"/>
                  </td>
                  <td width="10%">
                    <select type="text" class="form-control" id="items-<?php echo $type['id']?>-first" name="items-<?php echo $type['id']?>-first" style="width:160px;" title="Select Type" required>
                      <option value=""></option>';
                      <?php foreach ($roles as $role) {?>
                        <option value="<?php echo $role['id']?>" <?php echo ($signature_item['first']==$role['id']?'selected':'');?>><?php echo $role['type_name']?></option>';
                      <?php }?>
                    </select>
                  </td>
                  <td width="10%">
                    <select type="text" class="form-control" id="items-<?php echo $type['id']?>-second" name="items-<?php echo $type['id']?>-second" style="width:160px;" title="Select Type" required>
                      <option value=""></option>';
                      <?php foreach ($roles as $role) {?>
                        <option value="<?php echo $role['id']?>" <?php echo ($signature_item['second']==$role['id']?'selected':'');?>><?php echo $role['type_name']?></option>';
                      <?php }?>
                    </select>
                  </td>
                  <td width="10%">
                    <select type="text" class="form-control" id="items-<?php echo $type['id']?>-third" name="items-<?php echo $type['id']?>-third" style="width:160px;" title="Select Type" required>
                      <option value=""></option>';
                      <?php foreach ($roles as $role) {?>
                        <option value="<?php echo $role['id']?>" <?php echo ($signature_item['third']==$role['id']?'selected':'');?>><?php echo $role['type_name']?></option>';
                      <?php }?>
                    </select>
                  </td>
                  <td width="10%">
                    <select type="text" class="form-control" id="items-<?php echo $type['id']?>-fourth" name="items-<?php echo $type['id']?>-fourth" style="width:160px;" title="Select Type" required>
                      <option value=""></option>';
                      <?php foreach ($roles as $role) {?>
                        <option value="<?php echo $role['id']?>" <?php echo ($signature_item['fourth']==$role['id']?'selected':'');?>><?php echo $role['type_name']?></option>';
                      <?php }?>
                    </select>
                  </td>
                  <td width="10%">
                    <select type="text" class="form-control" id="items-<?php echo $type['id']?>-fifth" name="items-<?php echo $type['id']?>-fifth" style="width:160px;" title="Select Type" required>
                      <option value=""></option>';
                      <?php foreach ($roles as $role) {?>
                        <option value="<?php echo $role['id']?>" <?php echo ($signature_item['fifth']==$role['id']?'selected':'');?>><?php echo $role['type_name']?></option>';
                      <?php }?>
                    </select>
                  </td>
                  <td width="10%">
                    <select type="text" class="form-control" id="items-<?php echo $type['id']?>-sixth" name="items-<?php echo $type['id']?>-sixth" style="width:160px;" title="Select Type" required>
                      <option value=""></option>';
                      <?php foreach ($roles as $role) {?>
                        <option value="<?php echo $role['id']?>" <?php echo ($signature_item['sixth']==$role['id']?'selected':'');?>><?php echo $role['type_name']?></option>';
                      <?php }?>
                    </select>
                  </td>
                  <td width="10%">
                    <select type="text" class="form-control" id="items-<?php echo $type['id']?>-seventh" name="items-<?php echo $type['id']?>-seventh" style="width:160px;" title="Select Type" required>
                      <option value=""></option>';
                      <?php foreach ($roles as $role) {?>
                        <option value="<?php echo $role['id']?>" <?php echo ($signature_item['seventh']==$role['id']?'selected':'');?>><?php echo $role['type_name']?></option>';
                      <?php }?>
                    </select>
                  </td>
                  <td width="10%">
                    <select type="text" class="form-control" id="items-<?php echo $type['id']?>-eighth" name="items-<?php echo $type['id']?>-eighth" style="width:160px;" title="Select Type" required>
                      <option value=""></option>';
                      <?php foreach ($roles as $role) {?>
                        <option value="<?php echo $role['id']?>" <?php echo ($signature_item['eighth']==$role['id']?'selected':'');?>><?php echo $role['type_name']?></option>';
                      <?php }?>
                    </select>
                  </td>
                  <td width="10%">
                    <select type="text" class="form-control" id="items-<?php echo $type['id']?>-ninth" name="items-<?php echo $type['id']?>-ninth" style="width:160px;" title="Select Type" required>
                      <option value=""></option>';
                      <?php foreach ($roles as $role) {?>
                        <option value="<?php echo $role['id']?>" <?php echo ($signature_item['ninth']==$role['id']?'selected':'');?>><?php echo $role['type_name']?></option>';
                      <?php }?>
                    </select>
                  </td>
                  <td width="10%">
                    <button type="button" onclick="editSubmitRow(<?php echo $signature_item['id']?>,<?php echo $type['id']?>)" class="btn btn-light btn-sm btn-circle"><span class="text-success">&#10004;</span></button>
                    <button type="button" onclick="cancelRow(<?php echo $type['id']?>)" class="btn btn-light btn-sm btn-circle"><span class="text-danger">&#10007;</span></button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      <?php }?>
    </div>
  </div>
<?php }?>