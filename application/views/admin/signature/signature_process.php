<?php 
  initiate_breadcrumb('Add Signature Roles',
    '',//form_id
    //$this->data['module']['id'],//module_id
    //$this->data['log_permission']['view'],
    //$this->data['email_permission']['view'],
    '',
    '',
    '',
    'empty',//options
    array(array('name'=> 'Signature Roles','url'=> 'admin/signature'))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($signature)) {
      echo form_open(base_url('admin/signature/edit/'.$signature['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
    }else{
      echo form_open(base_url('admin/signature/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label for="module" class="text-right control-label col-form-label" style="font-size:12px;">*Modules</label>
                <select id="module_id" name="module_id" class="selectpicker form-control"  data-container="body" data-live-search="true" title="Select Module" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($modules as $module){?>
                    <?php if (isset($signature)): ?>
                      <option value="<?php echo $module['id']?>" <?php if (isset($signature)){echo ($signature['module_id']==$module['id']?'selected':'');}?>>
                        <?php echo $module['name']?>
                      </option>
                    <?php endif; ?>
                  <?php }?>
                </select>
              </div> 
              <div class="col-sm-4">
                <label for="hotel" class="text-right control-label col-form-label" style="font-size:12px;">*Hotels</label>
                <select id="hotel_id" name="hotel_id" class="selectpicker form-control"  data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php if (isset($signature)){echo ($signature['hotel_id']==$hotel['id']?'selected':'');}?>>
                      <?php echo $hotel['hotel_name']?>
                    </option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label for="hCondition" class="text-right control-label col-form-label" style="font-size:12px;">*Hotel Condition</label>
                <select id="hotel_condition" name="hotel_condition" class="selectpicker form-control"  data-container="body" data-live-search="true" title="Select Condition" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                    <option value="==" <?php if (isset($signature)){echo ($signature['hotel_condition']=='=='?'selected':'');}?>>==</option>
                    <option value="!=" <?php if (isset($signature)){echo ($signature['hotel_condition']=='!='?'selected':'');}?>>!=</option>
                    <option value=">=" <?php if (isset($signature)){echo ($signature['hotel_condition']=='>='?'selected':'');}?>>>=</option>
                    <option value="<=" <?php if (isset($signature)){echo ($signature['hotel_condition']=='<='?'selected':'');}?>><=</option>
                    <option value=">" <?php if (isset($signature)){echo ($signature['hotel_condition']=='>'?'selected':'');}?>>></option>
                    <option value="<" <?php if (isset($signature)){echo ($signature['hotel_condition']=='<'?'selected':'');}?>><</option>
                </select>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label for="department" class="text-right control-label col-form-label" style="font-size:12px;">*Departments</label>
                <select id="dep_code" name="dep_code" class="selectpicker form-control"  data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php if (isset($signature)){echo ($signature['dep_code']==$department['id']?'selected':'');}?>>
                      <?php echo $department['dep_name']?>
                    </option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label for="dCondition" class="text-right control-label col-form-label" style="font-size:12px;">*Department Condition</label>
                <select id="department_condition" name="department_condition" class="selectpicker form-control"  data-container="body" data-live-search="true" title="Select Condition" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                    <option value="==" <?php if (isset($signature)){echo ($signature['department_condition']=='=='?'selected':'');}?>>==</option>
                    <option value="!=" <?php if (isset($signature)){echo ($signature['department_condition']=='!='?'selected':'');}?>>!=</option>
                    <option value=">=" <?php if (isset($signature)){echo ($signature['department_condition']=='>='?'selected':'');}?>>>=</option>
                    <option value="<=" <?php if (isset($signature)){echo ($signature['department_condition']=='<='?'selected':'');}?>><=</option>
                    <option value=">" <?php if (isset($signature)){echo ($signature['department_condition']=='>'?'selected':'');}?>>></option>
                    <option value="<" <?php if (isset($signature)){echo ($signature['department_condition']=='<'?'selected':'');}?>><</option>
                </select>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label for="department" class="text-right control-label col-form-label" style="font-size:12px;">*Special Column</label>
                <select id="special" name="special" class="selectpicker form-control"  data-container="body" data-live-search="true" title="Select Special Column" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                </select>
              </div>
              <div class="col-sm-4">
                <label for="sCondition" class="text-right control-label col-form-label" style="font-size:12px;">*Special Condition</label>
                <select id="special_condition" name="special_condition" class="selectpicker form-control"  data-container="body" data-live-search="true" title="Select Condition" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                    <option value="==" <?php if (isset($signature)){echo ($signature['special_condition']=='=='?'selected':'');}?>>==</option>
                    <option value="!=" <?php if (isset($signature)){echo ($signature['special_condition']=='!='?'selected':'');}?>>!=</option>
                    <option value=">=" <?php if (isset($signature)){echo ($signature['special_condition']=='>='?'selected':'');}?>>>=</option>
                    <option value="<=" <?php if (isset($signature)){echo ($signature['special_condition']=='<='?'selected':'');}?>><=</option>
                    <option value=">" <?php if (isset($signature)){echo ($signature['special_condition']=='>'?'selected':'');}?>>></option>
                    <option value="<" <?php if (isset($signature)){echo ($signature['special_condition']=='<'?'selected':'');}?>><</option>
                </select>
              </div>
              <div class="col-sm-4">
                <label for="sValue" class="text-right control-label col-form-label" style="font-size:12px;">*Special Value</label>
                <input type="text" class="form-control" name="special_value" placeholder="Special Value" value="<?php if (isset($signature)){echo $signature['special_value'];}?>">
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label for="lValue" class="text-right control-label col-form-label" style="font-size:12px;">*Limitation Value</label>
                <input type="text" class="form-control" name="limitation" placeholder="Limitation Value" value="<?php if (isset($signature)){echo $signature['limitation'];}?>">
              </div>
              <div class="col-sm-4">
                <label for="lCondition" class="text-right control-label col-form-label" style="font-size:12px;">*Limitation Condition</label>
                <select id="limit_condition" name="limit_condition" class="selectpicker form-control"  data-container="body" data-live-search="true" title="Select Condition" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                    <option value="==" <?php if (isset($signature)){echo ($signature['limit_condition']=='=='?'selected':'');}?>>==</option>
                    <option value="!=" <?php if (isset($signature)){echo ($signature['limit_condition']=='!='?'selected':'');}?>>!=</option>
                    <option value=">=" <?php if (isset($signature)){echo ($signature['limit_condition']=='>='?'selected':'');}?>>>=</option>
                    <option value="<=" <?php if (isset($signature)){echo ($signature['limit_condition']=='<='?'selected':'');}?>><=</option>
                    <option value=">" <?php if (isset($signature)){echo ($signature['limit_condition']=='>'?'selected':'');}?>>></option>
                    <option value="<" <?php if (isset($signature)){echo ($signature['limit_condition']=='<'?'selected':'');}?>><</option>
                </select>
              </div>
            </div>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <div style="width:100%;">
                    <table id="Items-list-table" class="table table-hover">
                      <tr>
                        <button type="button" name="add" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="storeSelected()">
                          <i class="mdi mdi-database-plus"></i>
                        </button>
                      </tr>
                      <thead   style="background-color:#0f597a;color:#FFF">
                        <tr id="head">
                          <?php if (isset($signature)) {$i=1;$ids = '';foreach($signature_items as $row){ $ids= $i.','.$ids;?>
                            <th>
                              <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo $row['id']?>)" class="btn btn-light btn-sm btn-circle btn_remove">
                                <i class="fas fa-trash-alt text-danger"></i>
                              </button>
                            </th>
                          <?php } }?>  
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="rowIds" id="row">          
                          <?php if (isset($signature)) {$i=1;$ids = '';foreach($signature_items as $row){ $ids= $i.','.$ids;?>
                            <td id="role<?php echo $i?>">
                              <select type="text" class="form-control" name="items[<?php echo $i?>][role]" title="Select Role" required>
                                <option value=""></option>
                                <?php foreach ($roles as $role): ?>
                                  <option value="<?php echo $role['id']?>" <?php echo ($row['role']==$role['id'])?'selected':'';?>><?php echo $role['name']?></option>
                                <?php endforeach; ?>
                              </select>
                            </td>                   
                          <?php $i++;} }?>  
                        </tr>
                        <tr class="hidden">
                          <td class="hidden" id="all-items-ids"></td>
                        </tr>
                      </tbody>
                    </table>
                    <input class="form-control " type="hidden" id="allstoredIds"  value="<?php echo (isset($signature)) ? $ids :''?>">
                  </div>
                  <br>
                </div>
              </div>  
            </div> 
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('signature',$uploads,$this->data['module']['id'],$gen_id,'signature','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <input type="submit" name="submit" class="btn btn-cyan btn-lg" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>        
<?php $this->load->view('admin/signature/signature.js.php');?>