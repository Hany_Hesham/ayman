     <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                        <?php if($user['fullname']){ 
                              echo '<h4 class="page-title">'.$user['fullname'].'</h4>';
                            }else{
                              echo '<h4 class="page-title">New User</h4>';
                            }
                          ?>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">الرئيسية </a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url('admin/users');?>">قائمة الأعضاء </a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $user['fullname'] ?></li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                     <div class="card-body">
                        <div class="custom-tab">
                            <nav dir="rtl">
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="custom-nav-home-tab" data-toggle="tab" href="#custom-nav-home" role="tab" aria-controls="custom-nav-home" aria-selected="true">الملف الشخصي </a>
                                    <a class="nav-item nav-link" id="custom-nav-profile-tab" data-toggle="tab" href="#custom-nav-profile" role="tab" aria-controls="custom-nav-profile" aria-selected="false">الصلاحيات </a>
                                </div>
                            </nav>
                            <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                              
                                <div class="tab-pane fade show active" id="custom-nav-home" role="tabpanel" aria-labelledby="custom-nav-home-tab">
                                 <?php echo form_open(base_url('admin/users/add'), 'class="form-horizontal"');  ?> 
                                      <div class="card-body card-block">
                                        <input type="hidden" class="form-control" name="id" value="<?php echo $user['id']?>" />
                                        <div class="form-group row">
                                          <div class="col-sm-3"></div>
                                           <div class="col-sm-6">
                                                <input type="text" id="username" name="username" placeholder="Enter your username" id="inputWarning2i" class="form-control-warning form-control" value="<?php echo $user['username']?>" dir="rtl" required>
                                           </div>
                                          <label for="username" class=" col-sm-3 text-left control-label col-form-label">الأسم الأول </label>
                                        </div>
                                        <div class="form-group row">
                                          <div class="col-sm-3"></div>
                                          <div class="col-sm-6">
                                              <input type="text" id="fullname" name="fullname" placeholder="Enter your fullname" class="form-control" value="<?php echo $user['fullname']?>" dir="rtl" required>
                                           </div>
                                          <label for="fullname" class=" col-sm-3 text-left control-label col-form-label">الأسم الأخير </label>
                                        </div>
                                        <div class="form-group row">
                                          <div class="col-sm-3"></div>
                                          <div class="col-sm-6">
                                              <input type="text" id="email" name="email" placeholder="Enter email" class="form-control" value="<?php echo $user['email']?>" dir="rtl" required>
                                          </div> 
                                          <label for="email" class=" col-sm-3 text-left control-label col-form-label">البريد الإلكتروني  </label>
                                        </div>
                                         <div class="form-group row">
                                          <div class="col-sm-3"></div>
                                           <div class="col-sm-6">
                                            <select id="branch_id" name="branches[]" multiple class="selectpicker form-control" id="number-multiple" data-container="body" data-live-search="true" title="إختر " data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false"  style="height: 100% !important;width:100%;" required>
                                                   <?php foreach($branches as $branch){?>
                                                    <option value="<?php echo $branch['id']?>" <?php echo (array_assoc_value_exists($selected_branches, 'branch_id', $branch['id']))? 'selected="selected"' : set_select('branch[]',$branch['id'] ); ?>><?php echo $branch['branch_name']?></option>
                                                  <?php }?>
                                              </select>
                                           </div>   
                                          <label for="hotel" class=" col-sm-3 text-left control-label col-form-label">الفرع  </label>
                                        </div>
                                        <div class="form-group row">
                                          <div class="col-sm-3"></div>
                                          <div class="col-sm-6">
                                             <select id="role_id" name="role_id" class="selectpicker form-control custom-select" style="height: 100% !important;width:100%;" data-live-search="true"  title="إختر " required>
                                                   <?php foreach($roles as $role){?>
                                                    <option value="<?php echo $role['id']?>" <?php if($user['role_id'] == $role['id']){echo'selected';}?>><?php echo $role['name']?></option>
                                                  <?php }?>
                                                </select>
                                          </div>
                                         <label for="role" class=" col-sm-3 text-left control-label col-form-label">الجروب  </label>
                                        </div>
                                        <div class="form-group row">
                                          <div class="col-sm-3"></div>
                                          <div class="col-sm-6">
                                             <select id="department_id" name="department_id" class="selectpicker form-control custom-select" style="height: 100% !important;width:100%;" data-live-search="true"  title="إختر " required>
                                                   <?php foreach($departments as $department){?>
                                                    <option value="<?php echo $department['id']?>" <?php if($user['department'] == $department['id']){echo'selected';}?>><?php echo $department['dep_name']?></option>
                                                  <?php }?>
                                                </select>
                                          </div>
                                          <label for="role" class=" col-sm-3 text-left control-label col-form-label">القسم </label>
                                        </div>
                                        <div class="row form-group row">
                                          <div class="col-sm-3"></div>
                                              <div class="col-sm-6">
                                                  <input type="password" id="password" name="password" placeholder="Enter password" class=" form-control" dir="rtl">
                                              </div>
                                              <label for="password" class=" col-sm-3 text-left control-label col-form-label">الرقم السري  </label>
                                        </div>                        
                                        </div>
                                      <button type="submit" name="submit" class="btn btn-primary btn-lg" style="float:right;margin-top:5%;">
                                              <i class="fa fa-dot-circle-o"></i> حفظ البيانات
                                     </button>
                                    <?php echo form_close( ); ?>
                                  </div>
                               
                                <div class="tab-pane fade" id="custom-nav-profile" role="tabpanel" aria-labelledby="custom-nav-profile-tab">
                                   <?php $this->load->view('admin/users/permission_tab')?>
                                </div>
                            </div>
                        </div>
                     </div>
                  </div>
                </div>
             </div>
         </div>   
<script type="text/javascript">
initDTable_normal('permission-list-table','1',
                   [
                        {"mod":"mod"},
                        {"mod":"mod"},
                        {"mod":"mod"},
                        {"mod":"mod"},
                        {"mod":"mod"},
                       ]
          );

</script>