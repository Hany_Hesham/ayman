<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="latest_posts style2" style="background-color: #FFF; box-shadow: 5px 5px 9px #ebebeb;border-radius: 15px;">
            <!-- Title -->
            <h3 class="m_title">
               <?php echo $category['header'] ?>
            </h3>
            <table id="category-<?php echo $category['id'] ?>" class="searchable table table-borderless table-responsive-lg table-responsive-sm table-responsive-md table-responsive-xl " style="width:100%">
                <thead>
                    <tr>
                        <th width="0%" style="display: none !important;"></th>    
                        <th width="40%"></th>
                        <th width="10%"></th>
                        <th width="20%"></th>
                        <th width="10%"></th>
                        <th width="10%"></th>
                        <th width="20%"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $i = 0;
                        $icons = [
                            '<i style="color:#C9B037;" class="fas fa-trophy"></i>',
                            '<i style="color:#D7D7D7;" class="fab fa-empire"></i>',
                            '<i style="color:#AD8A56;" class="fas fa-shield-alt"></i>',
                            '<i style="color:" class="fas fa-handshake text-secondary"></i>',
                        ];
                        $alphaRank = [
                            'Congratulations! 1-st Place',
                            'Congratulations! 2-nd Place',
                            'Congratulations! 3-rd Place',
                            'Thank you for participating',
                        ];
                        foreach($projects as $project){
                            if($i == 0){
                                $project['icon'] = $icons[0];
                                $project['competitor_rank'] = $alphaRank[0];
                            }elseif($i == 1){
                                $project['icon'] = $icons[1];
                                $project['competitor_rank'] = $alphaRank[1];
                            }elseif($i == 2){
                                $project['icon'] = $icons[2];
                                $project['competitor_rank'] = $alphaRank[2];
                            }else{
                                $project['icon'] = $icons[3];
                                $project['competitor_rank'] = $alphaRank[3];
                            }
                            
                    ?>
                    <tr>
                        <td style="display: none !important;"><?php echo $project['total_score']?></td>
                        <td>
                            <div class="kb-search--post">
                                <!-- Title -->
                                    <h2 class="tbk__title" style="font-size: 18px !important;">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <strong style="font-size: 14px;"><?php echo $project['first_name'].' '.$project['middle_name'] ?></strong>
                                            </div>
                                        </div>
                                    </h2>
                                <!-- Post information -->
                            </div>
                        </td>
                        <td>
                            <strong style="font-size: 14px;"><?php echo $project['school']?></strong>
                        </td>
                        <td>
                            <?php echo $project['total_score'] ?>
                        </td>
                        <td>
                            <p style="font-size: 12px;">
                                <strong><?php echo $project['competitor_rank']?></strong>
                            </p>
                        </td>
                        <td>
                            <h2><?php echo $project['icon']?></h2>
                        </td>
                        <td>
                            <a  class="btn btn-fullcolor " style="background-color: #cd2122 !important;color:#FFF;"
                                href="<?php echo base_url('clients/home/project/'.$project['code'])?>">
                                <?php echo translate('Project', $this->data['language']) ?>
                            </a>
                        </td>
                    </tr>
                    <?php $i++;
                     }?>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
		$(`#category-${<?php echo $category['id'] ?>}`).DataTable({
            "order": [[ 0, "desc" ]],
            "ordering": false
        });
	} );
</script>