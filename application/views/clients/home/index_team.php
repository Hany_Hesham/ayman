<section class="section-50 section-sm-90 section-lg-120 bg-gray-lighter">
      <div class="shell">
        <div class="range">
          <div class="cell-xs-12 text-center">
            <h3><?php echo translate('Meet Our Team', $this->data['language']) ?></h3>
            <p><?php echo translate('We are a team of developers, designers and leads who love what they do.', $this->data['language']) ?></p>
          </div>
        </div>
        <div class="range offset-top-40">
          <?php foreach($teams as $team){?>
            <div class="cell-xs-6 cell-sm-4 cell-md-3 offset-top-60 offset-xs-top-0" style="padding-top:20px;">
              <div class="thumbnail thumbnail-variant-1">
                <div class="thumbnail-image"><img src="<?php echo base_url('site_assets/images/team/'.$team['img'])?>" alt="" width="189" height="189" style="height:250px;width:250px;"/>
                  <div class="thumbnail-image-inner"><a href="callto:#" class="icon icon-md material-icons-local_phone link-primary-inverse-v2"></a><a href="/cdn-cgi/l/email-protection#3f1c" class="icon icon-md-smaller fa-envelope-o link-white"></a></div>
                </div>
                <div class="thumbnail-caption">
                  <h5 class="header"><a href="team-member-profile.html"><?php echo translate($team['header'], $this->data['language']) ?></a></h5>
                  <p><?php echo translate($team['title'], $this->data['language']) ?></p>
                </div>
              </div>
            </div>
          <?php }?>   
        </div>
      </div>
    </section>