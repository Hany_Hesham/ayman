<?php $this->load->view($this->data['inner_slider']);?>
<section class="hg_section pt-80 pb-50">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="kl-title-block clearfix text-left tbk-symbol-- tbk-icon-pos--after-title">
          <!-- Title with custom font, size and weight -->
          <h3 class="tbk__title kl-font-alt fs-xl fw-bold">
            <?php echo translate($what_is_readysteadyshare['header'], $this->data['language']) ?>
          </h3>
          <!--/ Title -->

          <!-- Sub-Title with custom size and weight -->
          <h4 class="tbk__subtitle fs-s fw-vthin">
            <?php echo translate($what_is_readysteadyshare['paragraph'], $this->data['language']) ?>
          </h4>
          <!--/ Sub-Title -->
        </div>

        <!-- separator -->
        <div class="hg_separator clearfix mb-60">
        </div>
        <div class="kl-title-block clearfix text-left tbk-symbol-- tbk-icon-pos--after-title">
          <!-- Title with custom font, size and weight -->
          <h3 class="tbk__title kl-font-alt fs-xl fw-bold">
            <?php echo translate($about_us['header'], $this->data['language']) ?>
          </h3>
          <!--/ Title -->

          <!-- Sub-Title with custom size and weight -->
          <h4 class="tbk__subtitle fs-s fw-vthin">
            <?php echo translate($about_us['paragraph'], $this->data['language']) ?>
          </h4>
          <!--/ Sub-Title -->
        </div>

        <!-- separator -->
        <div class="hg_separator clearfix mb-60">
        </div>
        <div class="kl-title-block clearfix text-left tbk-symbol-- tbk-icon-pos--after-title">
          <!-- Title with custom font, size and weight -->
          <h3 class="tbk__title kl-font-alt fs-xl fw-bold">
            <?php echo translate($about_us_competition_description['header'], $this->data['language']) ?>
          </h3>
          <!--/ Title -->

          <!-- Sub-Title with custom size and weight -->
          <h4 class="tbk__subtitle fs-s fw-vthin">
            <?php echo translate($about_us_competition_description['paragraph'], $this->data['language']) ?>
          </h4>
          <!--/ Sub-Title -->
        </div>
        <!--/ separator -->
      </div>
      <!--/ col-sm-12 col-md-12 -->

      <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="row">
      
          <div class="col-sm-12 col-md-6 col-lg-6">
            <!-- Icon box float left -->
            <div class="kl-iconbox kl-iconbox--align-left text-left kl-iconbox--theme-default">
              <div class="kl-iconbox__inner">
                <div class="kl-iconbox__icon-wrapper">
                  <!-- Icon -->
                  <img class="kl-iconbox__icon" src="<?php echo base_url('site_assets/images/business-set1-02.svg')?>" alt="GRAPHIC DESIGN">
                  <!--/ Icon -->
                </div>
                <!--/ .kl-iconbox__icon-wrapper -->

                <!-- content -->
                <div class="kl-iconbox__content-wrapper">
                  <!-- Title -->
                  <div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
                    <h3 class="kl-iconbox__title">
                      <?php echo translate($about_us_design['header'], $this->data['language']) ?>
                    </h3>
                  </div>
                  <!--/ Title -->

                  <!-- Description -->
                  <div class="kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
                    <p class="kl-iconbox__desc">
                      <?php echo translate($about_us_design['paragraph'], $this->data['language']) ?>
                    </p>
                  </div>
                  <!--/ Description -->
                </div>
                <!--/ .kl-iconbox__content-wrapper -->
              </div>
              <!--/ kl-iconbox__inner -->
            </div>
            <!--/ Icon box float left -->
          </div>
          <!--/ col-sm-12 col-md-6 col-lg-6 -->
        </div>
        <!--/ row -->

        <div class="row">
          <div class="col-sm-12 col-md-6 col-lg-6">
            <!-- Icon box float left -->
            <div class="kl-iconbox kl-iconbox--align-left text-left kl-iconbox--theme-default ">
              <div class="kl-iconbox__inner">
                <div class="kl-iconbox__icon-wrapper ">
                  <!-- Icon -->
                  <img class="kl-iconbox__icon" src="<?php echo base_url('site_assets/images/ib-ico-4.svg')?>" alt="SEO SERVICES">
                  <!--/ Icon -->
                </div>
                <!--/ .kl-iconbox__icon-wrapper -->

                <!-- content -->
                <div class="kl-iconbox__content-wrapper">
                  <!-- Title -->
                  <div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
                    <h3 class="kl-iconbox__title">
                      <?php echo translate($about_us_coding['header'], $this->data['language']) ?>                      </h3>
                  </div>
                  <!--/ Title -->

                  <!-- Description -->
                  <div class="kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
                    <p class="kl-iconbox__desc">
                    <?php echo translate($about_us_coding['paragraph'], $this->data['language']) ?>
                    </p>
                  </div>
                  <!--/ Description -->
                </div>
                <!--/ .kl-iconbox__content-wrapper -->
              </div>
              <!--/ .kl-iconbox__inner -->
            </div>
            <!--/ Icon box float left -->
          </div>
          <!--/ col-sm-12 col-md-6 col-lg-6 -->

          <div class="col-sm-12 col-md-6 col-lg-6">
            <!-- Icon box float left -->
            <div class="kl-iconbox kl-iconbox--align-left text-left kl-iconbox--theme-default ">
              <div class="kl-iconbox__inner">
                <div class="kl-iconbox__icon-wrapper">
                  <!-- Icon -->
                  <img class="kl-iconbox__icon" src="<?php echo base_url('site_assets/images/business-set1-04.svg')?>" alt="MARKETING">
                  <!--/ Icon -->
                </div>
                <!--/ .kl-iconbox__icon-wrapper -->

                <!-- content -->
                <div class="kl-iconbox__content-wrapper">
                  <!-- Title -->
                  <div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
                    <h3 class="kl-iconbox__title">
                      <?php echo translate($about_us_voting['header'], $this->data['language']) ?> 
                    </h3>
                    </h3>
                  </div>
                  <!--/ Title -->

                  <!-- Description -->
                  <div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
                    <p class="kl-iconbox__desc">
                      <?php echo translate($about_us_voting['paragraph'], $this->data['language']) ?> 
                    </p>
                  </div>
                  <!--/ Description -->
                </div>
                <!--/ .kl-iconbox__content-wrapper -->
              </div>
              <!--/ .kl-iconbox__inner -->
            </div>
            <!--/ Icon box float left -->
          </div>
          <!--/ col-sm-12 col-md-6 col-lg-6 -->
        </div>
        <!--/ row -->
      </div>
      <!--/ col-sm-12 col-md-12 col-lg-6 -->

      <div class="col-sm-12 col-md-12 col-lg-6">
        <!-- spacer -->
        <div class="th-spacer clearfix" style="height: 60px;">
        </div>
        <!--/ spacer -->

        <!-- Skills Diagram -->
        <div id="skills_diagram_el" class="kl-skills-diagram">
          <div class="kl-skills-legend legend-topright">
            <h4>
            <?php echo translate('LEGEND', $this->data['language']) ?>
            </h4>

            <!-- Skills -->
            <ul class="kl-skills-list">
              <li data-percent="70" style="background-color:#8dc9e8;"><?php echo translate('DIVERSITY', $this->data['language']) ?></li>
              <li data-percent="80" style="background-color:#bdc3c7;"><?php echo translate('PASSION', $this->data['language']) ?> </li>
              <li data-percent="90" style="background-color:#97be0d;"><?php echo translate('INNOVATION', $this->data['language']) ?></li>
            </ul>
          </div>
          <!--/ .kl-skills-legend .legend-topright -->

          <!-- Skills Diagram wrapper -->
          <div class="skills-responsive-diagram">
            <div id="thediagram" class="kl-diagram" data-width="600" data-height="600" data-maincolor="#193340" data-maintext="Skills" data-fontsize="20px Arial" data-textcolor="#ffffff"></div>
          </div>
          <!--/ .skills-responsive-diagram -->
        </div>
        <!-- Required scripts for Skills Diagram -->
        <script type="text/javascript" src="<?php echo base_url('site_assets/js/plugins/raphael_diagram/raphael-min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('site_assets/js/plugins/raphael_diagram/init.js')?>"></script>
        <!--/ Skills Diagram -->
      </div>
      <!--/ col-sm-12 col-md-12 col-lg-6 -->
    </div>
    <!--/ row -->
  </div>
  <!--/ container -->
</section>

<section class="hg_section bg-white pt-80 pb-100">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-md-12">

        <!-- Recent Work carousel 1 style 2 element -->
        <div class="recentwork_carousel recentwork_carousel--2">
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-4">
              <!-- Left side -->
              <div class="recentwork_carousel__left mb-md-50">
                <!-- Title -->
                <h3 class="recentwork_carousel__title m_title">
                <?php echo translate('Partners', $this->data['language']) ?>
                </h3>
                <!--/ Title -->

                <!-- Slick navigation -->
                <div class="hgSlickNav recentwork_carousel__controls"></div>
              </div>
              <!--/ Left side - .recentwork_carousel__left -->
            </div>
            <!--/ col-sm-12 col-md-12 col-lg-4 -->

            <div class="col-sm-12 col-md-12 col-lg-8">
              <!-- Recent Work - carousel wrapper -->
              <div class="recentwork_carousel__crsl-wrapper">
                <div class="recent_works1 recentwork_carousel__crsl js-slick" data-slick='{
                  "infinite": true,
                  "slidesToShow": 3,
                  "slidesToScroll": 1,
                  "swipeToSlide": true,
                  "autoplay": true,
                  "autoplaySpeed": 5000,
                  "speed": 500,
                  "cssEase": "ease-in-out",
                  "appendArrows": ".recentwork_carousel--2 .hgSlickNav",		
                  "arrows": true,
                  "responsive": [
                    {
                      "breakpoint": 1199,
                      "settings": {
                        "slidesToShow": 2
                      }
                    },
                    {
                      "breakpoint": 767,
                      "settings": {
                        "slidesToShow": 2
                      }
                    },
                    {
                      "breakpoint": 480,
                      "settings": {
                        "slidesToShow": 1
                      }
                    }
                  ]
                }'>

                  <?php foreach($partners as $partner){ ?>
                    <div class="recent-work_carousel-item">
                      <!-- Portfolio link container -->
                      <a href="javascript:void(0)" class="recentwork_carousel__link">
                        <!-- Hover container -->
                        <div class="hover recentwork_carousel__hover">
                          <!-- Item image with custom height -->
                          <div class="carousel-item--height300">
                            <img src="<?php echo base_url('site_assets/uploads/partners/'.$partner['img'])?>" class="recentwork_carousel__img cover-fit-img" alt="" title="" />
                          </div>
                          <!--/ Item image with custom height -->

                          <!-- Hover shadow overlay -->
                          <span class="hov recentwork_carousel__hov"></span>
                          <!--/ Hover shadow overlay -->
                        </div>
                        <!--/ Hover container -->

                        <!-- Content details -->
                        <div class="details recentwork_carousel__details">
                          <!-- Title -->
                          <h4 class="recentwork_carousel__crsl-title">
                          <?php echo translate(strtoupper($partner['title']), $this->data['language']) ?>
                          </h4>
                          <!--/ Title -->
                        </div>
                        <!--/ Content details -->
                      </a>
                      <!--/ Portfolio link container -->
                    </div>
                  <?php }?>

                  <!--/ Item #1 -->
                </div>
                <!--/ .recent_works1 .recentwork_carousel__crsl .js-slick -->
              </div>
              <!--/  Recent Work - carousel wrapper - .recentwork_carousel__crsl-wrapper -->
            </div>
            <!--/ col-sm-12 col-md-12 col-lg-8 -->
          </div>
          <!--/ row -->
        </div>
        <!--/ Recent Work carousel 1 style 2 element -->
      </div>
      <!--/ col-sm-12 col-md-12 -->
    </div>
    <!--/ row -->
  </div>
  <!--/ container-fluid -->
</section>