  <main class="page-content">
    <section style="background-image: url(<?php echo base_url('site_assets/images/dived/1298266-top-freediving-wallpaper-1920x1080-for-ipad-pro.jpg');?>);" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
      <div class="shell">
        <div class="page-title">
          <h2> <?php echo translate('Gallery', $this->data['language']) ?></h2>
        </div>
      </div>
    </section>
    <section class="section-50 section-sm-90 section-md-bottom-120 section-lg-bottom-165">
      <div class="shell isotope-wrap text-center">
        <div class="range">
          <div class="cell-xs-12">
            <ul class="isotope-filters-responsive">
              <li>
                <p> <?php echo translate('Choose your category', $this->data['language']) ?>:</p>
              </li>
              <li class="block-top-level">
                <button data-custom-toggle="#isotope-1" data-custom-toggle-hide-on-blur="true" class="isotope-filters-toggle btn btn-sm btn-default"> <?php echo translate('Filter', $this->data['language']) ?><span class="caret"></span></button>
                <div id="isotope-1" class="isotope-filters isotope-filters-minimal isotope-filters-horizontal">
                  <ul class="list-inline">
                    <li><a data-isotope-filter="*" data-isotope-group="gallery" href="#" class="active"> <?php echo translate('All', $this->data['language']) ?></a></li>
                    <?php foreach($points as $point){?>
                      <li>
                        <a data-isotope-filter="<?php echo $point['points']?>" data-isotope-group="gallery" href="#"><?php echo translate(ucfirst( str_replace('_', ' ',$point['points'])), $this->data['language'])?>
                        </a
                      ></li>
                    <?php }?>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
          <div class="cell-xs-12 offset-top-40">
            <div class="row">
              <div data-isotope-layout="fitRows" data-isotope-group="gallery" data-photo-swipe-gallery="gallery" class="isotope isotope-gutter-default">
                <?php foreach($gallerys as $row){?>
                  <div data-filter="<?php echo $row['points']?>" class="col-xs-12 col-sm-6 col-md-4 isotope-item">
                    <div class="thumbnail thumbnail-variant-3"><a href="gallery-item.html" class="link link-external"><span class="icon icon-sm fa fa-link"></span></a>
                      <figure>
                         <img src="<?php echo base_url('site_assets/images/gallery/'.$row['img'])?>" alt="" width="370" height="278" style="height:280px;"/> 
                      </figure>
                      <div class="caption">
                        <a href="<?php echo base_url('site_assets/images/gallery/'.$row['img'])?>" data-photo-swipe-item="" data-size="1200x900" class="link link-original"></a>
                      </div>
                    </div>
                  </div>
                <?php }?>  
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
