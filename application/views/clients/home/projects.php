<?php $this->load->view($this->data['inner_slider']);?>
<section class="hg_section pt-80 pb-0">
	<?php 
	$categoryDevs = [];
	$categoryIds = [];
	foreach($age_categories as $category){
		array_push($categoryIds, $category['id']);
		array_push($categoryDevs, $category['paragraph']);
	?>

		<div id="<?php echo $category['paragraph']?>" class="categories-table-containers container">
	   </div><br>
	<?php }?>
</section>	
<script>
	let categoryIds = JSON.parse(`<?php echo json_encode($categoryIds)?>`);
	let categoryDevs = JSON.parse(`<?php echo json_encode($categoryDevs)?>`);
	for (let index = 0; index < categoryIds.length; index++) {
		const element = categoryIds[index];
		getViewAjax('clients/home','get_projects_by_category',categoryIds[index], categoryDevs[index]);
		
	}
	function addLike(project_id, container_id, category_id){
		$.ajax({
			type: 'post',
			url: '<?php echo base_url("clients/home/like_project/") ?>'+project_id+'/ajax',
			data: {},
			success: function(data) { 
				getViewAjax('clients/home','get_projects_by_category',category_id, container_id);
				// setTimeout(() => {
					// 	drawLikedProjects();
					// },1000)
				},
				error: function (jqXHR, textStatus, errorThrown) {
				} 
			});
			// btn-element btn btn-fullcolor btn-skewed' : 'btn-element btn btn-lined btn-skewed lined-gray
		}
		
	<?php if(isset($this->global_data['userData'])){?>
		drawLikedProjects();
		function drawLikedProjects(){
			let voterLikes = JSON.parse(`<?php echo isset($this->global_data['userData'])? json_encode($this->global_data['userData']['liked']) : [];?>`);
			let categoryDevs = JSON.parse(`<?php echo json_encode($categoryDevs)?>`);
			console.log(voterLikes);
			for (let categoryDev of categoryDevs) {
				for(let voterLike of voterLikes){
					if(voterLike.vote == 1){
					$(`#${categoryDev} #projectLike-${voterLike.project_id}`).css("color","#4c76b5");
					}
				}
			}
		}
	<?php }?>
</script>