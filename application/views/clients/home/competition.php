<?php $this->load->view($this->data['inner_slider']);?>

<section class="hg_section pt-80 pb-80">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="latest_posts style2" style="background-color: #FFF; box-shadow: 5px 5px 9px #ebebeb;border-radius: 15px;">
                    <!-- Title -->
                    <div class="circle-text-box style2">
                        <!-- Circle title wrapper -->
                        <div class="circle-headline">
                            <!-- Circle title -->
                            <span class="wpk-circle-span">
                                <span style="font-size: 14px;font-weight:bold;"> <?php echo translate('Yearly', $this->data['language']) ?> </span>
                            </span>
                            <!--/ Circle title -->

                            <!-- Title -->
                            <h4 class="wpk-circle-title">
                                <?php echo translate('Competitions', $this->data['language']) ?> 
                            </h4>
                            <!--/ Title -->
                        </div>
                    </div>
                    <table class="searchable table table-borderless table-responsive-lg table-responsive-sm table-responsive-md table-responsive-xl " style="width:100%">
                        <thead>
                            <tr>
                                <th width="0%" style="display: none !important;"></th>    
                                <th width="60%"></th>
                                <th width="40%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($yearly_competitions as $competition){?>
                            <tr>
                                <td style="display: none !important;"><?php echo $competition['rank']?></td>
                                <td>
                                    <div class="kb-search--post">
                                        <!-- Title -->
                                        <h2 class="tbk__title" style="font-size: 18px !important;">
                                            <?php echo $competition['title'] ?>
                                        </h2>
                                        <div class="portfolio-item-desc">
                                            <!-- Description wrapper -->
                                            <div class="portfolio-item-desc-inner">
                                                <p>
                                                    <?php echo translate($competition['description'], $this->data['language']) ?> 
                                                </p>
                                            </div>
                                            <a href="#" class="portfolio-item-more-toggle js-toggle-class" data-target=".portfolio-item-desc" data-target-class="is-opened" data-more-text="see more" data-less-text="show less">
                                                <span class="fas fa-chevron-down"></span>
                                            </a>
                                        </div>
                                        <!-- Post information -->
                                        <div class="post_details">
											<!-- Author -->
											<span class="itemAuthor">
                                              <?php echo translate('Started', $this->data['language']) ?> <a href="#"><strong><?php echo translate('At', $this->data['language']) ?></strong></a>
											</span>
											<span class="infSep"> / </span>
											<span class="itemDateCreated">
												<span class="far fa-calendar-alt"></span> 
												<?php echo $competition['start_date'] ?>
                                            </span>
                                            <span class="infSep"> , </span>
                                            <span class="itemAuthor">
                                              <?php echo translate('Ended', $this->data['language']) ?> <a href="#"><strong><?php echo translate('At', $this->data['language']) ?></strong></a>
											</span>
											<span class="infSep"> / </span>
											<span class="itemDateCreated">
												<span class="far fa-calendar-times"></span> 
												<?php echo $competition['end_date'] ?>
                                            </span>
											<!--/ Date created -->
										</div>
                                        <div class="row" style="padding: 0 0 7 7px;">
                                            <div class="col-lg-4 col-md-4 col-sm-6">
                                                <span class="fas fa-credit-card" style="color: #dc3545;"></span>
                                                <strong style="font-size: 18px;"><?php echo money_formater($competition['price'],$competition['currency']) ?></strong>
                                            </div>
                                        </div>	
                                        <?php if($competition['start_date'] <= date('Y-m-d') && $competition['end_date'] >= date('Y-m-d')) {?>
                                            <div class="pt-itemlinks itemLinks">
                                                <a  class="btn btn-fullcolor " style="background-color: #cd2122 !important;color:#FFF;"
                                                    href="<?php echo base_url('clients/home/competition_register/'.$competition['id'])?>">
                                                    Register Now
                                                </a>
                                            </div>
                                        <?php }?>
                                    </div>
                                </td>
                                <td>
                                    <div class="portfolio-item-right mfp-gallery images">
                                        <a href="<?php echo base_url('site_assets/uploads/competitions/'.$competition['image'])?>" class="hoverLink" data-lightbox="mfp" data-mfp="image" title="Progressively harness" >
                                            <span class="hoverBorderWrapper">
                                                <img src="<?php echo base_url('site_assets/uploads/competitions/'.$competition['image'])?>" 
                                                class="img-fluid" alt="" title="" />
                                                <span class="theHoverBorder"></span>
                                            </span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php }?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
       </div><br>
       <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="latest_posts style2" style="background-color: #FFF; box-shadow: 5px 5px 9px #ebebeb;border-radius: 15px;">
                    <!-- Title -->
                    <div class="circle-text-box style2">
                        <!-- Circle title wrapper -->
                        <div class="circle-headline">
                            <!-- Circle title -->
                            <span class="wpk-circle-span">
                                <span style="font-size: 14px;font-weight:bold;"> <?php echo translate('Weekly', $this->data['language']) ?> </span>
                            </span>
                            <!--/ Circle title -->

                            <!-- Title -->
                            <h4 class="wpk-circle-title">
                                <?php echo translate('Competitions', $this->data['language']) ?> 
                            </h4>
                            <!--/ Title -->
                        </div>
                    </div>
                    <table class="searchable table table-borderless table-responsive-lg table-responsive-sm table-responsive-md table-responsive-xl " style="width:100%">
                        <thead>
                            <tr>
                                <th width="0%" style="display: none !important;"></th>    
                                <th width="60%"></th>
                                <th width="40%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($weekly_competitions as $competition){?>
                            <tr>
                                <td style="display: none !important;"><?php echo $competition['rank']?></td>
                                <td>
                                    <div class="kb-search--post">
                                        <!-- Title -->
                                        <h2 class="tbk__title" style="font-size: 18px !important;">
                                            <?php echo $competition['title'] ?>
                                        </h2>
                                        <div class="portfolio-item-desc">
                                            <!-- Description wrapper -->
                                            <div class="portfolio-item-desc-inner">
                                                <p>
                                                    <?php echo translate($competition['description'], $this->data['language']) ?> 
                                                </p>
                                            </div>
                                            <a href="#" class="portfolio-item-more-toggle js-toggle-class" data-target=".portfolio-item-desc" data-target-class="is-opened" data-more-text="see more" data-less-text="show less">
                                                <span class="fas fa-chevron-down"></span>
                                            </a>
                                        </div>
                                        <!-- Post information -->
                                        <div class="post_details">
											<!-- Author -->
											<span class="itemAuthor">
                                              <?php echo translate('Started', $this->data['language']) ?> <a href="#"><strong><?php echo translate('At', $this->data['language']) ?></strong></a>
											</span>
											<span class="infSep"> / </span>
											<span class="itemDateCreated">
												<span class="far fa-calendar-alt"></span> 
												<?php echo $competition['start_date'] ?>
                                            </span>
                                            <span class="infSep"> , </span>
                                            <span class="itemAuthor">
                                              <?php echo translate('Ended', $this->data['language']) ?> <a href="#"><strong><?php echo translate('At', $this->data['language']) ?></strong></a>
											</span>
											<span class="infSep"> / </span>
											<span class="itemDateCreated">
												<span class="far fa-calendar-times"></span> 
												<?php echo $competition['end_date'] ?>
                                            </span>
											<!--/ Date created -->
										</div>
                                        <div class="row" style="padding: 0 0 7 7px;">
                                            <div class="col-lg-4 col-md-4 col-sm-6">
                                                <span class="fas fa-credit-card" style="color: #34CC99;"></span>
                                                <strong style="font-size: 18px;"><?php echo money_formater($competition['price'],$competition['currency']) ?></strong>
                                            </div>
                                        </div>	
                                        <?php if($competition['start_date'] <= date('Y-m-d') && $competition['end_date'] >= date('Y-m-d')) {?>
                                            <div class="pt-itemlinks itemLinks">
                                                <a  class="btn btn-fullcolor "
                                                    href="<?php echo base_url('clients/home/competition_register/'.$competition['id'])?>">
                                                    <?php echo translate('Register Now', $this->data['language']) ?>
                                                </a>
                                            </div>
                                        <?php }?>   
                                    </div>
                                </td>
                                <td>
                                    <div class="portfolio-item-right mfp-gallery images">
                                        <a href="<?php echo base_url('site_assets/uploads/competitions/'.$competition['image'])?>" class="hoverLink" data-lightbox="mfp" data-mfp="image" title="Progressively harness" >
                                            <span class="hoverBorderWrapper">
                                                <img src="<?php echo base_url('site_assets/uploads/competitions/'.$competition['image'])?>" 
                                                class="img-fluid" alt="" title="" />
                                                <span class="theHoverBorder"></span>
                                            </span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php }?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
       </div>
    </div>
    <!--/ container -->
</section>

<script>
    $(document).ready(function() {
		$(`.searchable`).DataTable({
            "order": [[ 0, "desc" ]]
        });
	} );
</script>