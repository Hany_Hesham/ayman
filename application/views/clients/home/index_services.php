<section data-on="false" data-md-on="true" style="background-image: url(<?php echo base_url('site_assets/images/dive/325459.jpg')?>);" class="rd-parallax bg-gray-base bg-image">
<div data-speed="0.33" data-type="media" data-url="<?php echo base_url('site_assets/images/dive/325459.jpg')?>" class="rd-parallax-layer"></div>
  <div data-speed="0" data-type="html" class="rd-parallax-layer">
    <div class="section-50 section-sm-75 section-lg-top-100 section-lg-bottom-120">
      <div class="shell">
        <div class="range">
          <?php foreach($services as $service){?>
            <div class="cell-sm-6 cell-md-4" style="padding-top:15px;">
              <article class="icon-box-vertical">
                <h5 class="icon-box-header"><?php echo translate($service['title'], $this->data['language']) ?></h5>
                <p><?php echo translate($service['paragraph'], $this->data['language']) ?></p>
              </article>
            </div>
          <?php }?>  
        </div>
      </div>
    </div>
  </div>
</section>
 <section class="section-60 section-sm-100 bg-primary">
    <div class="shell text-center text-md-center">
      <div class="range range-md-middle range-md-center">
        <div class="cell-md-12 cell-lg-12">
          <h3><?php echo translate('Welcome to Dive more Group ', $this->data['language']) ?></h3>
        </div>
      </div>
    </div>
  </section>