<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="latest_posts style2" style="background-color: #FFF; box-shadow: 5px 5px 9px #ebebeb;border-radius: 15px;">
            <!-- Title -->
            <h3 class="m_title">
               <?php echo $category['header'] ?>
            </h3>
            <table id="category-<?php echo $category['id'] ?>" class="searchable table table-borderless table-responsive-lg table-responsive-sm table-responsive-md table-responsive-xl " style="width:100%">
                <thead>
                    <tr>
                        <th width="0%" style="display: none !important;"></th>    
                        <th width="60%"></th>
                        <th width="40%"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($projects as $project){?>
                    <tr>
                        <td style="display: none !important;"><?php echo $project['votes']?></td>
                        <td>
                            <div class="kb-search--post">
                                <!-- Title -->
                                <a href="<?php echo base_url('clients/home/project/'.$project['code'])?>">
                                    <h2 class="tbk__title" style="font-size: 18px !important;">
                                        <?php echo $project['title'] ?>
                                    </h2>
                                    <div class="kb-search--entry">
                                            <p>
                                                <?php  $cut = strlen($project['description']) > 100 ?  100 : 70 ;
                                            echo substr_replace($project['description'], "...", $cut);?>
                                        </p>
                                    </div>
                                </a>
                                <!-- Post information -->
                                
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                        <i class="fas fa-flask"></i>
                                        <?php echo $project['competition_title'] ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                        <span class="far fa-list-alt"></span>
                                        <?php echo $project['first_name'].' '.$project['middle_name'] ?>
                                    </div>
                                </div>
                                <!-- <div class="row" style="padding: 0 0 7 7px;">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                        <span class="far fa-calendar-alt"></span>
                                        <span><//?php echo $project['started_at'] ?></span>
                                    </div>
                                </div>	 -->
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <a href="javascript:void(0)" 
                                        <?php echo isset($this->global_data['is_voter']) && $this->global_data['is_voter'] && $project['competition_active'] >= 1 
                                        && $project['vote_start_date'] <= date('Y-m-d') && $project['vote_end_date'] >= date('Y-m-d') ? '':'style="pointer-events: none"' ?>
                                            onclick="addLike(`<?php echo $project['id']?>`,
                                             `<?php echo $category['paragraph'] ?>`, 
                                             `<?php echo $category['id'] ?>`)">
                                            <span id="projectLike-<?php echo $project['id'] ?>" class="far fa-thumbs-up"></span>
                                            <?php echo $project['votes'] ?>
                                        </a>
                                        <span class="fas fa-eye" style="padding-left:5px;"></span>
                                        <?php echo $project['votes'] ?>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="portfolio-item-right mfp-gallery images">
                                <a href="<?php echo base_url('site_assets/uploads/projects/'.$project['image'])?>" class="hoverLink" data-lightbox="mfp" data-mfp="image" title="Progressively harness" >
                                    <span class="hoverBorderWrapper">
                                        <img src="<?php echo base_url('site_assets/uploads/projects/'.$project['image'])?>" 
                                        class="img-fluid" alt="" title="" />
                                        <span class="theHoverBorder"></span>
                                    </span>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
		$(`#category-${<?php echo $category['id'] ?>}`).DataTable({
            "order": [[ 0, "desc" ]]
        });
        drawLikedProjects();
        // btn-element btn btn-fullcolor btn-skewed' : 'btn-element btn btn-lined btn-skewed lined-gray
	} );
    function drawLikedProjects(){
        <?php if(isset($this->global_data['userData'])){?>
            let voterLikes = JSON.parse(`<?php echo json_encode($this->global_data['userData']['liked'])?>`);
            for(let voterLike of voterLikes){
                if(voterLike.vote == 1){
                $(`#projectLike-${voterLike.project_id}`).css("color","#4c76b5");
                }
            }
        <?php }?>
        }
</script>