<?php $this->load->view($this->data['inner_slider']);?>
<section class="hg_section pt-80 pb-0">
	<?php 
	$categoryDevs = [];
	$categoryIds = [];
	foreach($age_categories as $category){
		array_push($categoryIds, $category['id']);
		array_push($categoryDevs, $category['paragraph']);
	?>

		<div id="<?php echo $category['paragraph']?>" class="categories-table-containers container">
	   </div><br>
	<?php }?>
</section>	
<script>
	let categoryIds = JSON.parse(`<?php echo json_encode($categoryIds)?>`);
	let categoryDevs = JSON.parse(`<?php echo json_encode($categoryDevs)?>`);
	for (let index = 0; index < categoryIds.length; index++) {
		const element = categoryIds[index];
		getViewAjax('clients/home','get_previous_projects_by_category',categoryIds[index], categoryDevs[index]);
		
	}
	// drawLikedProjects();
	// function drawLikedProjects(){
	// 	let voterLikes = JSON.parse(`<?//php echo json_encode($this->global_data['userData']['liked'])?>`);
	// 	let categoryDevs = JSON.parse(`<?//php echo json_encode($categoryDevs)?>`);
	// 	for (let categoryDev of categoryDevs) {
	// 		for(let voterLike of voterLikes){
	// 			if(voterLike.vote == 1){
	// 			$(`#${categoryDev} #projectLike-${voterLike.project_id}`).css("color","#4c76b5");
	// 			}
	// 		}
	// 	}
	// }
</script>