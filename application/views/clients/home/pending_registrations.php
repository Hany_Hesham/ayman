<style>
	.custom-label{
		margin-left: 10px;
	}
</style>
<?php $this->load->view($this->data['inner_slider']);?>
<section id="content" class="hg_section pt-80 pb-80">
			<div class="container">
				<div class="row">
					<!-- Content page with right sidebar-->
					<div class="right_sidebar col-sm-12 col-md-12 col-lg-9 mb-md-50">
                        <div class="latest_posts style2" style="background-color: #FFF; box-shadow: 5px 5px 9px #ebebeb;border-radius: 15px;">
							<!-- Title -->
							<h3 class="m_title">
								Profile Info
                                <a href="javascript:void(0)" onclick="editUserProfile()"
                                 style="float:right;color:#cd2122;">
                                    <i style="font-size: 25px;" class="icon-gi-ico-2"></i>
                                </a>
							</h3>
							<!--/ Title -->
							<!-- Posts -->
							<div class="user-info">
								<ul class="posts">
									<!-- Post -->
									<li class="post">
										<!-- Details & tags -->
										<div class="details">
											<span class="cat">
												<i class="far fa-user" style="font-size: 25px;;"></i>
											</span>
										</div>
										<h6 ><?php echo $competitor['name'].' '.$competitor['middle_name'].' '.$competitor['last_name']?></h6><br>
										<div class="details">
											<span class="cat">
												<i class="icon-gi-ico-13" style="font-size: 25px;;"></i>
											</span>
										</div>
										<h6 ><?php echo $competitor['city']?></h6><br>
										<div class="details">
											<span class="cat">
												<i class="far fa-building" style="font-size: 25px;;"></i>
											</span>
										</div>
										<h6 ><?php echo $competitor['school']?></h6><br>
										<div class="details">
											<span class="cat">
												<i class="fas fa-address-card" style="font-size: 25px;;"></i>
											</span>
										</div>
										<h6 ><?php echo $competitor['email']?></h6><br>
										<div class="details">
											<span class="cat">
												<i class="fas fa-mobile-alt" style="font-size: 25px;;"></i>
											</span>
										</div>
										<h6 ><?php echo $competitor['phone']?></h6><br>
									</li>
									<!--/ Post -->
								</ul>
							</div>
							<div class="user-info-edit" style="display: none;">
								<form  id="edit-account-info" class="register_panel" name="register_panel" method="post"
								action="<?php echo base_url('clients/Competitors/edit/'.$competitor['id'])?>"
									enctype="multipart/form-data" action="#">
									<div class="cr_response"></div>
									<div class="row">
										<div class="col-sm-4 kl-fancy-form">
											<input type="text" id="edit-username" name="reg_name"
											class="form-control inputbox kl-fancy-form-input kl-fw-input" 
											placeholder="type your first name" 
											value="<?php echo $competitor['name']?>" required>
											<label class="kl-font-alt kl-fancy-form-label custom-label">
												<?php echo translate('First NAME', $this->data['language']) ?>
											</label>
											<span class="text-danger" style="font-size: 12px; display:none" id="error-edit-username">
												Please enter a valid first name
											</span>
										</div>
	
										<div class="col-sm-4 kl-fancy-form">
											<input type="text" id="edit-middle-name" name="reg_middle_name"
											class="form-control inputbox kl-fancy-form-input kl-fw-input" 
											placeholder="type your middle name" 
											value="<?php echo $competitor['middle_name']?>" required>
											<label class="kl-font-alt kl-fancy-form-label custom-label">
												<?php echo translate('Middle NAME', $this->data['language']) ?>
											</label>
											<span class="text-danger" style="font-size: 12px; display:none" id="error-edit-middle-name">
												Please enter a valid middle name
											</span>
										</div>
										<div class="col-sm-4 kl-fancy-form">
											<input type="text" id="edit-lastname" name="reg_lastname"
											class="form-control inputbox kl-fancy-form-input kl-fw-input" 
											placeholder="type your last name" 
											value="<?php echo $competitor['last_name']?>" required>
											<label class="kl-font-alt kl-fancy-form-label custom-label">
												<?php echo translate('Last NAME', $this->data['language']) ?>
											</label>
											<span class="text-danger" style="font-size: 12px; display:none" id="error-edit-lastname">
												Please enter a valid last name
											</span>
										</div>
									</div>
									<div class="row">
									   <div class="col-sm-4 kl-fancy-form">
											<input type="date" id="edit-birth_date" name="birth_date" 
											class="form-control inputbox kl-fancy-form-input kl-fw-input" 
											value="<?php echo $competitor['birth_date']?>" required>
											<label class="kl-font-alt kl-fancy-form-label custom-label">
											<?php echo translate('Date of Birth', $this->data['language']) ?>
											</label>
											<span class="text-danger" style="font-size: 12px; display:none" id="error-edit-birth_date">
												Please enter a valid date
											</span>
										</div>
										<div class="col-sm-4 kl-fancy-form">
											<input type="text" id="edit-city" name="city" 
											class="form-control inputbox kl-fancy-form-input kl-fw-input" 
											value="<?php echo $competitor['city']?>" required>
											<label class="kl-font-alt kl-fancy-form-label custom-label">
											<?php echo translate('City', $this->data['language']) ?>
											</label>
											<span class="text-danger" style="font-size: 12px; display:none" id="error-edit-city">
												Please enter a valid city name
											</span>
										</div>
										<div class="col-sm-4 kl-fancy-form">
											<input type="text" id="edit-school" name="school" 
											class="form-control inputbox kl-fancy-form-input kl-fw-input" 
											value="<?php echo $competitor['school']?>" required>
											<label class="kl-font-alt kl-fancy-form-label custom-label">
											<?php echo translate('School', $this->data['language']) ?>
											</label>
											<span class="text-danger" style="font-size: 12px; display:none" id="error-edit-school">
												Please enter a valid school name
											</span>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6 kl-fancy-form">
											<input type="text" id="edit-email" name="reg_email" 
											class="form-control inputbox kl-fancy-form-input kl-fw-input"
											placeholder="your-email@website.com"
											value="<?php echo $competitor['email']?>" required>
											<label class="kl-font-alt kl-fancy-form-label custom-label">
												<?php echo translate('EMAIL', $this->data['language']) ?>
											</label>
											<span class="text-danger" style="font-size: 12px; display:none" id="error-edit-email">
												Please enter a valid email
											</span>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6 kl-fancy-form">
											<select class="form-control" name="countryCode" id="edit-countryCodeOptions" 
											style="height: 100%;">
											</select>
											<label class="kl-font-alt kl-fancy-form-label custom-label">
											<?php echo translate('Country Code', $this->data['language']) ?>
											</label>
										</div>
										<div class="col-sm-6 kl-fancy-form">
											<input type="text" id="edit-whatsapp" name="whatsapp"
											class="form-control inputbox kl-fancy-form-input kl-fw-input"
											value="<?php echo $competitor['phone']?>" required>
											<label class="kl-font-alt kl-fancy-form-label custom-label">
											<?php echo translate('Whatsapp Number', $this->data['language']) ?>
											</label>
											<span class="text-danger" style="font-size: 12px; display:none" id="error-edit-whatsapp">
												Please enter a valid whatsapp number
											</span>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6 kl-fancy-form">
											<input type="file" name="profile_picture" id="edit-profile_picture" 
											class="form-control inputbox kl-fancy-form-input kl-fw-input" >
											<label class="kl-font-alt kl-fancy-form-label custom-label">
												Profile Picture
											</label>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6 kl-fancy-form">
											<input type="password" id="edit-pass" name="password" 
											class="form-control inputbox kl-fancy-form-input kl-fw-input"
											 placeholder="*****">
											<label class="kl-font-alt kl-fancy-form-label custom-label">
												<?php echo translate('PASSWORD', $this->data['language']) ?>
											</label>
										</div>
										<div class="col-sm-6 kl-fancy-form">
											<input type="password" id="edit-pass2" name="password2" 
											class="form-control inputbox kl-fancy-form-input kl-fw-input"
											 placeholder="*****">
											<label class="kl-font-alt kl-fancy-form-label custom-label">
												<?php echo translate('CONFIRM PASSWORD', $this->data['language']) ?>
											</label>
										</div>
									</div>
									<div>
										<input type="submit" id="edit-user-submit" name="submit" class="btn zn_sub_button btn-block btn-fullcolor btn-md" value="Save">
									</div>
								</form>
							</div>
							<!--/ Posts -->
						</div><br><br>
						<!-- Page title & subtitle -->
						<div id="pending-registration" class=" text-left tbk-symbol--line  tbk-icon-pos--after-title">
							<!-- Title -->
							<h2 class="tbk__title kl-font-alt fs-xl fw-semibold black" style="font-size: 20px;">
								YOUR Pending Registrations
							</h2>

							<!-- Symbol line -->
							<span class="tbk__symbol ">
								<span></span>
							</span>
						</div>
						<!--/ Page title & subtitle -->

						<!-- Text box -->
						<div class="text_box" style="background-color: #FFF; box-shadow: 5px 5px 9px #ebebeb;border-radius: 15px;">
							<div class="kl-store" style="padding: 20px;">
									<table style="padding: 20px;"
									 class="searchable-tables table table-borderless table-responsive-lg table-responsive-sm table-responsive-md table-responsive-xl">
										<thead style="background-color: #34cc99;color:#FFF;">
											<tr>
												<th class="product-thumbnail" width="25%">
												</th>
												<th class="product-name" width="50%">
													Competition
												</th>
												<th class="product-price" width="15%">
													Status
												</th>
												<th class="product-price" width="15%">
													Price
												</th>
												<th class="product-subtotal" width="10%">
													Resume
												</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($pending_projects as $pending_project){?>
												<tr class="cart_item">
													<td class="product-thumbnail" >
														<a href="#">
															<img src="<?php echo base_url('site_assets/uploads/competitions/'.$pending_project['competition_image'])?>" style="width: 100% !important;" class="" alt="Kallyas Product" title="Kallyas Produc" />
														</a>
													</td>
													<td class="product-name">
														<a href="javascript:void(0)"><?php echo $pending_project['competition_title']?></a>
														<dl class="variation">
															<p>
																<?php echo translate( $pending_project['competition_description'], $this->data['language']) ?>	
															</p>
														</dl>
													</td>
													<td class="product-name">
														<dl class="variation">
															<p>
																<?php echo translate( $pending_project['status_name'], $this->data['language']) ?>	
															</p>
														</dl>
													</td>
													<td class="product-price">
														<span class="amount">
														<?php echo money_formater($pending_project['competition_price'],$pending_project['competition_currency']) ?>
														</span>
													</td>
													<td class="product-subtotal">
														<a href="<?php echo base_url('clients/home/competition_register/'.$pending_project['competition_id'])?>" 
															class="btn btn-fullcolor" style="background-color: #cd2122 !important;">
															<!-- Register Now -->
															<i class="icon-noun_61152" style="font-size: 25px;"></i>
														</a>
													</td>
												</tr>
											<?php }?>
										</tbody>
									</table>
							</div>
						</div><br><br>
                        <div id="current-projects" class=" text-left tbk-symbol--line  tbk-icon-pos--after-title">
							<!-- Title -->
							<h2 class="tbk__title kl-font-alt fs-xl fw-semibold black" style="font-size: 20px;">
								YOUR Current Projects
							</h2>

							<!-- Symbol line -->
							<span class="tbk__symbol ">
								<span></span>
							</span>
						</div>
                        <div class="text_box" style="background-color: #FFF; box-shadow: 5px 5px 9px #ebebeb;border-radius: 15px;">
							<div class="kl-store" style="padding: 20px;">
							    <table class="searchable-tables table table-borderless table-responsive-lg table-responsive-sm table-responsive-md table-responsive-xl"
								style="padding: 20px;">
										<thead style="background-color: #34cc99;color:#FFF;">
											<tr>
												<th class="product-thumbnail" width="25%">
												</th>
												<th class="product-name" width="50%">
													Competition
												</th>
												<th class="product-subtotal" width="10%">
													Action
												</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($current_projects as $current_project){?>
												<tr class="cart_item">
													<td class="product-thumbnail" >
														<a href="#">
															<img src="<?php echo base_url('site_assets/uploads/projects/'.$current_project['image'])?>" style="width: 100% !important;" class="" alt="Kallyas Product" title="Kallyas Produc" />
														</a>
													</td>
													<td class="product-name">
														<a href="#"><?php echo $current_project['title']?></a>
														<dl class="variation">
															<p>
																<?php echo translate( $current_project['description'], $this->data['language']) ?>	
															</p>
														</dl>
													</td>
													<td class="product-subtotal">
														<a href="<?php echo base_url('clients/home/project/'.$current_project['code'])?>" 
															class="btn btn-fullcolor" style="background-color: #cd2122 !important;">
															<!-- Register Now -->
															<i class="icon-noun_61152" style="font-size: 25px;"></i>
														</a>
													</td>
												</tr>
											<?php }?>
										</tbody>
									</table>
							</div>
						</div>
					</div>
					<!--/ Content page col-sm-12 col-md-12 col-lg-9 mb-md-50 -->

					<!-- Sidebar -->
					<div class="col-sm-12 col-md-12 col-lg-3">
						<div id="sidebar-widget" class="sidebar" style="box-shadow: 5px 5px 9px #ebebeb;border-radius: 15px;">
							<!-- Widget -->
							<div class="widget">
								<!-- Title -->
								<h3 class="widgettitle title">
									Your Projects
								</h3>

								<!-- Search widget -->
								<div class="widget_search">
									<!-- Search wrapper -->
									<div class="search gensearch__wrapper">
										<form id="searchform" class="gensearch__form" action="#" method="get" >
											<input id="s" name="q" maxlength="20" class="inputbox gensearch__input" 
                                            type="text" size="20" value="SEARCH ..." 
                                            onblur="if (this.value=='') this.value='SEARCH ...';" 
                                            onfocus="if (this.value=='SEARCH ...') 
                                            this.value='';">
                                            <button type="submit" id="searchsubmit-sidebar" value="go" class="gensearch__submit fas fa-search"></button>
										</form>
									</div>
									<!-- Search wrapper -->
								</div>
								<!--/ Search widget -->
							</div>
							<!--/ Search widget -->

							<!-- Product categories widget -->
							<div id="kl-store_product_categories-2" class="widget kl-store widget_product_categories">
								<!-- Title -->
								<h3 class="widgettitle title">
                                   LAST PROJECTS
								</h3>

								<!-- Product category list -->
								<ul class="product-categories">
									<li class="cat-item">
										<a href="#">TOTAL</a><span class="count">(<?php echo count($old_projects)?>)</span>
										<ul class="children">
											<?php foreach($old_projects as $project){?>
												<li class="cat-item">
													<a href="<?php echo base_url('clients/home/project/'.$project['code'])?>">
														<?php echo $project['title']?>
													</a>
												</li>
											<?php }?>
										</ul>
									</li>
								</ul>
								<!--/ Product category list -->
							</div>
							<!--/ Product categories widget -->
						</div>
					</div>
					<!--/ Sidebar col-sm-12 col-md-12 col-lg-3 -->
				</div>	
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
<script>
    $(document).ready(function() {
		$(`.searchable-tables`).DataTable({
            "order": [[ 0, "desc" ]]
        });
	} );

	$(document).ready(function(){
		$('#edit-countryCodeOptions').append(`${countryCodeOptions}`)
        $('#edit-account-info').keyup(function(attr){
			if(attr.target.id == 'edit-pass' || attr.target.id == 'edit-pass2'){
				passwordConfirm()
			}else{
				validateEditUser();
			}
        });
    });

	function editUserProfile(){
		if($('.user-info').is(":visible")){
			$('.user-info').hide();
			$('.user-info-edit').show();
		}else if($('.user-info-edit').is(":visible")){
			$('.user-info').show();
			$('.user-info-edit').hide();
		}
	}

	function validateEditUser(){
        let username = validateField('edit-username', 'text');
		let middleName = validateField('edit-middle-name', 'text');
		let lastName = validateField('edit-lastname', 'text');
        let birth_date = validateField('edit-birth_date', '');
        let city = validateField('edit-city', 'text');
        let school = validateField('edit-school', 'text');
        let email = validateField('edit-email', 'email');
        let whatsapp = validateField('edit-whatsapp', 'number');
        if(username === true && middleName === true && lastName === true && birth_date === true && city === true && school === true
            && email === true && whatsapp === true ){
            $('#edit-user-submit').attr('disabled',false);
		}else{
            $('#edit-user-submit').attr('disabled',true);
        }
    }
	function passwordConfirm(){
		let password = $('#edit-pass').val();
		let confirmedPassword = $('#edit-pass2').val();
		if(password != '' || confirmedPassword != ''){
			if(password === confirmedPassword){
				$('#edit-user-submit').attr('disabled',false);
			}else{
				$('#edit-user-submit').attr('disabled',true);
			}
		}else{
			$('#edit-user-submit').attr('disabled',false);
		}
	}
</script>