<?php $this->load->view($this->data['inner_slider']);?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <!-- Title element -->
            <div class="kl-title-block text-center">
                <!-- Title with custom font size and thin style -->
                <h3 class="tbk__title fs-xxl fw-thin">
                    <span class="fw-semibold fs-xxxl fs-xs-xl tcolor">
                    <?php echo translate('Results For', $this->data['language']) ?> </span>
                    / <?php echo $competition['title']?>
					<?php if(isset($competition['results_file']) && $competition['results_file'] !='' && $competition['results_file'] !='0'){?>
						<a style="" href="<?php echo base_url('site_assets/uploads/competitions/'.$competition['results_file'])?>" download class="btn-element btn btn-fullcolor btn-skewed" title="Download Results">
							<span> <?php echo translate('Download Results', $this->data['language']) ?> </span></span>
						<i class="fas fa-cloud-download-alt"></i>
						</a>
					<?php }?>
                </h3>
            </div>
            <!--/ Title element -->
        </div>
    </div>
</div>
<section class="hg_section pt-80 pb-0">
	<?php 
	$categoryDevs = [];
	$categoryIds = [];
	foreach($age_categories as $category){
		array_push($categoryIds, $category['id']);
		array_push($categoryDevs, $category['paragraph']);
	?>

		<div id="<?php echo $category['paragraph']?>" class="categories-table-containers container">
	   </div><br>
	<?php }?>
</section>	
<script>
	let categoryIds = JSON.parse(`<?php echo json_encode($categoryIds)?>`);
	let categoryDevs = JSON.parse(`<?php echo json_encode($categoryDevs)?>`);
    let competition_id = `<?php echo $competition['id']?>`;
	for (let index = 0; index < categoryIds.length; index++) {
		const element = categoryIds[index];
		getViewAjax('clients/home',`get_competition_results_by_category/${competition_id}`,categoryIds[index], categoryDevs[index]);
		
	}
</script>