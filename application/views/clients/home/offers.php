
<div class="section-50 section-sm-bottom-75 section-lg-bottom-100 bg-whisper">
  <div class="shell">
    <div class="range">
      <div class="cell-xs-12">
        <div class="product-main">
         <?php foreach ($allOffers as $offer){ ?>
          <div class="product product-item-fullwidth">
            <div class="product-slider">
              <div class="product-slider-inner">
                <div  class="owl-carousel owl-style-minimal">
                  <div class="item">
                    <figure>
                       <img src="<?php echo base_url('site_assets/images/offer/'.$offer['image'])?>" alt="" width="400" height="360"/> 
                    </figure>
                  </div>
                </div>
              </div>
            </div>
            <div class="product-main">
              <?php if($offer['special'] == 1){ ?>
                <div class="product-label-wrap">
                  <div class="label-custom product-label label-danger"><?php echo translate('Special', $this->data['language']) ?></div>
                </div>
              <?php } ?>
              <div class="product-main-inner">
                <div class="product-body">
                  <h5 class="product-header"><a href="shop-product.html"><?php echo translate($offer['name'], $this->data['language']) ?></a></h5>
                  <div class="product-rating">
                    <ul class="list-rating">
                      <?php $stars = rand(3,6); for ($i=1; $i <=$stars ; $i++) { ?>
                         <li><span class="icon icon-xxs material-icons-star"></span></li>
                      <?php }?>
                      <?php $empty_stars = 5-$stars; for ($i=1; $i <=$empty_stars ; $i++) {?>
                                       <li><span class="icon icon-xxs material-icons-star_border"></span></li>
                      <?php }?>
                    </ul>
                    <span class="text-light"><?php echo rand(20,100);?><?php echo translate('customer reviews', $this->data['language']) ?></span> 
                  </div>
                  <div class="product-description">
                    <p><?php echo translate($offer['data'], $this->data['language']) ?></p>
                  </div>
                </div>
                <div class="product-aside">
                  <div class="product-aside-top">
                    <p class="price-irrelevant"></p>
                    <p class="pricing-object pricing-object-xl price-current"><span class="small small-middle"><?php echo translate($offer['currency'], $this->data['language']) ?></span><span class="price"><?php echo translate($offer['price'], $this->data['language']) ?></span><span class="small small-bottom"><?php echo translate('.00', $this->data['language']) ?></span></p>
                  </div>
                  <div class="product-aside-bottom">
                    <div class="stepper-wrap">
                    </div>
                      <a href="<?php echo base_url('clients/offers/offerd/'.$offer['id']);?>" class="btn btn-icon btn-icon-left btn-primary product-control"><span class="icon icon-sm fa-shopping-cart"></span><span><?php echo translate('Book Now', $this->data['language']) ?></span></a> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php }?>
      </div>
    </div>
  </div>
</div>