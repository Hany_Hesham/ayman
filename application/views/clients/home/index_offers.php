<section id="section-start-journey" class="section-50 section-sm-90 section-lg-top-120 section-lg-bottom-145">
  	<div class="shell">
    	<div class="range range-lg-center">
      		<div class="cell-lg-10 text-center text-sm-left">
      			<h3> <?php echo translate('Special Offers', $this->data['language']) ?></h3>
      		</div>
    	</div>
    	<div class="range range-md-bottom range-sm-center">
    		<?php foreach ($allOffers as $offer):?>
      			<div class="cell-sm-6 cell-md-4 cell-lg-3" style="margin-bottom: 2%;">
        			<div class="product product-item-default">
          				<div class="product-slider">
            				<div class="product-slider-inner">
              					<div data-items="1" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="true" data-dots="true" data-animation-in="fadeIn" data-animation-out="fadeOut" class="owl-carousel owl-style-minimal">
                					<div class="item">
                  						<figure>
                  							<img src="<?php echo base_url('site_assets/images/offer/'.$offer['image'])?>" alt="" width="270" height="360"/> 
                  						</figure>
                					</div>
                					<div class="item">
                  						<figure>
                  							<img src="<?php echo base_url('site_assets/images/offer/'.$offer['image'])?>" alt="" width="270" height="360"/> 
                  						</figure>
                					</div>
              					</div>
            				</div>
          				</div>
          				<?php if($offer['special'] == 1):?>
            				<div class="product-label-wrap product-label-wrap-left">
              					<div class="label-custom product-label label-danger"> <?php echo translate('Special', $this->data['language']) ?></div>
            				</div>
          				<?php endif; ?>
          				<div class="product-main">
            				<h6 class="product-header"><a href="#"><?php echo  translate($offer['name'], $this->data['language'])?></a></h6>
          				</div>
          				<div class="product-footer">
            				<div class="product-footer-front pricing-wrap">
              					<p class="pricing-object pricing-object-sm price-current">
                					<span class="small small-middle"><?php echo  translate($offer['currency'], $this->data['language'])?></span>
                					<span class="price"><?php echo  translate($offer['price'], $this->data['language'])?></span>
                					<span class="small small-bottom"></span>
              					</p>
            				</div>
            				<div class="product-footer-behind">
            					<a href="<?php echo base_url('clients/offers/offerd/'.$offer['id']);?>" class="btn btn-icon btn-icon-left btn-primary product-control"><span class="icon icon-sm fa-shopping-cart"></span></a>
            				</div>
          				</div>
        			</div>
      			</div>
    		<?php endforeach; ?>
  		</div>
	</div>
</section>