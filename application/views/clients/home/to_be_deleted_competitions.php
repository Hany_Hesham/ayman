<?php $this->load->view($this->data['inner_slider']);?>

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <!-- Title element with icon symbol and custom bottom padding -->
            <div class="kl-title-block text-center tbk-symbol--icon">
                <!-- Title with custom alternative font, size and default theme color -->
                <h3 class="tbk__title kl-font-alt fs-xl tcolor">
                    LATEST COMPETITION
                </h3>
            </div>
            <!--/ Title element with icon symbol -->

            <!-- Offer banners element -->
            <div class="offer-banners ob--resize-no-resize">
                <div class="row">

                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <!-- Offer banner image link -->
                        <a href="#" target="_blank" class="offer-banners-link hoverBorder" title="">
                            <!-- Border image wrapper -->
                            <span class="hoverBorderWrapper">
                                <!-- Image -->
                                <img src="<?php echo base_url('site_assets/images/banner-competitions.png')?>" class="img-fluid offer-banners-img" alt="" title="" />

                                <!-- Hover border -->
                                <span class="theHoverBorder"></span>
                            </span>
                            <!--/ Border image wrapper -->
                        </a>
                        <!--/ Offer banner image link -->
                    </div>
                    <!--/ col-sm-12 col-md-12 col-lg-12 -->
                </div>
                <!--/ row -->
            </div>
            <!--/ Offer banners element -->
        </div>
        <!--/ col-md-12 col-sm-12 -->
    </div>
    <!--/ row -->
</div>
	<!-- FEATURED / LATEST / BEST SELLING Carousels section -->
<section class="hg_section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 mb-30">
                <div class="shop-latest">
                    <div class="spp-products-rows">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Title with default theme color  -->
                                <h3 class="m_title tcolor spp-title">
                                    Choose Your Competition
                                </h3>
                            </div>
                            <!--/ col-sm-12 -->

                            <div class="col-sm-12">
                                <!-- Shop latest carousel -->
                                <div class="shop-latest-carousel spp-carousel sppCrs--c">	
                                    <!-- Featured products list -->
                                    <ul class="featured_products spp-list js-slick" data-slick='{
                                        "infinite": true,
                                        "slidesToShow": 3,
                                        "slidesToScroll": 1,
                                        "swipeToSlide": true,
                                        "autoplay": true,
                                        "autoplaySpeed": 3000,
                                        "speed": 500,
                                        "cssEase": "ease-in-out",
                                        "arrows": true,
                                        "appendArrows": ".spp-carousel .hgSlickNav.hgSlickNav_0",
                                        "responsive": [
                                            {
                                                "breakpoint": 1199,
                                                "settings": {
                                                    "slidesToShow": 3
                                                }
                                            },
                                            {
                                                "breakpoint": 992,
                                                "settings": {
                                                    "slidesToShow": 2
                                                }
                                            },
                                            {
                                                "breakpoint": 576,
                                                "settings": {
                                                    "slidesToShow": 1
                                                }
                                            }
                                        ]
                                    }'>
                                        <!-- Product #1 -->
                                        <li>
                                            <div class="product-list-item prod-layout-classic">
                                                <!-- Badge container -->
                                                <div class="hg_badge_container">
                                                    <span class="hg_badge_sale">Main!</span>
                                                </div>
                                                <!--/ Badge container -->

                                                <!-- Product container link -->
                                                <a href="#">
                                                    <span class="image kw-prodimage">
                                                        <img src="<?php echo base_url('site_assets/images/4096.jpg')?>"
                                                         class="kw-prodimage-img"  alt="Kallyas Product" title="Kallyas Product" />
                                                    </span>
                                                    <div class="details kw-details fixclear">
                                                        <!-- Title -->
                                                        <h3 class="kw-details-title" style="font-weight: bold;">
                                                            Main Competition
                                                        </h3>

                                                        <span style="font-size: 14px; color:black;">
                                                            tetsd Star rating --> tetsd Star rating -->
                                                            tetsd Star rating -->
                                                        </span><br>
                                                        <!-- Price -->
                                                        <span class="price">
                                                            <ins data-now="NOW">
                                                                <span class="amount">£1,999.00</span>
                                                            </ins>
                                                        </span><br>
                                                        <!--/ Price -->

                                                        <!-- Star rating -->
                                                        <div style="overflow: hidden;float: left;top: 0;left: 0;padding-top: 0.7em;">
                                                            <span style="width:100%">
                                                                <!-- Button -->
                                                                <!-- <a href="#register_form_panel" class="btn btn-fullcolor kl-cta-ribbon" 
                                                                     target="_self" style="background-color: #cd2122 !important;color:#FFF;">
                                                                    Register Now
                                                                </a> -->
                                                                <a  class="btn btn-fullcolor " style="background-color: #cd2122 !important;color:#FFF;"
                                                                    href="<?php echo base_url('clients/home/competition_register/1')?>">
                                                                    Register Now
                                                                </a>
                                                                <!--/ Button -->
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <!--/ details fixclear -->
                                                </a>
                                                <!-- Product container link -->

                                                <!-- Actions -->
                                                <div class="actions kw-actions">
                                                    <a href="product-page.html">
                                                        <span class="more-icon fas fa-compress"></span>
                                                    </a>
                                                </div>
                                                <!--/ Actions -->
                                            </div>
                                            <!--/ product-list-item -->
                                        </li>
                                        <!--/ Product #1 -->
                                         <!-- Product #1 -->
                                         <li>
                                            <div class="product-list-item prod-layout-classic">
                                                <!-- Badge container -->
                                                <div class="hg_badge_container">
                                                    <span class="hg_badge_sale">Main!</span>
                                                </div>
                                                <!--/ Badge container -->

                                                <!-- Product container link -->
                                                <a href="product-page.html">
                                                    <span class="image kw-prodimage">
                                                        <img src="<?php echo base_url('site_assets/images/4096.jpg')?>"
                                                         class="kw-prodimage-img"  alt="Kallyas Product" title="Kallyas Product" />
                                                    </span>
                                                    <div class="details kw-details fixclear">
                                                        <!-- Title -->
                                                        <h3 class="kw-details-title" style="font-weight: bold;">
                                                            Main Competition
                                                        </h3>

                                                        <span style="font-size: 14px; color:black;">
                                                            tetsd Star rating --> tetsd Star rating -->
                                                            tetsd Star rating -->
                                                        </span><br>
                                                        <!-- Price -->
                                                        <span class="price">
                                                            <ins data-now="NOW">
                                                                <span class="amount">£1,999.00</span>
                                                            </ins>
                                                        </span><br>
                                                        <!--/ Price -->

                                                        <!-- Star rating -->
                                                        <div style="overflow: hidden;float: left;top: 0;left: 0;padding-top: 0.7em;">
                                                            <span style="width:100%">
                                                                <!-- Button -->
                                                                <a href="#register_form_panel" class="btn btn-fullcolor kl-cta-ribbon" 
                                                                     target="_self" style="background-color: #cd2122 !important;color:#FFF;">
                                                                    Register Now
                                                                </a>
                                                                <!--/ Button -->
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <!--/ details fixclear -->
                                                </a>
                                                <!-- Product container link -->

                                                <!-- Actions -->
                                                <div class="actions kw-actions">
                                                    <a href="product-page.html">
                                                        <span class="more-icon fas fa-compress"></span>
                                                    </a>
                                                </div>
                                                <!--/ Actions -->
                                            </div>
                                            <!--/ product-list-item -->
                                        </li>
                                        <!--/ Product #1 -->
                                         <!-- Product #1 -->
                                         <li>
                                            <div class="product-list-item prod-layout-classic">
                                                <!-- Badge container -->
                                                <div class="hg_badge_container">
                                                    <span class="hg_badge_sale">Main!</span>
                                                </div>
                                                <!--/ Badge container -->

                                                <!-- Product container link -->
                                                <a href="product-page.html">
                                                    <span class="image kw-prodimage">
                                                        <img src="<?php echo base_url('site_assets/images/4096.jpg')?>"
                                                         class="kw-prodimage-img"  alt="Kallyas Product" title="Kallyas Product" />
                                                    </span>
                                                    <div class="details kw-details fixclear">
                                                        <!-- Title -->
                                                        <h3 class="kw-details-title" style="font-weight: bold;">
                                                            Main Competition
                                                        </h3>

                                                        <span style="font-size: 14px; color:black;">
                                                            tetsd Star rating --> tetsd Star rating -->
                                                            tetsd Star rating -->
                                                        </span><br>
                                                        <!-- Price -->
                                                        <span class="price">
                                                            <ins data-now="NOW">
                                                                <span class="amount">£1,999.00</span>
                                                            </ins>
                                                        </span><br>
                                                        <!--/ Price -->

                                                        <!-- Star rating -->
                                                        <div style="overflow: hidden;float: left;top: 0;left: 0;padding-top: 0.7em;">
                                                            <span style="width:100%">
                                                                <!-- Button -->
                                                                <a href="#register_form_panel" class="btn btn-fullcolor kl-cta-ribbon" 
                                                                     target="_self" style="background-color: #cd2122 !important;color:#FFF;">
                                                                    Register Now
                                                                </a>
                                                                <!--/ Button -->
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <!--/ details fixclear -->
                                                </a>
                                                <!-- Product container link -->

                                                <!-- Actions -->
                                                <div class="actions kw-actions">
                                                    <a href="product-page.html">
                                                        <span class="more-icon fas fa-compress"></span>
                                                    </a>
                                                </div>
                                                <!--/ Actions -->
                                            </div>
                                            <!--/ product-list-item -->
                                        </li>
                                        <!--/ Product #1 -->
                                         <!-- Product #1 -->
                                         <li>
                                            <div class="product-list-item prod-layout-classic">
                                                <!-- Badge container -->
                                                <div class="hg_badge_container">
                                                    <span class="hg_badge_sale">Main!</span>
                                                </div>
                                                <!--/ Badge container -->

                                                <!-- Product container link -->
                                                <a href="product-page.html">
                                                    <span class="image kw-prodimage">
                                                        <img src="<?php echo base_url('site_assets/images/4096.jpg')?>"
                                                         class="kw-prodimage-img"  alt="Kallyas Product" title="Kallyas Product" />
                                                    </span>
                                                    <div class="details kw-details fixclear">
                                                        <!-- Title -->
                                                        <h3 class="kw-details-title" style="font-weight: bold;">
                                                            Main Competition
                                                        </h3>

                                                        <span style="font-size: 14px; color:black;">
                                                            tetsd Star rating --> tetsd Star rating -->
                                                            tetsd Star rating -->
                                                        </span><br>
                                                        <!-- Price -->
                                                        <span class="price">
                                                            <ins data-now="NOW">
                                                                <span class="amount">£1,999.00</span>
                                                            </ins>
                                                        </span><br>
                                                        <!--/ Price -->

                                                        <!-- Star rating -->
                                                        <div style="overflow: hidden;float: left;top: 0;left: 0;padding-top: 0.7em;">
                                                            <span style="width:100%">
                                                                <!-- Button -->
                                                                <a href="#register_form_panel" class="btn btn-fullcolor kl-cta-ribbon" 
                                                                     target="_self" style="background-color: #cd2122 !important;color:#FFF;">
                                                                    Register Now
                                                                </a>
                                                                <!--/ Button -->
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <!--/ details fixclear -->
                                                </a>
                                                <!-- Product container link -->

                                                <!-- Actions -->
                                                <div class="actions kw-actions">
                                                    <a href="product-page.html">
                                                        <span class="more-icon fas fa-compress"></span>
                                                    </a>
                                                </div>
                                                <!--/ Actions -->
                                            </div>
                                            <!--/ product-list-item -->
                                        </li>
                                        <!--/ Product #1 -->
                                    </ul>
                                    <!--/ Featured products list -->

                                    <!-- Slick navigation -->
                                    <div class="hgSlickNav hgSlickNav_0 clearfix"></div>
                                </div>
                                <!--/ .shop-latest-carousel -->
                            </div>
                            <!--/ col-sm-12 -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ spp-products-rows -->
                </div>
                <!--/ shop latest -->
            </div>
            <!--/ col-sm-12 col-md-12 mb-30 -->

            <!--/ col-sm-12 col-md-12 mb-50 -->
        </div>
        <!--/ row -->
    </div>
    <div id="register_form_panel" class="mfp-hide contact-popup">
		<div class="contact-popup-panel">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<!-- Contact form pop-up element -->
						<div class="contactForm pop-up-form">
							<!-- Google reCaptcha required javascript file -->
							<script src='https://www.google.com/recaptcha/api.js'></script>

							<!-- Title -->
							<h3 class="m_title m_title_ext text-custom contact-popup-title tcolor">
								Competition Registration
							</h3>

							<form action="php_helpers/_contact-process.php" method="post" class="contact_form row mt-40" enctype="multipart/form-data">
								<div class="cf_response"></div>

								<div class="col-sm-6 kl-fancy-form">
									<input type="text" name="name" id="cf_name-pop-up" class="form-control" placeholder="Please enter your first name" value="" tabindex="1" maxlength="35" required>
									<label class="control-label">
										FIRSTNAME
									</label>
								</div>

								<div class="col-sm-6 kl-fancy-form">
									<input type="text" name="lastname" id="cf_lastname-pop-up" class="form-control" placeholder="Please enter your first last name" value="" tabindex="1" maxlength="35" required>
									<label class="control-label">
										LASTNAME
									</label>
								</div>

								<div class="col-sm-12 kl-fancy-form">
									<input type="text" name="email" id="cf_email-pop-up" class="form-control h5-email" placeholder="Please enter your email address" value="" tabindex="1" maxlength="35" required>
									<label class="control-label">
										EMAIL
									</label>
								</div>

								<div class="col-sm-12 kl-fancy-form">
									<input type="text" name="subject" id="cf_subject-pop-up" class="form-control" placeholder="Enter the subject message" value="" tabindex="1" maxlength="35" required>
									<label class="control-label">
										SUBJECT
									</label>
								</div>

								<div class="col-sm-12 kl-fancy-form">
									<textarea name="message" id="cf_message-pop-up" class="form-control" cols="30" rows="10" placeholder="Your message" tabindex="4" required></textarea>
									<label class="control-label">
										MESSAGE
									</label>
								</div>

								<!-- Google recaptcha required site-key (change with yours => https://www.google.com/recaptcha/admin#list) -->
								<div class="g-recaptcha" data-sitekey="SITE-KEY"></div>
								<!--/ Google recaptcha required site-key -->

								<div class="col-sm-12">
									<!-- Contact form send button -->
									<button class="btn btn-fullcolor" type="submit">
										Send
									</button>
								</div>
							</form>
						</div>
						<!--/ Contact form pop-up element -->
						<a  id="href-success-panel" style="display:none;" href="#success_panel-modal" class="create_account auth-popup-createacc kl-login-box auth-popup-link">
								
						</a>
						<div id="success_panel-modal" class="mfp-hide loginbox-popup auth-popup" style="background-color: #FFF;">
						<div class="inner-container register-panel auth-popup-panel">
							<h3 class="m_title m_title_ext text-custom auth-popup-title"></h3>
							<div class="thank-you-pop">
								<div id="ajax-loading-block" style="display: none;">
									<img id="success-image" src="<?php echo base_url('site_assets/images/ajax-loader.gif')?>" alt="">
								</div>
								<div id="ajax-success-block" style="display: none;">
									<img id="success-image" src="<?php echo base_url('site_assets/images/Green-success.png')?>" alt="">
									<h1>Thank You!</h1>
									<p>Your submission is received and we will contact you soon</p>
									<h3 class="cupon-pop">Your Id: <span>12345</span></h3>
								</div>
								<div id="ajax-failure-block" style="display: none;">
									<img id="failure-image"style="width:130px; height:auto;" src="<?php echo base_url('site_assets/images/failure-rounded.png')?>" alt="">	
								    <h1>Sorry!</h1>
									<p>Your submission is failed please try again</p>
									<a href="#register_panel" class="kl-login-box auth-popup-link">
										CREATE AN ACCOUNT
									</a>

								</div>					
 						</div>
						</div>
					</div>
					<!--/ col-md-12 col-sm-12 -->
				</div>
				<!--/ .row -->
			</div>
			<!--/ .container -->
		</div>
        </div>
    <!--/ container -->
</section>

<!--/ FEATURED / LATEST / BEST SELLING Carousels section -->
