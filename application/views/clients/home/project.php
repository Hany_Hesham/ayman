<?php

$this->load->view($this->data['inner_slider']);?>
<style>
    .portfolio-item-details-label{
        padding-right: 5px;
    }
</style>
<!-- Portfolio item section with custom paddings -->
<section class="hg_section pt-80 pb-0">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="row hg-portfolio-item mb-30">
                    <!-- Left side -->
                    <div id="pinned-trigger" class="col-sm-12 col-md-5 col-lg-5">
                        <!-- Portfolio item content -->
                        <div id="pinned-element" class="portfolio-item-content affixcontent mb-sm-50">
                            <!-- Page/Portfolio title -->
                            <h1 class="page-title portfolio-item-title">
                                <?php echo translate($project['title'], $this->data['language']) ?> 
                            </h1>

                            <!-- Description -->
                            <div class="portfolio-item-desc">
                                <!-- Description wrapper -->
                                <div class="portfolio-item-desc-inner">
                                    <p>
                                        <?php echo translate($project['description'], $this->data['language']) ?> 
                                    </p>
                                </div>
                                <a href="#" class="portfolio-item-more-toggle js-toggle-class" data-target=".portfolio-item-desc" data-target-class="is-opened" data-more-text="see more" data-less-text="show less">
                                    <span class="fas fa-chevron-down"></span>
                                </a>
                            </div>
                            <!--/ Description -->

                            <!-- Details -->
                            <ul class="portfolio-item-details clearfix">

                                
                                <li class="portfolio-item-details-year clearfix">
                                    <span class="portfolio-item-details-label">
                                        <?php echo translate('Competition Name', $this->data['language']) ?> 
                                    </span>
                                    <span class="portfolio-item-details-item"><?php echo $project['competition_title'] ?> </span>
                                </li>
                                <!-- Client details -->
                                <li class="portfolio-item-details-client clearfix">
                                    <span class="portfolio-item-details-label"><?php echo translate('Competitor Name', $this->data['language']) ?> </span>
                                    <span class="portfolio-item-details-item">
                                        <?php echo $project['first_name'].' '.$project['middle_name'].' '.$project['last_name']?>
                                    </span>
                                </li>
                                <!--/ Client details -->

                                <!-- Date details -->
                                <li class="portfolio-item-details-year clearfix">
                                    <span class="portfolio-item-details-label"><?php echo translate('Age', $this->data['language']) ?> </span>
                                    <span class="portfolio-item-details-item"><?php echo $project['age'] ?> </span>
                                </li>
                                <!--/ Date details -->

                                <!-- Services details -->
                                <li class="portfolio-item-details-services clearfix">
                                    <span class="portfolio-item-details-label"><?php echo translate('Education', $this->data['language']) ?> </span>
                                    <span class="portfolio-item-details-item"><?php echo $project['school'] ?></span>
                                </li>
                                <!--/ Services details -->

                                <!-- Partners details -->
                                <li class="portfolio-item-details-partners clearfix">
                                    <span class="portfolio-item-details-label"><?php echo translate('Coach Name', $this->data['language']) ?> </span>
                                    <span class="portfolio-item-details-item"><?php echo $project['trainer'] == 'with trainer' ? $project['trainer_name']: '----' ?></span>
                                </li>
                                <li class="portfolio-item-details-year clearfix">
                                    <span class="portfolio-item-details-label">
                                        <?php echo translate('Total Votes', $this->data['language']) ?> 
                                        <span class="far fa-thumbs-up" ></span>
                                    </span>
                                    <span class="portfolio-item-details-item"><?php echo $project['votes'] ?> </span>
                                </li>
                                <li class="portfolio-item-details-partners clearfix">
                                    <a href="<?php echo base_url('clients/home/like_project/'.$project['id'])?>" 
                                       class="btn-element btn btn-lined btn-skewed lined-gray" title=""
                                       <?php echo $this->global_data['is_voter'] && $project['competition_active'] >= 1 
                                       && $project['vote_start_date'] <= date('Y-m-d') && $project['vote_end_date'] >= date('Y-m-d')? '':'style="pointer-events: none"' ?>>
                                        <span class="visible-sm visible-xs visible-lg">
                                            <span id="projectLike-<?php echo $project['id']?>" class="far fa-thumbs-up" style="font-size: 20px;"></span>
                                        </span>
                                    </a>
                                    <a href="javascript:void(0)" 
                                        onclick="navigator.clipboard.writeText(`<?php echo base_url('clients/home/project/'.$project['code'])?>`)" 
                                        class="btn-element btn btn-lined btn-skewed lined-dark" title="">
                                        <span class="visible-sm visible-xs visible-lg">
                                            <span class="fas fa-share-alt" style="font-size: 20px;"></span>
                                        </span>
                                    </a>
                                </li>
                                <!--/ Partners details -->
                            </ul>
                            <!--/ Details -->

                            <!-- Other details sharing -->
                            <div class="portfolio-item-otherdetails clearfix">
                                <!-- Portfolio link buttons -->
                                <div class="portfolio-item-livelink">
                                    <a href="<?php echo $project['project_link']?>" target="_blank" class="btn btn-lined lined-custom" title="">
                                        <span class="visible-sm visible-xs visible-lg">
                                            <?php echo translate('PROJECT LIVE PREVIEW', $this->data['language']) ?>
                                        </span>
                                    </a>
                                </div>
                                <!--/ Portfolio link buttons -->
                            </div>
                            <!--/ .portfolio-item-otherdetails -->
                        </div>
                        <!--/ .portfolio-item-content mb-sm-50 -->
                    </div>
                    <!--/ col-sm-12 col-md-5 col-lg-5 -->

                    <!-- Right side -->
                    <div class="col-sm-12 col-md-7 col-lg-7">
                        <!-- Portfolio item right -->
                        <div class="portfolio-item-right mfp-gallery images">
                            <!-- Main portfolio image & pop-up -->
                            <a href="<?php echo base_url('site_assets/uploads/projects/'.$project['image'])?>" class="hoverLink" data-lightbox="mfp" data-mfp="image" title="Progressively harness" >
                                <!-- Border image wrapper -->
                                <span class="hoverBorderWrapper">
                                    <!-- Image -->
                                    <img src="<?php echo base_url('site_assets/uploads/projects/'.$project['image'])?>" class="img-fluid" alt="" title="" />
                                    <!-- Hover border -->
                                    <span class="theHoverBorder"></span>
                                   
                                </span>
                                <!--/ Border image wrapper -->
                            </a>
                            <!--/ Main portfolio image & pop-up -->

                          
                        </div>
                        <!--/ .portfolio-item-right -->
                    </div>
                    <!--/ col-sm-12 col-md-7 col-lg-7 -->
                </div>
                <!--/ row Portfolio item -->

                <!-- Separator element -->
            </div>
            <!--/ col-sm-12 col-md-12 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>
<!--/ Portfolio item section with custom paddings -->
<script>
    $(document).ready(function() {
        // btn-element btn btn-fullcolor btn-skewed' : 'btn-element btn btn-lined btn-skewed lined-gray
		let voterLikes = JSON.parse(`<?php echo json_encode($this->global_data['userData']['liked'])?>`);
        for(let voterLike of voterLikes){
            if(voterLike.project_id == `<?php echo $project['id']?>` && voterLike.vote == 1){
             $(`#projectLike-${voterLike.project_id}`).css("color","#4c76b5");
            }
        }
        console.log(voterLikes);
	} );
</script>
