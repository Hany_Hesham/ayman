<div class="shop-panel">
  <div class="shell text-center">
    <div class="range range-xs-middle range-xs-justify">
      <div class="cell-xs-4 text-xs-left">
        <ul class="shop-panel-list">
          <li id="<?php echo $offerData['id'].'/1'?>">
            <a  style="margin-left:5px;" class="view-controller no-print" id="offer" href="javascript: void(0);" onclick="getViewAjax('clients/offers','offer','<?php echo $offerData['id'].'/1'?>','mainPageBody')">
              <span class="icon icon-sm material-icons-view_list"></span> 
            </a>
          </li>
          <li id="<?php echo $offerData['id'].'/2'?>">
            <a  style="margin-left:5px;" class="view-controller no-print" id="offer" href="javascript: void(0);" onclick="getViewAjax('clients/offers','offer','<?php echo $offerData['id'].'/2'?>','mainPageBody')">
              <span class="icon icon-sm material-icons-view_module"></span>  
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="section-50 section-sm-bottom-75 section-lg-bottom-100 bg-whisper">
  <div class="shell">
    <div class="range">
      <?php foreach ($allOffers as $offer):?>
        <div class="cell-sm-6 cell-md-4 cell-lg-3">
          <div class="product product-item-default">
            <div class="product-slider">
              <div class="product-slider-inner">
                <div data-items="1" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="true" data-dots="true" data-animation-in="fadeIn" data-animation-out="fadeOut" class="owl-carousel owl-style-minimal">
                  <div class="item">
                    <figure>
                      <img src="<?php echo base_url('site_assets/images/offer/'.$offer['image'])?>" alt="" width="370" height="278" style="height:280px;"/> 
                    </figure>
                    <figure>
                      <img src="<?php echo base_url('site_assets/images/offer/'.$offer['image'])?>" alt="" width="370" height="278" style="height:280px;"/> 
                    </figure>
                  </div>
                </div>
              </div>
            </div>
            <?php if($offer['special'] == 1):?>
              <div class="product-label-wrap product-label-wrap-left">
                <div class="label-custom product-label label-danger"><?php echo translate('Special', $this->data['language']) ?></div>
              </div>
            <?php endif; ?>
            <div class="product-main">
              <h6 class="product-header"><a href="#"><?php echo translate($offer['name'], $this->data['language']) ?></a></h6>
            </div>
            <div class="product-footer">
              <div class="product-footer-front pricing-wrap">
                <p class="pricing-object pricing-object-sm price-current"><span class="small small-middle"><?php echo translate($offer['currency'], $this->data['language']) ?></span><span class="price"><?php echo translate($offer['price'], $this->data['language']) ?></span><span class="small small-bottom"></span></p>
              </div>
              <div class="product-footer-behind"><a href="<?php echo base_url('clients/offers/offerd/'.$offer['id']);?>" class="btn btn-icon btn-icon-left btn-primary product-control"><span class="icon icon-sm fa-shopping-cart"></span></a></div>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>