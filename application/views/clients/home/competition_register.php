<?php $this->load->view($this->data['inner_slider']);?>

<!-- Tabs element style 5 - section with custom paddings -->
<section class="hg_section pt-80 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <!-- Title element -->
                <div class="kl-title-block text-center">
                    <!-- Title with custom font size and thin style -->
                    <h3 class="tbk__title fs-xxl fw-thin">
                    <span class="fw-semibold fs-xxxl fs-xs-xl tcolor" style="color: #cd2122 !important;">
                        Register For</span> <?php echo $competition['title']?>
                    </h3>
                    <!--/ Title with custom font size and thin style -->

                    <!-- Spacer with custom height -->
                    <div style="height: 50px;"></div>
                </div>
                <!--/ Title element -->
            </div>
            <!--/ col-sm-12 col-md-12 -->

            <div class="col-sm-12 col-md-12">
                <!-- Tabs element style 5 -->
                <div class="tabbable tabs_style5">
                    <!-- Navigation menu -->
                    <ul class="nav fixclear">
                        <!-- Tab #1 -->
                        <li class="">
                            <a href="#tabs_competitor_info"  
                            <?php echo !isset($project) ? 'class="active"' : 'style="pointer-events: none"'?> data-toggle="tab">
                               <?php echo translate('Competitor Info', $this->data['language']) ?> 
                               <?php echo isset($project) ? '<i class="fa fa-check"> </i>' : ''?>      
                            </a>
                        </li>
                        <!--/ Tab #1 -->

                        <!-- Tab #2 -->
                        <li class="">
                            <a href="#tabs_payment_info"  <?php echo isset($project) && $project['status'] == 1 ? 'class="active"' : 'style="pointer-events: none"'?> data-toggle="tab">
                                <?php echo translate('Payment', $this->data['language']) ?>    
                                <?php echo isset($project) && $project['status'] >= 2 ? '<i class="fa fa-check"> </i>' : ''?>
                            </a>
                        </li>
                        <!--/ Tab #2 -->

                        <!-- Tab #3 -->
                        <li class="">
                            <a href="#tabs_add_project"<?php echo isset($project) && ($project['status'] == 2 || $project['status'] == 3) ? 'class="active"' : 'style="pointer-events: none"'?> data-toggle="tab">
                            <?php echo translate('Add Project', $this->data['language']) ?>    
                                <?php echo isset($project) && $project['status'] == 4 ? '<i class="fa fa-check"> </i>' : ''?>
                            </a>
                        </li>

                        <li class="">
                            <a href="#tabs_successfully_registerd"<?php echo isset($project) && $project['status'] == 4 ? 'class="active"' : 'style="pointer-events: none"'?> data-toggle="tab">
                            <?php echo translate('Successfully Registerd', $this->data['language']) ?>    
                                <?php echo isset($project) && $project['status'] == 4 ? '<i class="fa fa-check"> </i>' : ''?>
                            </a>
                        </li>
                        <!--/ Tab #3 -->
                    </ul>
                    <!--/ Navigation menu -->

                    <!-- Tabs content -->
                    <div class="tab-content">
                        <!-- Tab #1 -->
                        <div class="tab-pane fade <?php echo !isset($project) ? 'show active' : ''?>" id="tabs_competitor_info">
                            <form action="<?php echo base_url('clients/home/create_update_project/'.$competition_id.'/1')?>" method="post" 
                            id="competitor_info_form" class="contact_form row" enctype="multipart/form-data">
                            <!-- Response wrapper -->
                                <div class="cr_response"></div>
                                <div class="col-sm-6 kl-fancy-form">
                                    <input type="text" name="school" id="pr_school" class="form-control" placeholder="Please enter your school" value="" tabindex="1" required>
                                    <label class="control-label">
                                        <?php echo translate('Academy, School, Entity Name', $this->data['language']) ?>
                                    </label>
                                    <span class="text-danger" style="font-size: 12px; display:none" id="error-pr_school">
                                        <?php echo translate('Please enter a valid Academy, School, Entity Name', $this->data['language']) ?>
                                    </span>
                                </div>
                                <div class="col-sm-12">
                                    <ul class="payment_methods methods">
                                        <li class="payment_method_cheque">
                                            <input id="with_trainer_radio" type="radio" class="input-radio" 
                                            name="trainer" value="with trainer" checked="checked">
                                            <label for="with_trainer_radio" style="font-weight:600 !important ;font-size: 14px !important;"> 
                                                <?php echo translate('With Coach', $this->data['language']) ?>
                                            </label>
                                            <input id="without_trainer_radio" type="radio" class="input-radio" 
                                            name="trainer" value="without trainer">
                                            <label for="without_trainer_radio" style="font-weight:600 !important ;font-size: 14px !important;"> 
                                                <?php echo translate('Without Coach', $this->data['language']) ?>
                                            </label>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-sm-4 kl-fancy-form">
                                    <input type="text" name="trainer_name" id="trainer_name" class="form-control" placeholder="<?php echo translate('Coach Name', $this->data['language']) ?>" value="" tabindex="1">
                                    <label class="control-label">
                                        <?php echo translate('Coach Name', $this->data['language']) ?>
                                    </label>
                                    <span class="text-danger" style="font-size: 12px; display:none" id="error-trainer_name">
                                        <?php echo translate('Please enter a valid name', $this->data['language']) ?>
                                    </span>
                                </div>

                                <div class="col-sm-12">
                                    <!-- Contact form send button -->
                                    <input type="submit" id="competitor_info_form_submit" name="cr_submit" 
                                    class="btn btn-fullcolor" value="<?php echo translate('Save', $this->data['language']) ?>" style="background-color: #cd2122 !important; color: #FFF;" disabled>
                                </div>
                            </form>
                        </div>
                        <!--/ Tab #1 -->

                        <!-- Tab #2 -->
                        <div class="tab-pane fade <?php echo isset($project) && $project['status'] == 1 ? 'show active' : ''?>" id="tabs_payment_info">
                            <h4>
                                <table style="width: 30%;">
                                    <tr class="order-total" style="border: lightslategray;">
                                        <th style="color: #cd2122 !important;">
                                            <?php echo translate('Total', $this->data['language']) ?>
                                        </th>
                                        <td style="padding-left: 25px !important;">
                                            <strong>
                                                <span class="amount"><?php echo money_formater($competition['price'],$competition['currency']) ?></span>
                                            </strong>
                                        </td>
                                    </tr>
                                </table>    
                            </h4><br>
                           <form action="<?php echo base_url('clients/home/create_update_project/'.$competition_id.'/2')?>" method="post" 
                            id="payment_form" class="contact_form row" enctype="multipart/form-data">
                            <!-- Response wrapper -->
                                <div class="cr_response"></div>
                                <div class="col-sm-12">
                                <input type="text" name="project_id" value="<?php echo isset($project['id']) ? $project['id'] : '' ?>" style="display: none;">
                                <input type="text" name="competition_price" value="<?php echo $competition['price']?>" style="display: none;">
                                   <div id="payment" class="kl-store-checkout-payment">
                                        <ul class="payment_methods methods">
                                            <li class="payment_method_cheque">
                                                <input id="payment_method_cheque" type="radio" class="input-radio" name="payment_method" value="cash" checked="checked">
                                                <label for="payment_method_cheque">
                                                <?php echo translate('Cash Payment', $this->data['language']) ?> </label>
                                            </li>
                                            <li class="payment_method_vodafone" style="padding-top: 30px;padding-bottom:15px;">
                                                <input id="payment_method_vodafone" type="radio" class="input-radio" name="payment_method" value="vodafone cash" >
                                                <label for="payment_method_vodafone">
                                                <?php echo translate('Vodafone Cash', $this->data['language']) ?> 
                                                <img src="<?php echo base_url('site_assets/images/vodafone-cash.png')?>" style="height: 30px !important;width:60px;" alt="vodafone Acceptance Mark"></label>
                                            </li>
                                            
                                            <li class="payment_method_paypal">
                                                <input id="payment_method_paypal" type="radio" class="input-radio" name="payment_method" value="paypal">
                                                <label for="payment_method_paypal">
                                                <?php echo translate('PayPal', $this->data['language']) ?> <img src="https://www.paypalobjects.com/webstatic/mktg/Logo/AM_mc_vs_ms_ae_UK.png" alt="PayPal Acceptance Mark"></label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6 kl-fancy-form">
                                        <input type="file" name="payment_attach" id="payment-payment_attach" class="form-control" value="" tabindex="1" required>
                                        <label class="control-label">
                                            Payment Attach
                                        </label>
                                        <span class="text-danger" style="font-size: 12px; " id="error-payment-payment_attach">
                                            Please upload payment attach
                                        </span>
                                    </div>
                                    <div class="col-sm-12">
                                        <!-- Contact form send button -->
                                        <input type="submit" id="payment_form_submit" name="cr_submit" 
                                        class="btn btn-fullcolor" value="<?php echo translate('Save', $this->data['language']) ?>" style="background-color: #cd2122 !important; color: #FFF;" disabled>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--/ Tab #2 -->

                        <!-- Tab #3 -->
                        <div class="tab-pane fade <?php echo isset($project) && ($project['status'] == 2 || $project['status'] == 3) ? 'show active' : ''?>" id="tabs_add_project">
                            <!-- Title -->
                            <?php if(isset($project) && $project['status'] == 2){?>
                                <h4 class="ib2-info-message">
                                    <?php echo translate('Waiting Payment Confirmation', $this->data['language']) ?>
								</h4>
                            <?php }elseif(isset($project) && $project['status'] == 3){?>  
                                <form action="<?php echo base_url('clients/home/create_update_project/'.$competition_id.'/4')?>" method="post" 
                                    id="add_project_metaData_form" class="contact_form row" enctype="multipart/form-data">
                                    <!-- Response wrapper -->
                                        <div class="cr_response"></div>
                                        <input type="text" name="project_id" value="<?php echo $project['id']?>" style="display: none;">
                                        
                                        <div class="col-sm-6 kl-fancy-form">
                                            <input type="text" name="project_title" id="project_metaData_title" class="form-control" placeholder="Enter the project title" value="" tabindex="1" required>
                                            <label class="control-label">
                                                Project Title
                                            </label>
                                            <span class="text-danger" style="font-size: 12px; display:none" id="error-project_metaData_title">
                                                Please enter a valid title
                                            </span>
                                        </div>

                                        <div class="col-sm-6 kl-fancy-form">
                                            <input type="text" name="project_link" id="project_metaData_link" class="form-control" placeholder="Please enter your project link" value="" tabindex="1" required>
                                            <label class="control-label">
                                                Project Link
                                            </label>
                                            <span class="text-danger" style="font-size: 12px; display:none" id="error-project_metaData_link">
                                                Please enter a valid link
                                            </span>
                                        </div>


                                        <div class="col-sm-6 kl-fancy-form">
                                            <input type="file" name="project_image" id="project_metaData_image" class="form-control" placeholder="Please enter your first name" value="" tabindex="1" required>
                                            <label class="control-label">
                                                Project Image
                                            </label>
                                            <span class="text-danger" style="font-size: 12px; display:none" id="error-project_metaData_image">
                                                Please upload project image
                                            </span>
                                        </div>

                                        <div class="col-sm-12 kl-fancy-form">
                                            <textarea name="project_description" id="project_metaData_description" class="form-control" cols="30" rows="10" placeholder="Your message" tabindex="4" required></textarea>
                                            <label class="control-label">
                                                Project Description
                                            </label>
                                            <span class="text-danger" style="font-size: 12px; display:none" id="error-project_metaData_description">
                                                Please enter project description
                                            </span>
                                        </div>

                                        
                                        <div class="col-sm-12">
                                            <!-- Contact form send button -->
                                            <input type="submit" id="project_metaData_form_submit" name="cr_submit" 
                                            class="btn btn-fullcolor" value="<?php echo translate('Save', $this->data['language']) ?>" style="background-color: #cd2122 !important; color: #FFF;" disabled>
                                        </div>
                                    </form>
                            <?php }?>    
                        </div>
                        <!--/ Tab #3 -->
                        <!-- tab4 -->
                        <div class="tab-pane fade <?php echo isset($project) && $project['status'] == 4 ? 'show active' : ''?>" id="tabs_successfully_registerd">
                            <?php if( isset($project) && $project['status'] == 4){?>
                                <div class="row" style="background-color: #FFF;">
                                    <div class="col-md-6">
                                        <h1><?php echo translate('Thank You!', $this->data['language']) ?></h1>
                                        <p><?php echo translate('you have been registered successfully', $this->data['language']) ?></p>
                                        <div style="background-color:#efefef; border-radius: 9px;">
                                            <div style="padding:10px;">
                                                <strong style="font-size: 15px;"><?php echo translate('Copy your project link', $this->data['language']) ?></strong>
                                                <p>
                                                    <span id="project_link_text" style="font-size: 12px;">
                                                        <?php echo isset($project) ? base_url('clients/home/project/'.$project['code']) : ''?>
                                                    </span> 
                                                    <a href="javascript:void(0);" onclick="copyTextToClipboard()"><i class="fa fa-copy"></i></a>
                                                </p>
                                                <a href=" <?php echo isset($project) ? base_url('clients/home/project/'.$project['code']) : '#'?>" class="btn btn-fullcolor" style="background-color: #cd2122 !important;">
                                                    <?php echo translate('Visit Your Project', $this->data['language']) ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="background-color: #FFF; text-align:center;">
                                        <img id="success-image" src="<?php echo base_url('site_assets/images/animation_500_kw7u44j1.gif')?>" alt="">
                                    </div>
                                    <div class="col-md-3"> </div>
                                </div>
                            <?php }?>
                          </div>
                        <!-- /tab4 -->
                    </div>
                    <!--/ Tabs content -->
                </div>
                <!--/ Tabs element style 5 -->
            </div>
            <!--/ col-sm-12 col-md-12 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>


<script>
     $(document).ready(function(){
        $('#competitor_info_form').keyup(function(){
            validateCompetitor_info_form();
        });
        $('#payment_form').change(function(){
            validatePayment_form();
        });
        $('#add_project_metaData_form').on('keyup change', function(event){
            validateProject_metaData_form(event);
        });
    });

    function validateCompetitor_info_form(){
        let school = validateField('pr_school', 'text');
        if(school === true){
            $('#competitor_info_form_submit').attr('disabled',false);
        }else{
            $('#competitor_info_form_submit').attr('disabled',true);
        }
    }

    function validatePayment_form(){
        let attach = $('#payment-payment_attach').val();
        if(attach !== ''){
            $(`#error-payment-payment_attach`).hide();
            $('#payment_form_submit').attr('disabled',false);
        }else{
            $(`#error-payment-payment_attach`).show();
            $('#payment_form_submit').attr('disabled',true);
        }
    }

    function validateProject_metaData_form(event){
        let title = validateField('project_metaData_title', '');
        let project_link = validateField('project_metaData_link', 'link');
        let description = validateField('project_metaData_description', '');
        let attach = validateField('project_metaData_image', '');
        let image = $('#project_metaData_image').val();
        if(event.target.id === 'project_metaData_image'){
            console.log(event.target.id === 'project_metaData_image');
            image !== '' ?  $(`#error-project_metaData_image`).hide() :  $(`#error-project_metaData_image`).show();
        }
        if(project_link === true && title === true && attach === true  && description === true){
            $('#project_metaData_form_submit').attr('disabled',false);
        }else{
            $('#project_metaData_form_submit').attr('disabled',true);
        }
    }

    function copyTextToClipboard(){
        let copyText = $("#project_link_text").text();
        navigator.clipboard.writeText(copyText);
    }
</script>