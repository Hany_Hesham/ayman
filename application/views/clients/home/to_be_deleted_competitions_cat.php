<?php $this->load->view($this->data['inner_slider']);?>

<section class="hg_section pt-80 pb-80">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <!-- Title element -->
                <div class="kl-title-block text-center">
                    <!-- Title with custom font size and thin style -->
                    <h3 class="tbk__title fs-xxl fw-thin">
                    Choose Your<span class="fw-semibold fs-xxxl fs-xs-xl tcolor" style="color: #cd2122 !important;">
                      Competition</span> 
                    </h3>
                    <!--/ Title with custom font size and thin style -->

                    <!-- Spacer with custom height -->
                    <div style="height: 50px;"></div>
                </div>
                <!--/ Title element -->
            </div>
            <!--/ col-sm-12 col-md-12 -->

            <div class="col-sm-12 col-md-12">
                <!-- Pricing table element with 2 columns -->
                <div class="pricing-table-element green" data-columns="2">
                    <!-- Plan -->
                    <div class="plan-column">
                        <ul>
                            <!-- Title -->
                            <li class="plan-title">
                                <div class="inner-cell" style="color: #cd2122 !important;">
                                   Weekly Competitions
                                </div>
                            </li>
                            <!--/ Title -->

                            <!-- Price -->
                            <li class="subscription-price">
                                <div class="inner-cell">
                                    <span class="currency">$</span>
                                    <span class="price">6.99</span>
                                    per month
                                </div>
                            </li>
                            <!--/ Price -->

                            <li>
                                <div class="inner-cell">
                                    <img src="<?php echo base_url('site_assets/images/4096.jpg')?>">
                                </div>
                            </li>
                            
                            <li>
                                <div class="inner-cell">
                                Unlimited Users  Unlimited Users  Unlimited Users
                                    Unlimited Users  Unlimited Users  Unlimited Users
                                    Unlimited Users  Unlimited Users
                                </div>
                            </li>
                            <!--/ Cell #1 -->

                            <!-- Cell #9 -->
                            <li>
                                <div class="inner-cell">
                                    <!-- Button -->
                                    <a href="<?php echo base_url('clients/home/get_competitions/2')?>" class="btn btn-fullcolor" style="background-color: #cd2122 !important;">
                                        Register Now
                                    </a>
                                    <!--/ Button -->
                                </div>
                            </li>
                            <!--/ Cell #9 -->
                        </ul>
                    </div>
                    <!--/ Plan -->

                    <!-- Featured Plan -->
                    <div class="plan-column featured">
                        <ul>
                            <!-- Title -->
                            <li class="plan-title">
                                <div class="inner-cell">
                                    International Kids Coding Competitions
                                </div>
                            </li>
                            <!--/ Title -->

                            <!-- Price -->
                            <li class="subscription-price">
                                <div class="inner-cell" style="background-color: #cd2122 !important;">
                                    <span class="currency">$</span>
                                    <span class="price">99.99</span>
                                    per month
                                </div>
                            </li>

                            <li>
                                <div class="inner-cell">
                                    <img src="<?php echo base_url('site_assets/images/kids-learn-robotics.webp')?>">
                                </div>
                            </li>

                            <li>
                                <div class="inner-cell">
                                    Unlimited Users  Unlimited Users  Unlimited Users
                                    Unlimited Users  Unlimited Users  Unlimited Users
                                    Unlimited Users  Unlimited Users
                                </div>
                            </li>
                            <!--/ Cell #1 -->

                           
                            <!-- Cell #9 -->
                            <li>
                                <div class="inner-cell">
                                    <!-- Button -->
                                    <a href="<?php echo base_url('clients/home/get_competitions/1')?>" class="btn btn-fullcolor" style="background-color: #cd2122 !important;">
                                        Register Now
                                    </a>
                                    <!--/ Button -->
                                </div>
                            </li>
                            <!--/ Cell #9 -->
                        </ul>
                    </div>
                    <!--/ Featured Plan -->
                </div>
                <!--/ .pricing-table-element .green data-columns="2" -->
            </div>
            <!--/ col-sm-12 col-md-12 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>