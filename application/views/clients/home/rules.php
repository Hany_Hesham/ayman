<?php $this->load->view($this->data['inner_slider']);?>
<section class="hg_section bg-white pt-80 pb-80">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <!-- Title element -->
                <div class="kl-title-block text-center">
                    <!-- Title with custom font size and thin style -->
                    <h3 class="tbk__title fs-xxl fw-thin">
                        <span class="fw-semibold fs-xxxl fs-xs-xl tcolor">
                        <?php echo translate('Our Rules', $this->data['language']) ?></span>
                    </h3>
                    <!--/ Title with custom font size and thin style -->

                    <!-- Title -->
                    <!-- Spacer with custom height -->
                    <div style="height: 50px;"></div>
                </div>
                <!--/ Title element -->
            </div>
            <!--/ col-sm-12 col-md-12 -->

            <div class="col-sm-12 col-md-12">
                <!-- Vertical tabs style 1 element -->
                <div class="vertical_tabs kl-style-1">
                    <!-- Tabs wrapper -->
                    <div class="tabbable">
                        <!-- Navigation menu -->
                        <ul class="nav fixclear">
                            <!--  -->
                            <?php $count = 0;
                                foreach($rules as $rule){
                                $count++;
                            ?>
                                <li>
                                    <!-- Tab link id -->
                                    <a style="font-size: 14px !important;"
                                     href="#tab-<?php echo $rule['id']; ?>" class="<?php echo $count == 1 ? 'active' : ''?>" data-toggle="tab">
                                        <!-- Icon = .icon-gi-ico-5 -->
                                        <span class="<?php echo $rule['icon']; ?>"></span>
                                        <!-- Title -->
                                        <?php echo translate($rule['title'], $this->data['language']) ?>
                                    </a>
                                    <!--/ Tab link id -->
                                </li>
                            <?php }?>
                            <!--/ -->
                        </ul>
                        <!-- Navigation menu -->

                        <!-- Tabs content -->
                        <div class="tab-content">
                            <!--  -->
                           <?php $counter = 0;
                                foreach($rules as $rule){
                                $counter++;
                            ?>
                                <div class="tab-pane fade <?php echo $counter == 1 ? 'show active' : ''?>" id="tab-<?php echo $rule['id']; ?>">
                                    <!-- Title -->
                                    <h4>
                                        <?php echo translate($rule['title'], $this->data['language']) ?>
                                    </h4>
                                    <!-- Title -->

                                    <!-- Description -->
                                    <p>

                                        <?php echo translate($rule['content'], $this->data['language']) ?> 
                                        
                                        <!-- Image link -->
                                        <?php if($rule['files']){?>
                                            <div class="col-sm-12 col-md-8 offset-md-2">
                                                <!-- Album gallery element default style -->
                                                <div class="mfp-gallery images imgboxes_style1">
                                                    <?php $i = 0;
                                                    foreach($rule['files'] as $file){
                                                    $i++;
                                                    ?>
                                                        <!-- Main image album gallery -->
                                                        <a href="<?php echo base_url('site_assets/uploads/rules/'. $file['file_name'])?>" 
                                                            class="hoverBorder" data-lightbox="mfp" data-mfp="image" title="<?php echo $file['caption']?>">
                                                            <?php if($i==1){?>    
                                                                <img src="<?php echo base_url('site_assets/uploads/rules/'. $file['file_name'])?>"
                                                                class="img-fluid" alt="" title="" />
                                                                <h6>
                                                                <?php echo translate('View More...', $this->data['language']) ?>
                                                                </h6>
                                                            <?php }?>
                                                        </a> 
                                                    <?php }?>
                                                </div>
                                                <!--/ Album gallery element default style -->
                                            </div>
                                        <?php }?>

                                        <!--/ Image link -->
                                    </p>
                                    <?php if($rule['file_name']){ ?>
                                        <a class="btn btn-fullcolor " style="background-color: #cd2122 !important;color:#FFF;" 
                                            href="<?php echo base_url('site_assets/uploads/rules/'.$rule['file_name'])?>"
                                            download>
                                            <i class="fas fa-download"></i>
                                            <?php echo translate('Download File', $this->data['language']) ?>
                                        </a>
                                    <?php }?>
                                    <?php if($rule['video_link']){ ?>
                                        <a class="btn btn-fullcolor media-container__link" target="_blank" href="<?php echo $rule['video_link']?>" data-lightbox="iframe">
                                            <span class="fas fa-video" > </span>
                                            <?php echo translate('Watch Video', $this->data['language']) ?>
                                        </a>
                                    <?php }?>
                                    <!--/ Description -->
                                </div>
                             <?php }?>
                            <!--/  -->
                        </div>
                        <!--/ Tabs content -->
                    </div>
                    <!--/ Tabs wrapper -->
                </div>
                <!--/ Vertical tabs style 1 element -->
            </div>
            <!--/ col-sm-12 col-md-12 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>