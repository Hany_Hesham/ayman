 <section>
    <div data-loop="false" data-autoplay="false" data-simulate-touch="true" class="swiper-container swiper-slider swiper-variant-1 bg-gray-base">
      <div class="swiper-wrapper text-center">
        <?php foreach($sliders as $slider){?>
          <div data-slide-bg="<?php echo base_url('site_assets/images/slider/'.$slider['img'])?>" class="swiper-slide">
            <div class="swiper-slide-caption">
              <div class="shell">
                <div class="range range-sm-center">
                  <div class="cell-sm-11 cell-md-10 cell-lg-9">
                    <h2 data-caption-animate="fadeInUp" data-caption-delay="0s" class="slider-header"><?php echo translate($slider['first_title'], $this->data['language']) ?></h2>
                    <p data-caption-animate="fadeInUp" data-caption-delay="100" class="text-bigger slider-text"> <?php echo translate($slider['second_title'], $this->data['language']) ?></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php }?>
      </div>
      <div class="swiper-scrollbar"></div>
      <div class="swiper-nav-wrap">
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
      </div>
    </div>
  </section>