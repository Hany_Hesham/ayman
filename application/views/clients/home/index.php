<style>
  .circle-text-box .wpk-circle-span:after {
    content: ''; position: absolute; left: 0; top: 0; 
    display:inline-block; background-color: #cd2122; 
    width: 60px; height: 60px; line-height: 60px; border-radius: 50%;
     -webkit-transition: all 0.2s cubic-bezier(0.680, 0, 0.265, 1); 
     transition: all 0.2s cubic-bezier(0.680, 0, 0.265, 1);}

</style>
<?php $this->load->view($this->data['slider']);?>      
	<!-- Recent Work carousel - full width section -->
  <!-- Featured image + Description element - section with custom paddings -->
  <div class="hg_section pt-80 pb-80">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-8">
						<!-- Media container element with custom height 370px(.h-370) -->
						<div class="media-container h-370">
							<!-- Background source -->
							<div class="kl-bg-source">
								<!-- Background image -->
								<div class="kl-bg-source__bgimage" style="background-image: url(<?php echo base_url('site_assets/images/inner_slider/Banner.png')?>); background-repeat: no-repeat; background-attachment: scroll; background-position-x: center; background-position-y: top; background-size: cover;">
								</div>
								<!--/ Background image -->
							</div>
							<!--/ Background source -->
						</div>
						<!--/ Media container element with custom height 370px(.h-370) -->
					</div>
					<!--/ col-sm-12 col-md-12 col-lg-8 -->

					<div class="col-sm-12 col-md-12 col-lg-4">
						<!-- Custom full column -->
						<div class="col-full d-flex">
							<!-- Row with custom paddings -->
							<div class="row">
								<div class="col-md-12 col-sm-12 align-self-center">
									<!-- Title with custom bottom padding -->
									<div class="kl-title-block text-left pb-20">
										<!-- Title -->
										<h3 class="tbk__title">
											<?php echo translate($what_is_readysteadyshare['header'], $this->data['language']) ?>
										</h3>
									</div>
									<!-- Title with custom bottom padding -->

									<!-- Text box element -->
									<div class="text_box">
										<!-- Description -->
										<p>
											<?php echo translate($what_is_readysteadyshare['paragraph'], $this->data['language']) ?>
										</p>
										<!--/ Description -->

										<!-- Button full color style -->
										<a href="<?php echo base_url('about_us')?>" class="btn-element btn btn-fullcolor" title="READ MORE">
											<span><?php echo translate('Read More', $this->data['language']) ?></span>
										</a>
									</div>
									<!--/ Text box element -->
								</div>
								<!--/ col-md-12 col-sm-12 align-self-center -->
							</div>
							<!--/ row p-top-6 p-left-6 -->
						</div>
						<!--/ col-full d-flex -->
					</div>
					<!--/ col-sm-12 col-md-12 col-lg-4 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</div>
		<!--/ Featured image + Description element - section with custom paddings -->
	<section class="hg_section">
		<div class="container-fluid">
			<!-- Recent Work carousel 3 default style element -->
			<div class="recentwork_carousel recentwork_carousel_v3">
				<!-- Top container -->
				<div class="container recentwork_carousel__top-container">
					<div class="row">
						<div class="col-sm-12">
							<!-- Title -->
							<h3 class="recentwork_carousel__title m_title">
								<?php echo translate('RSS Winners', $this->data['language']) ?>
							</h3>
							<!--/ Title -->
							<div class="hgSlickNav recentwork_carousel__controls clearfix"></div>
						</div>
						<!--/ col-sm-12 -->
					</div>
					<!--/ row -->
				</div>
				<!--/ Top container -->
				<!-- Carousel wrapper -->
				<div class="recentwork_carousel__crsl-wrapper">
					<div class="recent_works3 recentwork_carousel__crsl js-slick" data-slick='{
						"infinite": true,
						"slidesToShow": 5,
						"slidesToScroll": 1,
						"swipeToSlide": true,
						"autoplay": true,
						"autoplaySpeed": 3000,
						"speed": 500,
						"cssEase": "ease-in-out",
						"arrows": true,
						"appendArrows": ".recentwork_carousel_v3 .hgSlickNav",
						"responsive": [
							{
								"breakpoint": 1199,
								"settings": {
									"slidesToShow": 3
								}
							},
							{
								"breakpoint": 767,
								"settings": {
									"slidesToShow": 2
								}
							},
							{
								"breakpoint": 480,
								"settings": {
									"slidesToShow": 1
								}
							}
						]
					}'>
						<?php foreach($winners as $winner) {?>
						<!-- Item #1 -->
							<div class="recent-work_carousel-item">
								<!-- Portfolio link container -->
								<?php if(isset($winner['project'])) {?>
									<a href="<?php echo base_url("clients/home/project/".$winner['project']['code'])?>" class="recentwork_carousel__link">
										<!-- Hover container -->
										<div class="hover recentwork_carousel__hover">
											<!-- Item image with custom height -->
											<div class="carousel-item--height280">
												<img src="<?php echo base_url('site_assets/uploads/users_profiles/'.$winner['profile_picture'])?>" class="recentwork_carousel__img cover-fit-img" alt="" title="" />
											</div>
											<!--/ Item image with custom height -->

											<!-- Hover shadow overlay -->
											<span class="hov recentwork_carousel__hov"></span>
											<!--/ Hover shadow overlay -->
										</div>
										<!--/ Hover container -->

										<!-- Content details -->
										<div class="details recentwork_carousel__details">
											<!-- Tags/Category -->
											<span class="recentwork_carousel__cat">
												<?php echo $winner['name'].' '. $winner['middle_name'].' '. $winner['last_name']?>
											</span>
											<span class="recentwork_carousel__cat" style="margin-top: 5px; background-color:#34CC99;">
												<?php echo $winner['age'].' '.translate('Years Old', $this->data['language']) ?>
											</span>
											<!--/ Tags/Category -->
										</div>
										<!--/ Content details -->
									</a>
								<?php }?>
								<!--/ Portfolio link container -->
							</div>
							<!--/ Item #1 -->
						<?php }?>
					</div>
					<!--/ .recent_works3 .recentwork_carousel__crsl .js-slick -->
				</div>
				<!--/ Carousel wrapper - .recentwork_carousel__crsl-wrapper -->
			</div>
			<!--/ Recent Work carousel 3 default style element -->
		</div>
		<!--/ container-fluid -->
	</section>
	<!--/ Recent Work carousel - full width section -->
	<!-- Video background + text section -->
		<section class="hg_section--relative pt-100 pb-100">
			<!-- Video Background -->
			<div class="kl-bg-source">
				<!-- Video background container -->
				<div class="kl-video-container kl-bg-source__video">
					<!-- Video wrapper -->
					<div class="kl-video-wrapper video-grid-overlay">
						<!-- Self Hosted Video Source -->
						<div class="kl-video valign halign" style="width: 100%; height: 100%;" data-setup='{ 
							"position": "absolute", 
							"loop": true, 
							"autoplay": true, 
							"muted": true, 
							"mp4": "<?php echo base_url('site_assets/videos/Banner Video.mp4')?>", 
							"poster": "<?php echo base_url('site_assets/videos/Screenshot 2022-01-08 021244.png')?>", 
							"video_ratio": "1.7778" }'>
						</div>
						<!-- Self Hosted Video Source -->
					</div>
					<!--/ Video wrapper -->
				</div>
				<!--/ Video background container (.kl-bg-source__video) -->

				<!-- Gradient overlay -->
				<div class="kl-bg-source__overlay" style="background:rgba(61,61,61,0.7)">
				</div>
				<!--/ Gradient overlay -->
			</div>
			<!--/ Video Background -->

			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="text-center">
							<h4 class="fs-xxl fw-thin text-white mb-20">
								 <em>Ready Steady Share</em> Scratch Programming Competition
							</h4>

							<h5 class="fs-l fw-thin mb-80 text-white opacity7">
              <?php echo translate('Participants start their journey by 
                    thinking about their project and what they want to accomplish with it', $this->data['language']) ?>
							</h5>
						</div>
						<!-- /.text-center text-white -->
					</div>
					<!--/ .col-md-12 -->

					<div class="col-sm-12 col-md-12 col-lg-4">
						<div class="stg-alignleft text-center-xs mb-60">
							<!-- Icon -->
							<i class="kl-icon fas fa-share ico-size-xxs custom-color hidden-xs"></i>

							<!-- Title with medium size, white and margin bottom 20 -->
							<h4 class="fs-m text-white mb-20">
                <?php echo translate($about_us_design['header'], $this->data['language']) ?>
							</h4>

							<!-- Content -->
							<div class="content text-white opacity9">
								<p>
                  <?php echo translate($about_us_design['paragraph'], $this->data['language']) ?>
								</p>
							</div>
							<!-- /.content -->
						</div>
						<!-- / stg-alignleft mb-50 -->
					</div>
					<!-- / col-sm-12 col-md-12 col-lg-4 -->

					<div class="col-sm-12 col-md-12 col-lg-4">
						<div class="stg-alignleft text-center-xs mb-60">
							<!-- Icon -->
							<i class="kl-icon fas fa-share ico-size-xxs custom-color hidden-xs"></i>

							<!-- Title with medium size, white and margin bottom 20 -->
							<h4 class="fs-m text-white mb-20">
                <?php echo translate($about_us_coding['header'], $this->data['language']) ?>
            	</h4>

							<!-- Content -->
							<div class="content text-white opacity9">
								<p>
                  <?php echo translate($about_us_coding['paragraph'], $this->data['language']) ?>
								</p>
							</div>
							<!-- /.content -->
						</div>
						<!-- /.icon-box stg-alignleft mb-50 -->
					</div>
					<!-- / col-sm-12 col-md-12 col-lg-4 -->

					<div class="col-sm-12 col-md-12 col-lg-4">
						<div class="stg-alignleft text-center-xs mb-60">
							<!-- Icon -->
							<i class="kl-icon fas fa-share ico-size-xxs custom-color hidden-xs"></i>

							<!-- Title with medium size, white and margin bottom 20 -->
							<h4 class="fs-m text-white mb-20">
                <?php echo translate($about_us_voting['header'], $this->data['language']) ?>
							</h4>

							<!-- Content -->
							<div class="content text-white opacity9">
								<p>
                  <?php echo translate($about_us_voting['paragraph'], $this->data['language']) ?>
								</p>
							</div>
							<!-- /.content -->
						</div>
						<!-- /.icon-box stg-alignleft mb-50 -->
					</div>
					<!-- / col-sm-12 col-md-12 col-lg-4 -->

					<div class="col-md-12">
						<div class="text-center text-white">

							<h3 class="fs-xxl fw-normal text-white">
								<strong><?php echo translate('with Scratch, ', $this->data['language']) ?></strong>
                <?php echo translate('the world is your oyster!', $this->data['language']) ?>
							</h3>
						</div>
						<!-- /.text-center text-white -->
					</div>
					<!-- / .col-md-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Video background + text section -->

		<!-- Services - section white and custom paddings -->
		<section class="hg_section bg-white pt-100 pb-80">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-4">
						<!-- Title element -->
						<div class="kl-title-block tbk-symbol--line">
							<!-- Title with custom size, weight and color style -->
							<h3 class="tbk__title fs-xxl fw-extrabold gray">
              <?php echo translate('Age Categories', $this->data['language']) ?><br> 
							</h3>

							<!-- Title bottom line symbol -->
							<div class="tbk__symbol">
								<span></span>
							</div>
						</div>
						<!--/ Title element -->
					</div>
					<!--/ col-sm-12 col-md-12 col-lg-4 -->

					<div class="col-sm-12 col-md-12 col-lg-8">
						<div class="row">
							<div class="col-sm-12 col-md-3 col-lg-3">
								<!-- Icon box float left -->
								<div class="kl-iconbox kl-iconbox--fleft text-left">
									<div class="kl-iconbox__inner">
						

										<!-- Content wrapper -->
                    <div class="circle-text-box style2">
                      <!-- Circle title wrapper -->
                      <div class="circle-headline">
                        <!-- Circle title -->
                        <span class="wpk-circle-span">
                          <span style="font-size: 18px;font-weight:bold;">7-8</span>
                        </span>
                        <!--/ Circle title -->

                        <!-- Title -->
                        <h4 class="wpk-circle-title">
                          Story
                        </h4>
                        <!--/ Title -->
                      </div>
                      <!--/ Description -->
                    </div>
										<!--/ Content wrapper -->
									</div>
									<!--/ .kl-iconbox__inner -->
								</div>
								<!--/ Icon box float left -->
							</div>
              <div class="col-sm-12 col-md-4 col-lg-4">
								<!-- Icon box float left -->
								<div class="kl-iconbox kl-iconbox--fleft text-left">
									<div class="kl-iconbox__inner">
						

										<!-- Content wrapper -->
                    <div class="circle-text-box style2">
                      <!-- Circle title wrapper -->
                      <div class="circle-headline">
                        <!-- Circle title -->
                        <span class="wpk-circle-span">
                          <span style="font-size: 18px;font-weight:bold;">9-11</span>
                        </span>
                        <!--/ Circle title -->

                        <!-- Title -->
                        <h4 class="wpk-circle-title">
                          story and quiz
                        </h4>
                        <!--/ Title -->
                      </div>
                      <!--/ Description -->
                    </div>
										<!--/ Content wrapper -->
									</div>
									<!--/ .kl-iconbox__inner -->
								</div>
								<!--/ Icon box float left -->
							</div>
              <div class="col-sm-12 col-md-5 col-lg-5">
								<!-- Icon box float left -->
								<div class="kl-iconbox kl-iconbox--fleft text-left">
									<div class="kl-iconbox__inner">
						

										<!-- Content wrapper -->
                    <div class="circle-text-box style2">
                      <!-- Circle title wrapper -->
                      <div class="circle-headline">
                        <!-- Circle title -->
                        <span class="wpk-circle-span">
                          <span style="font-size: 18px;font-weight:bold;">12-16</span>
                        </span>
                        <!--/ Circle title -->

                        <!-- Title -->
                        <h4 class="wpk-circle-title">
                          story and game
                        </h4>
                        <!--/ Title -->
                      </div>
                      <!--/ Description -->
                    </div>
										<!--/ Content wrapper -->
									</div>
									<!--/ .kl-iconbox__inner -->
								</div>
								<!--/ Icon box float left -->
							</div>
							<!--/ col-sm-12 col-md-6 col-lg-6 -->
						</div>
					</div>
					<!--/ col-sm-12 col-md-12 col-lg-8 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!-- Services - section white and custom paddings -->

  <!-- Call to action section with custom background color and 0 paddings -->
  <section class="hg_section p-0">
    <div class="container-fluid no-pad-cols">
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <!-- Media container style 2 element - with custom height(.h-420) -->
          <div class="media-container style2 h-270 d-flex justify-content-center">
            <!-- Background source -->
            <div class="kl-bg-source">
              <!-- Background color -->
              <div class="kl-bg-source__bgimage" style="background-color: #34CC99;">
              </div>
              <!--/ Background color -->
            </div>
            <!--/ Background source -->

            <!-- Title element + button - vertical aligned -->
            <div class="kl-title-block text-center align-self-center">
              <!-- Title with custom size, weight and color style -->
              <h3 class="tbk__title fs-xl fw-semibold white">
              <?php echo translate('Choose Your Competition', $this->data['language']) ?><br> 
              </h3>

              <!-- Aligned center -->
              <div class="text-center">
                <!-- Button lined medium style -->
                <a href="<?php echo base_url('competition')?>" target="_self" class="btn-element btn btn-lined btn-md" title="LET'S DO IT!">
                  <span>LET'S DO IT!</span>
                </a>
              </div>
            </div>
            <!--/ Title element + button - vertical aligned -->
          </div>
          <!--/ Media container style 2 element - with custom height(.h-420) d-flex justify-content-center -->
        </div>
        <!--/ col-md-12 col-sm-12 -->
      </div>
      <!--/ row -->
    </div>
    <!--/ .container-fluid -->
  </section>
  <!--/ Call to action section with 0 paddings -->
<section class="hg_section bg-white pt-80 pb-100">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-md-12">

        <!-- Recent Work carousel 1 style 2 element -->
        <div class="recentwork_carousel recentwork_carousel--2">
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-4">
              <!-- Left side -->
              <div class="recentwork_carousel__left mb-md-50">
                <!-- Title -->
                <h3 class="tbk__title fs-xxl fw-extrabold recentwork_carousel__title gray">
                  <?php echo translate('Partners', $this->data['language']) ?><br> 
                </h3>
                <!--/ Title -->

                <!-- Slick navigation -->
                <div class="hgSlickNav recentwork_carousel__controls"></div>
              </div>
              <!--/ Left side - .recentwork_carousel__left -->
            </div>
            <!--/ col-sm-12 col-md-12 col-lg-4 -->

            <div class="col-sm-12 col-md-12 col-lg-8">
              <!-- Recent Work - carousel wrapper -->
              <div class="recentwork_carousel__crsl-wrapper">
                <div class="recent_works1 recentwork_carousel__crsl js-slick" data-slick='{
                  "infinite": true,
                  "slidesToShow": 3,
                  "slidesToScroll": 1,
                  "swipeToSlide": true,
                  "autoplay": true,
                  "autoplaySpeed": 5000,
                  "speed": 500,
                  "cssEase": "ease-in-out",
                  "appendArrows": ".recentwork_carousel--2 .hgSlickNav",		
                  "arrows": true,
                  "responsive": [
                    {
                      "breakpoint": 1199,
                      "settings": {
                        "slidesToShow": 2
                      }
                    },
                    {
                      "breakpoint": 767,
                      "settings": {
                        "slidesToShow": 2
                      }
                    },
                    {
                      "breakpoint": 480,
                      "settings": {
                        "slidesToShow": 1
                      }
                    }
                  ]
                }'>

                  <?php foreach($partners as $partner){ ?>
                    <div class="recent-work_carousel-item">
                      <!-- Portfolio link container -->
                      <a href="javascript:void(0)" class="recentwork_carousel__link">
                        <!-- Hover container -->
                        <div class="hover recentwork_carousel__hover">
                          <!-- Item image with custom height -->
                          <div class="carousel-item--height50">
                            <img src="<?php echo base_url('site_assets/uploads/partners/'.$partner['img'])?>" class="recentwork_carousel__img cover-fit-img" alt="" title="" />
                          </div>
                          <!--/ Item image with custom height -->

                          <!-- Hover shadow overlay -->
                          <span class="hov recentwork_carousel__hov"></span>
                          <!--/ Hover shadow overlay -->
                        </div>
                        <!--/ Hover container -->

                        <!-- Content details -->
                        <div class="details recentwork_carousel__details">
                          <!-- Title -->
                          <h4 class="recentwork_carousel__crsl-title">
                          <?php echo translate(strtoupper($partner['title']), $this->data['language']) ?>
                          </h4>
                          <!--/ Title -->
                        </div>
                        <!--/ Content details -->
                      </a>
                      <!--/ Portfolio link container -->
                    </div>
                  <?php }?>

                  <!--/ Item #1 -->
                </div>
                <!--/ .recent_works1 .recentwork_carousel__crsl .js-slick -->
              </div>
              <!--/  Recent Work - carousel wrapper - .recentwork_carousel__crsl-wrapper -->
            </div>
            <!--/ col-sm-12 col-md-12 col-lg-8 -->
          </div>
          <!--/ row -->
        </div>
        <!--/ Recent Work carousel 1 style 2 element -->
      </div>
      <!--/ col-sm-12 col-md-12 -->
    </div>
    <!--/ row -->
  </div>
  <!--/ container-fluid -->
</section>