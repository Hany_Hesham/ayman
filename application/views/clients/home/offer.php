<main class="page-content">
  <section style="background-image: url(<?php echo base_url('site_assets/images/dived/1298266-top-freediving-wallpaper-1920x1080-for-ipad-pro.jpg');?>);" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
    <div class="shell">
      <div class="page-title">
        <h2><?php echo translate($offer['name'], $this->data['language']) ?></h2>
      </div>
    </div>
  </section>
  <section class="section-50 section-sm-75 section-lg-bottom-120">
    <div class="shell">
      <div class="range range-lg-center">
        <div class="cell-lg-10">
          <div class="product product-single">
            <div class="product-main">
              <div class="product-slider">
                <div data-items="1" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="true" data-dots="true" data-animation-in="fadeIn" data-animation-out="fadeOut" class="owl-carousel owl-carousel-bottom owl-style-minimal">
                  <div class="item">
                    <img src="<?php echo base_url('site_assets/images/offer/'.$offer['image'])?>" alt="" width="370" height="278" style="height:280px;"/> 
                  </div>
                  <div class="item">
                    <img src="<?php echo base_url('site_assets/images/offer/'.$offer['image'])?>" alt="" width="370" height="278" style="height:280px;"/> 
                  </div>
                </div>
              </div>
              <div class="product-body">
                <h4 class="product-header"><?php echo  translate($offer['name'], $this->data['language'])?></h4>
                <ul class="product-info">
                  <li>
                   <?php  echo form_open('', 'class="rd-mailform form-modern offset-top-22" id="book_trip-all-form"'); ?>
                        <div class="range">
                          <div class="cell-sm-6 offset-top-30 offset-sm-top-0">
                            <div class="form-group">
                             <input id="billing-first-name" type="text" name="first_name" data-constraints="@Required" placeholder="First Name" class="form-control" required>
                            </div>
                          </div>
                          <div class="cell-sm-6 offset-top-30 offset-sm-top-0">
                            <div class="form-group">
                              <input id="billing-last-name" type="text" name="last_name" data-constraints="@Required" class="form-control" placeholder="Last Name" required>
                            </div>
                          </div>
                          <div class="cell-xs-12 offset-top-30 offset-sm-top-40">
                            <div class="form-group">
                              <input id="billing-email" type="email" name="email" data-constraints="@Email @Required" class="form-control" placeholder="Email" required>
                            </div>
                          </div>
                          <div class="cell-xs-6 offset-top-30 offset-sm-top-40">
                            <div class="form-group">
                              <input id="billing-phone" type="text" name="phone" data-constraints="@Numeric @Required" class="form-control" placeholder="Phone" required>
                            </div>
                          </div>
                          <div class="cell-xs-6 offset-top-30 offset-sm-top-40">
                            <div class="form-group">
                              <input id="feedback-date" type="text" name="date" data-constraints="@Required" data-time-picker="date" 
                              class="form-control form-control-has-validation" data-dtp="dtp_Lj1Oo" placeholder="Date" required><span class="form-validation"></span>
                            </div>
                          </div>
                          <input id="pax-input" type="hidden" name="pax" value="">
                          <input type="hidden" name="price" value="<?php echo translate($offer['price'], $this->data['language'])?>">
                          <input class="price price-calculate" type="hidden" name="total_cost" value="">
                          <input class="currency-input" type="hidden" name="currency" value="<?php echo translate($offer['currency'], $this->data['language']) ?>">
                          <input class="offer_id" type="hidden" name="offer_id" value="<?php echo translate($offer['id'], $this->data['language']) ?>">
                          <input class="activity_name" type="hidden" name="activity_name" value="<?php echo translate($offer['activity_name'], $this->data['language']) ?>">
                          <input class="sub_name" type="hidden" name="sub_name" value="<?php echo translate($offer['sub_name'], $this->data['language']) ?>">
                       </div>
                  </li>
                  <li>
                    <dl>
                      <dt><?php echo translate('Quantity', $this->data['language']) ?></dt>
                      <dd>
                        <div class="stepper-lg">
                          <input id="paxNumber" type="number" data-zeros="false" value="1" min="1" max="100" onchange="priceClaculate()">
                        </div>
                      </dd>
                    </dl>
                  </li>
                </ul>
                <div class="product-panel">
                  <div class="pricing-wrap">
                    <div class="pricing-object pricing-object-md">
                      <span class="small small-middle"><?php echo translate('EGP', $this->data['language']) ?></span>
                      <span  class="price price-calculate"><?php echo translate($offer['price'], $this->data['language']) ?></span>
                      <span class="small small-bottom"></span>
                    </div>
                  </div>
                   <button type="submit" onclick="processFormData()" class="btn btn-primary btn-block"><span class="icon icon-sm fa-shopping-cart"></span> <span><?php echo translate('Book', $this->data['language']) ?></span></button>
                  <?php echo form_close(); ?> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- related products-->
  <section class="section-50 section-sm-90 bg-whisper">
    <div class="shell">
      <div class="range range-lg-center">
        <div class="cell-lg-10 text-center text-sm-left">
          <h3><?php echo translate('Related Products', $this->data['language']) ?></h3>
        </div>
      </div>
      <div class="range offset-top-30">
        <?php foreach ($allOffers as $offer):?>
          <div class="cell-sm-6 cell-md-4 cell-lg-3 offset-top-30">
            <div class="product product-item-default">
              <div class="product-slider">
                <div class="product-slider-inner">
                  <div  class="owl-carousel owl-style-minimal">
                    <div class="item">
                      <figure>
                         <img src="<?php echo base_url('site_assets/images/offer/'.$offer['image'])?>" alt="" width="400" height="360"/> 
                      </figure>
                    </div>
                  </div>
                </div>
              </div>
              <?php if($offer['special'] == 1){?>
                <div class="product-label-wrap product-label-wrap-left">
                  <div class="label-custom product-label label-danger"><?php echo translate('Special', $this->data['language']) ?></div>
                </div>
              <?php } ?>
              <div class="product-main">
                <h6 class="product-header"><a href="shop-product.html"><?php echo translate($offer['name'], $this->data['language']) ?></a></h6>
              </div>
              <div class="product-footer">
                <div class="product-footer-front pricing-wrap">
                  <p class="pricing-object pricing-object-sm price-current">
                    <span class="small small-middle"><?php echo translate('$', $this->data['language']) ?></span>
                    <span class="price"><?php echo  translate($offer['price'], $this->data['language'])?></span>
                    <span class="small small-bottom"><?php echo translate('.00', $this->data['language']) ?></span></p>
                </div>
                <div class="product-footer-behind"><a href="<?php echo base_url('clients/offers/offerd/'.$offer['id']);?>" class="btn btn-icon btn-icon-left btn-primary product-control"><span class="icon icon-sm fa-shopping-cart"></span><span><?php echo translate('Book', $this->data['language']) ?></span></a></div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </section>
  <!-- end of related products -->
</main>
<div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;padding-top:15% !important" 
       aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document ">
        <div class="modal-content">
            <div class="modal-body">
              <h5 class="text-center" id="loading-part">
                <div class="" id="process-loader">
                   <img src="<?php echo base_url();?>assets/images/spinner.gif"
                       style = " width: 30%;height:30%;">
                </div>
                <?php echo translate('Please Wait', $this->data['language']) ?>
              </h5>
              <div id="success-part">
                <h1 class="text-center">
                  <strong style="font-size:100px;color:#3EC1D5;margin-left:5% !important"><i class="fa fa-check-circle"></i></strong>
                </h1>
                <h6 class="text-center"><?php echo translate('Success', $this->data['language']) ?></h6>
              </div>
            </div>
        </div>
    </div>
  </div>

<script type="text/javascript">
  function priceClaculate(){
    let price = '<?php echo  $offer['price']?>';
    let totalPrice = Number(price)*Number($('#paxNumber').val());
      $('.price-calculate').val(totalPrice);
      $('.price-calculate').text(totalPrice);
      $('#pax-input').val($('#paxNumber').val());
  }
  function processFormData(){
    if ( !$('#billing-first-name').val() ||  !$('#billing-last-name').val() ||  !$('#billing-email').val() ||  !$('#billing-phone').val() 
         ||  !$('#feedback-date').val()) {
          return false;
    }else{
          $('#success-modal').modal('show');
          $('#success-modal .modal-body #success-part').hide();
          $('#success-modal .modal-body #loading-part').show();
           var formdata = $('form').serialize();
           console.log(formdata);
           $.ajax({
             type: 'post',
             url: '<?php echo base_url("clients/offers/book_trip")?>',
             data: {formdata:formdata},
             success: function(data) { 
               setTimeout(function(){
                $('#success-modal .modal-body #loading-part').hide();
                $('#success-modal .modal-body #success-part').show();
                document.getElementById("contact-us-all-form").reset();
               // $('.contact-us-form').val('');
                }, 1000);
                 
                 }
            });
    }
  }
  

</script>