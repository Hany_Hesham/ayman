<?php $this->load->view($this->data['inner_slider']);?>
		<!-- Title section with custom background color and paddings -->
		<section class="hg_section bg-white pt-70 pb-20">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<!-- Title element custom -->
						<div class="kl-title-block clearfix text-center tbk-symbol--border">
							<!-- Title with custom font size, thin style and black color -->
							<h3 class="tbk__title fs-xxl fw-thin black">
								A little bit about us
							</h3>
							<!--/ Title with custom font size, thin style and black color -->

							<!-- Sub-title with custom font size and paddings -->
							<h6 class="tbk__subtitle fs-normal pb-50">
								Among many reasons, what makes Kallyas so special is the large number of built-in features and integrations, along premium powerful plugins.
								<br>
								You be the creative, while we're going to ensure a proper infrastructure for your website, all live in the frontend of your website.
								<br>
								Building a website has never been so easy and comfortable.
							</h6>
							<!--/ Sub-title with custom font size and paddings -->

							<!-- Separator -->
							<div class="tbk__symbol">
								<span></span>
							</div>
						</div>
						<!--/ Title element custom -->
					</div>
					<!--/ col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Title section with custom background color and paddings -->
		<!-- Testimonials & Partners section with background color, gloss overlay and custom paddings -->
		<section class="hg_section--relative bg-black-gray pt-80 pb-100" style="background-color: #222222;">
			<!-- Background -->
			<div class="kl-bg-source">
				<!-- Gloss overlay -->
				<div class="kl-bg-source__overlay-gloss"></div>
				<!--/ Gloss overlay -->
			</div>
			<!--/ Background -->

			<!-- Content -->
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<!-- Testimonials & Partners element - light text style -->
						<div class="testimonials-partners testimonials-partners--light">


							<!-- Partners element -->
							<div class="ts-pt-partners ts-pt-partners--y-title clearfix">
								<!-- Title -->
								<div class="ts-pt-partners__title">
									Partners
								</div>
								<!--/ Title -->

								<!-- Partners carousel wrapper -->
								<div class="ts-pt-partners__carousel-wrapper">
									<div class="ts-pt-partners__carousel js-slick" data-slick='{
										"infinite": true,
										"autoplay": true,
										"autoplaySpeed": 3000,
										"arrows": false,
										"dots": false,
										"slidesToShow": 5,
										"slidesToScroll": 1,
										"waitForAnimate": false,
										"speed": 0,
										"swipe": false,
										"respondTo": "slider",
										"responsive": [
											{
									  			"breakpoint": 1024,
										  		"settings": {
													"slidesToShow": 4,
													"slidesToScroll": 4
								  				}
											},
											{
									  			"breakpoint": 767,
										  		"settings": {
													"slidesToShow": 3,
													"slidesToScroll": 3
								  				}
											},
											{
									  			"breakpoint": 520,
										  		"settings": {
													"slidesToShow": 2,
													"slidesToScroll": 2
								  				}
											},
											{
									  			"breakpoint": 380,
										  		"settings": {
													"slidesToShow": 1,
													"slidesToScroll": 1
								  				}
											}
										]

										}'>

											<!-- Item #1 -->
											<div class="ts-pt-partners__carousel-item">
												<!-- Partner image link -->
												<a class="ts-pt-partners__link" href="#" target="_self" title="">
													<!-- Image -->
													<img class="ts-pt-partners__img img-fluid" src="<?php echo base_url('site_assets/images/logo6.svg')?>" alt="" title="" />
													<!--/ Image -->
												</a>
												<!--/ Partner image link -->
											</div>
											<!--/ Item #1 -->

											<!-- Item #2 -->
											<div class="ts-pt-partners__carousel-item">
												<!-- Partner image link -->
												<a class="ts-pt-partners__link" href="#" target="_self" title="">
													<!-- Image -->
													<img class="ts-pt-partners__img img-fluid" src="<?php echo base_url('site_assets/images/logo7.svg')?>" alt="" title="" />
													<!--/ Image -->
												</a>
												<!--/ Partner image link -->
											</div>
											<!--/ Item #2 -->

											<!-- Item #3 -->
											<div class="ts-pt-partners__carousel-item">
												<!-- Partner image link -->
												<a class="ts-pt-partners__link" href="#" target="_self" title="">
													<!-- Image -->
													<img class="ts-pt-partners__img img-fluid" src="<?php echo base_url('site_assets/images/logo8.svg')?>" alt="" title="" />
													<!--/ Image -->
												</a>
												<!--/ Partner image link -->
											</div>
											<!--/ Item #3 -->

											<!-- Item #4 -->
											<div class="ts-pt-partners__carousel-item">
												<!-- Partner image link -->
												<a class="ts-pt-partners__link" href="#" target="_self" title="">
													<!-- Image -->
													<img class="ts-pt-partners__img img-fluid" src="<?php echo base_url('site_assets/images/logo1.svg')?>" alt="" title="" />
													<!--/ Image -->
												</a>
												<!--/ Partner image link -->
											</div>
											<!--/ Item #4 -->

											<!-- Item #5 -->
											<div class="ts-pt-partners__carousel-item">
												<!-- Partner image link -->
												<a class="ts-pt-partners__link" href="#" target="_self" title="">
													<!-- Image -->
													<img class="ts-pt-partners__img img-fluid" src="<?php echo base_url('site_assets/images/logo2.svg')?>" alt="" title="" />
													<!--/ Image -->
												</a>
												<!--/ Partner image link -->
											</div>
											<!--/ Item #5 -->

											<!-- Item #6 -->
											<div class="ts-pt-partners__carousel-item">
												<!-- Partner image link -->
												<a class="ts-pt-partners__link" href="#" target="_self" title="">
													<!-- Image -->
													<img class="ts-pt-partners__img img-fluid" src="<?php echo base_url('site_assets/images/logo3.svg')?>" alt="" title="" />
													<!--/ Image -->
												</a>
												<!--/ Partner image link -->
											</div>
											<!--/ Item #6 -->

											<!-- Item #7 -->
											<div class="ts-pt-partners__carousel-item">
												<!-- Partner image link -->
												<a class="ts-pt-partners__link" href="#" target="_self" title="">
													<!-- Image -->
													<img class="ts-pt-partners__img img-fluid" src="<?php echo base_url('site_assets/images/logo4.svg')?>" alt="" title="" />
													<!--/ Image -->
												</a>
												<!--/ Partner image link -->
											</div>
											<!--/ Item #7 -->

											<!-- Item  #8-->
											<div class="ts-pt-partners__carousel-item">
												<!-- Partner image link -->
												<a class="ts-pt-partners__link" href="#" target="_self" title="">
													<!-- Image -->
													<img class="ts-pt-partners__img img-fluid" src="<?php echo base_url('site_assets/images/logo5.svg')?>" alt="" title="" />
													<!--/ Image -->
												</a>
												<!--/ Partner image link -->
											</div>
											<!--/ Item #8 -->
									</div>
									<!--/ .ts-pt-partners__carousel js-slick -->
								</div>
								<!--/ Partners carousel wrapper -->
							</div>
							<!--/ Partners element - .ts-pt-partners -->
						</div>
						<!--/ Testimonials & Partners element - light text style -->
					</div>
					<!--/ col-md-12 col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ Content .container -->
		</section>
		<!--/ Testimonials & Partners section with background color, gloss overlay and custom paddings -->
		<!-- Services section with custom background color and paddings -->
		<section class="hg_section bg-white pb-50">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-4 mb-md-50">
						<div class="custom mr-20 mb-sm-30">
							<!-- Title element -->
							<div class="kl-title-block clearfix text-left tbk-symbol--line tbk-icon-pos--after-title pb-0">
								<!-- Title -->
								<h3 class="tbk__title fw-bold">
									Cream of the crop
								</h3>
								<!--/ Title -->

								<!-- Sub-title -->
								<h6 class="gray">
									HUGE VARIETY OF SLIDERS
								</h6>

								<!-- Title bottom symbol -->
								<div class="tbk__symbol">
									<span class="tbg"></span>
								</div>
								<!--/ Title bottom symbol -->
							</div>
							<!--/ Title element -->

							<ul class="fancy-list ml-0">
								<li>Multiple header layouts</li>
								<li>Over 100 unique elements</li>
								<li>Ready for all niches</li>
								<li>Unlimited homepages</li>
								<li>Visual slider builder</li>
							</ul>
						</div>
						<!--/ custom mr-20 mb-sm-30 -->
					</div>
					<!--/ col-sm-12 col-md-12 col-lg-4 mb-md-50 -->
					
					<div class="col-sm-12 col-md-12 col-lg-8">
						<div class="row">
							<div class="col-sm-6 col-md-4 col-lg-4">
								<div class="kl-icon-box mb-50">
									<!-- Icon -->
									<i class="kl-icon ico-size-xxl icon-gi-ico-1 tcolor"></i>

									<!-- Title with cutom size and margin -->
									<h4 class="fs-s mt-10">
										HIGHLY CUSTOMIZABLE
									</h4>

									<!-- Content -->
									<div class="content">
										<p>
											This template includes lots of customizations and combinations.
										</p>
									</div>
									<!-- /.content -->
								</div>
								<!-- /.icon-box mb-50 -->
							</div>
							<!--/ col-sm-6 col-md-4 col-lg-4 -->

							<div class="col-sm-6 col-md-4 col-lg-4">
								<div class="kl-icon-box mb-50">
									<!-- Icon -->
									<i class="kl-icon ico-size-xxl icon-gi-ico-7 tcolor"></i>

									<!-- Title with cutom size and margin -->
									<h4 class="fs-s mt-10">
										WELL DOCUMENTED
									</h4>

									<!-- Content -->
									<div class="content">
										<p>
											Documentation include with explanations for most of the options.
										</p>
									</div>
									<!-- /.content -->
								</div>
								<!-- /.icon-box mb-50 -->
							</div>
							<!--/ col-sm-6 col-md-4 col-lg-4 -->

							<div class="col-sm-6 col-md-4 col-lg-4">
								<div class="kl-icon-box mb-50">
									<!-- Icon -->
									<i class="kl-icon ico-size-xxl icon-gi-ico-4 tcolor"></i>

									<!-- Title with cutom size and margin -->
									<h4 class="fs-s mt-10">
										UNLIMITED COLORS
									</h4>

									<!-- Content -->
									<div class="content">
										<p>
											With just a few clicks you can add any color you want.
										</p>
									</div>
									<!-- /.content -->
								</div>
								<!-- /.icon-box mb-50 -->
							</div>
							<!--/ col-sm-6 col-md-4 col-lg-4 -->

							<div class="col-sm-6 col-md-4 col-lg-4">
								<div class="kl-icon-box mb-50">
									<!-- Icon -->
									<i class="kl-icon ico-size-xxl icon-gi-ico-7 tcolor"></i>

									<!-- Title with cutom size and margin -->
									<h4 class="fs-s mt-10">
										WEBSITE MARKETING
									</h4>

									<!-- Content -->
									<div class="content">
										<p>
											Web is evolving and portable devices are the fastest growing market of the last decade.
										</p>
									</div>
									<!-- /.content -->
								</div>
								<!-- /.icon-box mb-50 -->
							</div>
							<!--/ col-sm-6 col-md-4 col-lg-4 -->

							<div class="col-sm-6 col-md-4 col-lg-4">
								<div class="kl-icon-box mb-50">
									<!-- Icon -->
									<i class="kl-icon ico-size-xxl icon-gi-ico-8 tcolor"></i>

									<!-- Title with cutom size and margin -->
									<h4 class="fs-s mt-10">
										PROJECTS VARIETY
									</h4>

									<!-- Content -->
									<div class="content">
										<p>
											This fancy template can be used as a one page website, landing page, corporate and so on..
										</p>
									</div>
									<!-- /.content -->
								</div>
								<!-- /.icon-box mb-50 -->
							</div>
							<!--/ col-sm-6 col-md-4 col-lg-4 -->

							<div class="col-sm-6 col-md-4 col-lg-4">
								<div class="kl-icon-box mb-50">
									<!-- Icon -->
									<i class="kl-icon ico-size-xxl icon-gi-ico-14 tcolor"></i>

									<!-- Title with cutom size and margin -->
									<h4 class="fs-s mt-10">
										STUNING ELEMENTS
									</h4>

									<!-- Content -->
									<div class="content">
										<p>
											Yet highly intuitive and easy to use, in just a matter of hours become a pro.
										</p>
									</div>
									<!-- /.content -->
								</div>
								<!-- /.icon-box mb-50 -->
							</div>
							<!--/ col-sm-6 col-md-4 col-lg-4 -->
						</div>
						<!--/ row -->
					</div>
					<!--/ col-sm-12 col-md-12 col-lg-8 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Services section with custom background color and paddings -->
		<!-- Team carousel element section with custom paddings -->
		<section class="hg_section pt-100 pb-50">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<!-- Title element centered and custom paddings -->
						<div class="kl-title-block clearfix text-center pb-50">
							<!-- Title with custom weight -->
							<h3 class="tbk__title fw-bold">
								The awesome team
							</h3>
							<!--/ Title -->

							<!-- Sub-title with custom size and color -->
							<h6 class="fs-s gray">
								SOME COOL GUYS TO WORK WITH
							</h6>
						</div>
						<!--/ Title element centered and custom paddings -->

						<!-- Team Carousel wrapper element -->
						<div class="team-boxes__carousel-wrapper">
							<div class="team-boxes__carousel js-slick" data-slick='{
								"infinite": true,
								"slidesToShow": 4,
								"slidesToScroll": 1,
								"autoplay": false,
								"autoplaySpeed": 5000,
								"arrows": true,
								"appendArrows": ".hgSlickNav",
								"responsive": [
									{
										"breakpoint": 1199,
										"settings": {
											"slidesToShow": 3
										}
									},
									{
										"breakpoint": 767,
										"settings": {
											"slidesToShow": 2
										}
									},
									{
										"breakpoint": 480,
										"settings": {
											"slidesToShow": 1
										}
									}
								]
							}'>
								<!-- Team member #1 -->
								<div class="team-member tm-hover text-center">
									<!-- Image with pop-up -->
									<a class="grayscale-link" data-lightbox="image" data-mfp="image" href="<?php echo base_url('site_assets/images/member-04-270x270.jpg')?>">
										<img src="<?php echo base_url('site_assets/images/member-04-270x270.jpg')?>" class="img-fluid" alt="" title="" />
									</a>

									<!-- Title -->
									<h5 class="mmb-title">
										James Daughter
									</h5>

									<!-- Position -->
									<h6 class="mmb-position">
										UX Designer
									</h6>

									<!-- Description -->
									<p class="mmb-desc">
										The tech industry changes so quickly that it often feels like many of the tools and techniques you're using today were all but unknown two years ago.
									</p>

									<span class="separator"></span>

									<!-- Social icons list -->
									<ul class="mmb-social-list">
										<li>
											<!-- Twitter -->
											<a href="#" class="kl-icon fab fa-twitter" title="Twitter"></a>
										</li>
										<li>
											<!-- Facebook -->
											<a href="#" class="kl-icon fab fa-facebook-f" title="Facebook"></a>
										</li>
										<li>
											<a href="#" class="kl-icon fab fa-google-plus-g" title="Google+"></a>
										</li>
									</ul>
								</div>
								<!--/ Team member #1 -->

								<!-- Team member #2 -->
								<div class="team-member tm-hover text-center">
									<!-- Image with pop-up -->
									<a class="grayscale-link" data-lightbox="image" data-mfp="image" href="<?php echo base_url('site_assets/images/member-05-270x270.jpg')?>">
										<img src="<?php echo base_url('site_assets/images/member-05-270x270.jpg')?>" class="img-fluid" alt="" title="" />
									</a>

									<!-- Title -->
									<h5 class="mmb-title">
										Melissa Johnson
									</h5>

									<!-- Position -->
									<h6 class="mmb-position">
										Project Manager
									</h6>

									<!-- Description -->
									<p class="mmb-desc">
										Research emerging products and use information to update the store’s merchandise. Empathy is the most important skill for any customer-facing role.
									</p>

									<span class="separator"></span>

									<!-- Social icons list -->
									<ul class="mmb-social-list">
										<li>
											<!-- Twitter -->
											<a href="#" class="kl-icon fab fa-twitter" title="Twitter"></a>
										</li>
										<li>
											<!-- Facebook -->
											<a href="#" class="kl-icon fab fa-facebook-f" title="Facebook"></a>
										</li>
										<li>
											<a href="#" class="kl-icon fab fa-google-plus-g" title="Google+"></a>
										</li>
									</ul>
								</div>
								<!--/ Team member #2 -->

								<!-- Team member #3 -->
								<div class="team-member tm-hover text-center">
									<!-- Image with pop-up -->
									<a class="grayscale-link" data-lightbox="image" data-mfp="image" href="<?php echo base_url('site_assets/images/member-06-270x270.jpg')?>">
										<img src="<?php echo base_url('site_assets/images/member-06-270x270.jpg')?>" class="img-fluid" alt="" title="" />
									</a>

									<!-- Title -->
									<h5 class="mmb-title">
										Richard Brown
									</h5>

									<!-- Position -->
									<h6 class="mmb-position">
										Developer
									</h6>

									<!-- Description -->
									<p class="mmb-desc">
										His usually involves implementing all the visual elements that users see and use in the web application, as well as all the web services and APIs.
									</p>

									<span class="separator"></span>

									<!-- Social icons list -->
									<ul class="mmb-social-list">
										<li>
											<!-- Twitter -->
											<a href="#" class="kl-icon fab fa-twitter" title="Twitter"></a>
										</li>
										<li>
											<!-- Facebook -->
											<a href="#" class="kl-icon fab fa-facebook-f" title="Facebook"></a>
										</li>
										<li>
											<a href="#" class="kl-icon fab fa-google-plus-g" title="Google+"></a>
										</li>
									</ul>
								</div>
								<!--/ Team member #3 -->

								<!-- Team member #4 -->
								<div class="team-member tm-hover text-center">
									<!-- Image with pop-up -->
									<a class="grayscale-link" data-lightbox="image" data-mfp="image" href="<?php echo base_url('site_assets/images/member-07-270x270.jpg')?>">
										<img src="<?php echo base_url('site_assets/images/member-07-270x270.jpg')?>" class="img-fluid" alt="" title="" />
									</a>

									<!-- Title -->
									<h5 class="mmb-title">
										Roy Gold
									</h5>

									<!-- Position -->
									<h6 class="mmb-position">
										Copyrighter
									</h6>

									<!-- Description -->
									<p class="mmb-desc">
										The most significant point is that patent and copyright laws support the expansion of the range of creative human activities that can be commodified.
									</p>

									<span class="separator"></span>

									<!-- Social icons list -->
									<ul class="mmb-social-list">
										<li>
											<!-- Twitter -->
											<a href="#" class="kl-icon fab fa-twitter" title="Twitter"></a>
										</li>
										<li>
											<!-- Facebook -->
											<a href="#" class="kl-icon fab fa-facebook-f" title="Facebook"></a>
										</li>
										<li>
											<a href="#" class="kl-icon fab fa-google-plus-g" title="Google+"></a>
										</li>
									</ul>
								</div>
								<!--/ Team member #4 -->

								<!-- Team member #5 -->
								<div class="team-member tm-hover text-center">
									<!-- Image with pop-up -->
									<a class="grayscale-link" data-lightbox="image" data-mfp="image" href="<?php echo base_url('site_assets/images/member-03-370x370.jpg')?>">
										<img src="<?php echo base_url('site_assets/images/member-03-370x370.jpg')?>" alt="" title="" class="img-fluid">
									</a>

									<!-- Title -->
									<h5 class="mmb-title">
										Andrew Stone Jr.
									</h5>

									<!-- Position -->
									<h6 class="mmb-position">
										Portfolio manager
									</h6>

									<!-- Description -->
									<p class="mmb-desc">
										With project management methodology in increasing demand, employers are actively searching for individuals who can lead project teams and deliver quality.
									</p>

									<span class="separator"></span>

									<!-- Social icons list -->
									<ul class="mmb-social-list">
										<li>
											<!-- Twitter -->
											<a href="#" class="kl-icon fab fa-twitter" title="Twitter"></a>
										</li>
										<li>
											<!-- Facebook -->
											<a href="#" class="kl-icon fab fa-facebook-f" title="Facebook"></a>
										</li>
										<li>
											<a href="#" class="kl-icon fab fa-google-plus-g" title="Google+"></a>
										</li>
									</ul>
								</div>
								<!--/ Team member #5 -->
							</div>
							<!--/ .team-boxes__carousel .js-slick  -->

							<!-- Slick navigation -->
							<div class="hgSlickNav"></div>
						</div>
						<!--/ Team Carousel wrapper element -->
					</div>
					<!--/ col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Team carousel element section with custom paddings -->