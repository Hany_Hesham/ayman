		<!-- Slideshow - Static content Product presentation + bottom mask style 4 -->
		<div class="kl-slideshow static-content__slideshow uh_neutral_color maskcontainer--mask4 ">
			<div class="bgback">
			</div>

			<!-- Static content wrapper with default height (not fullscreen) -->
			<div class="kl-slideshow-inner static-content__wrapper static-content--height">
				<!-- Static content background source -->
				<div class="static-content__source ">
					<!-- Background -->
					<div class="kl-bg-source">
						<!-- Background image -->
						<div class="kl-bg-source__bgimage" style="background-image: url(<?php echo base_url('site_assets/images/slider/'.$this->data['background_slider']['img'])?>); background-repeat: no-repeat; background-attachment: scroll; background-position-x: center; background-position-y: center; background-size: cover;">
						</div>
						<!--/ Background image -->

						<!-- Color overlay -->
						<div class="kl-bg-source__overlay" style="background-color: rgba(0,16,35,0.5)">
						</div>
						<!--/ Color overlay -->
					</div>
					<!--/ Background -->

					<!-- Animated Sparkles -->
					<div class="th-sparkles"></div>
					<!--/ Animated Sparkles -->
				</div>
				<!--/ .static-content__source -->

				<!-- Static content container -->
				<div class="static-content__inner container">
					<div class="kl-slideshow-safepadding sc__container">
						<!-- Product zoom style -->
						<div class="static-content productzoom-style">
							<div class="row">
								<div class="col-sm-12 col-md-12 col-lg-5 mb-30">
									<!-- Sub-title -->
									<h3 class="static-content__subtitle">
										<span class="fw-thin">
											<?php echo translate($this->data['background_slider']['first_title'], $this->data['language']) ?>
										</span>
									</h3>

									<!-- Features list -->
									<ul class="sc__features">
										<li>
											<span class="fas fa-check white-icon"></span>
											<?php echo translate($this->data['background_slider']['second_title'], $this->data['language']) ?>
										</li>
									</ul>
									<!--/ Features list -->

									<a href="<?php echo base_url('rules')?>" class="btn-element btn btn-fullcolor btn-skewed bb-skewed2 btn-md" target="_blank" title="Register">
										<span><?php echo translate('How to Register', $this->data['language']) ?></span>
									</a>
									<!--/ Buy button - full color style -->
								</div>
								<!--/ col-sm-12 col-md-12 col-lg-5 mb-30 -->

								<div class="col-sm-12 col-md-12 col-lg-7">
									<!-- Screenshot with loupe element -->
									<div id="screenshot" class="sc__screenshot">
										<!-- Image container -->
										<div class="image">
											<!-- Product loupe pop-up & image -->
											<a class="kjq-loupe" data-lightbox="image" href="<?php echo base_url('site_assets/images/slider/'.$this->data['slider_magnified_image']['img'])?>">
												<!-- Image -->
												<img src="<?php echo base_url('site_assets/images/slider/'.$this->data['slider_magnified_image']['img'])?>" class="img-fluid" alt="Product" title="Product" />
											</a>
											<!-- Product loupe pop-up & image -->

											<!-- Loupe -->
											<div class="sc__loupe"></div>
										</div>
										<!--/ Image container -->
									</div>
									<!--/ Screenshot with loupe element -->
								</div>
								<!--/ col-sm-12 col-md-12 col-lg-7 -->
							</div>
							<!--/ row -->
						</div>
						<!--/ .productzoom-style -->
					</div>
					<!-- /.sc__container -->
				</div>
				<!--/ Static content container -->
			</div>
			<!--/ Static content wrapper with default height (not fullscreen) -->

			<!-- Bottom mask style 4 -->
			<div class="kl-bottommask kl-bottommask--mask4">
				<svg width="5000px" height="27px" class="svgmask " viewBox="0 0 5000 27" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<defs>
						<filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask4">
							<feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
							<feGaussianBlur stdDeviation="1.5" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
							<feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
							<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.35 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
							<feMerge>
								<feMergeNode in="SourceGraphic"></feMergeNode>
								<feMergeNode in="shadowMatrixInner1"></feMergeNode>
							</feMerge>
						</filter>
					</defs>
					<path d="M3.63975516e-12,-0.007 L2418,-0.007 L2434,-0.007 C2434,-0.007 2441.89,0.742 2448,2.976 C2454.11,5.21 2479,15 2479,15 L2492,21 C2492,21 2495.121,23.038 2500,23 C2505.267,23.03 2508,21 2508,21 L2521,15 C2521,15 2545.89,5.21 2552,2.976 C2558.11,0.742 2566,-0.007 2566,-0.007 L2582,-0.007 L5000,-0.007 L5000,27 L2500,27 L3.63975516e-12,27 L3.63975516e-12,-0.007 Z" class="bmask-bgfill" filter="url(#filter-mask4)" fill="#fbfbfb"></path>
				</svg>
			</div>
			<!--/ Bottom mask style 4 -->	
		</div>
		<!--/ Slideshow - Static content Product presentation + bottom mask style 4 -->