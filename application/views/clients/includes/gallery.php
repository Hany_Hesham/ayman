<?php $this->load->view($this->data['inner_slider']);?>
<!-- Photo gallery alternative style element section with custom paddings -->
		<section class="hg_section pt-80 pb-30">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<!-- Title element -->
						<div class="kl-title-block clearfix text-center tbk-symbol--line tbk-icon-pos--after-title">
							<!-- Title with bold weight -->
							<h3 class="tbk__title fw-bold">
								Team building creative activities
							</h3>
							<!--/ Title -->

							<!-- Sub-title with custom size and color -->
							<h6 class="fs-s gray">
								That we really, really appreciate!
							</h6>

							<!-- Title bottom symbol with default Kallyas color -->
							<div class="tbk__symbol">
								<span class="tbg"></span>
							</div>
							<!--/ Title bottom symbol with default Kallyas color -->
						</div>
						<!--/ Title element -->

						<!-- Photo Gallery alternative -->
						<div class="photogallery-widget">
							<!-- Photo gallery wrapper -->
							<div class="photogallery-wrapper">
								<div class="caroufredsel custom stg-slimmer-arrows">
									<!-- Slides -->
									<ul class="slides">
										<!-- Slide image #1 -->
										<li data-thumb="<?php echo base_url('site_assets/images/quickgal/1.jpg')?>">
											<!-- Image -->
											<img src="<?php echo base_url('site_assets/images/quickgal/1.jpg')?>" class="img-fluid" alt="" title="" />

											<!-- Title caption -->
											<h4 class="pg-caption">
												Going to the <strong>grand lake</strong><br>
												with my friends.
											</h4>
										</li>
										<!--/ Slide image #1 -->

										<!-- Slide image #2 -->
										<li data-thumb="<?php echo base_url('site_assets/images/quickgal/2.jpg')?>">
											<!-- Image -->
											<img src="<?php echo base_url('site_assets/images/quickgal/2.jpg')?>" class="img-fluid" alt="" />

											<!-- Title caption -->
											<h4 class="pg-caption">
												I love this city<br>
												<a href="#">@NY</a>
											</h4>
										</li>
										<!--/ Slide image #2 -->

										<!-- Slide image #3 -->
										<li data-thumb="<?php echo base_url('site_assets/images/quickgal/3.jpg')?>">
											<!-- Image -->
											<img src="<?php echo base_url('site_assets/images/quickgal/3.jpg')?>" class="img-fluid" alt="" title="" />

											<!-- Title caption -->
											<h4 class="pg-caption">
												Everyday Meeting<br>
												<a href="#">@The office</a>
											</h4>
										</li>
										<!--/ Slide image #3 -->

										<!-- Slide image #4 -->
										<li data-thumb="<?php echo base_url('site_assets/images/quickgal/10.jpg')?>">
											<!-- Image -->
											<img src="<?php echo base_url('site_assets/images/quickgal/10.jpg')?>" class="img-fluid" alt="" title="" />

											<!-- Title caption -->
											<h4 class="pg-caption">
												Teambuilding hangout<br>
												<a href="#">@Crazy Mountain</a>
											</h4>
										</li>
										<!--/ Slide image #4 -->

										<!-- Slide image #5 -->
										<li data-thumb="<?php echo base_url('site_assets/images/quickgal/5.jpg')?>">
											<!-- Image -->
											<img src="<?php echo base_url('site_assets/images/quickgal/5.jpg')?>" class="img-fluid" alt="" title="" />

											<!-- Title caption -->
											<h4 class="pg-caption">
												Going places<br>
												<a href="#">@Crazy Mountain</a>
											</h4>
										</li>
										<!--/ Slide image #5 -->

										<!-- Slide image #6 -->
										<li data-thumb="<?php echo base_url('site_assets/images/quickgal/6.jpg')?>">
											<!-- Image -->
											<img src="<?php echo base_url('site_assets/images/quickgal/6.jpg')?>" class="img-fluid" alt="" title="" />

											<!-- Title caption -->
											<h4 class="pg-caption">
												Le Tour Eiffel<br>
												<a href="#">@Paris</a>
											</h4>
										</li>
										<!--/ Slide image #6 -->

										<!-- Slide image #7 -->
										<li data-thumb="<?php echo base_url('site_assets/images/quickgal/7.jpg')?>">
											<!-- Image -->
											<img src="<?php echo base_url('site_assets/images/quickgal/7.jpg')?>" class="img-fluid" alt="" title="" />

											<!-- Title caption -->
											<h4 class="pg-caption">
												Teambuilding hangout<br>
												<a href="#">@PalmBeach</a>
											</h4>
										</li>
										<!--/ Slide image #7 -->

										<!-- Slide image #8 -->
										<li data-thumb="<?php echo base_url('site_assets/images/quickgal/8.jpg')?>">
											<!-- Image -->
											<img src="<?php echo base_url('site_assets/images/quickgal/8.jpg')?>" class="img-fluid" alt="" title="" />

											<!-- Title caption -->
											<h4 class="pg-caption">
												Into the fog<br>
												<a href="#">@Mistic lake</a>
											</h4>
										</li>
										<!--/ Slide image #8 -->

										<!-- Slide image #9 -->
										<li data-thumb="<?php echo base_url('site_assets/images/quickgal/9.jpg')?>">
											<!-- Image -->
											<img src="<?php echo base_url('site_assets/images/quickgal/9.jpg')?>" class="img-fluid" alt="" title="" />

											<!-- Title caption -->
											<h4 class="pg-caption">
												Teambuilding hangout<br>
												<a href="#">@Crazy Mountain</a>
											</h4>
										</li>
										<!--/ Slide image #9 -->
									</ul>
									<!-- Slides -->

									<!-- Navigation arrows -->
									<div class="cfs--navigation">
										<a href="#" class="cfs--prev"></a>
										<a href="#" class="cfs--next"></a>
									</div>
									<!--/ Navigation arrows -->

									<!-- Counter -->
									<div class="cfs-counter"></div>
								</div>
							</div>
							<!--/ Photo gallery wrapper -->
						</div>
						<!--/ Photo Gallery alternative -->
					</div>
					<!--/ col-sm-12 col-md-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</section>
		<!--/ Photo gallery alternative style section with custom paddings -->