<!-- Support Panel -->
	<input type="checkbox" id="support_p" class="panel-checkbox">
	<div class="support_panel">
		<div class="support-close-inner">
			<label for="support_p" class="spanel-label inner">
				<span class="support-panel-close">×</span>
			</label>
		</div>	
		<div class="container">		
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-9">
					<!-- Title -->
					<h4 class="m_title mb-20">
						HOW TO SHOP
					</h4>

					<!-- Content - how to shop steps -->
					<div class="m_content how_to_shop">
						<div class="row">
							<div class="col-sm-4">
								<span class="number">1</span> Login or create new account.
							</div>
							<!--/ col-sm-4 -->

							<div class="col-sm-4">
								<span class="number">2</span> Review your order.
							</div>
							<!--/ col-sm-4 -->

							<div class="col-sm-4">
								<span class="number">3</span> Payment <strong>FREE</strong> shipment
							</div>
							<!--/ col-sm-4 -->
						</div>
						<!--/ row -->

						<p>
							If you still have problems, please let us know, by sending an email to support@website.com . Thank you!
						</p>
					</div>
					<!--/ Content - how to shop steps -->
				</div>
				<!--/ col-sm-12 col-md-12 col-lg-9 -->

				<div class="col-sm-12 col-md-12 col-lg-3">
					<!-- Title -->
					<h4 class="m_title mb-20">
						SHOWROOM HOURS
					</h4>

					<!-- Content -->
					<div class="m_content">
						Mon-Fri 9:00AM - 6:00AM<br>
						Sat - 9:00AM-5:00PM<br>
						Sundays by appointment only!
					</div>
					<!--/ Content -->
				</div>
				<!--/ col-sm-12 col-md-12 col-lg-3 -->
			</div>
			<!--/ row -->
		</div>
		<!--/ container -->
	</div>
<!--/ Support Panel -->