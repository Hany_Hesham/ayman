
		<!-- Footer - Default Style 1 -->
		<footer id="footer" data-footer-style="1">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-9 mb-30">
						<!-- Title -->
						<h1 class="title m_title">
							<?php echo $this->data['about_us']['title'] ?>
						</h1>
						<div class="sbs">
							<ul class="menu">
								<li><a href="<?php echo base_url('') ?>">HOME</a></li>
								<li><a href="<?php echo base_url('about_us') ?>"><?php echo translate('About US', $this->data['language']) ?></a></li>
								<li><a href="<?php echo base_url('rules') ?>"><?php echo translate('Rules', $this->data['language']) ?></a></li>
								<!-- <li><a href="<?//php //echo base_url('gallery') ?>"><?//php //echo translate('Gallery', $this->data['language']) ?></a></li> -->
								<li><a href="<?php echo base_url('competition') ?>"><?php echo translate('Competition', $this->data['language']) ?></a></li>
								<li><a href="<?php echo base_url('result') ?>"><?php echo translate('Result', $this->data['language']) ?></a></li>
								<!-- will show if loged in -->
								<li><a href="<?php echo base_url('projects') ?>"><?php echo translate('Projects', $this->data['language']) ?></a></li>
								<li><a href="<?php echo base_url('previous') ?>"><?php echo translate('Projects Archive', $this->data['language']) ?></a></li>
							</ul>
						</div>
					</div>
					<!--/ col-sm-12 col-md-5 mb-30 -->

					<div class="col-sm-12 col-md-3 mb-30">
						<!-- Title -->
						<h3 class="title m_title">
							GET IN TOUCH
						</h3>

						<!-- Contact details -->
						<div class="contact-details">
							<p>
								Tel: <a href="tel:<?php echo $this->data['contact']['paragraph']?>"><?php echo $this->data['contact']['paragraph']?></a><br>
								Email: <a href="mailto:<?php echo $this->data['email']['paragraph']?>"><?php echo $this->data['email']['paragraph']?></a>
							</p>
							<p>
								<a href="<?php echo $this->data['location']['img']?>" target="_blank">
									<i class="icon-map-marker white-icon"></i>
									<?php echo translate('Open in Google Maps', $this->data['language']) ?>
								</a>
							</p>
						</div>
						<!--/ .contact-details -->
					</div>
					<!--/ col-sm-12 col-md-3 mb-30 -->
				</div>
				<!--/ row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="bottom clearfix">
							<!-- social-icons -->
							<ul class="social-icons sc--clean clearfix">
								<li class="title">GET SOCIAL</li>
								<?php  foreach ($this->data['social_media'] as $social_media):?>
										<li>
											<a href="<?php echo $social_media['paragraph'] ?>" target="_blank" title="<?php echo translate($social_media['title'], $this->data['language']) ?>">
												<i class="<?php echo $social_media['img'] ?>"></i>
											</a>
										</li>
									<?php endforeach; ?>
							</ul>
							<!--/ social-icons -->

							<!-- copyright -->
							<div class="copyright">
								<a href="<?php echo base_url()?>">
									<img width="80" height="25" src="<?php echo base_url('site_assets/images/logos/'.$this->data['logo']['img'])?>" alt="">
								</a>
								
								<p>
									© 2021 All rights reserved.
								</p>
							</div>
							<!--/ copyright -->
						</div>
						<!--/ bottom -->
					</div>
					<!--/ col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</footer>
		<!--/ Footer - Default Style 1 -->
	</div>
	<!--/ Page Wrapper -->


	<!-- Login Panel content -->
	<?php $this->load->view('clients/includes/login-modal')?>

	<!-- sign up modal form -->
	<?php $this->load->view('clients/includes/signup-modal')?>

	<div id="forgot_panel" class="mfp-hide loginbox-popup auth-popup forgot-popup">
		<div class="inner-container forgot-panel auth-popup-panel">
			<h3 class="m_title m_title_ext text-custom auth-popup-title">
				FORGOT YOUR DETAILS?
			</h3>

			<form class="forgot_form" name="forgot_form" method="post" action="#">
				<div class=" kl-fancy-form">
					<input type="text" id="forgot-email" name="user_login" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="...">
					<label class="kl-font-alt kl-fancy-form-label">
						USERNAME OR EMAIL
					</label>
				</div>

				<div class="">
					<input type="submit" id="recover" name="submit" class="btn btn-block zn_sub_button btn-fullcolor btn-md" value="SEND MY DETAILS!">
				</div>
				
				<div class="links auth-popup-links">
					<a href="#login_panel" class="kl-login-box auth-popup-link">
						AAH, WAIT, I REMEMBER NOW!
					</a>
				</div>
			</form>
		</div>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	</div>
	<!--/ Login Panel content -->

	<!-- Contact form pop-up element content -->
	<div id="contact_panel" class="mfp-hide contact-popup">
		<div class="contact-popup-panel">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<!-- Contact form pop-up element -->
						<div class="contactForm pop-up-form">
							<!-- Google reCaptcha required javascript file -->
							<script src='https://www.google.com/recaptcha/api.js'></script>

							<!-- Title -->
							<h3 class="m_title m_title_ext text-custom contact-popup-title tcolor">
								GET A QUOTE
							</h3>
							<h4 class="tbk__subtitle fw-thin">
								We'll do everything we can to make our next best project!
							</h4>

							<form action="php_helpers/_contact-process.php" method="post" class="contact_form row mt-40" enctype="multipart/form-data">
								<div class="cf_response"></div>

								<div class="col-sm-6 kl-fancy-form">
									<input type="text" name="name" id="cf_name-pop-up" class="form-control" placeholder="Please enter your first name" value="" tabindex="1" maxlength="35" required>
									<label class="control-label">
										FIRSTNAME
									</label>
								</div>

								<div class="col-sm-6 kl-fancy-form">
									<input type="text" name="lastname" id="cf_lastname-pop-up" class="form-control" placeholder="Please enter your first last name" value="" tabindex="1" maxlength="35" required>
									<label class="control-label">
										LASTNAME
									</label>
								</div>

								<div class="col-sm-12 kl-fancy-form">
									<input type="text" name="email" id="cf_email-pop-up" class="form-control h5-email" placeholder="Please enter your email address" value="" tabindex="1" maxlength="35" required>
									<label class="control-label">
										EMAIL
									</label>
								</div>

								<div class="col-sm-12 kl-fancy-form">
									<input type="text" name="subject" id="cf_subject-pop-up" class="form-control" placeholder="Enter the subject message" value="" tabindex="1" maxlength="35" required>
									<label class="control-label">
										SUBJECT
									</label>
								</div>

								<div class="col-sm-12 kl-fancy-form">
									<textarea name="message" id="cf_message-pop-up" class="form-control" cols="30" rows="10" placeholder="Your message" tabindex="4" required></textarea>
									<label class="control-label">
										MESSAGE
									</label>
								</div>

								<!-- Google recaptcha required site-key (change with yours => https://www.google.com/recaptcha/admin#list) -->
								<div class="g-recaptcha" data-sitekey="SITE-KEY"></div>
								<!--/ Google recaptcha required site-key -->

								<div class="col-sm-12">
									<!-- Contact form send button -->
									<button class="btn btn-fullcolor" type="submit">
										Send
									</button>
								</div>
							</form>
						</div>
						<!--/ Contact form pop-up element -->
						<a  id="href-success-panel" style="display:none;" href="#success_panel-modal" class="create_account auth-popup-createacc kl-login-box auth-popup-link">
								
						</a>
						<div id="success_panel-modal" class="mfp-hide loginbox-popup auth-popup" style="background-color: #FFF;">
						<div class="inner-container register-panel auth-popup-panel">
							<h3 class="m_title m_title_ext text-custom auth-popup-title"></h3>
							<div class="thank-you-pop">
								<div id="ajax-loading-block" style="display: none;">
									<img id="success-image" src="<?php echo base_url('site_assets/images/ajax-loader.gif')?>" alt="">
								</div>
								<div id="ajax-success-block" style="display: none;">
									<img id="success-image" src="<?php echo base_url('site_assets/images/Green-success.png')?>" alt="">
									<h1>Thank You!</h1>
									<p>you have been registered successfully</p>
								</div>
								<div id="ajax-failure-block" style="display: none;">
									<img id="failure-image"style="width:130px; height:auto;" src="<?php echo base_url('site_assets/images/failure-rounded.png')?>" alt="">	
								    <h1>Sorry!</h1>
									<p>Your submission is failed please try again</p>
									<p style="color: #cd2122 !important;display:none;" id="email-exist-message">this email already exists</p>
									<a href="#register_panel" class="kl-login-box auth-popup-link">
										CREATE AN ACCOUNT
									</a>

								</div>					
 						</div>
						</div>
					</div>
					<!--/ col-md-12 col-sm-12 -->
				</div>
				<!--/ .row -->
			</div>
			<!--/ .container -->
		</div>