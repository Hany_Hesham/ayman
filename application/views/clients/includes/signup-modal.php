<div id="register_panel" class="mfp-hide loginbox-popup auth-popup">
		<div class="inner-container register-panel auth-popup-panel">
			<h3 class="m_title m_title_ext text-custom auth-popup-title">
			<?php echo translate('CREATE ACCOUNT', $this->data['language']) ?>
			</h3>

			<form  id="reg-account-info" class="register_panel" name="register_panel" method="post"
			enctype="multipart/form-data" action="#">
				<div class="kl-fancy-form">
					<input type="text" id="reg-username" name="reg_name"
					 class="form-control inputbox kl-fancy-form-input kl-fw-input" 
					 placeholder="type your first name" required>
					<label class="kl-font-alt kl-fancy-form-label">
					    <?php echo translate('First NAME', $this->data['language']) ?>
				    </label>
					<span class="text-danger" style="font-size: 12px; display:none" id="error-reg-username">
					    Please enter a valid first name
				    </span>
				</div>

				<div class="kl-fancy-form">
					<input type="text" id="reg-middle-name" name="reg_middle_name"
					 class="form-control inputbox kl-fancy-form-input kl-fw-input" 
					 placeholder="type your middle name" required>
					<label class="kl-font-alt kl-fancy-form-label">
					    <?php echo translate('Middle NAME', $this->data['language']) ?>
				    </label>
					<span class="text-danger" style="font-size: 12px; display:none" id="error-reg-middle-name">
					    Please enter a valid middle name
				    </span>
				</div>

				<div class="kl-fancy-form">
					<input type="text" id="reg-lastname" name="reg_lastname"
					 class="form-control inputbox kl-fancy-form-input kl-fw-input" 
					 placeholder="type your last name" required>
					<label class="kl-font-alt kl-fancy-form-label">
					    <?php echo translate('Last NAME', $this->data['language']) ?>
				    </label>
					<span class="text-danger" style="font-size: 12px; display:none" id="error-reg-lastname">
					    Please enter a valid last name
				    </span>
				</div>

				<div class="kl-fancy-form">
					<input type="date" id="reg-birth_date" name="birth_date" 
					class="form-control inputbox kl-fancy-form-input kl-fw-input" required>
					<label class="kl-font-alt kl-fancy-form-label">
					<?php echo translate('Date of Birth', $this->data['language']) ?>
					</label>
					<span class="text-danger" style="font-size: 12px; display:none" id="error-reg-birth_date">
					    Please enter a valid date
				    </span>
				</div>

				<div class="kl-fancy-form">
					<input type="text" id="reg-city" name="city" 
					class="form-control inputbox kl-fancy-form-input kl-fw-input" required>
					<label class="kl-font-alt kl-fancy-form-label">
					<?php echo translate('City', $this->data['language']) ?>
					</label>
					<span class="text-danger" style="font-size: 12px; display:none" id="error-reg-city">
					    Please enter a valid city name
				    </span>
				</div>

				<div class="kl-fancy-form">
					<input type="text" id="reg-school" name="school" 
					class="form-control inputbox kl-fancy-form-input kl-fw-input" required>
					<label class="kl-font-alt kl-fancy-form-label">
					<?php echo translate('School', $this->data['language']) ?>
					</label>
					<span class="text-danger" style="font-size: 12px; display:none" id="error-reg-school">
					    Please enter a valid school name
				    </span>
				</div>

				<div class="kl-fancy-form">
					<input type="text" id="reg-email" name="reg_email" 
					class="form-control inputbox kl-fancy-form-input kl-fw-input"
					 placeholder="your-email@website.com" required>
					<label class="kl-font-alt kl-fancy-form-label">
						<?php echo translate('EMAIL', $this->data['language']) ?>
					</label>
					<span class="text-danger" style="font-size: 12px; display:none" id="error-reg-email">
					    Please enter a valid email
				    </span>
				</div>
				
				<div class="kl-fancy-form" style="height: 100%;">
					<select class="form-control" name="countryCode" id="countryCodeOptions" 
					style="height: 100%;">
					</select>
					<label class="kl-font-alt kl-fancy-form-label">
					<?php echo translate('Country Code', $this->data['language']) ?>
					</label>
				</div>

				<div class="kl-fancy-form">
					<input type="text" id="reg-whatsapp" name="whatsapp"
					 class="form-control inputbox kl-fancy-form-input kl-fw-input" required>
					<label class="kl-font-alt kl-fancy-form-label">
					<?php echo translate('Whatsapp Number', $this->data['language']) ?>
					</label>
					<span class="text-danger" style="font-size: 12px; display:none" id="error-reg-whatsapp">
					    Please enter a valid whatsapp number
				    </span>
				</div>

				<div class="kl-fancy-form">
					<input type="file" name="profile_picture" id="reg-profile_picture" 
					class="form-control inputbox kl-fancy-form-input kl-fw-input" >
					<label class="kl-font-alt kl-fancy-form-label">
						Profile Picture
					</label>
				</div>

				<div class=" kl-fancy-form">
					<input type="password" id="reg-pass" name="password" 
					class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="*****" required>
					<label class="kl-font-alt kl-fancy-form-label">
					    <?php echo translate('PASSWORD', $this->data['language']) ?>
					</label>
				</div>

				<div class="kl-fancy-form">
					<input type="password" id="reg-pass2" name="password2" 
					class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="*****" required>
					<label class="kl-font-alt kl-fancy-form-label">
						<?php echo translate('CONFIRM PASSWORD', $this->data['language']) ?>
					</label>
				</div>

				<div class="">
					<input type="submit" id="signup" name="submit" class="btn zn_sub_button btn-block btn-fullcolor btn-md" value="CREATE MY ACCOUNT" disabled>
				</div>

				<div class="links auth-popup-links">
					<a href="#login_panel" class="kl-login-box auth-popup-link">
						ALREADY HAVE AN ACCOUNT?
					</a>
				</div>
			</form>
		</div>
	</div>

<script>
    $(document).ready(function(){
		$('#countryCodeOptions').append(`${countryCodeOptions}`)
        $('#reg-account-info').keyup(function(){
            validateSignup();
        });
    });
    $(function () {
        $('#reg-account-info').on('submit', function (e) {
            $('#href-success-panel')[0].click();
            $('#ajax-loading-block').show();
            e.preventDefault();  
			var newformData = new FormData(document.getElementById("reg-account-info"));      
            $.ajax({
            type: 'post',
            url: '<?php echo base_url("clients/Competitors/add") ?>',
            data: newformData,
			processData: false,     
			contentType: false,     
			cache: false,
            success: function(data) { 
                setTimeout(function(){
                $('#ajax-loading-block, #ajax-failure-block').hide();
                $('#ajax-success-block').show();
                $(`#reg-pass, #reg-pass2, #reg-username, #reg-birth_date, #reg-city, #reg-school, 
                #reg-email, #reg-whatsapp`).val('');
                }, 1000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#ajax-loading-block, #ajax-success-block').hide();
                $('#ajax-failure-block').show();
                if(jqXHR.responseJSON !== undefined 
                && jqXHR.responseJSON.hasOwnProperty('type') 
                && jqXHR.responseJSON.type == 'this email already exist'){
                    $('#email-exist-message').show();
                }else{
                    $('#email-exist-message').hide();
                } 
            }	
            });
        });
    });

    function cofirmPassword(){
        let password = $('#reg-pass').val();
        let confirmedPassword = $('#reg-pass2').val();
        if( (password != '' && confirmedPassword != '') && (password === confirmedPassword)){
            return true;
        }else{
            return false;
        }
    }

    function validateSignup(){
        let username = validateField('reg-username', 'text');
		let middleName = validateField('reg-middle-name', 'text');
		let lastName = validateField('reg-lastname', 'text');
        let birth_date = validateField('reg-birth_date', '');
        let city = validateField('reg-city', 'text');
        let school = validateField('reg-school', 'text');
        let email = validateField('reg-email', 'email');
        let whatsapp = validateField('reg-whatsapp', 'number');
        let password = cofirmPassword();
        if(username === true && middleName === true && lastName === true && birth_date === true && city === true && school === true
            && email === true && whatsapp === true  && password === true){
            $('#signup').attr('disabled',false);
            }else{
            $('#signup').attr('disabled',true);
            }
    }
        
    </script>