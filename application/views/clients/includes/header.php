<!-- Header style 1 -->
<style>
	/* width */
	::-webkit-scrollbar {
	width: 10px;
	}

	/* Track */
	::-webkit-scrollbar-track {
	background: #f1f1f1; 
	}

	/* Handle */
	::-webkit-scrollbar-thumb {
	background: #888; 
	}

	/* Handle on hover */
	::-webkit-scrollbar-thumb:hover {
	background: #555; 
	}
</style>
<header id="header" class="site-header cta_button" data-header-style="1">
	<!-- Header background -->
	<div class="kl-header-bg"></div>
	<!--/ Header background -->

	<!-- Header wrapper -->
	<div class="site-header-wrapper">
		<!-- Header Top wrapper -->
		<div class="site-header-top-wrapper">
			<!-- Header Top container -->
			<div class="siteheader-container container">
				<!-- Header Top -->
				<div class="site-header-row site-header-top d-flex flex-row">
					<!-- Header Top Left Side -->
					<div class="site-header-top-left w-md-60 w-50 d-flex justify-content-start">
						<!-- Header Top Social links -->
						<ul class="topnav social-icons sc--clean align-self-center">
							<?php  foreach ($this->data['social_media'] as $social_media):?>
								<li>
									<a href="<?php echo $social_media['paragraph'] ?>" target="_blank" title="<?php echo translate($social_media['title'], $this->data['language']) ?>">
										<i class="<?php echo $social_media['img'] ?>"></i>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
						<!--/ Header Top Social links -->

						<div class="clearfix visible-xxs">
						</div>

						<!-- Top Header contact text -->
						<div class="kl-header-toptext align-self-center">	
							<span class="topnav-item--text">QUESTIONS? CALL: </span>
							<a href="tel:<?php echo $this->data['contact']['paragraph'] ?>" class="fw-bold"><?php echo $this->data['contact']['paragraph'] ?></a>
							<i class="<?php echo $this->data['contact']['img'] ?>"></i>
						</div>
						<!--/ Top Header contact text -->
					</div>
					<!--/ .site-header-top-left -->

					<!-- Header Top Right Side -->
					<div class="site-header-top-right w-md-40 w-50 d-flex justify-content-end">
						<!-- Languages -->
						<div class="topnav topnav--lang align-self-center">
							<div class="languages drop">
								<a href="#" class="topnav-item">
									<span class="fas fa-globe xs-icon"></span>
									<span class="topnav-item--text"><?php echo translate('LANGUAGES', $this->data['language']) ?></span>
								</a>
								<div class="pPanel">
									<ul class="inner">
										<li class="toplang-item active">
											<a href="<?php echo base_url('clients/home/language_converter/english');?>">
												<img src="<?php echo base_url('site_assets/images/flags/eng.png')?>" alt="English" class="toplang-flag "> <?php echo translate('English', $this->data['language']) ?>
											</a>
										</li>
										<li class="toplang-item">
											<a href="<?php echo base_url('clients/home/language_converter/arabic');?>">
												<img src="<?php echo base_url('site_assets/images/flags/arb.png')?>" alt="Arabic" class="toplang-flag "> <?php echo translate('Arabic', $this->data['language']) ?>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!--/ Languages -->

						<!-- <div class="topnav support--panel align-self-center"> -->
							<!-- Support panel trigger -->
							<!-- <label for="support_p" class="topnav-item spanel-label">
								<i class="fas fa-info-circle support-info"></i>
								<span class="topnav-item--text">SUPPORT</span>
							</label> -->
							<!--/ Support panel trigger -->
						<!-- </div> -->

						<!-- Login trigger -->
						<?php if($this->global_data['is_logged'] == false) {?>
						<div class="topnav login--panel align-self-center">
							<a class="topnav-item popup-with-form" href="#login_panel">
								<i class="login-icon fas fa-sign-in-alt visible-xs xs-icon"></i>
								<span class="topnav-item--text">LOGIN</span>
							</a>
						</div>
						<?php }?>
						<!--/ Login trigger -->	
						<!-- logined user section  -->
						<?php if($this->global_data['is_competitor'] == true) {?>
								<div class="mainnav mainnav--cart d-flex align-self-center">
							<div class="drop">
								<a href="#" class="kl-cart-button" title="User Profile">
									<i class="fas fa-user lg-icon" style="color: #FFF;" data-count="<?php echo count($this->global_data['notifications'])?>"></i>
								</a>
								<div class="pPanel">
									<div class="inner cart-container">
										<div class="widget_shopping_cart_content">
											<ul class="cart_list product_list_widget ">
												<li>
												<a href="#" class="product-title">
													<?php 
														$picture_url = $this->global_data['userData']['competitor_data']['profile_picture'] !='' &&
														$this->global_data['userData']['competitor_data']['profile_picture'] !== '0' ?
														base_url('site_assets/uploads/users_profiles/'. $this->global_data['userData']['competitor_data']['profile_picture'])
														:
														base_url('site_assets/images/4096.jpg');
															
													?>
													<img src="<?php echo $picture_url?>" style="background-color: #FFF;" alt="" title="">
													<br><?php echo $this->global_data['userData']['name']?>   
												</a>
												</li>
												<li>	
													<a href="<?php echo base_url('competition');?>" class="product-title">
														<strong>
															<i class="icon-process3" style="color: #cd2122 !important;font-size:22px;"></i>
															<?php echo translate('Competitions', $this->data['language']) ?>
														</strong>  
													</a>
													<a href="<?php echo base_url('clients/home/pending_registrations/#pending-registration');?>" class="product-title">
														<strong>
															<i class="icon-gi-ico-11" style="color: #cd2122 !important;font-size:22px;"></i>
															<?php echo translate('Pending Projects', $this->data['language']) ?>
														</strong>  
													</a>
													<a href="<?php echo base_url('clients/home/pending_registrations/#current-projects');?>" class="product-title">
														<strong>
															<i class="icon-noun_61152" style="color: #cd2122 !important;font-size:22px;"></i>
															<?php echo translate('Projects', $this->data['language']) ?>
														</strong>  
													</a>
												</li>
											</ul>
											<div style="overflow-y: scroll; overflow-x: hidden;height:150px;">
												<h5 style="color: #cd2122 !important;">                      
													<?php echo translate('notifications', $this->data['language']) ?>
												</h5>
												<ul class="">
													<?php if(isset($this->global_data['notifications']) && count($this->global_data['notifications']) > 0){
														foreach($this->global_data['notifications'] as $notify){?>
														<li>
															<a href="<?php echo $notify['link']?>" onclick="delNotify(<?php echo $notify['id']?>)" style="font-size:12px;">
																<div class="row">
																	<div class="col-sm-1">
																		<i class="icon-process1" style="color: #cd2122 !important;font-size:22px;"></i>
																	</div>
																	<div class="col-sm-10">
																		<?php echo translate($notify['message'], $this->data['language']) ?><br>
																		<strong style="font-size:11px;"><?php echo $notify['timestamp']?></strong>
																	</div>
																</div>
														    </a>
														</li>

													<?php }}?>												</ul>
											</div>
											<hr>
											<p class="buttons">
												<a href="<?php echo base_url('clients/competitors/logout');?>" class="button wc-forward">
													<?php echo translate('Log Out', $this->data['language']) ?>
												</a>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php }elseif($this->global_data['is_voter'] == true) {?>
								<div class="mainnav mainnav--cart d-flex align-self-center">
							<div class="drop">
								<a href="#" class="kl-cart-button" title="User Profile">
									<i class="fas fa-user lg-icon" style="color: #FFF;" data-count="0"></i>
								</a>
								<div class="pPanel">
									<div class="inner cart-container">
										<div class="widget_shopping_cart_content">
											<ul class="cart_list product_list_widget ">
												<li>
												<a href="#" class="product-title">
													<img src="<?php echo $this->global_data['userData']['picture']->url?>" style="background-color: #FFF;" alt="" title="">
													<br><?php echo $this->global_data['userData']['first_name'].' '.$this->global_data['userData']['last_name']?>   
												</a>
												</li>
											</ul>
											<p class="buttons">
												<a href="<?php echo base_url('clients/voters/logout');?>" class="button wc-forward">
													<?php echo translate('Log Out', $this->data['language']) ?>
												</a>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php }?>
						<!-- end of logined user section -->
					<!--/ .site-header-top-right -->
				</div>
				<!--/ .site-header-row .site-header-top -->

				<!-- Header separator -->
				<div class="separator site-header-separator"></div>
				<!--/ Header separator -->
			</div>
			<!--/ .siteheader-container .container -->
		</div>
		<!--/ Header Top wrapper -->

		<!-- Header Main wrapper -->
		<div class="site-header-main-wrapper d-flex">
			<!-- Header Main container -->
			<div class="siteheader-container container align-self-center">
				<!-- Header Main -->
				<div class="site-header-row site-header-main d-flex flex-row justify-content-between">
					<!-- Header Main Left Side -->
					<div class="site-header-main-left d-flex justify-content-start align-items-center">
						<!-- Logo container-->
						<div class="logo-container hasInfoCard logosize--yes">
							<!-- Logo -->
							<h1 class="site-logo logo" id="logo">
								<a href="<?php echo base_url('') ?>" title="">
									<img src="<?php echo base_url('site_assets/images/logos/'.$this->data['logo']['img'])?>" class="logo-img" alt="" title="" />
								</a>
							</h1>
							<!--/ Logo -->

							<!-- InfoCard -->
							<div id="infocard" class="logo-infocard">
								<div class="custom">
									<div class="row">
										<div class="col-sm-6 left-side d-flex">
											<div class="align-self-center">
												<div class="infocard-wrapper text-center">
													<img src="<?php echo base_url('site_assets/images/logos/'.$this->data['logo']['img'])?>" class="mb-25" alt="" title="" />
													<h1>
														<?php echo $this->data['about_us']['title'] ?>
													</h1>
													<p>
														<?php echo $this->data['about_us']['paragraph'] ?>
													</p>
												</div>
												<!--/ infocard-wrapper -->
											</div>
											<!--/ .align-self-center -->
										</div>
										<!--/ col-sm-6 left-side d-flex -->

										<div class="col-sm-6 right-side">
											<div class="custom contact-details">
												<p>
													<?php echo translate($this->data['location']['paragraph'], $this->data['language']) ?><br>
													<a href="mailto:<?php echo $this->data['email']['paragraph']?>"><?php echo $this->data['email']['paragraph']?></a>
												</p>
												<a href="<?php echo $this->data['location']['img']?>" class="map-link" target="_blank" title="">
													<span class="fas fa-map-marker-alt white-icon mr-10"></span>
													<span><?php echo translate('Open in Google Maps', $this->data['language']) ?></span>
												</a>
											</div>
											<div style="height:20px;">
											</div>
											<!-- Social links clean style -->
											<ul class="social-icons sc--clean">
												<?php  foreach ($this->data['social_media'] as $social_media):?>
													<li><a href="<?php echo $social_media['paragraph'] ?>" target="_blank" title="<?php echo translate($social_media['title'], $this->data['language']) ?>" class="<?php echo $social_media['img'] ?>"></a></li>
												<?php endforeach; ?>
											</ul>
											<!--/ Social links clean style -->
										</div>
										<!--/ col-sm-6 right-side -->
									</div>
									<!--/ row -->
								</div>
								<!--/ custom -->
							</div>
							<!--/ InfoCard -->
						</div>
						<!--/ logo container-->

						<!-- Separator -->
						<div class="separator visible-xxs"></div>
						<!--/ Separator -->
					</div>
					<!--/ .site-header-main-left -->

					<!-- Header Main Center Side -->
					<div class="site-header-main-center d-flex justify-content-center align-items-center">
						<!-- Main Menu wrapper -->
						<div class="main-menu-wrapper">
							<!-- Responsive menu trigger -->
							<div id="zn-res-menuwrapper">
								<a href="#" class="zn-res-trigger "></a>
							</div>
							<!--/ responsive menu trigger -->

							<!-- main menu -->
							<div id="main-menu" class="main-nav zn_mega_wrapper">
								<ul id="menu-main-menu" class="main-menu clearfix">
									<li>
										<a href="<?php echo base_url('') ?>">HOME</a>
									</li>
									<li>
										<a href="<?php echo base_url('about_us') ?>"><?php echo translate('About US', $this->data['language']) ?></a>
									</li>
									<li>
										<a href="<?php echo base_url('rules') ?>"><?php echo translate('Rules', $this->data['language']) ?></a>
									</li>
									<!-- <li>
										<a href="<?//php //echo base_url('gallery') ?>">
										<?//php// echo translate('Gallery', $this->data['language']) ?>
									</a>
									</li> -->
									<li>
										<a href="<?php echo base_url('competition') ?>"><?php echo translate('Competition', $this->data['language']) ?></a>
									</li>
									<li>
										<a href="<?php echo base_url('result') ?>"><?php echo translate('Result', $this->data['language']) ?></a>
									</li>
									<!-- will show if loged in -->
									<li>
										<a href="<?php echo base_url('projects') ?>"><?php echo translate('Projects', $this->data['language']) ?></a>
									</li>
									<li>
										<a href="<?php echo base_url('previous') ?>"><?php echo translate('Projects Archive', $this->data['language']) ?></a>
									</li>
								</ul>
							</div>
							<!--/ main menu -->
						</div>
						<!--/ .main-menu-wrapper -->
					</div>
					<!--/ .site-header-main-center -->
					<!--/ .site-header-main-right -->
				</div>
				<!--/ .site-header-row .site-header-main -->
			</div>
			<!--/ .siteheader-container .container -->
		</div>
		<!--/ Header Main wrapper -->
	</div>
	<!--/ Header wrapper -->
</header>
<!-- / Header style 1 -->

<script>
	function delNotify(id){
     var url=getUrl()+'clients/home/delete_notify/'+id;      
          $.ajax({
             'async':false,
              url: url,
              dataType: "json",
                success: function(data){ 
                  },
               }); 
                
        } 
</script>