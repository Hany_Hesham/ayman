
		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">

					<div class="row gutter-40 col-mb-80">
						<!-- Post Content
						============================================= -->
						<div class="postcontent col-lg-9">

							<p><span class="dropcap">F</span>oster best practices effectiveness inspire breakthroughs solve immunize turmoil. Policy dialogue peaceful The Elders rural global support. Process inclusive innovate readiness, public sector complexity. Lifting people up cornerstone partner, technology working families civic engagement activist recognize potential global network. Countries tackling solution respond change-makers tackle. Assistance, giving; fight against malnutrition experience in the field lasting change scalable. Empowerment long-term, fairness policy community progress social responsibility; Cesar Chavez recognition. Expanding community ownership visionary indicator pursue these aspirations accessibility. Achieve; worldwide, life-saving initiative facilitate. New approaches, John Lennon humanitarian relief fundraise vaccine Jane Jacobs community health workers Oxfam. Our ambitions informal economies.</p>

							<blockquote class="topmargin bottommargin">
								<p>Human rights healthcare immunize; advancement grantees. Medical supplies; meaningful, truth technology catalytic effect. Promising development capacity building international enable poverty.</p>
							</blockquote>

							<div class="row">
								<div class="col-md-6">
									<p>Provide, Aga Khan, interconnectivity governance fairness replicable, new approaches visionary implementation. End hunger evolution, future promising development youth. Public sector, small-scale farmers; harness facilitate gender. Contribution dedicated global change movements, prosperity accelerate progress citizens of change. Elevate; accelerate reduce child mortality; billionaire philanthropy fluctuation, plumpy'nut care opportunity catalyze. Partner deep.</p>
								</div>

								<div class="col-md-6">
									<p>Frontline harness criteria governance freedom contribution. Campaign Angelina Jolie natural resources, Rockefeller peaceful philanthropy human potential. Justice; outcomes reduce carbon emissions nonviolent resistance human being. Solve innovate aid communities; benefit truth rural development UNICEF meaningful work. Generosity Action Against Hunger relief; many voices impact crisis situation poverty pride. Vaccine carbon.</p>
								</div>
							</div>

						</div><!-- .postcontent end -->

						<!-- Sidebar
						============================================= -->
						<div class="sidebar col-lg-3">
							<div class="sidebar-widgets-wrap">

								<div class="widget widget_links clearfix">

									<h4>Pages</h4>
									<ul>
										<li><a href="#"><div>About Us</div></a></li>
									</ul>

								</div>

								<div class="widget clearfix">

									<h4>Recent Posts</h4>
									<div class="posts-sm row col-mb-30" id="post-list-sidebar">
										<div class="entry col-12">
											<div class="grid-inner row no-gutters">
												<div class="col-auto">
													<div class="entry-image">
														<a href="#"><img src="<?php echo base_url('site_assets/images/magazine/small/3.jpg')?>" alt="Image"></a>
													</div>
												</div>
												<div class="col pl-3">
													<div class="entry-title">
														<h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
													</div>
													<div class="entry-meta">
														<ul>
															<li>10th July 2021</li>
														</ul>
													</div>
												</div>
											</div>
										</div>

										<div class="entry col-12">
											<div class="grid-inner row no-gutters">
												<div class="col-auto">
													<div class="entry-image">
														<a href="#"><img src="<?php echo base_url('site_assets/images/magazine/small/2.jpg')?>" alt="Image"></a>
													</div>
												</div>
												<div class="col pl-3">
													<div class="entry-title">
														<h4><a href="#">Elit Assumenda vel amet dolorum quasi</a></h4>
													</div>
													<div class="entry-meta">
														<ul>
															<li>10th July 2021</li>
														</ul>
													</div>
												</div>
											</div>
										</div>

										<div class="entry col-12">
											<div class="grid-inner row no-gutters">
												<div class="col-auto">
													<div class="entry-image">
														<a href="#"><img src="<?php echo base_url('site_assets/images/magazine/small/1.jpg')?>" alt="Image"></a>
													</div>
												</div>
												<div class="col pl-3">
													<div class="entry-title">
														<h4><a href="#">Debitis nihil placeat, illum est nisi</a></h4>
													</div>
													<div class="entry-meta">
														<ul>
															<li>10th July 2021</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>

								
							</div>
						</div><!-- .sidebar end -->
					</div>

				</div>
			</div>
		</section><!-- #content end -->