<!doctype html>
<html dir="ltr" lang="en-US" class="no-js">
  <head>
    <!-- meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Uncomment the meta tags you are going to use! Be relevant and don't spam! -->
    <meta name="keywords" content="projects, robotics" />
    <meta name="description" content="Robotics">
    <!-- Title -->
    <title>RSS</title>
    <!-- Url of your website (without extra pages) 
      <link rel="canonical" href="https://kallyas-template.net" />
    -->
    <!-- Restrict google from scanning info from Dmoz or YahooDir
      More here: http://www.seoboy.com/what-are-the-meta-tags-noodp-and-noydir-used-for-in-seo/
      Also more on robots here https://yoast.com/articles/robots-meta-tags/ 
      <meta name="robots" content="noodp,noydir"/>
    -->
    <!--
      Social media tags and more >>>>> http://moz.com/blog/meta-data-templates-123 <<<<<
      Debugging tools:
      - https://dev.twitter.com/docs/cards/validation/validator
      - https://developers.facebook.com/tools/debug
      - http://www.google.com/webmasters/tools/richsnippets
      - http://developers.pinterest.com/rich_pins/validator/
    -->
    <!-- Google Authorship and Publisher Markup. You can also simply add your name.
      Author = Owner, Publisher = who built the website. Add profile url in href="".
      Profile url example: https://plus.google.com/1130658794498306186 or replace [Google+_Profile] below with your profile # 
      <link rel="author" href="https://plus.google.com/[Google+_Profile]/posts"/>
      <link rel="publisher" href="https://plus.google.com/[Google+_Page_Profile]"/>
    -->
    <!-- Schema.org markup for Google+ 
      <meta itemprop="name" content="Kallyas Premium Template">
      <meta itemprop="description" content="This is the page description">
      <meta itemprop="image" content="">
    -->
    <!-- Open Graph Protocol meta tags.
      Used mostly for Facebook, more here http://ogp.me/ 
      <meta property="og:locale" content="en"/>
      <meta property="og:type" content="website"/>
      <meta property="og:title" content="Kallyas Premium Template"/>
      <meta property="og:description" content="Kallyas is an ultra-premium, responsive theme built for todays websites."/>
      <meta property="og:site_name" content="Kallyas Premium Template"/>
    -->
    <!-- Url of your website 
      <meta property="og:url" content=""/>
    -->
    <!-- Representative image 
      <meta property="og:image" content=""/>
    -->
    <!-- Twitter Cards
      Will generate a card based on the info below.
      More here: http://davidwalsh.name/twitter-cards or https://dev.twitter.com/docs/cards 
      <meta name="twitter:card" content="summary">
    -->
    <!-- Representative image 
      <meta name="twitter:image" content="">
      <meta name="twitter:domain" content="hogash.com">
      <meta name="twitter:site" content="@hogash">
      <meta name="twitter:creator" content="@hogash">
    -->
    <!-- Url of your website 
      <meta name="twitter:url" content="">
      <meta name="twitter:title" content="How to Create a Twitter Card">
      <meta name="twitter:description" content="Twitter's new Twitter Cards API allows developers to add META tags to their website, and Twitter will build card content from links to a given site.">
    -->
    <!-- GeoLocation Meta Tags / Geotagging. Used for custom results in Google.
      Generator here http://mygeoposition.com/ 
      <meta name="geo.placename" content="Chicago, IL, USA" />
      <meta name="geo.position" content="41.8781140;-87.6297980" />
      <meta name="geo.region" content="US-Illinois" />
      <meta name="ICBM" content="41.8781140, -87.6297980" />
    -->
    <!-- Dublin Core Metadata Element Set
      Using DC metadata is advantageous from an SEO perspective because search engines might interpret the extra code as an effort to make page content as descriptive and relevant as possible.
      <link rel="schema.DC" href="http://purl.org/DC/elements/1.0/" />
      <meta name="DC.Title" content="Kallyas Premium Template, Kallyas Responsive Template" />
      <meta name="DC.Creator" content="hogash" />
      <meta name="DC.Type" content="software" />
      <meta name="DC.Date" content="2018-10-01" />
      <meta name="DC.Format" content="text/html" />
      <meta name="DC.Language" content="en" />
    -->
    <!-- end descriptive meta tags -->
    <!-- Retina Images -->
    <!-- Simply uncomment to use this script !! More here http://retina-images.complexcompulsions.com/
      <script>(function(w){var dpr=((w.devicePixelRatio===undefined)?1:w.devicePixelRatio);if(!!w.navigator.standalone){var r=new XMLHttpRequest();r.open('GET','/retinaimages.php?devicePixelRatio='+dpr,false);r.send()}else{document.cookie='devicePixelRatio='+dpr+'; path=/'}})(window)</script>
      <noscript><style id="devicePixelRatio" media="only screen and (-moz-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)">html{background-image:url("php-helpers/_retinaimages.php?devicePixelRatio=2")}</style></noscript>-->
    <!-- End Retina Images -->
    <!-- iDevices & Retina Favicons -->
    <link rel="apple-touch-icon-precomposed" type="image/x-icon" href="<?php echo base_url('site_assets/images/logos/'.$this->data['logo']['img'])?>" sizes="72x72" />
    <link rel="apple-touch-icon-precomposed" type="image/x-icon" href="<?php echo base_url('site_assets/images/logos/'.$this->data['logo']['img'])?>" sizes="114x114" />
    <link rel="apple-touch-icon-precomposed" type="image/x-icon" href="<?php echo base_url('site_assets/images/logos/'.$this->data['logo']['img'])?>" sizes="144x144" />
    <link rel="apple-touch-icon-precomposed" type="image/x-icon" href="<?php echo base_url('site_assets/images/logos/'.$this->data['logo']['img'])?>" />
    <!--  Desktop Favicons  -->
    <link rel="icon" type="image/png" href="<?php echo base_url('site_assets/images/logos/'.$this->data['logo']['img'])?>" sizes="32x32">
    <!-- Google Fonts CSS Stylesheet // More here http://www.google.com/fonts#UsePlace:use/Collection:Open+Sans -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,600italic,700,800,800italic" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <!-- ***** Boostrap Custom / Addons Stylesheets ***** -->
    <link rel="stylesheet" href="<?php echo base_url('site_assets/css/bootstrap.css')?>" type="text/css" media="all">
    <!-- Font Awesome icons library -->
    <link rel="stylesheet" href="<?php echo base_url('site_assets/fonts/font-awesome/css/font-awesome.min.css')?>" type="text/css" media="all">
    <!-- Laptop Slider required CSS -->
    <link rel="stylesheet" href="<?php echo base_url('site_assets/css/sliders/laptop-slider/laptop-slider.css')?>" type="text/css" media="all">
    <!-- ***** Main + Responsive & Base sizing CSS Stylesheet ***** -->
    <link rel="stylesheet" href="<?php echo base_url('site_assets/css/template.css')?>" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo base_url('site_assets/css/responsive.css')?>" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo base_url('site_assets/css/base-sizing.css')?>" type="text/css" media="all">
    <!-- Required custom CSS file for this niche -->
    <link rel="stylesheet" href="<?php echo base_url('site_assets/css/niches/custom-business.css')?>" type="text/css" />
    <!-- Custom CSS Stylesheet (where you should add your own css rules) -->
    <link rel="stylesheet" href="<?php echo base_url('site_assets/css/custom.css" type="text/css')?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('site_assets/DataTables/datatables.min.css')?>"/>
 
    <!-- Modernizr Library -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/modernizr.min.js')?>"></script>
    <!-- jQuery Library -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/jquery.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/custome.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('site_assets/DataTables/datatables.min.js')?>"></script>
  </head>
  <body class="">
    <input type="hidden" id="baseurl" name="baseurl" value="<?php echo base_url(); ?>" />
    <?php $this->load->view($this->data['support']);?>      
    <!-- Page Wrapper -->
    <div id="page_wrapper">
      <?php $this->load->view($this->data['header']);?>      
      <?php $this->load->view($view);?>
      <?php $this->load->view($this->data['footer']);?>   
      <!--/ .contact-popup-panel -->
      <button title="Close (Esc)" type="button" class="mfp-close">×</button>
    </div><!-- #wrapper end -->
    
    <!-- ToTop trigger -->
    <a href="#" id="totop">TOP</a>
    <!--/ ToTop trigger -->
    <!-- JS FILES // These should be loaded in every page -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/kl-plugins.js')?>"></script>
    <!-- JS FILES // Loaded on this page -->
    <!-- Required js script file for iOS slider element (Laptop Slider) -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/plugins/_sliders/ios/jquery.iosslider.min.js')?>"></script>

    <!-- Required js trigger file for Laptop Slider element -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/trigger/slider/laptop-slider/kl-laptop-slider.js')?>"></script>

    <!-- Slick required js script for Recent Work Carousel element -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/plugins/_sliders/slick/slick.js')?>"></script>
    <!-- Required script for Photo gallery alternative (Caroufredsel) -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/plugins/_sliders/caroufredsel/jquery.carouFredSel-packed.js')?>"></script>
    <!-- Required js trigger for Recent Work Carousel element -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/trigger/kl-slick-slider.js')?>"></script>
    <!-- Required script for PhotoWall element - Isotope filter -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/plugins/jquery.isotope.min.js')?>"></script>

    <!-- Required js trigger for Photowall element -->
    <script type="text/javascript">
      jQuery(document).ready(function(){
        var photoWall = jQuery('#photowall');
        // check if images loaded
        // and initiate isotope plugin
        photoWall.imagesLoaded(function(){
          var doIsotope = photoWall.isotope({
            itemSelector : '.isotope-item',
            masonry: {
              columnWidth: '.grid-sizer',
              gutter:0
            }
          });
          doIsotope.isotope('layout');

          jQuery(window).on('resize', function(){
            photoWall.isotope('layout');
          }).trigger('resize');
          // End isotope
        });
      });
    </script>
    <!-- Required js script for jQuery Loupe -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/plugins/jquery_loupe/jquery-loupe.js')?>"></script>
    <!-- Required jQuery migrate plugin for jQuery Product Loupe element -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/jquery-migrate.min.js')?>"></script>
    <!-- Required js script trigger for Product Loupe element -->
    <script type="text/javascript">
      (function(){
        jQuery('.kjq-loupe').loupe({
          'default_zoom': 150,
          'default_size' : 160,
          'apply_overlay' : false,
          'drop_shadow' : false
        });
      })();
    </script>
    <!-- Required js scripts for Parallax background sections plugin element -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/plugins/parallax/KyHtmlParallax.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/plugins/parallax/parallax.js')?>"></script>
    <!-- Custom Kallyas JS codes -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/kl-scripts.js')?>"></script>
    <!-- Custom user JS codes -->
    <script type="text/javascript" src="<?php echo base_url('site_assets/js/kl-custom.js')?>"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-XXXXX-X', 'auto');
        ga('send', 'pageview');
      </script>
    -->
  </body>
</html>