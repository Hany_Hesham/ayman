<div id="login_panel" class="mfp-hide loginbox-popup auth-popup">
    <div class="inner-container login-panel auth-popup-panel">
        <h3 class="m_title m_title_ext text-custom auth-popup-title tcolor">
            <?php echo translate('SIGN IN YOUR ACCOUNT TO HAVE ACCESS TO DIFFERENT FEATURES', $this->data['language']) ?>
        </h3>

        <form id="sign-in-form" class="login_panel" name="login_panel" method="post" action="#">
            <div class=" kl-fancy-form">
                <input type="text" id="sign-email" name="sign_email" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="user@domain.com">
                <label class="kl-font-alt kl-fancy-form-label">
                    <?php echo translate('Email', $this->data['language']) ?>
                </label>
                <span class="text-danger" style="font-size: 12px; display:none" id="error-sign-email">
                    <?php echo translate('Please enter a valid email', $this->data['language']) ?>
                </span>
            </div>

            <div class=" kl-fancy-form">
                <input type="password" id="sign-password" name="sign_password" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder=" <?php echo translate('type password', $this->data['language']) ?>">
                <label class="kl-font-alt kl-fancy-form-label">
                    <?php echo translate('PASSWORD', $this->data['language']) ?>
                </label>
            </div>
            <div id="invalid-user-login-error" style="display:none;text-align:center" class=" kl-fancy-form">
                <span class="text-danger" style="font-size: 16px;" id="error-sign-email">
                    <?php echo translate('Invalid email or password!', $this->data['language']) ?>
                </span>
            </div>

            <label class="auth-popup-remember" for="kl-rememberme">
                <input type="checkbox" name="rememberme" id="kl-rememberme" value="forever" class="auth-popup-remember-chb"> Remember Me 
            </label>

            <input type="submit" id="login-submit" name="submit_button" class="btn zn_sub_button btn-fullcolor btn-block btn-fullwidth" value="LOG IN" disabled>
          
            <a href="<?php echo $this->global_data['face_book_auth_endPoint']?>" style="display:inline-block;width: 100%;" class="btn zn_sub_button btn-primary btn-block btn-fullwidth" title="SOME TITLE">
                <span><i class="fab fa-facebook-f" style="font-size: 20px;margin-right:20px;"></i>
                    <?php echo translate(' LOG IN WITH FACEBOOK', $this->data['language']) ?></span>
            </a>
						

            <input type="hidden" value="login" class="" name="form_action">
            <input type="hidden" value="login" class="" name="action">
            <input type="hidden" value="#" class="" name="submit">

            <div class="links auth-popup-links">
                <a href="#register_panel" class="create_account auth-popup-createacc kl-login-box auth-popup-link">
                    <?php echo translate('CREATE AN ACCOUNT', $this->data['language']) ?>
                </a>

                <span class="sep auth-popup-sep"></span>

                <a href="#forgot_panel" class="kl-login-box auth-popup-link">
                    <?php echo translate('FORGOT YOUR PASSWORD?', $this->data['language']) ?>
                </a>
            </div>
        </form>
    </div>
    <button title="Close (Esc)" type="button" class="mfp-close">×</button>
</div>

<script>
    $(document).ready(function(){
        $('#sign-in-form').keyup(function(){
            validateLogin();
        });
    });

    $(function () {
        $('#sign-in-form').on('submit', function (e) {
            $('#ajax-loading-block').show();
            e.preventDefault();         
            var formdata = $('form').serialize();
            $.ajax({
            type: 'post',
            url: '<?php echo base_url("clients/Competitors/login") ?>',
            data: {formdata:formdata},
            success: function(data) { 
                location.reload();
                // window.location.replace(getUrl()+JSON.parse(data));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#login-submit').attr('disabled', true);
                $('#invalid-user-login-error').show();
            }	
            });
        });
    });

    function validateLogin(){
        let email = validateField('sign-email', 'email');
        let password = validateField('sign-password', '');
        if(email === true && password === true){
            $('#login-submit').attr('disabled',false);
            }else{
            $('#login-submit').attr('disabled',true);
            }
    }

</script>