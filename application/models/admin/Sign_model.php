<?php

  class Sign_model extends CI_Model{
       
    function getRoleSign($module_id, $form_id, $typed = FALSE){
      $this->db->select('signatures.*, user_groups.name AS role');
      $this->db->join('user_groups', 'signatures.role_id = user_groups.id', 'left');
      $this->db->where('signatures.module_id', $module_id);  
      $this->db->where('signatures.form_id', $form_id);  
      if ($typed) {
        $this->db->where('signatures.typed', $typed);  
      }
      $this->db->where('signatures.deleted !=', 1);   
      $this->db->order_by('signatures.rank', 'ASC');
      $query = $this->db->get('signatures');
      return $query->result_array();
    }

    function getSignedForm($module_id, $user_id){
      $this->db->select('signatures.form_id');
      $this->db->where('signatures.module_id', $module_id);  
      $this->db->where('signatures.user_id', $user_id);  
      $this->db->where('signatures.deleted !=', 1);   
      $this->db->order_by('signatures.form_id', 'ASC');
      $query = $this->db->get('signatures');
      return $query->result_array();
    }

    function get_all_mails($type = FALSE){
      $this->db->select('mails_queue.*');
      if ($type) {
        $this->db->like('mails_queue.type', $type); 
      }
      $this->db->where('mails_queue.finished', 0); 
      $this->db->where('mails_queue.sent_at IS NULL'); 
      $query = $this->db->get('mails_queue');
      return $query->result_array();
    }

    function updateMailData($id){
      $this->db->update('mails_queue', array('finished'=> 1, 'sent_at'=> date("Y-m-d H:i:s")), "id = ".$id);
      return ($this->db->affected_rows() == 1)? TRUE : FALSE;
    }

    function getSignerRoles($type){
      $this->db->select('sign_role.*');
      $this->db->where('sign_role.type', $type);  
      $this->db->order_by('sign_role.rank', 'ASC');
      $query = $this->db->get('sign_role');
      return $query->result_array();
    }

    function getSignedRoles($type){
      $this->db->select('sign_role.role');
      $this->db->where_in('sign_role.type', $type);  
      $this->db->group_by('sign_role.role');
      $this->db->order_by('sign_role.type', 'ASC');
      $query = $this->db->get('sign_role');
      return $query->result_array();
    }

    function getRoles($roles){
      $this->db->select('user_groups.*');
      $this->db->where_in('user_groups.id', $roles);  
      $this->db->order_by('user_groups.name', 'ASC');
      $query = $this->db->get('user_groups');
      return $query->result_array();
    }

    function getRoleTypes($module_id = FALSE){
      $this->db->select('sign_types.*');
      if ($module_id) {
        $this->db->where('sign_types.module_id', $module_id);  
      }
      $this->db->where('sign_types.deleted', 0);
      $this->db->order_by('sign_types.priority', 'ASC');
      $query = $this->db->get('sign_types');
      return $query->result_array();
    }

    function sign($id, $uid){
      $this->db->update('signatures', array('user_id'=> $uid, 'reject'=> 0, 'reason'=> ''), "id = ".$id);
      return ($this->db->affected_rows() == 1)? TRUE : FALSE;
    }

    function signRank($module_id, $form_id, $rank, $uid, $typed = FALSE){
      $this->db->update('signatures', array('user_id'=> $uid, 'reject'=> 0, 'reason'=>''), "module_id = ". $module_id." AND form_id = ". $form_id." AND typed = ". $typed." AND rank = ".$rank);
      return ($this->db->affected_rows() >= 1)? TRUE : FALSE;
    }

    function deleteSign($module_id, $form_id, $typed = FALSE){
      $this->db->where('signatures.module_id', $module_id);   
      $this->db->where('signatures.form_id', $form_id);  
      $this->db->where('signatures.rank !=', 0);  
      $this->db->where('signatures.rank !=', 100);  
      if ($typed) {
        $this->db->where('signatures.typed', $typed);   
      } 
      $this->db->update('signatures', array('deleted'=> 1));
      return ($this->db->affected_rows() >= 1)? TRUE : FALSE;
    }

    function resetItems($form_id){
      $this->db->update('pr_items', array('po_id'=> 0), "pr_id = ". $form_id);
      return ($this->db->affected_rows() >= 1)? TRUE : FALSE;
    }

    function costApprove($database, $form_id, $status){
      $this->db->update($database, array('cost_ap'=> $status), "id = ".$form_id);
      return ($this->db->affected_rows() == 1)? TRUE : FALSE;
    }

    function unSign($id){
      $this->db->update('signatures', array('user_id'=> NULL, 'reject'=> 0, 'reason'=> ''), "id = ".$id);
      return ($this->db->affected_rows() == 1)? TRUE : FALSE;
    }

    function reject($id, $reason, $uid){
      $this->db->update('signatures', array('user_id'=> $uid, 'reject'=> 1, 'reason'=> $reason), "id = ".$id);
      return ($this->db->affected_rows() == 1)? TRUE : FALSE;
    }

    function updateState($database, $form_id, $status){
      $this->db->update($database, array('status'=> $status), "id = ".$form_id);
      return ($this->db->affected_rows() == 1)? TRUE : FALSE;
    }

    function updateTyped($database, $form_id, $typed){
      $this->db->update($database, array('typed'=> $typed), "id = ".$form_id);
      return ($this->db->affected_rows() == 1)? TRUE : FALSE;
    }

    function updateStage($database, $form_id, $typed){
      if ($typed == 1) {
        $stage = 'First';
      }elseif ($typed == 2) {
        $stage = 'Second';
      }elseif ($typed == 3) {
        $stage = 'Third';
      }elseif ($typed == 4) {
        $stage = 'Fourth';
      }elseif ($typed == 5) {
        $stage = 'Fifth';
      }else{
        $stage = '';
      }
      $this->db->update($database, array('stage'=> $stage), "id = ".$form_id);
      return ($this->db->affected_rows() == 1)? TRUE : FALSE;
    }

    function updateNextSign($database, $form_id, $status){
      $this->db->update($database, array('role_id'=> $status), "id = ".$form_id);
      return ($this->db->affected_rows() == 1)? TRUE : FALSE;
    }

    function updateRebak($database, $form_id, $data){
      $this->db->update($database, array('reback'=> $data), "id = ".$form_id);
      return ($this->db->affected_rows() == 1)? TRUE : FALSE;
    }

    function reset($module_id, $form_id, $rank, $typed = FALSE){
      $this->db->update('signatures', array('user_id'=> 0, 'reject'=> 0, 'reason'=> ''), "module_id = ". $module_id." AND form_id = ". $form_id." AND typed = ". $typed." AND rank >= ".$rank);
      return ($this->db->affected_rows() >= 1)? TRUE : FALSE;
    }

    function reset_reason($module_id, $form_id, $reason, $rank, $typed = FALSE){
      $this->db->update('signatures', array('user_id'=> 0, 'reject'=> 0, 'reason'=> $reason), "module_id = ". $module_id." AND form_id = ". $form_id." AND typed = ". $typed." AND rank = ".$rank);
      return ($this->db->affected_rows() >= 1)? TRUE : FALSE;
    }

    function addSignature($module_id, $form_id, $role_id, $rank, $hid = FALSE, $typed = FALSE) {
      $data = array(
        'module_id' => $module_id,
        'form_id'   => $form_id,
        'role_id'   => $role_id,
        'rank'      => $rank,
        'hid'       => $hid,
        'typed'     => $typed
      );
      $this->db->insert('signatures', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    public function getQueue($role_id, $hid='', $dep_id=''){
      if ($hid) {
        $query = 'SELECT user_hotels.*
        FROM user_hotels 
        WHERE user_hotels.hotel_id = '.$hid;
      }else{
      $query = 'SELECT user_hotels.*
        FROM user_hotels';
      }
      $records = $this->db->query($query);
      $rows    =  $records->result_array();
      $uids = array();
      if ($dep_id) {
        foreach ($rows as $row) {
          array_push($uids, $row['user_id']);
        }
        if ($uids) {
          $this->db->select('users.*');
          $this->db->where_in('users.id',$uids);
          $users = $this->db->get_where('users',array('department'=>$dep_id))->result_array();
        }else{
          $users = array();
        }
      }else{
        foreach ($rows as $row) {
          $selected_roles = json_decode($row['role_id']);
          if ($selected_roles) {
            if (in_array($role_id, $selected_roles)) {
              array_push($uids, $row['user_id']);
            }
          }
        }
        if ($uids) {
          $this->db->select('users.*');
          $this->db->where_in('users.id',$uids);
          $users = $this->db->get('users')->result_array();
        }else{
          $users = array();
        }
      }
      return $users;
    }

    function getSignByID($ids){
      $this->db->select('signatures.*');
      $this->db->where_in('signatures.id', $ids);  
      $this->db->where('signatures.deleted', 0);   
      $this->db->order_by('signatures.form_id', 'ASC');
      $query = $this->db->get('signatures');
      return $query->result_array();
    }

  }

?>