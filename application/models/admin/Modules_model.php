<?php

	class Modules_model extends CI_Model{
       
       public function get_modules(){
         
         $this->db->order_by('rank','ASC');
         
         return $this->db->get('modules')->result_array();
       
       }

       public function get_module_by($id='',$name=''){
         
         if ($id) {
         
             $this->db->where('modules.id',$id);
          }
          
         if ($name) {
             
             $this->db->where('modules.name',$name);

          }
         
         return $this->db->get('modules')->row_array();
       
       }
   }

?>