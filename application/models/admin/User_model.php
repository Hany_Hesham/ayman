<?php

	class User_model extends CI_Model{



		public function add_user($data){

			$this->db->insert('users', $data);

			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;

		}

		public function add_user_branches($user_id,$branch){
	       $data= array(
				'user_id'      => $user_id,
				'branch_id'    => $branch
			);

			$this->db->insert('user_branches', $data);

			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;

		}

        public function delete_user_branches($uid){

        	$this->db->where('user_branches.user_id',$uid);

        	$this->db->delete('user_branches');
        }

		public function get_all_users($uids){
            $this->db->where_in('users.id',$uids);
            $this->db->where('users.deleted',0);
            $this->db->where('users.disabled',0);
			return $this->db->get('users')->result_array();

		}

		public function get_user_branches($user_id){
			$this->db->select('user_branches.*,branches.code AS hotel_code,branches.branch_name');
			$this->db->join('branches','user_branches.branch_id = branches.id','left');
			$this->db->where('user_branches.user_id',$user_id);
			$query = $this->db->get('user_branches');
			return $result = $query->result_array();
		}


		public function get_user_roles($branch_id,$uid){
			
			$this->db->select('user_branches.role_id');

			return $this->db->get_where('user_branches',array('user_branches.branch_id' => $branch_id,'user_branches.user_id' => $uid))->row_array();
			 
		}

		public function update_user_branches($id,$data){

            $this->db->update('user_branches', array('role_id' => $data), "id = ".$id);
			
			 if ($this->db->affected_rows() >= 0) {
                  
                   return $this->db->affected_rows();
              } 
		}

		public function get_users_by_granted_branches($hids){
			$this->db->select('user_branches.user_id');
			$this->db->where_in('user_branches.branch_id',$hids);
		    return $this->db->get('user_branches')->result_array();
		 }

        public function get_users($uids,$start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){

        	if (!$search) {
	            $this->db->where_in('users.id',$uids);
	            $this->db->where('users.deleted',0);
        	  }else{
	        	if ($search) {
                     $searched = $this->search_users($search,$uids);
                      if ($searched) {
	                      $this->db->where_in('users.id',$searched);
	                      $this->db->where('users.deleted',0);
                        }else{

                        	$this->db->where_in('users.id',0);
                        	$this->db->where('users.deleted',0);
                        }
	        	    }
                 }
           
              $this->db->order_by($col_name,$order);
              
              if ($filter_count == 'count') {
             
                 return $this->db
              	 ->get("users")
                 ->num_rows();
              
              }else{

	              return $this->db
	              ->limit($length,$start)
	              ->get("users")
	              ->result_array();
              	
              }
        	  
        }

        public function search_users($search,$uids){
            $this->db->select('users.id');
    		$this->db->like('id',$search);
    		$this->db->or_like('username',$search);
    		$this->db->or_like('fullname',$search);
    		$this->db->or_like('email',$search);
    		$this->db->where_in('users.id',$uids);
    		$search_ids = $this->db->get('users')->result_array();
    		$new_ids =array();
    		foreach ($search_ids as $searched) {
    			if (!in_array($searched['id'], $uids)){
    				unset($searched['id']);
    			 }else{
    			 	array_push($new_ids, $searched['id']);
    			 }
    			
    		   }
    		   return $new_ids;
           }

		public function get_user_by_id($id){

			 $this->db->select('users.*,user_groups.name as role_name,departments.dep_name as department_name,departments.code as dep_code');

             $this->db->join('user_groups','users.role_id = user_groups.id','left');

              $this->db->join('departments','users.department = departments.id','left');
		     
		     $this->db->where('users.id',$id);

		     $query = $this->db->get('users');

		     return ($query->num_rows() > 0 )? $query->row_array() : FALSE;

		}

		public function edit_user($data, $id){

			$this->db->where('id', $id);

			$this->db->update('users', $data);

			return true;

		}

		public function count_users(){
           
            return $this->db->get_where("users",array('deleted'=>0,'disabled'=>0))->num_rows();
		  }

  // ------------------------------------------ premission part ------------------------------------------------------------

     public function get_group_permissions($role_id,$module_id){
	 	
	 	$this->db->select('user_groups_permission.*');
	 	
	 	$this->db->where('user_groups_permission.role_id',$role_id);

	 	$this->db->where('user_groups_permission.module_id',$module_id);
	 	
	 	return $this->db->get('user_groups_permission')->row_array();
	 	
	 }

	 public function get_access_modules($user_id,$module_id){

	 	$this->db->select('user_permissions.*');
	 	
	 	$this->db->where('user_permissions.user_id',$user_id);

	    $this->db->where('user_permissions.module_id',$module_id);
	 	
	 	return $this->db->get('user_permissions')->row_array();

	 }

     public function add_permission($data){

		$this->db->insert('user_permissions', $data);
	
		return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		}


	 public function update_permission_by_user_id($id, $user_id, $data){

	 	$this->db->where('user_permissions.user_id', $user_id);
	 	
	 	$this->db->where('user_permissions.id', $id);
		
		$this->db->update('user_permissions', $data);
		
		return true;
	  }	

	  public function update_permission_bulk($module_id, $user_id, $data){

	 	$this->db->where('user_permissions.user_id', $user_id);
	 	
	 	$this->db->where('user_permissions.module_id', $module_id);
		
		$this->db->update('user_permissions', $data);
		
		return true;
	  }	
    
     public function get_role($role_id){
     	
     	$this->db->where('user_groups.id',$role_id);
     	
     	return $this->db->get('user_groups')->row_array();
     }

     public function add_role_name($data){

		$this->db->insert('user_groups',['name' => $data]);
	
		return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		
		}

     public function add_group_permission($data){

		$this->db->insert('user_groups_permission', $data);
	
		return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		
		}

	 public function update_group_permission_by_id($id, $role_id, $data){

	 	$this->db->where('user_groups_permission.role_id', $role_id);
	 	
	 	$this->db->where('user_groups_permission.id', $id);
		
		$this->db->update('user_groups_permission', $data);
		
		return true;
	  }

	public function get_user_access($module_id,$user_id){

	 	$this->db->select('user_permissions.*');
	 	
	 	$this->db->where('user_permissions.user_id',$user_id);

	    $this->db->where('user_permissions.module_id',$module_id);
	 	
	 	return $this->db->get('user_permissions')->row_array();

	 }  		



	}



?>