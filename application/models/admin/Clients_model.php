<?php

	class Clients_model extends CI_Model{

		public function get_clients($start,$length,$search=false,$order,$col_name,$filter_count){
                
                $query = 'SELECT clients.* FROM clients';

		       if ($search) {
		       	    $query .= 'WHERE (client_name like "'.$search.'%" OR phone like "'.$search.'%" OR address like "'.$search.'%"
		  	    	        OR contact like "'.$search.'%"  OR cont_email like "'.$search.'%")';
		             }

		            $query .= ' ORDER BY '.$col_name.' '.$order.'';

		          	if ($filter_count == 'count') { 

		      	     	 $records = $this->db->query($query);
		                 return $records->num_rows();

			           }else{

		      	     	 $query .=' LIMIT '.$start.','.$length.'';
			             $records = $this->db->query($query);
			             return $records->result_array();

			            }   

		    }


        public function get_all_clients(){
			
			return $this->db->get('clients')->result_array();

		  }  


		public function add_client($data){

			$this->db->insert('clients', $data);

			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;

		 }  

		public function get_client($client_id){
              $this->db->select('clients.*,branches_group.branch_group');
              $this->db->join('branches_group','clients.group_id = branches_group.id','left');
              $this->db->where('clients.id',$client_id);
	          return $this->db->get('clients')->row_array();
          	
           }  

         
        public function update_client($client_id,$data){

		 	$this->db->where('clients.id', $client_id);
		 				
			$this->db->update('clients', $data);
			
			 if ($this->db->affected_rows() >= 0) {
                  
                   return $this->db->affected_rows();
              } 
        
          }	 


          public function search_clients($values){
             $query = 'SELECT clients.*,branches_group.branch_group
		          	   	FROM  clients
						LEFT JOIN branches_group ON clients.group_id = branches_group.id 
	  	    	        WHERE clients.deleted = 0 AND (clients.group_id = 5 OR group_id = '.$values['group_id'].') AND 
	  	    	        client_name like "%'.$values['text'].'%"';

             $query .=' LIMIT 20';
             
             $records = $this->db->query($query);
             
             return $records->result_array();


        }  
      
       public function get_client_transactions($client_id,$start,$length,$search=false,$order,$col_name,$filter_count){
	         $query = 'SELECT pos.*,po_status.status_name FROM pos
					   LEFT JOIN po_status ON pos.status = po_status.id
					   WHERE pos.client_id = '.$client_id.'';
		  	    	        
		  	    if ($search) {
		  	    	$query .= ' AND (pos.serial like "%'.$search.'%" OR pos.delivery_date like "%'.$search.'%"
		  	    	            OR po_status.status_name like "%'.$search.'%")';
		  	       } 	

	          	if ($filter_count == 'count') { 
	      	     	 $records = $this->db->query($query);
	                 return $records->num_rows();
		           }else{
	      	     	 $query .=' ORDER BY '.$col_name.' '.$order.'';
	      	     	 $query .=' LIMIT '.$start.','.$length.'';
		             $records = $this->db->query($query);
		             return $records->result_array();

		            }   

	        }


	        public function get_all_client_transactions($client_id){

	        	    $this->db->select('po_items.*');
	            
		        	$this->db->join('pos','po_items.po_id = pos.id','left');

		        	$this->db->join('clients','pos.client_id = clients.id','left');

		        	$this->db->where('pos.client_id',$client_id);
	                    
			        return $this->db->get('po_items')->result_array();

			  }        
      
      




  }
?>	