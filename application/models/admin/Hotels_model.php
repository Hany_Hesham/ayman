<?php

	class Hotels_model extends CI_Model{
	
  		function __contruct(){
			parent::__construct;
  	  	}

  		public function get_data($hotelid, $serverip){
        	$db = "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = " . $serverip . ")(PORT = 1521)))(CONNECT_DATA=(SID=v8)))";
	        $connect = oci_connect('v8live', 'live', $db, 'UTF8', OCI_DEFAULT);
        	$sql = "SELECT DISTINCT V8_REP_CURREXCHANGESETUP.ZCUR_SHORTDESC NAME, V8_REP_CURREXCHANGESETUP.ZXCH_RATE RATE, V8_REP_CURREXCHANGESETUP.ZXCH_FROMTIME TDATE FROM V8_REP_CURREXCHANGESETUP WHERE V8_REP_CURREXCHANGESETUP.ZXCH_FROMTIME = (select max(V8_REP_CURREXCHANGESETUP.ZXCH_FROMTIME) FROM V8_REP_CURREXCHANGESETUP)";
        	if ($connect) {
            	$querys = oci_parse($connect, $sql);
            	$x = oci_execute($querys, OCI_DEFAULT);
            	if ($x) {
	                $data = array();
	                oci_fetch_all($querys, $data, NULL, NULL, OCI_FETCHSTATEMENT_BY_ROW);
            	}
        	}
        	$RData = array();
        	if ($data) {
	    		foreach ($data as $key => $rate) {
	                $rates = array(
	                    'hotel_id' => $hotelid,
	                    'currency_id' => $this->get_currency_id($rate['NAME']),
	                    'rate' => $rate['RATE']
	                );
	            	$RData[] = $rates;
	            }
	        }
            return $RData;
        }

        function get_currency_id($rate){
	      	$this->db->select('currencies.id');
	      	 $this->db->like('currencies.name', $rate);
	      	$query = $this->db->get('currencies')->row()->id;
	      	return $query;
	    }

	    function get_hotel_currency($hotelid){
	      	$this->db->select('currencies_rate.*, currencies.symbol AS currency');
	      	$this->db->join('currencies','currencies_rate.currency_id = currencies.id','LEFT');
	      	$this->db->where('currencies_rate.hotel_id', $hotelid);
	      	$this->db->order_by('currencies_rate.timestamp', 'DESC');
	      	$this->db->group_by('currencies_rate.currency_id');
	      	$query = $this->db->get('currencies_rate')->result_array();
	      	return $query;
	    }

	    function insert_rate($data) {
      		$this->db->insert('currencies_rate', $data);
      		return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    	}
	
	  	function get_hotels($hids ='', $hcodes =''){
	  		$this->db->select('hotels.*,companies.name AS company_name');
	  		$this->db->join('companies','hotels.company_id = companies.id','LEFT');
	  		if ($hids) {
	  			$this->db->where_in('hotels.id', $hids);
	  		}
	  		if ($hcodes) {
	  			$this->db->where_in('hotels.code', $hcodes);
	  		}
			$this->db->where('hotels.deleted', 0);
			return $this->db->get('hotels')->result_array();
	  	}

  	function get_hotels_by_ids($hids){
		
		$this->db->where_in('id', $hids);
		$this->db->where('hotels.deleted', 0);
        $this->db->select('hotels.id,code');
		
		return $this->db->get('hotels')->result_array();
  	  }  

  	function get_by_id($id){
		$this->db->where('id', $id);
		return $this->db->get('hotels')->row_array();
  	  }

  	function get_code($id){
		$this->db->select("code");
		$this->db->where("id", $id);
		return $this->db->get('hotels')->row_array();
  	  }

  	function getby_user($uid){	
		$query = $this->db->query('SELECT hotels.id, hotels.hotel_name,hotels.company_id FROM hotels
		                           JOIN user_hotels ON hotels.id = user_hotels.hotel_id 
		                           WHERE deleted = 0 AND  user_id ="'.$uid.'"');
		return $query->result_array();
  	}


	public function get_hotels_ajax($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
	       if ($search) {

	    	         $query = 'SELECT hotels.*,hotels_group.hotel_group FROM hotels
	    	                LEFT Join hotels_group ON hotels.group_id = hotels_group.id
		  	    	        WHERE hotels.deleted = 0 AND 
		  	    	        (hotel_name like "'.$search.'%" OR hotels_group.hotel_group like "'.$search.'%" OR code like "'.$search.'%") 
		  	    	        ORDER BY '.$col_name.' '.$order.'';

	          	if ($filter_count == 'count') { 

	      	     	 $records = $this->db->query($query);
	                 return $records->num_rows();

		           }else{

	      	     	 $query .=' LIMIT '.$start.','.$length.'';
		             $records = $this->db->query($query);
		             return $records->result_array();

		            }   

	          }elseif(!$search){

	          		 $this->db->select('hotels.*,hotels_group.hotel_group');
	          		 $this->db->join('hotels_group','hotels.group_id = hotels_group.id','LEFT');
                     $this->db->where(array('hotels.deleted'=> 0));
                     $this->db->order_by($col_name,$order);
              
              if ($filter_count == 'count') {
    
	                 return $this->db
	              	 ->get("hotels")
	                 ->num_rows();
              
              }else{

		             return $this->db
		             ->limit($length,$start)
		             ->get("hotels")
		             ->result_array();
              	
              }   
          }
    }


    public function get_all_hotels(){
		
		return $this->db->get_where('hotels',array('deleted'=>0))->result_array();

    }  

    public function add_hotel($data){

			$this->db->insert('hotels', $data);

			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;

		 }  


	public function update_hotel($hotel_id,$data){

		 	$this->db->where('hotels.id', $hotel_id);
		 				
			$this->db->update('hotels', $data);
			
			 if ($this->db->affected_rows() >= 0) {
                  
                   return $this->db->affected_rows();
              } 
        
          }	 	 

}
?>
