<?php

class Branches_model extends CI_Model{
	
  	function __contruct(){
		parent::__construct;
  	  }
	
  	function get_branches($hids =''){
  		
  		$this->db->where_in('id', $hids);

		return $this->db->get('branches')->result_array();
  	  }

  	function get_branches_by_ids($hids){
		
		$this->db->where_in('id', $hids);
        
        $this->db->select('branches.id,code');
		
		return $this->db->get('branches')->result_array();
  	  }  

  	function get_by_id($id){
		$this->db->where('id', $id);
		return $this->db->get('branches')->row_array();
  	  }

  	function get_code($id){
		$this->db->select("code");
		$this->db->where("id", $id);
		return $this->db->get('branches')->row_array();
  	  }

  	function getby_user($uid){	
		$query = $this->db->query('SELECT branches.id, branches.hotel_name,branches.company_id FROM branches
		                           JOIN user_branches ON branches.id = user_branches.branch_id 
		                           WHERE deleted = 0 AND  user_id ="'.$uid.'"');
		return $query->result_array();
  	}


	public function get_branches_ajax($start,$length,$search=false,$order,$col_name,$filter_count){
	       if ($search) {

	    	         $query = 'SELECT branches.*,branches_group.branch_group FROM branches
	    	                LEFT Join branches_group ON branches.group_id = branches_group.id
		  	    	        WHERE branches.deleted = 0 AND 
		  	    	        (hotel_name like "'.$search.'%" OR branches_group.branch_group like "'.$search.'%" OR code like "'.$search.'%") 
		  	    	        ORDER BY '.$col_name.' '.$order.'';

	          	if ($filter_count == 'count') { 

	      	     	 $records = $this->db->query($query);
	                 return $records->num_rows();

		           }else{

	      	     	 $query .=' LIMIT '.$start.','.$length.'';
		             $records = $this->db->query($query);
		             return $records->result_array();

		            }   

	          }elseif(!$search){

	          		 $this->db->select('branches.*,branches_group.branch_group');
	          		 $this->db->join('branches_group','branches.group_id = branches_group.id','LEFT');
                     $this->db->where(array('branches.deleted'=> 0));
                     $this->db->order_by($col_name,$order);
              
              if ($filter_count == 'count') {
    
	                 return $this->db
	              	 ->get("branches")
	                 ->num_rows();
              
              }else{

		             return $this->db
		             ->limit($length,$start)
		             ->get("branches")
		             ->result_array();
              	
              }   
          }
    }


    public function get_all_branches(){
		
		return $this->db->get_where('branches',array('deleted'=>0))->result_array();

    }  

    public function add_hotel($data){

			$this->db->insert('branches', $data);

			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;

		 }  


	public function update_hotel($branch_id,$data){

		 	$this->db->where('branches.id', $branch_id);
		 				
			$this->db->update('branches', $data);
			
			 if ($this->db->affected_rows() >= 0) {
                  
                   return $this->db->affected_rows();
              } 
        
          }	 	 

}
?>
