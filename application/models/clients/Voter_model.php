<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voter_model extends CI_Model {
    function __construct() {
        $this->tableName = 'voters';
        $this->primaryKey = 'id';
    }

    public function getRowWithValue($column, $value)
    {
        return $this->db->get_where( $this->tableName,[$column => $value])->row_array();
    }

    public function update_voter($voter_id, $data)
    {
        $date = new DateTime();
        $data['updated_at'] = $date->format('Y-m-d H:i:s');
        $this->db->where($this->tableName.'.id', $voter_id);			
        $this->db->update($this->tableName, $data);
            if ($this->db->affected_rows() >= 0) {
            return $this->db->affected_rows();
            } 
    }

    public function create_voter($data){
        $date = new DateTime();
        $fData = [
            'fb_user_id' => $data['id'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'picture' => json_encode($data['picture']),
            'fb_access_token' => $data['fb_access_token'],
            'oauth_provider' => 1,
            'created_at' => $date->format('Y-m-d H:i:s'),
            'updated_at' => $date->format('Y-m-d H:i:s'),
        ];
        $this->db->insert($this->tableName, $fData);
        return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
      }

    
    // /*
    //  * Insert / Update facebook profile data into the database
    //  * @param array the data for inserting into the table
    //  */
    // public function checkUser($userData = array()){
    //     if(!empty($userData)){
    //         //check whether user data already exists in database with same oauth info
    //         $this->db->select($this->primaryKey);
    //         $this->db->from($this->tableName);
    //         $this->db->where(array('oauth_provider'=>$userData['oauth_provider'], 'oauth_uid'=>$userData['oauth_uid']));
    //         $prevQuery = $this->db->get();
    //         $prevCheck = $prevQuery->num_rows();
            
    //         if($prevCheck > 0){
    //             $prevResult = $prevQuery->row_array();
                
    //             //update user data
    //             $userData['modified'] = date("Y-m-d H:i:s");
    //             $update = $this->db->update($this->tableName, $userData, array('id' => $prevResult['id']));
                
    //             //get user ID
    //             $userID = $prevResult['id'];
    //         }else{
    //             //insert user data
    //             $userData['created']  = date("Y-m-d H:i:s");
    //             $userData['modified'] = date("Y-m-d H:i:s");
    //             $insert = $this->db->insert($this->tableName, $userData);
                
    //             //get user ID
    //             $userID = $this->db->insert_id();
    //         }
    //     }
        
    //     //return user ID
    //     return $userID?$userID:FALSE;
    // }
}