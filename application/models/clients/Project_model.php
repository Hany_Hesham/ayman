<?php

	class Project_model extends CI_Model{

        public function add_project($data){
          $this->db->insert('projects', $data);
          return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
        }

        public function get_project($project_id)
        {
            return $this->db->get_where('projects',['id'=> $project_id, 'deleted' => 0])->row_array();
        }

        public function get_project_byCode($code)
        { 
            $this->db->select('projects.*, competitions.title AS competition_title, 
            competitions.description AS competition_description, competitions.vote_start_date,competitions.vote_end_date,competitions.active AS competition_active,
             competitors.name AS first_name,competitors.middle_name,competitors.last_name, competitors.age
              ');
            $this->db->join('competitors','projects.competitor_id = competitors.id','left');
            $this->db->join('competitions','projects.competition_id = competitions.id','left');
            return $this->db->get_where('projects',['projects.code'=> $code, 'projects.deleted' => 0])->row_array();   
        }
        public function get_activated_projects_by_category($category)
        {
            $this->db->select('projects.*, competitions.title AS competition_title, 
                competitions.active AS competition_active,competitions.end_date,
                competitions.vote_start_date,competitions.vote_end_date, competitions.description AS competition_description,
                competitors.name AS first_name,competitors.middle_name,competitors.last_name, 
                competitors.age,competitors.school, site_meta_data.id AS category_id, 
                site_meta_data.header AS category_name, site_meta_data.title AS category_condition');
            $this->db->join('competitors','projects.competitor_id = competitors.id','left');
            $this->db->join('competitions','projects.competition_id = competitions.id','left');
            $this->db->join('site_meta_data','projects.category = site_meta_data.id','left');
            $this->db->where([
                'competitions.active !='=> 0, 
                'projects.status' => 5,
                'projects.deleted' => 0,
                'site_meta_data.id' => $category]);
            $this->db->order_by('projects.votes', 'DESC');
            return $this->db->get('projects')->result_array();
        }

        public function get_unactivated_projects_by_category($category)
        {
            $this->db->select('projects.*, competitions.title AS competition_title, 
                competitions.active,competitions.end_date, competitions.description AS competition_description,
                competitors.name AS first_name,competitors.middle_name,competitors.last_name, 
                competitors.age,competitors.school, site_meta_data.id AS category_id, 
                site_meta_data.header AS category_name, site_meta_data.title AS category_condition');
            $this->db->join('competitors','projects.competitor_id = competitors.id','left');
            $this->db->join('competitions','projects.competition_id = competitions.id','left');
            $this->db->join('site_meta_data','projects.category = site_meta_data.id','left');
            $this->db->where([
                'competitions.active'=> 0, 
                'projects.status >' => 4,
                'projects.deleted' => 0,
                'site_meta_data.id' => $category]);
            $this->db->order_by('projects.votes', 'DESC');
            return $this->db->get('projects')->result_array();
        }

        public function get_competition_results_by_category($competition_id, $category)
        {
            $this->db->select('projects.*, competitions.title AS competition_title, 
                competitions.active,competitions.end_date, competitions.description AS competition_description,
                competitors.name AS first_name,competitors.middle_name,competitors.last_name, 
                competitors.age,competitors.school, site_meta_data.id AS category_id, 
                site_meta_data.header AS category_name, site_meta_data.title AS category_condition');
            $this->db->join('competitors','projects.competitor_id = competitors.id','left');
            $this->db->join('competitions','projects.competition_id = competitions.id','left');
            $this->db->join('site_meta_data','projects.category = site_meta_data.id','left');
            $this->db->where([
                'competitions.active'=> 0, 
                'projects.competition_id' => $competition_id,
                'projects.status >' => 4,
                'projects.deleted' => 0,
                'site_meta_data.id' => $category]);
            $this->db->order_by('projects.total_score', 'DESC');
            return $this->db->get('projects')->result_array();
            // return $this->parse_competition_results($results);
        }

        public function get_user_competition_project($competition_id, $competitor_id)
        {
            return $this->db->get_where('projects',[
                'competition_id'=> $competition_id, 
                'competitor_id'=> $competitor_id, 
                'deleted' => 0
                ])->row_array();
        }

        public function get_user_pending_projects($competitor_id)
        {
            $this->db->select('projects.*,competitions.id AS competition_id, competitions.title AS competition_title,
            competitions.image AS competition_image, competitions.price AS competition_price, competitions.currency AS competition_currency,
            competitions.active,competitions.end_date, competitions.description AS competition_description, 
            site_meta_data.id AS status_id, site_meta_data.header AS status_name');
            $this->db->join('competitions','projects.competition_id = competitions.id','left');
            $this->db->join('site_meta_data','projects.status = site_meta_data.link','left');
            $this->db->where([
                'competitions.active' => 1,
                'status <='=> 4, 
                'competitor_id'=> $competitor_id, 
                'projects.deleted' => 0,
                'competitions.deleted' => 0
                ]);
            return $this->db->get('projects')->result_array();    
        }

        public function get_user_current_projects($competitor_id)
        {
            $this->db->select('projects.*, competitions.title AS competition_title, 
            competitions.active,competitions.end_date, competitions.description AS competition_description');
            $this->db->join('competitions','projects.competition_id = competitions.id','left');
            $this->db->where([
                'competitions.active' => 1,
                'status >'=> 4, 
                'competitor_id'=> $competitor_id, 
                'projects.deleted' => 0,
                'competitions.deleted' => 0
                ]);
            return $this->db->get('projects')->result_array();    
        }

        public function get_user_old_projects($competitor_id)
        {
            $this->db->select('projects.*, competitions.title AS competition_title, 
            competitions.active,competitions.end_date, competitions.description AS competition_description');
            $this->db->join('competitions','projects.competition_id = competitions.id','left');
            $this->db->where([
                'competitions.active' => 0,
                'status >'=> 4, 
                'competitor_id'=> $competitor_id, 
                'projects.deleted' => 0,
                'competitions.deleted' => 0
                ]);
            return $this->db->get('projects')->result_array();    
        }

        public function update_project($project_id, $data)
        {
            $this->db->where('projects.id', $project_id);			
			$this->db->update('projects', $data);
			 if ($this->db->affected_rows() >= 0) {
                return $this->db->affected_rows();
              } 
        }

        public function process_vote_project($data){
            $this->db->trans_start();
            $row =  $this->db->get_where('votes',[
                    'voter_id' => $data['voter_id'],
                    'project_id' => $data['project_id'],
                ])->row_array();
            if(isset($row)){
                $vote = $row['vote'] == 0 ? 1 : 0;
                $this->db->where([
                    'id' => $row['id']
                ]);	
                $this->db->update('votes', ['vote' => $vote]);
            }else{
                $this->db->insert('votes', $data);
            }    	
            $this->db->select('vote, count(vote) AS vote_count from votes');
            $res = $this->db->where([
                'project_id'=> $data['project_id'], 
                'vote' => 1
                ])->get()->row_array();
            $this->db->where('id', $data['project_id']);
            $this->db->update('projects', ['votes' => $res['vote_count']]);
            $this->db->trans_complete();
        }

        public function getVoterLikes($user_id)
        {
            return $this->db->get_where('votes',['voter_id'=> $user_id])->result_array();
        }

  }
?>	