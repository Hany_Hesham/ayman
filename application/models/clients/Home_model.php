<?php

	class Home_model extends CI_Model{

        public function get_sliders(){
            $this->db->order_by('slider.rank','ASC');
            return $this->db->get_where('slider',array('deleted'=>0))->result_array();  
        }

        public function get_slider($id){
            return $this->db->get_where('slider',array('deleted'=>0, 'id' => $id))->row_array();  
        }

        public function get_gallarys(){
            $this->db->order_by('gallary.rank','ASC');
            return $this->db->get_where('gallary',array('deleted'=>0))->result_array();  
        }

        public function get_events(){
            $this->db->order_by('events.rank','ASC');
            return $this->db->get_where('events',array('deleted'=>0, 'active'=>1))->result_array();  
        }  

        // public function get_boat_infos($boat_id){
        //     $this->db->select('boat_info.*, status.status_name');
        //     $this->db->join('status', 'boat_info.status = status.id','left');
        //     $this->db->order_by('boat_info.rank','ASC');
        //     return $this->db->get_where('boat_info',array('deleted'=>0, 'boat_id'=>$boat_id, 'status'=>2))->result_array();  
        // } 

        // public function get_boat_cabins($boat_id){
        //     $this->db->distinct();
        //     $this->db->select('cabins.*, status.status_name');
        //     $this->db->join('status', 'cabins.status = status.id','left');
        //     $this->db->order_by('cabins.rank','ASC');
        //     return $this->db->get_where('cabins',array('deleted'=>0, 'boat_id'=>$boat_id, 'status'=>2))->result_array();  
        // }  

        // public function get_boat_packages($boat_id){
        //     $this->db->select('packages.*, status.status_name');
        //     $this->db->join('status', 'packages.status = status.id','left');
        //     $this->db->order_by('packages.rank','ASC');
        //     return $this->db->get_where('packages',array('deleted'=>0, 'boat_id'=>$boat_id, 'status'=>2))->result_array();  
        // }  

        // public function add_booking($data){
        //   $this->db->insert('booking', $data);
        //   return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
        // } 

        public function get_sit_metaData($meta,$type){
            $this->db->order_by('rank','ASC');
        	if ($type=='single') {
				return $this->db->get_where('site_meta_data',array('meta'=>$meta))->row_array();
        	}elseif($type=='multi'){
                return $this->db->get_where('site_meta_data',array('meta'=>$meta))->result_array();
        	}

         }   
         
         public function get_sit_metaData_by_id($id){
				return $this->db->get_where('site_meta_data',array('id'=>$id))->row_array();

         }   

        public function get_categories(){
            $this->db->order_by('rank','ASC');
            return $this->db->get_where('site_groups',array('deleted'=>0))->result_array();

         }  

         public function get_category($serial){
            return $this->db->get_where('site_groups',array('serial'=>$serial,'deleted'=>0))->row_array();

         }  

         public function get_category_items($cat_id){
         	$this->db->order_by('site_group_items.rank','ASC');
            return $this->db->get_where('site_group_items',array('group_id'=>$cat_id,'deleted'=>0))->result_array();
         } 

         public function get_gallery_unique(){
         	$this->db->select('site_meta_data.points');
         	$this->db->order_by('site_meta_data.rank','ASC');
         	$this->db->where(array('meta'=>'gallery'));
         	$this->db->group_by('site_meta_data.points');
            return $this->db->get_where('site_meta_data')->result_array();
         }

         public function get_portfolio_images($limit){
                $this->db->limit($limit);
                return $this->db->get_where('site_meta_data',array('meta'=>'gallery'))->result_array();

         } 

        public function add_client_mail($data){
          $this->db->insert('clients_mail_queue', $data);
          return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
         }             

        public function add_booking_trip($data){
          $this->db->insert('bookings', $data);
          return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
         } 

        // public function get_hotels(){
        //     $this->db->select('hotel.*');
        //     $this->db->where('hotel.deleted', 0);
        //     $this->db->order_by('hotel.id','ASC');
        //     return $this->db->get('hotel')->result_array();
        // }    
        
        public function get_competition($competition_id)
        {
            return $this->db->get_where('competitions',[
                    'deleted'=> 0,
                    'id' => $competition_id,
                ])->row_array();
        }
        
        public function get_active_competition($type)
        {
            $today = date("Y-m-d");
            return $this->db->get_where('competitions',[
                    'deleted'=> 0,
                    'active' => 1,
                    'type' => $type,
                ])->result_array();
        }

        public function get_rules()
        {
            $this->db->order_by('rules.rank','ASC');
            $rules =  $this->db->get_where('rules',[
                'deleted'=> 0,
                'disabled' => 0,
            ])->result_array();
            foreach($rules as $key => $rule){
                $rules[$key]['files'] = $this->db->get_where('site_files',[
                    'module_id'=> 27,
                    'form_id' => $rule['id'],
                ])->result_array();
            }
            return $rules;
        }

        public function get_inactive_competition($type)
        {
            $today = date("Y-m-d");
            return $this->db->get_where('competitions',[
                    'deleted'=> 0,
                    'active' => 0,
                    'type' => $type,
                ])->result_array();
        }

        public function get_winners()
        {
            $this->db->join('competitors', 'winners.competitor_id = competitors.id','left');
            $this->db->order_by('winners.rank','ASC');
            $winners = $this->db->get_where('winners',[
                'winners.disabled'=> 0,
            ])->result_array();
            foreach($winners as $key => $winner){
                $winners[$key]['project'] = $this->db->get_where('projects',[
                    'competitor_id' => $winner['competitor_id'],
                    'competition_id' => $winner['competition_id'],
                ])->row_array();
            }
            return $winners;
        }


  }
?>	