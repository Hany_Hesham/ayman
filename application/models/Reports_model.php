<?php

class Reports_model extends CI_Model{

	public function get_reports(){
		$this->db->order_by('rank','ASC');		
		return $this->db->get_where('reports',array('deleted'=>0))->result_array();

	}  

	public function get_reports_modules(){
		$this->db->select('reports.*,modules.name as module_name');
		$this->db->join('modules','reports.module_id = modules.id','left');
		$this->db->where(array('reports.deleted'=>0));
		$this->db->group_by('module_id');
		$this->db->order_by('rank','ASC');		
		return $this->db->get('reports')->result_array();

	}  

	public function get_reports_by_module($module_id){
		$this->db->order_by('rank','ASC');		
		return $this->db->get_where('reports',array('deleted'=>0,'module_id'=>$module_id))->result_array();

	}


	public function custom_search_query($custom_search,$extra_filters='', $table){
		$query='';
		if (isset($custom_search['hotel_id']) && $custom_search['hotel_id'] !='') {
			$query .=' AND '.$table.'.hid IN('.implode(',',$custom_search['hotel_id']).')';
		}
		if (isset($custom_search['dep_code']) && $custom_search['dep_code'] !='') {
			$query .=' AND '.$table.'.dep_code IN("'.implode('","',$custom_search['dep_code']).'")';
		}  
		if (isset($custom_search['toDate']) && $custom_search['fromDate'] && $custom_search['toDate']) {
			$query .=' AND DATE_FORMAT('.$table.'.timestamp,GET_FORMAT(DATE,"JIS")) BETWEEN ("'.$custom_search['fromDate'].'") AND ("'.$custom_search['toDate'].'")';
		}
		if (isset($custom_search['toDate']) && $custom_search['fromDate'] && !$custom_search['toDate']) {
			$query .=' AND DATE_FORMAT('.$table.'.timestamp,GET_FORMAT(DATE,"JIS")) >= "'.$custom_search['fromDate'].'"';
		}
		if (isset($custom_search['toDate']) && $custom_search['toDate'] && !$custom_search['fromDate']) {
			$query .=' AND DATE_FORMAT('.$table.'.timestamp,GET_FORMAT(DATE,"JIS")) <= "'.$custom_search['toDate'].'"';
		} 
		if (isset($custom_search['status']) && $custom_search['status'] !='') {
			$query .=' AND '.$table.'.status IN('.implode(',',$custom_search['status']).')';
		}    

		return $query; 	   	

	}        
   //=================================== Reservations Reports  ===================================----------------------------      

	public function get_reservation_report($uhotels,$t_attr='',$custom_search='',$filter_count=''){
		$query = 'SELECT reservation.*,hotels.hotel_name,status.status_color,status.status_name,res_resource_table.type_name AS resource_type,
		res_board_table.type_name AS board_name ,res_type_table.type_name AS types_name
		FROM reservation
		LEFT JOIN hotels ON reservation.hid = hotels.id
		LEFT JOIN status ON reservation.status = status.id
		LEFT JOIN meta_data AS res_resource_table ON reservation.res_source_id = res_resource_table.id
		LEFT JOIN meta_data AS res_board_table ON reservation.board_type_id = res_board_table.id
		LEFT JOIN meta_data AS res_type_table ON reservation.res_type_id = res_type_table.id
		WHERE reservation.deleted = 0 AND reservation.hid IN('.implode(',',$uhotels).')';

		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','reservation');  
			if ($custom_search['reservation_resources'] !='') {
				$query .=' AND res_source_id IN('.implode(',',$custom_search['reservation_resources']).')';
			}
		} 
		if ($t_attr) {
			if ($t_attr['search']) {
				$query .= ' AND (reservation.recived_by like "%'.$t_attr['search'].'%" OR reservation.name like "%'.$t_attr['search'].'%"
				OR reservation.discount like "%'.$t_attr['search'].'%" OR reservation.currency like "%'.$t_attr['search'].'%" 
				OR reservation.room_type like "%'.$t_attr['search'].'%" OR reservation.arrival like "%'.$t_attr['search'].'%" 
				OR reservation.departure like "%'.$t_attr['search'].'%" OR reservation.agent like "%'.$t_attr['search'].'%" 
				OR reservation.remarks like "%'.$t_attr['search'].'%")';
			}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'';
				$query .=' LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
			}        
		}   
		$records = $this->db->query($query);
		return $records->result_array();
	}

	public function get_all_report_reservation($uhotels){
		$this->db->where_in('reservation.hid',$uhotels);
		return $this->db->get_where('reservation',array('deleted'=>0))->result_array();
	}


//=================================== Financial Reports  ===================================----------------------------    

//---------------------out go reports  

//--out go itmes
	public function get_out_go_item_report($uhotels,$t_attr='',$custom_search='',$filter_count=''){
		$query = 'SELECT out_go_items.*,out_go.hid,out_go.address,out_go.out_with,out_go.deleted,
		          out_go.date,out_go.re_date,hotels.hotel_name,status.status_color,status.status_name,
		          departments.dep_name,(SELECT out_go_date_change.date FROM out_go_date_change WHERE out_go_date_change.out_id = out_go.id ORDER BY out_go_date_change.timestamp DESC LIMIT 1) AS new_changed_date
		FROM out_go_items
		LEFT JOIN out_go ON out_go_items.out_id = out_go.id
		LEFT JOIN departments ON out_go.dep_code = departments.id
		LEFT JOIN hotels ON out_go.hid = hotels.id
		LEFT JOIN status ON out_go.status = status.id
		WHERE  out_go_items.deleted = 0 AND out_go.deleted = 0 AND out_go.hid IN('.implode(',',$uhotels).')';

		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','out_go');
			if ($custom_search['out_del_status'] !='') {
				if ($custom_search['out_del_status'][0]=='delivered') {
					$query .=' AND delivered = 1 AND  out_go_items.quantity !=0';
				}elseif($custom_search['out_del_status'][0]=='not delivered'){
					$query .=' AND delivered = 0 AND out_go_items.quantity !=0  AND out_go.out_with IS NOT NULL';
				}
			}  
		} 
		if ($t_attr) {
			if ($t_attr['search']) {
				$query .= ' AND (out_go_items.description like "%'.$t_attr['search'].'%" OR out_go_items.remarks like "%'.$t_attr['search'].'%")';
			}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'';
				$query .=' LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
			}        
		}   
		$records = $this->db->query($query);
		return $records->result_array();
	}

	public function get_all_report_out_go_items($uhotels){
		$this->db->select('out_go_items.*,out_go.hid');
		$this->db->join('out_go','out_go_items.out_id = out_go.id','left');
		$this->db->where_in('out_go.hid',$uhotels);
		return $this->db->get_where('out_go_items',array('out_go_items.deleted'=>0))->result_array();
	}

//--changed return date
	public function get_changed_return_date_report($uhotels,$t_attr='',$custom_search='',$filter_count=''){
		$query = 'SELECT out_go.*,hotels.hotel_name,status.status_color,status.status_name,departments.dep_name
		FROM out_go
		LEFT JOIN departments ON out_go.dep_code = departments.id
		LEFT JOIN hotels ON out_go.hid = hotels.id
		LEFT JOIN status ON out_go.status = status.id
		JOIN out_go_date_change ON out_go.id = out_go_date_change.out_id
		WHERE out_go.deleted = 0 AND out_go.hid IN('.implode(',',$uhotels).')';

		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','out_go'); 
		} 
		if ($t_attr) {
			if ($t_attr['search']) {
				$query .= ' AND (out_go.id like "%'.$t_attr['search'].'%" OR out_go.address like "%'.$t_attr['search'].'%" OR out_go.out_with like "%'.$t_attr['search'].'%")';
			}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'';
				$query .=' LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
			}        
		}   
		$records = $this->db->query($query);
		return $records->result_array();
	}

	public function get_all_report_changed_return_date($uhotels){
		$this->db->select('out_go.*');
		$this->db->where_in('out_go.hid',$uhotels);
		return $this->db->get_where('out_go',array('out_go.deleted'=>0))->result_array();
	}

	public function get_changed_return_date($out_id){
		$this->db->select('date,reason');
		$this->db->where(array('out_id'=>$out_id));
		$this->db->order_by('timestamp','ASC');		
		$rows     =  $this->db->get('out_go_date_change')->result_array();
		$dates    =  array();
		$reasons  =  array();
		foreach ($rows as $row) {
			$dates[]   = $row['date'];
			$reasons[] = $row['reason'];
		}
		return array('dates'=>$dates,'reasons'=>$reasons );
	}


//===================================------------ Front Office Reports===================================------------------------------

//---------------------amenitys reports   

//--amenity report
	public function get_amenity_report($uhotels,$t_attr='',$custom_search='',$filter_count=''){
		$query = 'SELECT amenity_items.*,amenity.hid,amenity.date_time,amenity.ref,amenity.others,amenity.relations,amenity.reason,
		amenity.type_id,meta_data.type_name AS amenity_type, meta_data1.type_name AS amenity_treatment,
		hotels.hotel_name,status.status_color,status.status_name,users.fullname
		FROM amenity_items
		LEFT JOIN amenity   ON amenity_items.amen_id = amenity.id
		LEFT JOIN meta_data ON amenity.type_id = meta_data.id
		LEFT JOIN meta_data AS meta_data1 ON amenity_items.treatment_id = meta_data1.id
		LEFT JOIN users     ON amenity.uid = users.id
		LEFT JOIN hotels    ON amenity.hid = hotels.id
		LEFT JOIN status    ON amenity.status = status.id
		WHERE amenity_items.deleted = 0 AND amenity.hid IN('.implode(',',$uhotels).')';

		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','amenity');
			if ($custom_search['amenity_treatment'] !='') {
				$query .=' AND amenity_items.treatment_id IN('.implode(',',$custom_search['amenity_treatment']).')';
			}
		} 
		if ($t_attr) {
			if ($t_attr['search']) {
				$query .= ' AND (amenity_items.room like "%'.$t_attr['search'].'%" OR amenity_items.guest like "%'.$t_attr['search'].'%" OR amenity_items.nationality like "%'.$t_attr['search'].'%")';
			}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'';
				$query .=' LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
			}        
		}   
		$records = $this->db->query($query);
		return $records->result_array();
	}

	public function get_all_amenity_report($uhotels){
		$this->db->select('amenity_items.*,amenity.hid');
		$this->db->join('amenity','amenity_items.amen_id = amenity.id','left');
		$this->db->where_in('amenity.hid',$uhotels);
		return $this->db->get_where('amenity_items',array('amenity_items.deleted'=>0))->result_array();
	}

	public function get_amenities_refl($amen_id){
		$query = 'SELECT amenity_refl.other_ids FROM amenity_refl
		WHERE amenity_refl.deleted = 0 AND amenity_refl.amen_id = '.$amen_id.' ';
		$rows = $this->db->query($query)->result_array();
		$amenities    =  array();
		if ($rows) {
			foreach ($rows as $row) {
				foreach (json_decode($row['other_ids']) as $arr) {
					$amenities[]   = $arr;
				}
			}
		}
		if ($amenities) {
			return $amenities;
		}else{
			return array();
		}
	}

	function get_meta_data_by_ids($ids){
		if (!empty($ids)) {
			$this->db->select('meta_data.type_name');
			$this->db->where(array('deleted'=>0));
			$this->db->where_in('id',$ids);
			$this->db->group_by('id');
			return $this->db->get('meta_data')->result_array();
		}else{
			return array();
		}
	}

//--amenity summary report

	public function get_all_treatments_in($uhotels,$custom_search){
		$query = 'SELECT amenity_items.treatment_id,count(treatment_id) AS treatment_count,sum(pax) AS adult_pax_count,
		sum(child) AS child_pax_count,amenity.hid,meta_data.type_name AS amenity_treatment
		FROM amenity_items
		LEFT JOIN amenity   ON amenity_items.amen_id = amenity.id
		LEFT JOIN meta_data ON amenity_items.treatment_id = meta_data.id
		WHERE amenity_items.deleted = 0 AND amenity.hid IN('.implode(',',$uhotels).')';
		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','amenity');
		}
		$query .= ' GROUP BY treatment_id';
		return $this->db->query($query)->result_array();
	}

	public function get_rooms_count($uhotels,$custom_search){
		$query = 'SELECT count(room) AS room_count,amenity.hid
		FROM amenity_items
		LEFT JOIN amenity   ON amenity_items.amen_id = amenity.id
		WHERE amenity_items.deleted = 0 AND amenity.hid IN('.implode(',',$uhotels).')';
		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','amenity');
		}
		$query .= ' GROUP BY room';
		return $this->db->query($query)->result_array();
	}

	public function get_amenity_count($uhotels,$custom_search,$amenity_id){
		$query = 'SELECT count(amenity_items.id) AS amenity_count,amenity.hid
		FROM amenity_items
		LEFT JOIN amenity   ON amenity_items.amen_id = amenity.id
		WHERE amenity_items.deleted = 0 AND amenity.hid IN('.implode(',',$uhotels).') 
		AND amenity_items.other_ids LIKE CONCAT("%",'.$amenity_id.',"%")';
		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','amenity');
		}
		$query .= 'UNION 
		SELECT count(amenity_refl.id) AS amenity_count,amenity.hid
		FROM amenity_refl
		LEFT JOIN amenity   ON amenity_refl.amen_id = amenity.id
		WHERE amenity_refl.deleted = 0 AND amenity.hid IN('.implode(',',$uhotels).') 
		AND amenity_refl.other_ids LIKE CONCAT("%",'.$amenity_id.',"%")';
		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','amenity');
		}
		return $this->db->query($query)->row_array();
	}

	public function get_all_summary_amenity_report($uhotels){
		$this->db->select('amenity_items.*,amenity.hid');
		$this->db->join('amenity','amenity_items.amen_id = amenity.id','left');
		$this->db->where_in('amenity.hid',$uhotels);
		return $this->db->get_where('amenity_items',array('amenity_items.deleted'=>0))->result_array();
	}

//=================================== Workshop Reports  ===================================----------------------------     

//--Workshop Report
	public function get_workshop_report($uhotels,$t_attr='',$custom_search='',$filter_count=''){
		$query = 'SELECT workshop.*,workshop_orders.id as order_id,hotels.hotel_name,meta_data.type_name AS workshop_type
		FROM workshop
		LEFT JOIN status ON workshop.status = status.id
		LEFT JOIN hotels ON workshop.hid = hotels.id
		LEFT JOIN meta_data ON workshop.type = meta_data.id
		LEFT JOIN workshop_orders ON workshop.id = workshop_orders.workshop_id
		WHERE workshop.deleted = 0 AND workshop.hid IN('.implode(',',$uhotels).')';

		if ($custom_search) {
			$query .=$this->custom_search_query($custom_search,'extra_filters','workshop'); 
			if (isset($custom_search['status']) && $custom_search['status'] !='') {
				$query .=' AND workshop.stage="Third"';
			}   
			if ($custom_search['workshop_type'] !='') {
				$query .=' AND workshop.type IN('.implode(',',$custom_search['workshop_type']).')';
			}  
		} 
		if ($t_attr) {
			if ($t_attr['search']) {
				$query .= '  AND (workshop.id like "%'.$t_attr['search'].'%" OR workshop.timestamp like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%"  OR meta_data.type_name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%")';
			}
			if ($filter_count == 'count') { 
				return $this->db->query($query)->num_rows();
			}else{
				$query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'';
				$query .=' LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
			}        
		}   
		$records = $this->db->query($query);
		return $records->result_array();
	}

	public function get_all_report_workshop($uhotels){
		$this->db->where_in('workshop.hid',$uhotels);
		return $this->db->get_where('workshop',array('workshop.deleted'=>0))->result_array();
	}

	function get_workshop_items($workshop_id) {
		$this->db->select('description,ready_time,recieve_time');
		$this->db->where(array('workshop_items.deleted !='=>1,'workshop_items.workshop_id'=>$workshop_id));
		return $this->db->get('workshop_items')->result_array();
	}

	function get_workshop_order_items($order_id) {
		$this->db->select('workshop_order_items.*');
		$this->db->where(array('workshop_order_items.deleted !='=>1,'workshop_order_items.order_id'=>$order_id));
		return $this->db->get('workshop_order_items')->result_array();
	}


//=================================== projects Reports  ===================================---------------------------- 

//---------------- project detailed report     

	public function get_project_detailed_report($uhotels,$t_attr='',$custom_search='',$filter_count='',$Report_type=''){
		$query = 'SELECT projects.*, hotels.hotel_name, status.status_name, status.status_color, departments.dep_name,
		user_groups.name AS role_name, meta_data.type_name AS progress_name
		FROM projects
		LEFT JOIN hotels ON projects.hid = hotels.id
		LEFT JOIN status ON projects.status = status.id
		LEFT JOIN departments ON projects.dep_code = departments.id
		LEFT JOIN user_groups ON projects.role_id = user_groups.id
		LEFT JOIN meta_data ON projects.progress_id = meta_data.id
		WHERE projects.deleted = 0 AND projects.hid IN('.implode(',',$uhotels).')';
		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','projects');  
			if (isset($custom_search['project_type']) && $custom_search['project_type'] !='') {
				$query .=' AND projects.type_id IN('.implode(',',$custom_search['project_type']).')';
			}
			if(isset($custom_search['project_original']) && $custom_search['project_original'] !=''){
				$query .=' AND projects.origin_id = "'.$custom_search['project_original'].'"';
			}
			if(isset($custom_search['project_equipment']) && $custom_search['project_equipment'] !=''){
				$query .=' AND projects.new IN('.implode(',',$custom_search['project_equipment']).')';
			}
			if(isset($custom_search['project_renno']) && $custom_search['project_renno'] !=''){
				$query .=' AND projects.rennovation IN('.implode(',',$custom_search['project_renno']).')';
			}
			if(isset($custom_search['project_by_cost']) && $custom_search['project_by_cost'] !=''){
				if ($custom_search['project_by_cost']=='Over Cost') {
					$query .=' AND projects.true > projects.budget ';
				}elseif($custom_search['project_by_cost']=='Within The Cost'){
                    $query .=' AND projects.true <= projects.budget ';
				}
			}
			if(isset($custom_search['project_progress']) && $custom_search['project_progress'] !=''){
				$query .=' AND projects.progress_id IN('.implode(',',$custom_search['project_progress']).')';
			}
			if(isset($custom_search['project_delay']) && $custom_search['project_delay'] !=''){
				if ($custom_search['project_delay'] == 1) {
				    $query .=' AND projects.status = 1 AND projects.progress_id = 0 AND (projects.end < "'.date("Y-m-d H:i:s").'" AND projects.new_date < "'.date("Y-m-d H:i:s").'" ) ';
				}
				if ($custom_search['project_delay'] == 2) {
				    $query .=' AND projects.progress_id = 679 AND (projects.end < "'.date("Y-m-d H:i:s").'" AND projects.new_date < "'.date("Y-m-d H:i:s").'" ) ';
				}
				if ($custom_search['project_delay'] == 3) {
				    $query .=' AND projects.progress_id = 680 AND (projects.end < projects.done_date AND projects.new_date < projects.done_date ) ';
				}
			}
		} 
		if ($t_attr) {
			if ($t_attr['search']) {
				$query .= '  AND (projects.id like "%'.$t_attr['search'].'%" OR projects.code like "%'.$t_attr['search'].'%" OR projects.name like "%'.$t_attr['search'].'%")';
			}
			if ($Report_type=='summary') {
				$query .=' GROUP BY projects.hid';
			}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{

				$query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'';
				$query .=' LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
			}        
		}   
		$records = $this->db->query($query);
		return $records->result_array();
	}

	public function get_all_report_project_detailed($uhotels){
		$this->db->where_in('projects.hid',$uhotels);
		return $this->db->get_where('projects',array('deleted'=>0))->result_array();
	}


//---------------- project summary report     

	public function get_project_summary_report($uhotels,$t_attr='',$custom_search='',$filter_count='',$Report_type=''){
		$query = 'SELECT projects.*,SUM(budget_EGP) AS total_budget_epg,SUM(budget_USD) AS total_budget_usd,SUM(budget_EUR) AS total_budget_eur
		,SUM(budget) AS total_budget,SUM(cost_EGP) AS total_cost_epg,SUM(cost_USD) AS total_cost_usd,
		SUM(cost_EUR) AS total_cost_eur,SUM(cost) AS total_cost, hotels.hotel_name, status.status_name, status.status_color, departments.dep_name,
		user_groups.name AS role_name
		FROM projects
		LEFT JOIN hotels ON projects.hid = hotels.id
		LEFT JOIN status ON projects.status = status.id
		LEFT JOIN departments ON projects.dep_code = departments.id
		LEFT JOIN user_groups ON projects.role_id = user_groups.id
		WHERE projects.deleted = 0 AND projects.hid IN('.implode(',',$uhotels).')';
		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','projects');  
			if (isset($custom_search['project_type']) && $custom_search['project_type'] !='') {
				$query .=' AND projects.type_id IN('.implode(',',$custom_search['project_type']).')';
			}
			if(isset($custom_search['project_original']) && $custom_search['project_original'] !=''){
				$query .=' AND projects.origin_id = "'.$custom_search['project_original'].'"';
			}
			if(isset($custom_search['project_equipment']) && $custom_search['project_equipment'] !=''){
				$query .=' AND projects.new IN('.implode(',',$custom_search['project_equipment']).')';
			}
			if(isset($custom_search['project_renno']) && $custom_search['project_renno'] !=''){
				$query .=' AND projects.rennovation IN('.implode(',',$custom_search['project_renno']).')';
			}
			if(isset($custom_search['project_by_cost']) && $custom_search['project_by_cost'] !=''){
				if ($custom_search['project_by_cost']=='Over Cost') {
					$query .=' AND projects.true > projects.budget ';
				}elseif($custom_search['project_by_cost']=='Within The Cost'){
                    $query .=' AND projects.true <= projects.budget ';
				}
			}
			if(isset($custom_search['project_progress']) && $custom_search['project_progress'] !=''){
				$query .=' AND projects.progress_id IN('.implode(',',$custom_search['project_progress']).')';
			}
			if(isset($custom_search['project_delay']) && $custom_search['project_delay'] !=''){
				if ($custom_search['project_delay'] == 1) {
				    $query .=' AND projects.status = 1 AND projects.progress_id = 0 AND (projects.end < "'.date("Y-m-d H:i:s").'" AND projects.new_date < "'.date("Y-m-d H:i:s").'" ) ';
				}
				if ($custom_search['project_delay'] == 2) {
				    $query .=' AND projects.progress_id = 679 AND (projects.end < "'.date("Y-m-d H:i:s").'" AND projects.new_date < "'.date("Y-m-d H:i:s").'" ) ';
				}
				if ($custom_search['project_delay'] == 3) {
				    $query .=' AND projects.progress_id = 680 AND (projects.end < projects.done_date AND projects.new_date < projects.done_date ) ';
				}
			}
		} 
		if ($t_attr) {
			if ($t_attr['search']) {
				$query .= '  AND (projects.id like "%'.$search.'%" OR projects.code like "%'.$search.'%" OR projects.name like "%'.$search.'%")';
			}
			if ($Report_type=='summary') {
				$query .=' GROUP BY projects.hid';
			}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{

				$query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'';
				$query .=' LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
			}        
		}   
		$records = $this->db->query($query);
		return $records->result_array();
	}

	public function get_all_report_project_summary($uhotels){
		$this->db->where_in('projects.hid',$uhotels);
		$this->db->group_by('projects.hid');
		return $this->db->get_where('projects',array('deleted'=>0))->result_array();
	}





}
?>	