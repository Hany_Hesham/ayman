<?php

	defined('BASEPATH') OR exit('No direct script access allowed');



	class Log_activity extends MY_Controller {



		public function __construct(){

			parent::__construct();

            $this->load->model('admin/Modules_model');
	        $this->load->model('admin/General_model');
	        $this->load->model('admin/Log_model');
	            $this->data['user_id']      = $this->global_data['sessioned_user']['id'];
				$this->data['username']     = $this->global_data['sessioned_user']['username'];
				$this->data['module']       = $this->Modules_model->get_module_by('','التسجيل');
	            $this->data['permission']   = user_access($this->data['module']['id']);
	        }

        public function index(){

		       access_checker($this->data['permission']['view'],0,0,0,0,'admin/dashboard');	

				$data['view'] = 'admin/log/logs_view';
		
		        $this->load->view('admin/includes/layout',$data);

			  }  


	     public function log_ajax(){
	           
	          $dt_att  = $this->datatables_att();
	          $logs    = $this->Log_model->get_logs($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

	          $data = array();
	           foreach($logs as $log) {
	               $arr = array(); 
	                    $arr[] = '<span id="" style="padding-left:7%;" class="info">'.$log['id'].'</span>';
	                    $arr[] = '<a class="edit-group" onclick="getLogData('.$log['id'].')" href="javascript: void(0);" data-toggle="modal" data-target="#smallmodal" titel="view Details"><strong style="font-size:16px;"class="info">'.ucfirst($log['action']).'</strong></a>';
                        $arr[] = '<span id="">'.$log['log_time'].'</span>';  
	                    $arr[] = '<strong style="font-size:16px;"class="info">'.$log['target'].'</strong>';
	                    $arr[] = '<span id="" class="info">'.$log['module_name'].' No #.'.$log['target_id'].'</span>';
	                    $arr[] = '<span id="">'.$log['fullname'].'</span>';
	                    $arr[] = '<span id="">'.$log['comments'].'</span>'; 
	                   
	                    $data[] =$arr;
	                }
	         
	               $output = array(
	                     "draw" => $dt_att['draw'],
	                     "recordsTotal"    => count($this->Log_model->get_all_logs()),
	                     "recordsFiltered" => $this->Log_model->get_logs($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	                     "data" => $data
	                );
	          echo json_encode($output);
	          exit();
          
             }



    public function backup_me(){
          $this->load->dbutil();
          $prefs = array(     
              'format'      => 'zip',             
              'filename'    => 'my_db_backup.sql'
              );


          $backup =& $this->dbutil->backup($prefs); 
          $db_name = 'backup-on-'. date("Y-m-d-H-i-s") .'.zip';
          $save = 'pathtobkfolder/'.$db_name;
          $this->load->helper('file');
          write_file($save, $backup); 
          $this->load->helper('download');
          force_download($db_name, $backup);
      } 






	}

?>	 