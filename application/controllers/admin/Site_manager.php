<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Site_manager extends MY_Controller {
   
   		public function __construct(){
			parent::__construct();
        	$this->load->model('admin/Modules_model');
	     	$this->load->model('admin/General_model');
	      	$this->load->model('admin/Site_manager_model');
	      	$this->load->helper('mailer_helper');
  		  	$this->data['user_id']                 	= $this->global_data['sessioned_user']['id'];
  		  	$this->data['username']                	= $this->global_data['sessioned_user']['username'];
	      	$this->data['ubranches']               	= get_ubranches($this->data['user_id']);
	      	$this->data['dep_id']                  	= $this->global_data['sessioned_user']['department'];
  			$this->data['slider_module']           	= $this->Modules_model->get_module_by(1,'');
  			$this->data['competitions_module']      = $this->Modules_model->get_module_by(2,'');
  			$this->data['partners_module']           = $this->Modules_model->get_module_by(3,'');
  			$this->data['gallary_module']           = $this->Modules_model->get_module_by(4,'');
  			$this->data['projects_module']          = $this->Modules_model->get_module_by(5,'');
  			$this->data['event_module']           	= $this->Modules_model->get_module_by(6,'');
  			$this->data['about_module']            	= $this->Modules_model->get_module_by(14,'');
  			$this->data['module']                  	= $this->Modules_model->get_module_by(12,'');
  			$this->data['rules_module']         	= $this->Modules_model->get_module_by(27,'');
  			$this->data['products_module']         	= $this->Modules_model->get_module_by(16,'');
  			$this->data['portfolio_module']        	= $this->Modules_model->get_module_by(17,'');
  			$this->data['competitors_module']       = $this->Modules_model->get_module_by(18,'');
  			$this->data['contact_module']          	= $this->Modules_model->get_module_by(19,'');
  			$this->data['footer_module']           	= $this->Modules_model->get_module_by(20,'');
	      	$this->data['clients_req_module']      	= $this->Modules_model->get_module_by(21,'');
	      	$this->data['gallery_module']          	= $this->Modules_model->get_module_by(22,'');
	      	$this->data['results_module']           = $this->Modules_model->get_module_by(23,'');
	      	$this->data['winners_module']           = $this->Modules_model->get_module_by(24,'');
  			$this->data['language_module']         	= $this->Modules_model->get_module_by(25,'');
			$this->data['voters_module']         	= $this->Modules_model->get_module_by(26,'');
	      	$this->data['slider_permission']       	= user_access($this->data['slider_module']['id']);
	      	$this->data['competitions_permission']  = user_access($this->data['competitions_module']['id']);
	      	$this->data['partners_permission']      = user_access($this->data['partners_module']['id']);
	      	$this->data['gallary_permission']      	= user_access($this->data['gallary_module']['id']);
	      	$this->data['projects_permission']      = user_access($this->data['projects_module']['id']);
	      	$this->data['event_permission']      	= user_access($this->data['event_module']['id']);
			$this->data['about_permission']        = user_access(14);
			$this->data['rules_permission']     = user_access(27);
			$this->data['products_permission']     = user_access(16);
			$this->data['portfolio_permission']    = user_access(17);
			$this->data['competitors_permission'] = user_access(18);
			$this->data['voters_permission'] = user_access(26);
			$this->data['contact_permission']      = user_access(19);
			$this->data['footer_permission']       = user_access(20);
			$this->data['clients_req_permission']  = user_access(21);
			$this->data['gallery_permission']      = user_access(22);
			$this->data['results_permission']         = user_access(23);
			$this->data['winners_permission']        = user_access(24);
			$this->data['language_permission']     = user_access(25);
			$this->data['ubranches']               = get_ubranches($this->data['user_id']);
		}
		 
		public function index(){
			$data['view'] = 'admin/dashboard_index';
			$this->load->view('admin/includes/layout',$data);
		}
		/**
		*******************  projects manager ************************
		*/
		public function projects_manager(){
			access_checker($this->data['projects_permission']['view'],0,0,0,0,'admin/dashboard'); 
			$data['view']          = 'admin/site_manager/projects_manager';
			$this->load->view('admin/includes/layout',$data);
		}

		public function projects_ajax(){
			$dt_att  = $this->datatables_att();
			$statuss      = $this->Site_manager_model->get_site_metaData('project_status','multi');
			$items   = $this->Site_manager_model->get_projects($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
			$data = array();
			$i    =  1;
			foreach($items as $item) {
				$arr = array(); 
				$tools=array();
				$status_opt = '';
				foreach ($statuss as $status) {
					$status_opt .= '<a style="margin-left:5px;" href="javascript: void(0);" onclick="changer('.$item['id'].',`admin/site_manager`,`status_change`,`projects`,`status`,'.$status['link'].')">
						<b style="color:'.$status['paragraph'].'">'.$status['header'].'</b>
					</a>
					<div class="dropdown-divider"></div>';
				}
				$tools[] = '<a class="text-dark" href="javascript: void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<button type="button" class="btn btn-outline-secondary btn-sm" style="color:'.$item['status_color'].'">'.$item['header'].'</button>
				</a>
				<div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
					<div style="margin-top: 15px;">'.$status_opt.'</div>
				</div>';
				$arr[] = '<span>'.$i.'.</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
				<strong class="text-dark">Competitor / </strong>'.$item['first_name'].' '.$item['last_name'].'<br>
				<strong class="text-dark">Email / </strong>'.$item['email'].'<br>
				<strong class="text-dark">Phone / </strong>'.$item['phone'].'<br>
				<strong class="text-dark">School / </strong>'.$item['school'].'<br>
				<strong class="text-dark">Coach Name / </strong>'.$item['trainer_name'].'<br>
				<strong class="text-dark">Birth Date / </strong>'.$item['birth_date'].'<br>
				<strong class="text-dark">Age / </strong>'.getAgeFromDate($item['birth_date']).'<br>
				<strong class="text-dark">Age Category / </strong>'.$item['category_name'].'
				</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
				<strong class="text-dark">Project Code / </strong>'.$item['code'].'<br>
				<strong class="text-dark">Competition Title / </strong>'.$item['competition_title'].'
				</span>
				<br>'.implode("", $tools).'
				<a class="btn btn-danger btn-sm" href="'.base_url('clients/home/project/'.$item['code']).'" target="_blanck"><strong>View Project</strong></a>';
				$arr[] = '<span style="color:'.$item['status_color'].'">'.return_money_formater($item['competition_price'],$item['currency']).'</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">'.$item['payment'].'</span>';
				$arr[] = isset($item['payment_attach']) && $item['payment_attach'] !='' ? getImageViewer(base_url('site_assets/uploads/payments/'.$item['payment_attach'])) : "";
				$data[] =$arr;
				$i++;
			}
			$output = array(
				"draw" => $dt_att['draw'],
				"recordsTotal"    => count($this->Site_manager_model->get_all_projects()),
				"recordsFiltered" => $this->Site_manager_model->get_projects($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
				"data" => $data
			);
			echo json_encode($output);
			exit();
		}

		public function status_change($id, $column, $status_id){
			access_checker(0,0,$this->data['projects_permission']['edit'],0,0,'');
			$project = $this->Site_manager_model->get_project($id);
			if($status_id == 3){
				notify_site_users($this->data['projects_module']['id'], $project['competitor_id'],
			    'Your registration payment has been confirmed, please add your project data',
			    base_url('clients/home/competition_register/'.$project['competition_id'].'/'.$project['id'])
		        );
			}elseif($status_id == 5){
				$competitor = $this->Site_manager_model->get_competitor_by_id($project['competitor_id']);
				$category = '';
				$age = getAgeFromDate($competitor['birth_date']);
				if($age < 9){
				$category = 46;
				}elseif($age >= 9 && $age <= 12){
					$category = 47;
				}elseif($age > 12){
					$category = 48;
				}  
				$this->Site_manager_model->update_project(['category' => $category], $project['id']);
				notify_site_users($this->data['projects_module']['id'], $project['competitor_id'],
			    'Your project has been published',
			    base_url('clients/home/project/'.$project['code'])
		        );
			}
			status_changer('projects',$column, $status_id, $id);
			loger('Status Change', $this->data['projects_module']['id'], $this->data['projects_module']['name'],$id,0, 0, 0, 0, 0,'Changed project status:'.$id.'');
			echo json_encode(true);
			exit();
		}

		/**
		 * Competition manager
		 */
		public function competitions_manager(){
			access_checker($this->data['competitions_permission']['view'],0,0,0,0,'admin/dashboard'); 
			$data['view']          = 'admin/site_manager/competitions_manager';
			$this->load->view('admin/includes/layout',$data);
		}

		public function competitions_ajax(){
			$dt_att  = $this->datatables_att();
			$items   = $this->Site_manager_model->get_competitions($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
			$data = array();
			$i    =  1;
			foreach($items as $item) {
				$arr = array(); 
				$tools = array();
				$moneyCost = $item['price'] && $item['price'] != 0 ? return_money_formater($item['price'],$item['currency']): "";
				// $item['status_color'] = $item['active'] ? "": "#da542e";
				if($item['active'] ==0 ){
					$item['status_color'] = "#da542e";
					$status = "Inactive";
				}elseif($item['active'] == 1){
					$item['status_color'] = "";
					$status = "Active";
				}elseif($item['active'] == 2){
					$item['status_color'] = "#bda11a";
					$status = "Close Registration";
				}
				// $status = $item['active'] == 1 ? "Active": "Inactive";
				// $anotherStatus = $status == "Active" ? "Inactive": "Active";
				$status_opt = '<a class="text-dark" href="javascript: void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<button type="button" class="btn btn-outline-secondary btn-sm" style="color:'.$item['status_color'].'">'.$status.'</button>
								</a>                 
			                   <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
									<div style="margin-top: 15px;">
										<a style="margin-left:5px;" href="javascript: void(0);" onclick="changer('.$item['id'].',`admin/site_manager`,`change_competition_active`,`competitions`,`active`,1)">
											<b style="">Active</b>
										</a>
										<div class="dropdown-divider"></div>
										<a style="margin-left:5px;" href="javascript: void(0);" onclick="changer('.$item['id'].',`admin/site_manager`,`change_competition_active`,`competitions`,`active`,0)">
											<b style="color:#da542e">Inactive</b>
										</a>
										<div class="dropdown-divider"></div>
										<a style="margin-left:5px;" href="javascript: void(0);" onclick="changer('.$item['id'].',`admin/site_manager`,`change_competition_active`,`competitions`,`active`,2)">
											<b style="color:#bda11a">Close Registration</b>
										</a>
									</div>
								</div>';
				$tools[] = "<div class='info'>
								<a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='addEditCompetitionModal(".json_encode($item).")'>
								<strong>Edit <i class='mdi mdi-pencil'></i></strong>
								</a> 
								<a href='".base_url('admin/site_manager/results_manager/'.$item['id'])."' class='btn btn-danger btn-sm'>
								<strong>Results <i class='mdi mdi-trophy'></i></strong>
								</a> 
							</div>";
				$arr[] = '<span>'.$i.'.</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
					<strong class="text-dark">Title / </strong>'.$item['title'].'<br>
					<strong class="text-dark">code / </strong>'.$item['code'].'<br>
					<strong class="text-dark">Description / </strong>'.$item['description'].'
					<br>'.implode("", $tools);
				$arr[] = $status_opt;	
				$arr[] = '<span style="color:'.$item['status_color'].'">'.$item['type'].'</span>';	
				$arr[] = '<span style="color:'.$item['status_color'].'">
					<strong class="text-dark">Start Date / </strong>'.$item['start_date'].'<br>
					<strong class="text-dark">End Date/ </strong>'.$item['end_date'].'<br>
					<strong class="text-dark">Vote Start Date / </strong>'.$item['vote_start_date'].'<br>
					<strong class="text-dark">Vote End Date/ </strong>'.$item['vote_end_date'].'<br>
					<strong class="text-dark">Closed Date/ </strong>'.$item['closed_at'].'
					</span>';
				if(isset($item['results_file']) && $item['results_file'] !='' && $item['results_file'] !='0'){
					$arr[] = "<a href='".base_url('site_assets/uploads/competitions/'.$item['results_file'])."' class='btn btn-danger btn-sm' download>
					<strong>Results <i class='mdi mdi-cloud-download'></i></strong>
					</a> ";
				}else{
					$arr[] = '';
				}	
				$arr[] = '<span style="color:'.$item['status_color'].'">'.$moneyCost.'</span>';
				$arr[] = isset($item['image']) && $item['image'] !='' && $item['image'] !='0' ? getImageViewer(base_url('site_assets/uploads/competitions/'.$item['image'])) : "";
				$data[] =$arr;
				$i++;
			}
			$output = array(
				"draw" => $dt_att['draw'],
				"recordsTotal"    => count($this->Site_manager_model->get_all_competitions()),
				"recordsFiltered" => $this->Site_manager_model->get_competitions($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
				"data" => $data
			);
			echo json_encode($output);
			exit();
		}

		public function change_competition_active($id, $column, $value){
			access_checker(0,0,$this->data['competitions_permission']['edit'],0,0,'');
			$dateObj = new DateTime();
			$date = $value == 0 ? $dateObj->format('Y-m-d H:i:s'): '';
			$compData = [
				'active' => $value,
				'closed_at' => $date
			];
			$this->Site_manager_model->update_competition($compData, $id);
			loger('Active Change', $this->data['competitions_module']['id'], $this->data['competitions_module']['name'],$id,0, 0, 0, 0, 0,'Changed competition active:'.$id.'');
			echo json_encode(true);
			exit();
		}

    	public function add_competition(){
			access_checker(0,$this->data['competitions_permission']['creat'],0,0,0,'');
			if ($this->input->post()) {
				if($this->input->post('competition_id') != '') 
				    $this->edit_competition($this->input->post('competition_id'));
				$file_name   = do_upload("image",'competitions','site_assets/uploads');
				$results_file   = do_upload("results_file",'competitions','site_assets/uploads');
				$fdata = [
					'code' => get_competition_code(),
					'type' => $this->input->post('type'),
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'image' => $file_name,
					'results_file' => $results_file,
					'start_date' => $this->input->post('start_date'),
					'end_date' => $this->input->post('end_date'),
					'vote_start_date' => $this->input->post('vote_start_date'),
					'vote_end_date' => $this->input->post('vote_end_date'),
					'price' => $this->input->post('price'),
					'currency' => $this->input->post('currency'),
					'active' => 1,
				];
				$item_id = $this->Site_manager_model->add_competition($fdata);
				if ($item_id) {
					learn_translate($this->input->post('title'));
					learn_translate($this->input->post('description'));
					loger('Create',$this->data['competitions_module']['id'],$this->data['competitions_module']['name'],$item_id, 0, 0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'added Info: '.$item_id);
					redirect('admin/site_manager/competitions_manager');
				}  
			}
    	}    

		public function edit_competition($competition_id){
			access_checker(0,0,$this->data['competitions_permission']['edit'],0,0,'');
			if ($this->input->post()) {
				$file_name   = do_upload("image",'competitions','site_assets/uploads');
				$results_file   = do_upload("results_file",'competitions','site_assets/uploads');
				$fdata = [
					'type' => $this->input->post('type'),
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'start_date' => $this->input->post('start_date'),
					'end_date' => $this->input->post('end_date'),
					'vote_start_date' => $this->input->post('vote_start_date'),
					'vote_end_date' => $this->input->post('vote_end_date'),
					'price' => $this->input->post('price'),
					'currency' => $this->input->post('currency'),
				];
				if( $file_name) $fdata['image'] = $file_name;
				if( $results_file) $fdata['results_file'] = $results_file;
				$item_id = $this->Site_manager_model->update_competition($fdata, $competition_id);
				if ($item_id) {
					learn_translate($this->input->post('title'));
					learn_translate($this->input->post('description'));
					loger('Updated', $this->data['competitions_module']['id'], $this->data['competitions_module']['name'],$competition_id,0, 0, 0, 0, 0,'Changed competition:'.$competition_id.'');
					redirect('admin/site_manager/competitions_manager');
				}  
			}
    	}

		/**
		*******************  Results manager ************************
		*/
		public function results_manager($competition_id){
			access_checker($this->data['results_permission']['view'],0,0,0,0,'admin/dashboard');
			$data['competition_id'] = $competition_id; 
			$data['view']          = 'admin/site_manager/results_manager';
			$this->load->view('admin/includes/layout',$data);
		}

		public function results_ajax($competition_id){
			$dt_att  = $this->datatables_att();
			$items   = $this->Site_manager_model->get_results($competition_id,$dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
			$data = array();
			$i    =  1;
			foreach($items as $item) {
				$arr = array(); 
				$tools=array();
				$status_opt = '';
				$tools[] = "<div class='info'>
								<a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='addEditResultModal(".json_encode($item).")'>
								<strong>Edit <i class='mdi mdi-pencil'></i></strong>
								</a>  
							</div>";
				$arr[] = '<span>'.$i.'.</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
				<strong class="text-dark">Competitor / </strong>'.$item['first_name'].' '.$item['last_name'].'<br>
				<strong class="text-dark">Email / </strong>'.$item['email'].'<br>
				<strong class="text-dark">Phone / </strong>'.$item['phone'].'<br>
				<strong class="text-dark">School / </strong>'.$item['school'].'<br>
				<strong class="text-dark">Coach Name / </strong>'.$item['trainer_name'].'
				</span><br>'.implode("", $tools).'';
				$arr[] = '<span style="color:'.$item['status_color'].'">
				<strong class="text-dark">Project Code / </strong>'.$item['code'].'<br>
				<strong class="text-dark">Competition Title / </strong>'.$item['competition_title'].'
				</span>
				<br>
				<a class="btn btn-danger btn-sm" href="'.base_url('clients/home/project/'.$item['code']).'" target="_blanck"><strong>View Project</strong></a>';
				$arr[] = '<a class="btn btn-dark btn-sm" href="'.$item['project_link'].'" target="_blanck"><strong>View Full Project</strong></a>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
				<strong class="text-dark">'.$item['design_score'].'</strong></span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
				<strong class="text-dark">'.$item['logic_score'].'</strong></span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
				<strong class="text-dark">'.$item['presentation_score'].'</strong></span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
				<strong class="text-dark">'.$item['total_score'].'</strong></span>';
				$data[] =$arr;
				$i++;
			}
			$output = array(
				"draw" => $dt_att['draw'],
				"recordsTotal"    => count($this->Site_manager_model->get_all_results($competition_id)),
				"recordsFiltered" => $this->Site_manager_model->get_results($competition_id,$dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
				"data" => $data
			);
			echo json_encode($output);
			exit();
		}

		public function edit_project_score(){
			access_checker(0,0,$this->data['results_permission']['edit'],0,0,'');
			if ($this->input->post()) {
				$project_id = $this->input->post('project_id');
				$competition_id = $this->input->post('competition_id');
				$fdata = [
					'design_score' => $this->input->post('design_score'),
					'logic_score' => $this->input->post('logic_score'),
					'presentation_score' => $this->input->post('presentation_score'),
				];
				$total_score = (float)$fdata['design_score'] + (float)$fdata['logic_score'] + (float)$fdata['presentation_score'];
				$fdata['total_score'] = (float)$total_score;
				$item_id = $this->Site_manager_model->update_project($fdata, $project_id);
				if ($item_id) {
					$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
					loger('Updated', $this->data['results_module']['id'], $this->data['results_module']['name'],$project_id,0, 0, 0, 0, 0,'Changed project score:'.$project_id.'');
					redirect('admin/site_manager/results_manager/'.$competition_id);
				}  
			}
    	}

		public function export_competition_projects($competition_id){
			access_checker($this->data['results_permission']['view'],0,0,0,0,'admin/dashboard');
			$projects = $this->Site_manager_model->get_competition_projects($competition_id);
			header("Content-Type: application/xls");    
			header("Content-Disposition: attachment; filename=results.xls");  
			header("Pragma: no-cache"); 
			header("Expires: 0");
			$columns = ['Full Name', 'School', 'Age', 'Email', 'Coach Name','Internal Project Link', 
			            'Full Project link', 'Programming Score', 'Logic Score', 'Votes Score'
					   ]; 
			$excelData = implode("\t", $columns) . "\r\n"; 
			foreach($projects as $project){
				$fullName = $project['first_name'].' '.$project['middle_name'].' '.$project['last_name'];
				$data= [
					"","",
					$fullName,
					$project['school'],
					$project['age'],
					$project['email'],
					$project['trainer_name'], 
					base_url('clients/home/project/'.$project['code']),
					$project['project_link'],
					"",
					"",
					""
				];
				$excelData .= implode("\t", $data) . "\n"; 
			}
			echo $excelData; 
			exit;
		}
		/**
		 *  partners manager
		 */
		public function  partners_manager(){
			access_checker($this->data['partners_permission']['view'],0,0,0,0,'admin/dashboard'); 
			$data['partners'] = $this->Site_manager_model->get_site_metaData('partners','multi');
			$data['view']          = 'admin/site_manager/partners_manager';
			$this->load->view('admin/includes/layout',$data);
		}


    	public function add_partner(){
			access_checker(0,$this->data['partners_permission']['creat'],0,0,0,'');
			if ($this->input->post()) {
				if($this->input->post('partner_id') != '') 
				    $this->edit_partner($this->input->post('partner_id'));
				$file_name   = do_upload("img",'partners','site_assets/uploads');
				$fdata = [
					'title' => $this->input->post('title'),
					'img' => $file_name,
					'meta' => 'partners'
				];
				$item_id = $this->Site_manager_model->add_partner($fdata);
				if ($item_id) {
					$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
					loger('Create',$this->data['partners_module']['id'],$this->data['partners_module']['name'],$item_id, 0, 0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'added Info: '.$item_id);
					redirect('admin/site_manager/partners_manager');
				}  
			}
    	}    

		public function edit_partner($partner_id){
			access_checker(0,0,$this->data['partners_permission']['edit'],0,0,'');
			if ($this->input->post()) {
				$file_name   = do_upload("img",'partners','site_assets/uploads');
				$fdata = [
					'title' => $this->input->post('title'),
				];
				if( $file_name) $fdata['img'] = $file_name;
				$item_id = $this->Site_manager_model->update_partner($fdata, $partner_id);
				if ($item_id) {
					$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been modified Successfully!']);
					loger('Updated', $this->data['partners_module']['id'], $this->data['partners_module']['name'],$partner_id,0, 0, 0, 0, 0,'Changed partner:'.$partner_id.'');
					redirect('admin/site_manager/partners_manager');
				}  
			}
    	}

		public function delete_partner($id){
			access_checker(0,0,0,$this->data['partners_permission']['remove'],0,'');
			$item = $this->Site_manager_model->get_partner($id);
			$this->db->delete('site_meta_data',array('id' => $item['id']));	
			$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been deleted Successfully!']);
			loger('Delete', $this->data['partners_module']['id'], $this->data['partners_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted Info:'.$item['id'].'');
			redirect('admin/site_manager/partners_manager');
		}  

		/**
		 * Rules manager
		 */
		public function rules_manager(){
			access_checker($this->data['rules_permission']['view'],0,0,0,0,'admin/dashboard'); 
			$data['view']          = 'admin/site_manager/rules_manager';
			$this->load->view('admin/includes/layout',$data);
		}

		public function rules_ajax(){
			$dt_att  = $this->datatables_att();
			$items   = $this->Site_manager_model->get_rules($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
			$data = array();
			$i    =  1;
			foreach($items as $item) {
				$arr = array(); 
				$tools = array();
				$item['status_color'] = $item['disabled'] == 0 ? "": "#da542e";
				$status = $item['disabled'] == 0 ? "Active": "Inactive";
				$anotherStatus = $status == "Active" ? "Inactive": "Active";
				$status_opt = '<a class="text-dark" href="javascript: void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<button type="button" class="btn btn-outline-secondary btn-sm" style="color:'.$item['status_color'].'">'.$status.'</button>
								</a>                 
			                   <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
									<div style="margin-top: 15px;">
										<a style="margin-left:5px;" href="javascript: void(0);" onclick="changer('.$item['id'].',`admin/site_manager`,`change_rule_active`,`rules`,`disabled`,0)">
											<b style="">Active</b>
										</a>
										<div class="dropdown-divider"></div>
										<a style="margin-left:5px;" href="javascript: void(0);" onclick="changer('.$item['id'].',`admin/site_manager`,`change_rule_active`,`rules`,`disabled`,1)">
											<b style="color:#da542e">Inactive</b>
										</a>
									</div>
								</div>';
				$tools[] = "<div class='info'>
								<a href=".base_url('admin/site_manager/edit_rule/'.$item['id'])." class='btn btn-primary btn-sm'>
								<strong>Edit <i class='mdi mdi-pencil'></i></strong>
								</a> 
							</div>";
				$arr[] = '<span>'.$i.'.</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
					<strong class="text-dark">Title / </strong>'.$item['title'].'
					<br>'.implode("", $tools);
					$arr[] = '<span style="color:'.$item['status_color'].'">
					<strong class="text-dark">Content / </strong>'.$item['content'].'
					</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
					'.$item['rank'].'</span>';
				$arr[] = $status_opt;	
				$data[] =$arr;
				$i++;
			}
			$output = array(
				"draw" => $dt_att['draw'],
				"recordsTotal"    => count($this->Site_manager_model->get_all_rules()),
				"recordsFiltered" => $this->Site_manager_model->get_rules($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
				"data" => $data
			);
			echo json_encode($output);
			exit();
		}

		public function change_rule_active($id, $column, $value){
			access_checker(0,0,$this->data['rules_permission']['edit'],0,0,'');
			$ruleData = [
				'disabled' => $value
			];
			$this->Site_manager_model->update_rule($ruleData, $id);
			loger('Active Change', $this->data['rules_module']['id'], $this->data['rules_module']['name'],$id,0, 0, 0, 0, 0,'Changed rule active:'.$id.'');
			echo json_encode(true);
			exit();
		}

    	public function add_rule(){
			access_checker(0,$this->data['rules_permission']['creat'],0,0,0,'');
			$data['gen_id'] = get_file_code('site_files','I');
			$data['uploads'] = [];
			if ($this->input->post()) {
				$file_name   = do_upload("file_name",'rules','site_assets/uploads');
				$fdata = [
					'rank' => $this->input->post('rank'),
					'title' => $this->input->post('title'),
					'icon' => 'fas fa-pencil-alt',
					'content' => $this->input->post('content'),
					'file_name' => $file_name,
					'video_link' =>  $this->input->post('video_link')
				];
				$item_id = $this->Site_manager_model->add_rule($fdata);
				$this->General_model->update_files($this->input->post('gen_id'),15,$item_id,'site_files');
				if ($item_id) {
					learn_translate($this->input->post('title'));
					learn_translate($this->input->post('content'));
					loger('Create',$this->data['rules_module']['id'],$this->data['rules_module']['name'],$item_id, 0, 0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'added Info: '.$item_id);
					$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
					redirect('admin/site_manager/rules_manager');
				}  
			}
		$data['view'] = 'admin/site_manager/rule_create';
		$this->load->view('admin/includes/layout',$data);
    	}    

		public function edit_rule($rule_id){
			access_checker(0,0,$this->data['rules_permission']['edit'],0,0,'');
			$data['rule'] = $this->Site_manager_model->get_rule($rule_id);
			$data['uploads'] = $this->General_model->get_file_from_table(27, $rule_id,'site_files');
			$data['gen_id'] = $rule_id;
			if ($this->input->post()) {
				$file_name   = do_upload("file_name",'rules','site_assets/uploads');
				$fdata = [
					'rank' => $this->input->post('rank'),
					'title' => $this->input->post('title'),
					'content' => $this->input->post('content'),
					'video_link' =>  $this->input->post('video_link')
				];
				if( $file_name) $fdata['file_name'] = $file_name;
				$item_id = $this->Site_manager_model->update_rule($fdata, $rule_id);
				if ($item_id) {
					learn_translate($this->input->post('title'));
					learn_translate($this->input->post('content'));
					$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been modified Successfully!']);
					loger('Updated', $this->data['rules_module']['id'], $this->data['rules_module']['name'],$rule_id,0, 0, 0, 0, 0,'Changed rule:'.$rule_id.'');
					redirect('admin/site_manager/edit_rule/'.$rule_id);
				}  
			}
			$data['view'] = 'admin/site_manager/rule_create';
			$this->load->view('admin/includes/layout',$data);
    	}
		
		/**
		 * Competitors manager
		 */
		public function competitors_manager(){
			access_checker($this->data['competitors_permission']['view'],0,0,0,0,'admin/dashboard'); 
			$data['view']          = 'admin/site_manager/competitors_manager';
			$this->load->view('admin/includes/layout',$data);
		}

		public function competitors_ajax(){
			$dt_att  = $this->datatables_att();
			$items   = $this->Site_manager_model->get_competitors($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
			$data = array();
			$i    =  1;
			foreach($items as $item) {
				$arr = array(); 
				$tools = array();
				$item['status_color'] = $item['disabled'] == 0 ? "": "#da542e";
				$status = $item['disabled'] == 0 ? "Active": "Inactive";
				$anotherStatus = $status == "Active" ? "Inactive": "Active";
				$status_opt = '<a class="text-dark" href="javascript: void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<button type="button" class="btn btn-outline-secondary btn-sm" style="color:'.$item['status_color'].'">'.$status.'</button>
								</a>                 
			                   <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
									<div style="margin-top: 15px;">
										<a style="margin-left:5px;" href="javascript: void(0);" onclick="changer('.$item['id'].',`admin/site_manager`,`change_competitor_active`,`competitors`,`disabled`,0)">
											<b style="">Active</b>
										</a>
										<div class="dropdown-divider"></div>
										<a style="margin-left:5px;" href="javascript: void(0);" onclick="changer('.$item['id'].',`admin/site_manager`,`change_competitor_active`,`competitors`,`disabled`,1)">
											<b style="color:#da542e">Inactive</b>
										</a>
									</div>
								</div>';
				$arr[] = '<span>'.$i.'.</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
					<strong class="text-dark">Name / </strong>'.$item['name'].' '.$item['middle_name'].''.$item['last_name'].'<br>
					<strong class="text-dark">Email / </strong>'.$item['email'].'<br>
					</span><br>';
				$arr[] = '<span style="color:'.$item['status_color'].'">'.$item['city'].'</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">'.$item['school'].'</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">'.$item['birth_date'].'</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">'.$item['age'].'</span>';
				$arr[] = $status_opt."
				<a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='changeCompetitorPassword(".json_encode($item).")'>
				<strong>Change Password <i class='mdi mdi-pencil'></i></strong>
				</a> 
			";	
				$data[] =$arr;
				$i++;
			}
			$output = array(
				"draw" => $dt_att['draw'],
				"recordsTotal"    => count($this->Site_manager_model->get_all_competitors()),
				"recordsFiltered" => $this->Site_manager_model->get_competitors($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
				"data" => $data
			);
			echo json_encode($output);
			exit();
		}

		public function change_competitor_active($id, $column, $value){
			access_checker(0,0,$this->data['competitors_permission']['edit'],0,0,'');
			$compData = [
				'disabled' => $value
			];
			$this->Site_manager_model->update_competitor($compData, $id);
			loger('Active Change', $this->data['competitors_module']['id'], $this->data['competitors_module']['name'],$id,0, 0, 0, 0, 0,'Changed competitor active:'.$id.'');
			echo json_encode(true);
			exit();
		}

		public function change_competitor_password(){
			access_checker(0,0,$this->data['competitors_permission']['edit'],0,0,'');
			if($this->input->post()){
				$compData = [
					'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT)
				];
				$this->Site_manager_model->update_competitor($compData,  $this->input->post('competitor_id'));
				loger('Change competitor password', $this->data['competitors_module']['id'], $this->data['competitors_module']['name'],$this->input->post('competitor_id'),0, 0, 0, 0, 0,'Changed competitor password:'.$this->input->post('competitor_id').'');
				$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record have been updated Successfully!']);
				redirect('admin/site_manager/competitors_manager');
			}else{
				$this->session->set_flashdata(['alert'=>'failure','msg'=>"failed to update record"]);
				redirect('admin/site_manager/competitors_manager');
			}
		}

		/**
		 * voters manager
		 */
		public function voters_manager(){
			access_checker($this->data['voters_permission']['view'],0,0,0,0,'admin/dashboard'); 
			$data['view']          = 'admin/site_manager/voters_manager';
			$this->load->view('admin/includes/layout',$data);
		}

		public function voters_ajax(){
			$dt_att  = $this->datatables_att();
			$items   = $this->Site_manager_model->get_voters($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
			$data = array();
			$i    =  1;
			foreach($items as $item) {
				$arr = array(); 

				$arr[] = '<span>'.$i.'.</span>';
				$arr[] = '<span>'.$item['first_name'].'</span>';
				$arr[] = '<span>'.$item['last_name'].'</span>';
				$arr[] = '<span>'.$item['email'].'</span>';
				$arr[] = '<span>'.$item['oauth_provider'].'</span>';
				$arr[] = '<span>'.$item['created_at'].'</span>';
				$data[] =$arr;
				$i++;
			}
			$output = array(
				"draw" => $dt_att['draw'],
				"recordsTotal"    => count($this->Site_manager_model->get_all_voters()),
				"recordsFiltered" => $this->Site_manager_model->get_voters($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
				"data" => $data
			);
			echo json_encode($output);
			exit();
		}

		/**
		 * Winners manager
		 */
		public function winners_manager(){
			access_checker($this->data['winners_permission']['view'],0,0,0,0,'admin/dashboard'); 
			$data['competitors'] = $this->Site_manager_model->get_all_competitors();
			$data['competitions'] = $this->Site_manager_model->get_all_competitions_active();
			$data['view']          = 'admin/site_manager/winners_manager';
			$this->load->view('admin/includes/layout',$data);
		}

		public function winners_ajax(){
			$dt_att  = $this->datatables_att();
			$items   = $this->Site_manager_model->get_winners($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
			$data = array();
			$i    =  1;
			foreach($items as $item) {
				$arr = array(); 
				$tools = array();
				$item['status_color'] = $item['disabled'] == 0 ? "": "#da542e";
				$status = $item['disabled'] == 0 ? "Active": "Inactive";
				$anotherStatus = $status == "Active" ? "Inactive": "Active";
				$status_opt = '<a class="text-dark" href="javascript: void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<button type="button" class="btn btn-outline-secondary btn-sm" style="color:'.$item['status_color'].'">'.$status.'</button>
								</a>                 
			                   <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
									<div style="margin-top: 15px;">
										<a style="margin-left:5px;" href="javascript: void(0);" onclick="changer('.$item['id'].',`admin/site_manager`,`change_winner_active`,`winners`,`disabled`,0)">
											<b style="">Active</b>
										</a>
										<div class="dropdown-divider"></div>
										<a style="margin-left:5px;" href="javascript: void(0);" onclick="changer('.$item['id'].',`admin/site_manager`,`change_winner_active`,`winners`,`disabled`,1)">
											<b style="color:#da542e">Inactive</b>
										</a>
									</div>
								</div>';
				$tools[] = "<div class='info'>
								<a href='javascript: void(0);' class='btn btn-primary btn-sm' onclick='addEditWinnerModal(".json_encode($item).")'>
								<strong>Edit <i class='mdi mdi-pencil'></i></strong>
								</a> 
							</div>";
				$arr[] = '<span>'.$i.'.</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">
				<strong class="text-dark">Name / </strong>'.$item['name'].' '.$item['middle_name'].''.$item['last_name'].'<br>
				</span><br>'.implode("", $tools);
				$arr[] = '<span style="color:'.$item['status_color'].'">'.$item['competition_title'].'</span>';
				$arr[] = $status_opt;	
				$arr[] = '<span style="color:'.$item['status_color'].'">'.$item['email'].'</span>';
				$arr[] = '<span style="color:'.$item['status_color'].'">'.$item['rank'].'</span>';
				$arr[] = isset($item['profile_picture']) && $item['profile_picture'] !='' && $item['profile_picture'] !='0' ? getImageViewer(base_url('site_assets/uploads/users_profiles/'.$item['profile_picture'])) : "";
				$data[] =$arr;
				$i++;
			}
			$output = array(
				"draw" => $dt_att['draw'],
				"recordsTotal"    => count($this->Site_manager_model->get_all_winners()),
				"recordsFiltered" => $this->Site_manager_model->get_winners($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
				"data" => $data
			);
			echo json_encode($output);
			exit();
		}

		public function change_winner_active($id, $column, $value){
			access_checker(0,0,$this->data['winners_permission']['edit'],0,0,'');
			$compData = [
				'disabled' => $value,
			];
			$this->Site_manager_model->update_winner($compData, $id);
			loger('Active Change', $this->data['winners_module']['id'], $this->data['winners_module']['name'],$id,0, 0, 0, 0, 0,'Changed winner active:'.$id.'');
			echo json_encode(true);
			exit();
		}

    	public function add_winner(){
			access_checker(0,$this->data['winners_permission']['creat'],0,0,0,'');
			if ($this->input->post()) {
				if($this->input->post('id') != '') 
				    $this->edit_winner($this->input->post('id'));
				$fdata = [
					'rank' => $this->input->post('rank'),
					'competitor_id' => $this->input->post('competitor_id'),
					'competition_id' => $this->input->post('competition_id'),
				];
				$item_id = $this->Site_manager_model->add_winner($fdata);
				if ($item_id) {
					$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been Added Successfully!']);
					loger('Create',$this->data['winners_module']['id'],$this->data['winners_module']['name'],$item_id, 0, 0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'added Info: '.$item_id);
					redirect('admin/site_manager/winners_manager');
				}  
			}
    	}    

		public function edit_winner($winner_id){
			access_checker(0,0,$this->data['winners_permission']['edit'],0,0,'');
			if ($this->input->post()) {
				$fdata = [
					'rank' => $this->input->post('rank'),
					'competitor_id' => $this->input->post('competitor_id'),
					'competition_id' => $this->input->post('competition_id'),
				];
				$item_id = $this->Site_manager_model->update_winner($fdata, $winner_id);
				if ($item_id) {
					$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
					loger('Updated', $this->data['winners_module']['id'], $this->data['winners_module']['name'],$winner_id,0, 0, 0, 0, 0,'Changed winner:'.$winner_id.'');
					redirect('admin/site_manager/winners_manager');
				}  
			}
    	}    

		/**
		 * about us manager
		 */
		public function about_us_manager(){
			access_checker($this->data['about_permission']['view'],0,0,0,0,'admin/dashboard'); 
			$data['what_is_readysteadyshare'] = $this->Site_manager_model->get_site_metaData('about_us_what_is_readysteadyshare','single');
			$data['about_us'] = $this->Site_manager_model->get_site_metaData('about_us_rss','single');
			$data['about_us_competition_description'] = $this->Site_manager_model->get_site_metaData('about_us_competition_description','single');
			$data['about_us_design'] = $this->Site_manager_model->get_site_metaData('about_us_design','single');
			$data['about_us_coding'] = $this->Site_manager_model->get_site_metaData('about_us_coding','single');
			$data['about_us_voting'] = $this->Site_manager_model->get_site_metaData('about_us_voting','single');
			$data['social_media']	= $this->Site_manager_model->get_site_metaData('social_data' ,'multi');
			$data['contact']		= $this->Site_manager_model->get_site_metaData('call_us' ,'single');
			$data['about_us']		= $this->Site_manager_model->get_site_metaData('about_us' ,'single');
			$data['location']		= $this->Site_manager_model->get_site_metaData('location' ,'single');
			$data['email']		= $this->Site_manager_model->get_site_metaData('email_us' ,'single');
			$data['view']          = 'admin/site_manager/about_manager';
			$this->load->view('admin/includes/layout',$data);
		}    

		public function edit_about_us(){
			access_checker(0,0,$this->data['about_permission']['edit'],0,0,'');
			if ($this->input->post()) {
				$about_id =  $this->input->post('id');
				$fdata = [
					'header' => $this->input->post('header'),
					'paragraph' => $this->input->post('paragraph'),
				];
				$this->Site_manager_model->update_about_us($fdata, $about_id);
				learn_translate($this->input->post('header'));
				learn_translate($this->input->post('paragraph'));
				$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
				loger('Updated', $this->data['about_module']['id'], $this->data['about_module']['name'],$about_id,0, 0, 0, 0, 0,'Changed winner:'.$about_id.'');
				redirect('admin/site_manager/about_us_manager');

			}
    	}

		public function get_rowDataAjax($model,$func){
			$row_id = $this->input->post('search');
			$row = $this->$model->$func($row_id);	
			echo json_encode($row);
			exit();
		} 
 
		

	    // /**
		//  * Slider manager
		//  */
		// public function slider_manager(){
		// 	access_checker($this->data['slider_permission']['view'],0,0,0,0,'admin/dashboard');
		// 	$data['sliders'] = $this->Site_manager_model->get_all_sliders();
		// 	$data['view'] = 'admin/site_manager/slider_manager';
		// 	$this->load->view('admin/includes/layout',$data);
		// }    

		// public function edit_slider(){
		// 	access_checker(0,0,$this->data['slider_permission']['edit'],0,0,'');
		// 	if ($this->input->post()) {
		// 		$file_name   = do_upload("image",'slider','site_assets/images');
		// 		$about_id =  $this->input->post('id');
		// 		$fdata = [
		// 			'first_title' => $this->input->post('first_title'),
		// 			'second_title' => $this->input->post('second_title'),
		// 		];
		// 		if( $file_name) $fdata['img'] = $file_name;
		// 		$this->Site_manager_model->update_slider($fdata, $about_id);
		// 		$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
		// 		loger('Updated', $this->data['slider_module']['id'], $this->data['slider_module']['name'],$about_id,0, 0, 0, 0, 0,'Changed winner:'.$about_id.'');
		// 		redirect('admin/site_manager/slider_manager');

		// 	}
    	// }

      	// public function delete_info(){
	  	// 	access_checker(0,0,0,$this->data['competitions_permission']['remove'],0,'');
      	// 	if ($this->input->post('id')) {
	    //       	$id = $this->input->post('id');
	    //       	$item = $this->Site_manager_model->get_info_item($id);
	    //       	$this->db->delete('boat_info',array('id' => $item['id']));	
	    //       	loger('Delete', $this->data['competitions_module']['id'], $this->data['competitions_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted Info:'.$item['id'].'');
        //   		echo json_encode(true);
        //   		exit();
        // 	}  
      	// }  

// ------------------------------ slider manager ---------------------------------
    	public function slider_manager(){
      		access_checker(0,0,$this->data['slider_permission']['edit'],0,0,'admin/site_manager');
      		$data['sliders']   = $this->Site_manager_model->get_all_sliders();
        	if($this->input->post('items')){
          		foreach ($this->input->post('items') as $key => $item) {
            		if (isset($item['id'])) {
		                $item_data   = $this->Site_manager_model->get_slider_item($item['id']);
		                $file_name   = do_upload("items-".$key."-img",'slider','site_assets/images');
		                $item['img'] = $file_name; 
                		if ($item['img'] !=null) {
                			//die(print_r('exist'));
	                		if (($item['old_img'] != 0 )&&($item['img'] != $item['old_img'])) {
	                			if(file_exists("site_assets/images/slider/".$item['old_img'])){
	                	    		unlink("site_assets/images/slider/".$item['old_img']);
	                     		}
	                		}
                		}else{
                 			unset($item['img']);	
                		} 
                		unset($item['old_img']);
                		$file_name1   = do_upload("items-".$key."-thumb",'thumbs','site_assets/images/slider');
		                $item['thumb'] = $file_name1; 
                		if ($item['thumb'] !=null) {
                			//die(print_r('exist'));
	                		if (($item['old_thumb'] != 0 )&&($item['thumb'] != $item['old_thumb'])) {
	                			if(file_exists("site_assets/images/slider/thumbs/".$item['old_thumb'])){
	                	    		unlink("site_assets/images/slider/thumbs/".$item['old_thumb']);
	                     		}
	                		}
                		}else{
                 			unset($item['thumb']);	
                		} 
                		unset($item['old_thumb']);
                		$file_name2   = do_upload("items-".$key."-video",'slider','site_assets/images');
		                $item['video'] = $file_name2; 
                		if ($item['video'] !=null) {
                			//die(print_r('exist'));
	                		if (($item['old_video'] != 0 )&&($item['video'] != $item['old_video'])) {
	                			if(file_exists("site_assets/images/slider/".$item['old_video'])){
	                	    		unlink("site_assets/images/slider/".$item['old_video']);
	                     		}
	                		}
                		}else{
                 			unset($item['video']);	
                		} 
                		unset($item['old_video']);
                		$updated     = $this->Site_manager_model->update_slider($item,$item['id']);
                		if ($updated > 0) {
                    		loger('update',$this->data['slider_module']['id'],$this->data['slider_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated slider: '.$item_data['id']);
                		}
              		}
	            }
		    	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
		    	redirect('admin/site_manager/slider_manager');
        	}
      		$data['view'] = 'admin/site_manager/slider_manager';
      		$this->load->view('admin/includes/layout',$data);       
    	}

    	public function add_slider(){
		  	$fdata        =[
	          	'first_title'   => 'Enter Title',
	          	'second_title'  => 'Enter Text',
	          	'rank'         	=> 0,
	        ];
	    	$item_id = $this->Site_manager_model->add_slider($fdata);
	        if ($item_id) {
	            loger('Create',$this->data['slider_module']['id'],$this->data['slider_module']['name'],$item_id, 0, 0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'added slider: '.$item_id);
			    redirect('admin/site_manager/slider_manager');
	      	}  
    	}      

      	// public function delete_slider(){
	  	// 	access_checker(0,0,0,$this->data['slider_permission']['remove'],0,'');
      	// 	if ($this->input->post('id')) {
	    //       	$id = $this->input->post('id');
	    //       	$item = $this->Site_manager_model->get_slider_item($id);
	    //       	$this->db->delete('slider',array('id' => $item['id']));	
	    //       	loger('Delete', $this->data['slider_module']['id'], $this->data['slider_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted slider:'.$item['id'].'');
        //   		echo json_encode(true);
        //   		exit();
        // 	}  
      	// }  

// ------------------------------ package manager ---------------------------------
    	// public function package_manager(){
      	// 	access_checker(0,0,$this->data['package_permission']['edit'],0,0,'admin/site_manager');
      	// 	$data['packages']   = $this->Site_manager_model->get_all_packages();
        // 	if($this->input->post('items')){
        //   		foreach ($this->input->post('items') as $key => $item) {
        //     		if (isset($item['id'])) {
		//                 $item_data   = $this->Site_manager_model->get_package_item($item['id']);
		//                 $file_name   = do_upload("items-".$key."-image",'4','site_assets/images/portfolio');
		//                 $item['image'] = $file_name; 
        //         		if ($item['image'] !=null) {
        //         			//die(print_r('exist'));
	    //             		if (($item['old_image'] != 0 )&&($item['image'] != $item['old_image'])) {
	    //             			if(file_exists("site_assets/images/portfolio/4/".$item['old_image'])){
	    //             	    		unlink("site_assets/images/portfolio/4/".$item['old_image']);
	    //                  		}
	    //             		}
        //         		}else{
        //          			unset($item['image']);	
        //         		} 
        //         		unset($item['old_image']);
        //         		$updated     = $this->Site_manager_model->update_package($item,$item['id']);
        //         		if ($updated > 0) {
        //             		loger('update',$this->data['package_module']['id'],$this->data['package_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated Package: '.$item_data['id']);
        //         		}
        //       		}
	    //         }
		//     	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
		//     	redirect('admin/site_manager/package_manager');
        // 	}
      	// 	$data['view'] = 'admin/site_manager/package_manager';
      	// 	$this->load->view('admin/includes/layout',$data);       
    	// }

    	// public function add_package(){
		//   	$fdata        =[
	    //       	'title'   	=> 'Enter Title',
	    //       	'remarks'  	=> 'Enter Text',
	    //       	'rank'         	=> 0,
	    //     ];
	    // 	$item_id = $this->Site_manager_model->add_package($fdata);
	    //     if ($item_id) {
	    //         loger('Create',$this->data['package_module']['id'],$this->data['package_module']['name'],$item_id, 0, 0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'added Package: '.$item_id);
		// 	    redirect('admin/site_manager/package_manager');
	    //   	}  
    	// }    

      	// public function delete_package(){
	  	// 	access_checker(0,0,0,$this->data['package_permission']['remove'],0,'');
      	// 	if ($this->input->post('id')) {
	    //       	$id = $this->input->post('id');
	    //       	$item = $this->Site_manager_model->get_package_item($id);
	    //       	$this->db->delete('packages',array('id' => $item['id']));	
	    //       	loger('Delete', $this->data['package_module']['id'], $this->data['package_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted Package:'.$item['id'].'');
        //   		echo json_encode(true);
        //   		exit();
        // 	}  
      	// }  
// ------------------------------ gallary manager ---------------------------------
    	public function gallary_manager(){
      		access_checker(0,0,$this->data['gallary_permission']['edit'],0,0,'admin/site_manager');
      		$data['gallarys']   = $this->Site_manager_model->get_all_gallarys();
        	if($this->input->post('items')){
          		foreach ($this->input->post('items') as $key => $item) {
            		if (isset($item['id'])) {
		                $item_data   = $this->Site_manager_model->get_gallary_item($item['id']);
		                $file_name   = do_upload("items-".$key."-img",'full','site_assets/images/portfolio');
		                $item['img'] = $file_name; 
                		if ($item['img'] !=null) {
                			//die(print_r('exist'));
	                		if (($item['old_img'] != 0 )&&($item['img'] != $item['old_img'])) {
	                			if(file_exists("site_assets/images/portfolio/full/".$item['old_img'])){
	                	    		unlink("site_assets/images/portfolio/full/".$item['old_img']);
	                     		}
	                		}
                		}else{
                 			unset($item['img']);	
                		} 
                		unset($item['old_img']);
                		$item['thumb'] = $file_name; 
                		if ($item['thumb'] !=null) {
                			//die(print_r('exist'));
	                		if (($item['old_thumb'] != 0 )&&($item['thumb'] != $item['old_thumb'])) {
	                			if(file_exists("site_assets/images/portfolio/4/".$item['old_thumb'])){
	                	    		unlink("site_assets/images/portfolio/4/".$item['old_thumb']);
	                     		}
	                		}
                		}else{
                 			unset($item['thumb']);	
                		} 
                		unset($item['old_thumb']);
                		$updated     = $this->Site_manager_model->update_gallary($item,$item['id']);
                		if ($updated > 0) {
                    		loger('update',$this->data['gallary_module']['id'],$this->data['gallary_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated Gallary: '.$item_data['id']);
                		}
              		}
	            }
		    	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
		    	redirect('admin/site_manager/gallary_manager');
        	}
      		$data['view'] = 'admin/site_manager/gallary_manager';
      		$this->load->view('admin/includes/layout',$data);       
    	}

    	public function add_gallary(){
		  	$fdata        =[
	          	'title'   	=> 'Enter Title',
	          	'rank'         	=> 0,
	        ];
	    	$item_id = $this->Site_manager_model->add_gallary($fdata);
	        if ($item_id) {
	            loger('Create',$this->data['gallary_module']['id'],$this->data['gallary_module']['name'],$item_id, 0, 0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'added Gallary: '.$item_id);
			    redirect('admin/site_manager/gallary_manager');
	      	}  
    	}    

      	public function delete_gallary(){
	  		access_checker(0,0,0,$this->data['gallary_permission']['remove'],0,'');
      		if ($this->input->post('id')) {
	          	$id = $this->input->post('id');
	          	$item = $this->Site_manager_model->get_gallary_item($id);
	          	$this->db->delete('gallary',array('id' => $item['id']));	
	          	loger('Delete', $this->data['gallary_module']['id'], $this->data['gallary_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted Gallary:'.$item['id'].'');
          		echo json_encode(true);
          		exit();
        	}  
      	}  
// ------------------------------ event manager ---------------------------------
    	public function event_manager(){
      		access_checker(0,0,$this->data['event_permission']['edit'],0,0,'admin/site_manager');
      		$data['events']   = $this->Site_manager_model->get_all_events();
        	if($this->input->post('items')){
          		foreach ($this->input->post('items') as $key => $item) {
            		if (isset($item['id'])) {
		                $item_data   = $this->Site_manager_model->get_event_item($item['id']);
		                $file_name   = do_upload("items-".$key."-image",'images','site_assets');
		                $item['image'] = $file_name; 
                		if ($item['image'] !=null) {
                			//die(print_r('exist'));
	                		if (($item['old_img'] != 0 )&&($item['image'] != $item['old_image'])) {
	                			if(file_exists("site_assets/images/".$item['old_image'])){
	                	    		unlink("site_assets/images/".$item['old_image']);
	                     		}
	                		}
                		}else{
                 			unset($item['image']);	
                		} 
                		unset($item['old_image']);
                		$updated     = $this->Site_manager_model->update_event($item,$item['id']);
                		if ($updated > 0) {
                    		loger('update',$this->data['event_module']['id'],$this->data['event_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated Event: '.$item_data['id']);
                		}
              		}
	            }
		    	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
		    	redirect('admin/site_manager/event_manager');
        	}
      		$data['view'] = 'admin/site_manager/event_manager';
      		$this->load->view('admin/includes/layout',$data);       
    	}

    	public function add_event(){
		  	$fdata   =[
	          	'location'   	=> 'Enter Location',
	          	'describtion'  	=> 'Enter Describtion',
	          	'rank'         	=> 0,
	        ];
	    	$item_id = $this->Site_manager_model->add_event($fdata);
	        if ($item_id) {
	            loger('Create',$this->data['event_module']['id'],$this->data['event_module']['name'],$item_id, 0, 0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'added Event: '.$item_id);
			    redirect('admin/site_manager/event_manager');
	      	}  
    	}      

      	public function delete_event(){
	  		access_checker(0,0,0,$this->data['event_permission']['remove'],0,'');
      		if ($this->input->post('id')) {
	          	$id = $this->input->post('id');
	          	$item = $this->Site_manager_model->get_event_item($id);
	          	$this->db->delete('events',array('id' => $item['id']));	
	          	loger('Delete', $this->data['event_module']['id'], $this->data['event_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted Event:'.$item['id'].'');
          		echo json_encode(true);
          		exit();
        	}  
      	} 
// ------------------------------ About manager ---------------------------------
    	public function about_manager(){
      		access_checker(0,0,$this->data['about_permission']['edit'],0,0,'admin/site_manager');
      		if(!$this->input->post('submit')){
	        	$data['about']       = $this->Site_manager_model->get_site_metaData('about','single');
				$arrayFilter         = json_decode($data['about']['points'],true);
				for ($i = 1; $i < sizeof($arrayFilter); $i++) {
			    	for ($j=$i+1; $j < sizeof($arrayFilter); $j++) {
			        	if ($arrayFilter[$i]['rank'] > $arrayFilter[$j]['rank']) {
			            	$c = $arrayFilter[$i];
			            	$arrayFilter[$i] = $arrayFilter[$j];
			            	$arrayFilter[$j] = $c;
			        	}
			    	}
				}
				$data['points'] = $arrayFilter;
	    	}
        	if($this->input->post('submit')){
	        	$file_name   = do_upload("img",'about','site_assets/images');
            learn_translate($this->input->post('header'));
            learn_translate($this->input->post('title'));
            learn_translate($this->input->post('paragraph'));
	        	$fdata       = array(
					'header'    => $this->input->post('header'),
					'title'     => $this->input->post('title'),
					'paragraph' => $this->input->post('paragraph'),
					'points'    => json_encode($this->input->post('items'), JSON_UNESCAPED_UNICODE),
					'img'       => $file_name,
				);
        		if ($fdata['img'] !=null) {
                	if (($this->input->post('old_img') != 0 )&&($fdata['img'] != $this->input->post('old_img'))) {
                		if(file_exists("site_assets/images/about/".$this->input->post('old_img'))){
                	    	unlink("site_assets/images/about/".$this->input->post('old_img'));
                     	}
                	}
             	}else{
                	unset($fdata['img']);	
            	} 
	            $item_data   = $this->Site_manager_model->get_site_metaData('about','single');
	            $updated     = $this->Site_manager_model->update_about($fdata);
                if ($updated > 0) {
                    loger('update',$this->data['about_module']['id'],$this->data['about_module']['name'],0,0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'updated about data');
                }
			    $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
			    redirect('admin/site_manager/about_manager');
        	}
	      	$data['view'] = 'admin/site_manager/about_manager';
	      	$this->load->view('admin/includes/layout',$data);       
    	}   

// ------------------------------ Services manager ---------------------------------
    	public function service_manager(){
       		access_checker(0,0,$this->data['services_permission']['edit'],0,0,'admin/site_manager');
       		$data['services']         = $this->Site_manager_model->get_site_metaData('services','multi');
       		$data['font_icons']       = $this->General_model->get_font_icons();
        	if($this->input->post('items')){
          		foreach ($this->input->post('items') as $key => $item) {
            		if (isset($item['id'])) {
                		$item_data     = $this->Site_manager_model->get_site_metaData_item($item['id'],'services');
                		$updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
                		if ($updated > 0) {
                    		loger('update',$this->data['services_module']['id'],$this->data['services_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated service: '.$item_data['title']);
                		}
              		}else{
                		$item_id = $this->Site_manager_model->add_site_metaData_item($item);
               			if ($item_id) {
                			loger('Create', $this->data['services_module']['id'], $this->data['services_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added service:'.$item['title'].'');
                 		}
              		}
            	}
	    		$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
	    		redirect('admin/site_manager/service_manager');
        	}
      		$data['view'] = 'admin/site_manager/services_manager';
      		$this->load->view('admin/includes/layout',$data);       
    	}

		public function delete_service(){
	  		access_checker(0,0,0,$this->data['services_permission']['remove'],0,'');
      		if ($this->input->post('id')) {
          		$id = $this->input->post('id');
          		$item = $this->Site_manager_model->get_site_metaData_item($id,'services');
          		$this->db->delete('site_meta_data',array('id' => $item['id'],'meta'=>'services'));	
          		loger('Delete', $this->data['services_module']['id'], $this->data['services_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted slider:'.$item['title'].'');
          		echo json_encode(true);
          		exit();
        	}  
      	}   
// ------------------------------ Offers manager ---------------------------------
    	public function offer_manager(){
       		access_checker(0,0,$this->data['offer_permission']['edit'],0,0,'admin/site_manager');
       		$data['offers']         = $this->Site_manager_model->get_offers();
       		$data['activity']       = $this->Site_manager_model->get_activities();
       		$data['hotels']         = $this->Site_manager_model->get_hotels();
       		$data['sub_activity']   = $this->Site_manager_model->get_sub_activities();
        	if($this->input->post('items')){
          		foreach ($this->input->post('items') as $key => $item) {
                learn_translate($item['name']);
                learn_translate($item['data']);
            		if (isset($item['id'])) {
                		$item_data     = $this->Site_manager_model->get_offer($item['id']);
                		$file_name            = do_upload('items-'.$key.'-image', 'offer','site_assets/images');
                		if ($file_name) {
	            			$item['image']       = $file_name;
                		}
                		$updated       = $this->Site_manager_model->update_offer($item,$item['id']);
                		if ($updated > 0) {
                    		loger('update',$this->data['offers_module']['id'],$this->data['offers_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated offer: '.$item_data['name']);
                		}
              		}else{
              			$file_name            = do_upload('items-'.$key.'-image', 'offer','site_assets/images');
            			$item['image']       = $file_name;
                		$item_id = $this->Site_manager_model->add_offer($item);
               			if ($item_id) {
                			loger('Create', $this->data['offers_module']['id'], $this->data['offers_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added offer:'.$item['name'].'');
                 		}
              		}
            	}
	    		$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
	    		redirect('admin/site_manager/offer_manager');
        	}
      		$data['view'] = 'admin/site_manager/offers_manager';
      		$this->load->view('admin/includes/layout',$data);       
    	}

		public function delete_offer(){
	  		access_checker(0,0,0,$this->data['offer_permission']['remove'],0,'');
      		if ($this->input->post('id')) {
          		$id = $this->input->post('id');
          		$item = $this->Site_manager_model->get_offer($id);
          		$this->db->delete('offers',array('id' => $item['id']));	
          		loger('Delete', $this->data['offers_module']['id'], $this->data['offers_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted offer:'.$item['name'].'');
          		echo json_encode(true);
          		exit();
        	}  
      	}   

// ------------------------------ portfolio manager ---------------------------------
    	public function portfolio_manager(){
      		access_checker(0,0,$this->data['portfolio_permission']['edit'],0,0,'admin/site_manager');
      		$data['portfolios']   = $this->Site_manager_model->get_site_metaData('portfolio','multi');
        	if($this->input->post('items')){
          		foreach ($this->input->post('items') as $key => $item) {
            		if (isset($item['id'])) {
		                $item_data   = $this->Site_manager_model->get_site_metaData_item($item['id'],'portfolio');
		                $file_name   = do_upload("items-".$key."-img",'portfolio','site_assets/images');
		                $item['img'] = $file_name; 
                		if ($item['img'] !=null) {
	                		if (($item['old_img'] != 0 )&&($item['img'] != $item['old_img'])) {
	                			if(file_exists("site_assets/images/portfolio/".$item['old_img'])){
	                	    		unlink("site_assets/images/portfolio/".$item['old_img']);
	                     		}
	                		}
                		}else{
                 			unset($item['img']);	
                		} 
		                unset($item['old_img']);
		                $updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
		                if ($updated > 0) {
                    		loger('update',$this->data['portfolio_module']['id'],$this->data['portfolio_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated portfolio: '.$item_data['id']);
                		}
              		}else{
		              	$file_name   = do_upload("items-".$key."-img",'portfolio','site_assets/images');
		                $item['img'] = $file_name;  
		                $item_id = $this->Site_manager_model->add_site_metaData_item($item);
               			if ($item_id) {
                			loger('Create', $this->data['portfolio_module']['id'], $this->data['portfolio_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added portfolio:'.$item['id'].'');
                 		}
              		}
            	}
	    		$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
	    		redirect('admin/site_manager/portfolio_manager');
        	}
      		$data['view'] = 'admin/site_manager/portfolio_manager';
      		$this->load->view('admin/includes/layout',$data);       
    	}

		public function delete_portfolio(){
	  		access_checker(0,0,0,$this->data['portfolio_permission']['remove'],0,'');
      		if ($this->input->post('id')) {
          		$id = $this->input->post('id');
          		$item = $this->Site_manager_model->get_site_metaData_item($id,'portfolio');
         		$this->db->delete('site_meta_data',array('id' => $item['id'],'meta'=>'portfolio'));	
          		loger('Delete', $this->data['portfolio_module']['id'], $this->data['portfolio_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted portfolio:'.$item['id'].'');
          		echo json_encode(true);
          		exit();
        	}  
      	}   

// ------------------------------ gallery manager ---------------------------------
    public function gallery_manager(){
      access_checker(0,0,$this->data['gallery_permission']['edit'],0,0,'admin/site_manager');
      $data['gallerys']   = $this->Site_manager_model->get_site_metaData('gallery','multi');
        if($this->input->post('items')){
          foreach ($this->input->post('items') as $key => $item) {
            if (isset($item['id'])) {
                $item_data   = $this->Site_manager_model->get_site_metaData_item($item['id'],'gallery');
                $file_name   = do_upload("items-".$key."-img",'gallery','site_assets/images');
                $item['img'] = $file_name; 
                if ($item['img'] !=null) {
                  //die(print_r('exist'));
                  if (($item['old_img'] != 0 )&&($item['img'] != $item['old_img'])) {
                    if(file_exists("site_assets/images/gallery/".$item['old_img'])){
                        unlink("site_assets/images/gallery/".$item['old_img']);
                       }
                  }
                }else{
                 unset($item['img']); 
                } 
                unset($item['old_img']);
                $item['points'] = str_replace(' ', '_',$item['points']);
                $updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
                if ($updated > 0) {
                    loger('update',$this->data['gallery_module']['id'],$this->data['gallery_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated gallery: '.$item_data['id']);
                }
              }else{
                $file_name   = do_upload("items-".$key."-img",'gallery','site_assets/images');
                $item['img'] = $file_name;  
                $item_id = $this->Site_manager_model->add_site_metaData_item($item);
               if ($item_id) {
                loger('Create', $this->data['gallery_module']['id'], $this->data['gallery_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added gallery:'.$item['id'].'');
                 }
              }
            }
      $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
      redirect('admin/site_manager/gallery_manager');
        }
      $data['view'] = 'admin/site_manager/gallery_manager';
      $this->load->view('admin/includes/layout',$data);       
    }

    // public function add_gallery($id, $type, $key){
    //   access_checker(0,0,0,$this->data['gallery_permission']['edit'],0,'');
    //   if($type == 'new'){
    //     $file_name   = do_upload("items-".$key."-img",'gallery','site_assets/images');
    //     $fdata       = array(
    //       'header'    => $this->input->post('header'),
    //       'title'     => $this->input->post('title'),
    //       'points'    => $this->input->post('points'),
    //       'img'       => $file_name,
    //     );
    //     $item_id = $this->Site_manager_model->add_site_metaData_item($fdata);
    //     if ($item_id) {
    //       loger('Create', $this->data['gallery_module']['id'], $this->data['gallery_module']['name'], $item_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0,'added gallery:'.$item_id.'');
    //     }
    //   }else{
    //     $item_data   = $this->Site_manager_model->get_site_metaData_item($id,'gallery');
    //     $file_name   = do_upload("items-".$key."-img",'gallery','site_assets/images');
    //     $fdata       = array(
    //       'header'    => $this->input->post('header'),
    //       'title'     => $this->input->post('title'),
    //       'points'    => $this->input->post('points'),
    //       'img'       => $file_name,
    //     );
    //     if (!$fdata['img']) {
    //       unset($fdata['img']); 
    //     }
    //     $updated     = $this->Site_manager_model->update_site_metaData_item($fdata,$id);
    //     if ($updated > 0) {
    //       loger('update',$this->data['gallery_module']['id'],$this->data['gallery_module']['name'],$id,0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'updated gallery: '.$id);
    //     }
    //   }
    //   redirect('admin/site_manager/gallery_manager');
    // } 

  public function delete_gallery(){
    access_checker(0,0,0,$this->data['gallery_permission']['remove'],0,'');
      if ($this->input->post('id')) {
          $id = $this->input->post('id');
          $item = $this->Site_manager_model->get_site_metaData_item($id,'gallery');
         $this->db->delete('site_meta_data',array('id' => $item['id'],'meta'=>'gallery'));  
          loger('Delete', $this->data['gallery_module']['id'], $this->data['gallery_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted gallery:'.$item['id'].'');
          echo json_encode(true);
          exit();
        }  
      } 

// ------------------------------ team manager ---------------------------------
    public function team_manager(){
      access_checker(0,0,$this->data['team_permission']['edit'],0,0,'admin/site_manager');
      $data['teams']   = $this->Site_manager_model->get_site_metaData('team','multi');
        if($this->input->post('items')){
          foreach ($this->input->post('items') as $key => $item) {
            if (isset($item['id'])) {
                $item_data   = $this->Site_manager_model->get_site_metaData_item($item['id'],'team');
                $file_name   = do_upload("items-".$key."-img",'team','site_assets/images');
                $item['img'] = $file_name; 
                if ($item['img'] !=null) {
                  if (($item['old_img'] != 0 )&&($item['img'] != $item['old_img'])) {
                    if(file_exists("site_assets/images/team/".$item['old_img'])){
                        unlink("site_assets/images/team/".$item['old_img']);
                       }
                  }
                }else{
                 unset($item['img']); 
                } 
                unset($item['old_img']);
                $updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
                if ($updated > 0) {
                    loger('update',$this->data['team_module']['id'],$this->data['team_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated team: '.$item_data['id']);
                }
              }else{
                $file_name   = do_upload("items-".$key."-img",'team','site_assets/images');
                $item['img'] = $file_name;  
                $item_id = $this->Site_manager_model->add_site_metaData_item($item);
               if ($item_id) {
                loger('Create', $this->data['team_module']['id'], $this->data['team_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added team:'.$item['id'].'');
                 }
              }
            }
      $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
      redirect('admin/site_manager/team_manager');
        }
      $data['view'] = 'admin/site_manager/team_manager';
      $this->load->view('admin/includes/layout',$data);       
    }

  public function delete_team(){
    access_checker(0,0,0,$this->data['team_permission']['remove'],0,'');
      if ($this->input->post('id')) {
          $id = $this->input->post('id');
          $item = $this->Site_manager_model->get_site_metaData_item($id,'team');
         $this->db->delete('site_meta_data',array('id' => $item['id'],'meta'=>'team'));  
          loger('Delete', $this->data['team_module']['id'], $this->data['team_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted team:'.$item['id'].'');
          echo json_encode(true);
          exit();
        }  
      }   

// ------------------------------ contact manager ---------------------------------
    public function contact_manager(){
       access_checker(0,0,$this->data['contact_permission']['edit'],0,0,'admin/site_manager');
       $data['contacts']         = $this->Site_manager_model->get_site_metaData('contact_us','multi');
        if($this->input->post('items')){
          foreach ($this->input->post('items') as $key => $item) {
            if (isset($item['id'])) {
                $item_data     = $this->Site_manager_model->get_site_metaData_item($item['id'],'contact_us');
                $updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
                if ($updated > 0) {
                    loger('update',$this->data['contact_module']['id'],$this->data['contact_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated contact: '.$item_data['header']);
                }
              }
            }
	    $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
	    redirect('admin/site_manager/contact_manager');
        }
      $data['view'] = 'admin/site_manager/contact_manager';
      $this->load->view('admin/includes/layout',$data);       
    }  

    public function blank_translate(){
      learn_translate(' ');
      redirect('admin/site_manager/lang_manager');
    }

// ------------------------------ footer manager ---------------------------------
    public function footer_manager(){
      access_checker(0,0,$this->data['footer_permission']['edit'],0,0,'admin/site_manager');
      if(!$this->input->post('submit')){
	      $data['footer_about']          = $this->Site_manager_model->get_site_metaData('footer_about','single');
          $data['footer_info']           = $this->Site_manager_model->get_site_metaData('footer_info','single');
          $data['footer_tel']            = $this->Site_manager_model->get_site_metaData('footer_tel','single');
          $data['footer_email']          = $this->Site_manager_model->get_site_metaData('footer_email','single');
          $data['footer_working']        = $this->Site_manager_model->get_site_metaData('footer_working','single');
	    }
        if($this->input->post('items')){
          foreach ($this->input->post('items') as $key => $item) {
            if (isset($item['header'])) {
              learn_translate($item['header']);
            }
            if (isset($item['title'])) {
              learn_translate($item['title']);
            }
            if ($item['paragraph']) {
              learn_translate($item['paragraph']);
            }
            if (isset($item['id'])) {
                $item_data   = $this->Site_manager_model->get_site_metaData_item($item['id'],$item['meta']);
                $updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
                if ($updated > 0) {
                    loger('update',$this->data['footer_module']['id'],$this->data['footer_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated footer data: '.$item_data['id']);
                }
              }
            }
	    $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
	    redirect('admin/site_manager/footer_manager');
        }
      $data['view'] = 'admin/site_manager/footer_manager';
      $this->load->view('admin/includes/layout',$data);       
    }  

//--------------------------------------- Categories and products -----------------------------------------

    public function category_manager(){
      access_checker(0,0,$this->data['products_permission']['edit'],0,0,'admin/site_manager');
      if(!$this->input->post('submit')){
	      $data['categories']          = $this->Site_manager_model->get_categories();
	    }
        if($this->input->post()){
        	$file_name   = do_upload("img",'products','site_assets/images');
        	$serial     = strtoupper(str_pad(dechex( mt_rand( 2, 1648575 ) ), 6, '0', STR_PAD_LEFT)); 
        	$fdata      = array(
								  'group_name'    => $this->input->post('group_name'),
								  'serial'        => $serial,
								  'img'           => $file_name,
								  'rank'          => $this->input->post('rank'),
								  );
        	if ($fdata['img'] !=null) {
                if (($this->input->post('old_img') != 0 )&&($fdata['img'] != $this->input->post('old_img'))) {
                	if(file_exists("site_assets/images/products/".$this->input->post('old_img'))){
                	    unlink("site_assets/images/products/".$this->input->post('old_img'));
                     }
                }
             }else{
                unset($fdata['img']);	
            } 
        	if ($this->input->post('id')) {
        		    unset($item['serial']);
	                $item_data   = $this->Site_manager_model->get_category($this->input->post('id'));
	                $updated     = $this->Site_manager_model->update_category($fdata,$this->input->post('id'));
	                if ($updated > 0) {
	                    loger('update',$this->data['products_module']['id'],$this->data['products_module']['name'],$item_data['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'updated category: '.$item_data['id']);
	                }
              }else{
              	$file_name   = do_upload("items-".$key."-img",'products','site_assets/images');
                $item['img'] = $file_name;  
                $item_id = $this->Site_manager_model->add_category($fdata);
               if ($item_id) {
                loger('Create', $this->data['products_module']['id'], $this->data['products_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added category:'.$item_id.'');
                 }
              }
		    $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
		    redirect('admin/site_manager/category_manager');
	     }
      $data['view'] = 'admin/site_manager/category_manager';
      $this->load->view('admin/includes/layout',$data);       
    }  

    public function delete_category(){
	  access_checker(0,0,0,$this->data['products_permission']['remove'],0,'');
      if ($this->input->post('id')) {
          $id = $this->input->post('id');
          $item = $this->Site_manager_model->get_category($id);
          $this->db->update('site_groups', array('deleted' => 1), "id = ".$item['id']);	
          loger('Delete', $this->data['products_module']['id'], $this->data['products_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted category:'.$item['id'].'');
          echo json_encode(true);
          exit();
        }  
      } 

//-----------------------------------products manager

    public function products_index($gid){
      access_checker($this->data['products_permission']['view'],0,0,0,0,'admin/dashboard');	
      $data['category']         = $this->Site_manager_model->get_category($gid);
  		$data['view']          = 'admin/site_manager/products_index';
  		$this->load->view('admin/includes/layout',$data);

	  }  


	 public function products_ajax($gid){
	      $dt_att  = $this->datatables_att();
	      $items   = $this->Site_manager_model->get_products($gid,$dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

	      $data = array();
	      $i    =  1;
	       foreach($items as $item) {
	           $arr = array(); 
	           $tools=array();
	            if ($this->data['permission']['remove'] == 1) {
	            	if ($item['deleted'] ==0) {
	                     $tools[] = '<div class="info"><a href="javascript: void(0);" onclick="del('.$item['id'].',\''.$item['item_name'].'\',\'items\',\'admin/site_manager\',\'delete_product\')" titel="Delete item">
			                          <span style="color:red;">Delete</span>
			                     </a> </div> ';
			           }else{
	                 	 $tools[] = '<a href="javascript: void(0);" onclick="reOpen('.$item['id'].',\''.$item['item_name'].'\',\'items\',\'admin/site_manager\',\'reopen_product\')" titel="Delete item">
	                          <span style="color:blue;">Reopen</span>
	                     </a> </div> ';
			                 }
	                  }

	                $arr[] = '<span>'.$i.'.</span>';
	                $arr[] = '<a  href="'.base_url('admin/site_manager/product/'. $item['id']).'" titel="View Item"><span>'.$item['item_name'].'</span></a><br>'.implode("", $tools);
	                $arr[] = '<a  href="'.base_url('admin/site_manager/product/'. $item['id']).'" titel="View Item"><span>'.$item['serial'].'</span></a><br>';
	                $arr[] = '<span>'.$item['description'].'</span>'; 
	                $arr[] = '<span>'.$item['company'].'</span>';
	               
	                $data[] =$arr;
	                $i++;
	            }
	     
	           $output = array(
	                 "draw" => $dt_att['draw'],
	                 "recordsTotal"    => count($this->Site_manager_model->get_all_products($gid)),
	                 "recordsFiltered" => $this->Site_manager_model->get_products($gid,$dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	                 "data" => $data
	            );
	      echo json_encode($output);
	      exit();
	  
	     }


        public function product_process(){
        	$img_name    = do_upload("img",'products/product','site_assets/images');
        	$file_name   = do_upload("file",'products/product','site_assets/images');
        	$serial = strtoupper(str_pad(dechex( mt_rand( 0, 1048575 ) ), 7, '0', STR_PAD_LEFT)); 
    			$data = [
    			         'group_id'             => $this->input->post('group_id'),
    			         'item_name'            => $this->input->post('item_name'),
    			         'serial'               => $serial,
    			         'company'              => $this->input->post('company'),
    			         'rank'                 => $this->input->post('rank'),
    			         'description'          => $this->input->post('description'),
    			         'img'                  => $img_name,
    			         'file'                 => $file_name,
    			        ];

				if ($data['img'] !=null || $data['file'] !=null) {
	                if (($this->input->post('old_img') != 0 &&$data['img'] != $this->input->post('old_img')) || ($this->input->post('old_file') != 0 &&$data['file'] != $this->input->post('old_file'))) {
	                	if(file_exists("site_assets/images/products/product/".$this->input->post('old_img'))){
	                	    unlink("site_assets/images/products/product/".$this->input->post('old_img'));
	                     }
	                    if(file_exists("site_assets/images/products/product/".$this->input->post('old_file'))){
	                	    unlink("site_assets/images/products/product/".$this->input->post('old_file'));
	                     }
	                }
	             }else{
	                unset($data['img']);
	                unset($data['file']);	
	            } 

				if (!$this->input->post('id')) {
					$item   = $this->Site_manager_model->get_product_by($serial); 
			            if ($item) {
			            	$this->session->set_flashdata(['alert'=>'Warning','msg'=>'there is an item with the same serial, so please insert it once again!']);
			            	redirect('admin/site_manager/products_index/'.$this->input->post('group_id'));
			              }
					
					$item_id = $this->Site_manager_model->add_product($data);
				    if($item_id){	
				  	  $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
				  	  loger('Create', $this->data['products_module']['id'], $this->data['products_module']['name'], $item_id, 0, 0, json_encode($data, JSON_UNESCAPED_UNICODE), 0, 0,'added product:'.$item_id.'');
		             redirect('admin/site_manager/products_index/'.$this->input->post('group_id'));

				     }
				   }else{
				   	unset($data['serial']);
	                $item   = $this->Site_manager_model->get_product($this->input->post('id'));
				   	$updated = $this->Site_manager_model->update_product($this->input->post('id'),$data);
				   	if ($updated) {
				   	   $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
				   	    loger('update',$this->data['products_module']['id'],$this->data['products_module']['name'],$item['id'],0,json_encode($item, JSON_UNESCAPED_UNICODE),json_encode($data, JSON_UNESCAPED_UNICODE),0,0,'updated product: '.$item['id']);
				   	 }

	               redirect('admin/site_manager/product/'.$this->input->post('id'));
			   }      
		  
	       }


	    public function product($item_id){
          access_checker($this->data['products_permission']['view'],0,0,0,0,'admin/dashboard');	
          $data['item']  = $this->Site_manager_model->get_product($item_id);
          $data['category'] = $this->Site_manager_model->get_category($data['item']['group_id']);
	        if ($data['item']['deleted'] == 1) {
	         	 $this->session->set_flashdata(['alert'=>'Sorry','msg'=>'This Record has been Deleted before!']);
	         	 redirect('admin/items/items/index/'.$data['group']['id']);
	           }

			$data['view'] = 'admin/site_manager/product_manager';
		    $this->load->view('admin/includes/layout',$data);
	      } 

	    public function delete_product($id){
	      access_checker(0,0,0,$this->data['products_permission']['remove'],0,'');
          $item = $this->Site_manager_model->get_product($id);
	      $this->db->update('site_group_items', array('deleted' => 1), "id = ".$id);
          loger('Delete', $this->data['products_module']['id'], $this->data['products_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted product:'.$item['id'].'');
          echo json_encode(true);
          exit();

		} 

		public function reopen_product($id){
	        access_checker(0,0,0,$this->data['products_permission']['remove'],0,'');
	        $item = $this->Site_manager_model->get_product($id);
	        $this->db->update('site_group_items', array('deleted' =>0), "id = ".$id);
         	  loger('Reopened', $this->data['products_module']['id'], $this->data['products_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'reopened product:'.$item['id'].'');
	        echo json_encode(true);
            exit();
		}      

// ------------------------------ Clients Requests manager ---------------------------------

      public function clients_req_manager(){
        access_checker($this->data['clients_req_permission']['view'],0,0,0,0,'admin/dashboard'); 
        $data['view']          = 'admin/site_manager/clients_req_manager';
        $this->load->view('admin/includes/layout',$data);
      }  

     public function clients_req_ajax(){
        $dt_att  = $this->datatables_att();
        $items   = $this->Site_manager_model->get_clients_req($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
        $data = array();
        $i    =  1;
         foreach($items as $item) {
             $arr = array(); 
             $tools=array();
                //if ($item['status'] ==0) {
                      $tools[] = '<div class="info">
                                   <a href="javascript: void(0);" onclick="get_mailerView('.$item['id'].',`'.$item['email'].'`)">
                                      <span style="color:red;">Send</span>
                                   </a>';
                      // }

                  $arr[] = '<span>'.$i.'.</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['client_name'].'</span><br>'.implode("", $tools);
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['email'].'</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['subject'].'</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['message'].'</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['mail_status'].'</span>';
                  $data[] =$arr;
                  $i++;
              }
       
             $output = array(
                   "draw" => $dt_att['draw'],
                   "recordsTotal"    => count($this->Site_manager_model->get_all_clients_req()),
                   "recordsFiltered" => $this->Site_manager_model->get_clients_req($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
                   "data" => $data
              );
        echo json_encode($output);
        exit();
    
       }

    public function client_mailer(){
      $formdata=[];
      parse_str($this->input->post('formdata'),$formdata);     
      $updateData['status'] ='22'; 
      $updated = $this->Site_manager_model->update_client_mail($formdata['client_id'],$updateData);
      $result = mail_clients($formdata['email'],$formdata['subject'],$formdata['message']);
      echo json_encode(true);
      exit();  
              
    }

//============================================== language manager =============================
  public function lang_manager(){
        access_checker($this->data['language_permission']['view'],0,0,0,0,'admin/dashboard'); 
        $data['view']          = 'admin/site_manager/lang_manager';
        $this->load->view('admin/includes/layout',$data);
      }  
    
   public function lang_manager_ajax(){
      $dt_att  = $this->datatables_att();
      $rows   = $this->Site_manager_model->get_languages($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

      $data = array();
      $i    =  1;
       foreach($rows as $row) {
           $arr = array(); 
           $tools=array();
                if ($this->data['language_permission']['edit'] == 1) {
                   $tools[] = "<div class='info'>
                                  <a href='javascript: void(0);' onclick='fillLanguageModal(".json_encode($row).")'>
                                     <span style='color:blue;'>Edit</span>
                                  </a> 
                               </div> ";
                     }

                $arr[] = '<span>'.$i.'.</span>';
                $arr[] = '<a  href="javascript: void(0);" ><span class="text-danger">'.$row['english'].'</span></a><br>'.implode("", $tools);
                $arr[] = '<span>'.$row['german'].'</span>'; 
                $arr[] = '<span>'.$row['french'].'</span>'; 
                $arr[] = '<span>'.$row['arabic'].'</span>';
               
                $data[] =$arr;
                $i++;
            }
     
           $output = array(
                 "draw" => $dt_att['draw'],
                 "recordsTotal"    => count($this->Site_manager_model->get_all_languages()),
                 "recordsFiltered" => $this->Site_manager_model->get_languages($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
                 "data" => $data
            );
      echo json_encode($output);
      exit();
  
     }

     public function lang_translator(){
      access_checker(0,0,$this->data['language_permission']['edit'],0,0,'admin/site_manager');
        if($this->input->post()){
          $fdata      = array(
                  'english'    => $this->input->post('english'),
                  'german'     => $this->input->post('german'),
                  'french'     => $this->input->post('french'),
                  'arabic'     => $this->input->post('arabic'),
                  );
          if ($this->input->post('id')) {
                  $item_data   = $this->Site_manager_model->get_language($this->input->post('id'));
                  $updated     = $this->Site_manager_model->update_language($this->input->post('id'),$fdata);
                  if ($updated > 0) {
                      loger('update',$this->data['language_module']['id'],$this->data['language_module']['name'],$item_data['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'updated language: '.$item_data['id']);
                  }
              }else{
                $item_id = $this->Site_manager_model->add_language($fdata);
               if ($item_id) {
                loger('Create', $this->data['language_module']['id'], $this->data['language_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added language:'.$item_id.'');
                 }
              }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
        redirect('admin/site_manager/lang_manager');
       }     
    } 

//=================================================== end of language manager module
   public function notifications(){
    if (!$this->input->is_ajax_request()) {
         redirect('admin/dashboard'); 
      }
        $notifs = $this->General_model->get_notifications($this->data['user_id'],$this->data['ubranches'],$this->data['dep_id']);
          
            $data = array();

            $count = 0;
            
            if ($notifs) {
              
              foreach ($notifs as $notifs) {
            
          $data[] =$notifs;

          $count +=1;
          }
        $data['count'] = $count;  
        
            }else{
           
              $data[] =['key'=>'<div class="dropdown-divider"></div><h5 class="m-b-0 centered">لا توجد إخطارات  </h5><br>'];
           
            }

         echo json_encode($data);
            
           
           exit();

      } 


    public function get_search_results(){
         
          $s_results = $this->General_model->search_in_mc($this->data['ubranches'],$this->input->post('search_key'));
            
              $data = array();

              $count = 0;
              
              if ($s_results) {
              
            $data[] =$s_results;

            }else{
             
                $data[] =['key'=>'<div class="dropdown-divider"></div><h5 class="m-b-0 centered">No results found</h5><br>'];
             
              }

           echo json_encode($data);
              
             
             exit();

        }     


    public function delete_notify($id){

        $this->db->update('notifications', array('readedby' =>$this->data['user_id'],'deleted' => 1,'status' => 1), "id = ".$id);

        $this->notifications();
      }

	

}
