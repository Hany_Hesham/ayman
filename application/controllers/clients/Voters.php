<?php

use Mpdf\Mpdf;

defined('BASEPATH') OR exit('No direct script access allowed'); 
// require_once(APPPATH.'vendor/autoload.php');


class voters extends HY_Controller { 
    
    public $facbookConfig;

    function __construct() { 
        parent::__construct(); 
         
        // Load voter model 
        $this->load->model('clients/Voter_model');
        
    } 
    
    public function index(){ 
        if($this->global_data['is_logged']){
            redirect('clients/home');
        }elseif(isset($_GET['state']) && $this->facbookConfig['state'] == $_GET['state']){
            // die(print_r($_GET['state']));
            $this->tryAndLoginWithFacebook( $_GET ); 
        }
    } 

    /**
	 * Get an access token with the code from facebook
	 *
	 * Endpoint https://graph.facebook.com/{fb-graph-version}/oauth/access_token?client_id{app-id}&client_secret={app-secret}&redirect_uri={redirect_uri}&code={code}
	 *
	 * @param string $code
	 *
	 * @return array $response
	 */

    function getAccessTokenWithCode( $code) {
		// endpoint for getting an access token with code
		$endpoint =  $this->facbookConfig['facebookDomain'] . $this->facbookConfig['graphApiVersion'] . '/oauth/access_token';

		$params = array( // params for the endpoint
			'client_id' => $this->facbookConfig['clientId'],
			'client_secret' => $this->facbookConfig['clientSecret'],
			'redirect_uri' => $this->facbookConfig['redirectUri'],
			'code' => $code
		);

		// make the api call
		return $this->makeFacebookApiCall( $endpoint, $params );
	}
    /**
	 * Make call to facebook endpoint
	 *
	 * @param string $endpoint make call to this enpoint
	 * @param array $params array keys are the variable names required by the endpoint
	 *
	 * @return array $response
	 */
	function makeFacebookApiCall( $endpoint, $params ) {
		// open curl call, set endpoint and other curl params
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $endpoint . '?' . http_build_query( $params ) );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );

		// get curl response, json decode it, and close curl
		$fbResponse = curl_exec( $ch );
		$fbResponse = \json_decode( $fbResponse, TRUE );
		curl_close( $ch );

		return array( // return response data
			'endpoint' => $endpoint,
			'params' => $params,
			'has_errors' => isset( $fbResponse['error'] ) ? TRUE : FALSE, // boolean for if an error occured
			'error_message' => isset( $fbResponse['error'] ) ? $fbResponse['error']['message'] : '', // error message
			'fb_response' => $fbResponse // actual response from the call
		);
	}

    
	/**
	 * Try and log a user in with facebook
	 *
	 * @param array $get contains the url $_GET variables from the redirect uri after user authenticates with facebook
	 *
	 * @return array $response
	 */
	function tryAndLoginWithFacebook( $get ) {
		// assume fail
		$status = 'fail';
		$message = '';

		// reset session vars
		$_SESSION['fb_access_token'] = array();
		$_SESSION['fb_user_info'] = array();
		$_SESSION['eci_login_required_to_connect_facebook'] = false;

		if ( isset( $get['error'] ) ) { // error comming from facebook GET vars
			$message = $get['error_description'];
		} else { // no error in facebook GET vars
			// get an access token with the code facebook sent us
			$accessTokenInfo = $this->getAccessTokenWithCode($get['code'] );

			if ( $accessTokenInfo['has_errors'] ) { // there was an error getting an access token with the code
				$message = $accessTokenInfo['error_message'];
			} else { // we have access token! :D
				// set access token in the session
				$_SESSION['fb_access_token'] = $accessTokenInfo['fb_response']['access_token'];

				// get facebook user info with the access token
				$fbUserInfo = $this->getFacebookUserInfo( $_SESSION['fb_access_token'] );

				if ( !$fbUserInfo['has_errors'] && !empty( $fbUserInfo['fb_response']['id'] ) && !empty( $fbUserInfo['fb_response']['email'] ) ) { // facebook gave us the users id/email
					// 	all good!
					$status = 'ok';

					// save user info to session
					$_SESSION['fb_user_info'] = $fbUserInfo['fb_response'];

					// check for user with facebook id
					$userInfoWithId = $this->Voter_model->getRowWithValue('fb_user_id', $fbUserInfo['fb_response']['id'] );

					// check for user with email
					$userInfoWithEmail = $this->Voter_model->getRowWithValue('email', $fbUserInfo['fb_response']['email'] );

					if ( $userInfoWithId || ( $userInfoWithEmail && !$userInfoWithEmail['password'] ) ) { // user has logged in with facebook before so we found them
						// update user
						$this->Voter_model->update_voter( $userInfoWithId['id'], ['fb_access_token' => $_SESSION['fb_access_token']] );
						$userInfo = $this->Voter_model->getRowWithValue('id', $userInfoWithId['id'] );

						// save info to php session so they are logged in
						$_SESSION['is_logged_in'] = true;
						$_SESSION['user_info'] = $userInfo;
                        $_SESSION['is_voter'] = TRUE;
					} elseif ( $userInfoWithEmail && !$userInfoWithEmail['fb_user_id'] ) { // existing account exists for the email and has not logged in with facebook before
						$_SESSION['eci_login_required_to_connect_facebook'] = true;
					} else { // user not found with id/email sign them up and log them in
						// sign user up
						$fbUserInfo['fb_response']['fb_access_token'] = $_SESSION['fb_access_token'];
						$userId =  $this->Voter_model->create_voter( $fbUserInfo['fb_response'] );
						$userInfo =  $this->Voter_model->getRowWithValue('id', $userId );

						// save info to php session so they are logged in
						$_SESSION['is_logged_in'] = true;
						$_SESSION['user_info'] = $userInfo;
                        $_SESSION['is_voter'] = TRUE;
					}
				} else {
					$message = 'Invalid creds';
				}
			}
		}
        
        redirect('clients/home');
		// return array( // return status and message of login
		// 	'status' => $status,
		// 	'message' => $message,
		// );
	}

    /**
	 * Get a users facebook info
	 *
	 * Endpoint https://graph.facebook.com/me?fields={fields}&access_token={access-token}
	 *
	 * @param string $accessToken
	 *
	 * @return array $response
	 */
	function getFacebookUserInfo( $accessToken ) {
		// endpoint for getting a users facebook info
		$endpoint = $this->facbookConfig['facebookDomain'] . 'me';

		$params = array( // params for the endpoint
			'fields' => 'first_name,last_name,email,picture',
			'access_token' => $accessToken
		);

		// make the api call
		return $this->makeFacebookApiCall( $endpoint, $params );
	}

	/**
	 * Get debug info on an access token
	 *
	 * Endpoint https://graph.facebook.com/debug_token?input_token={access-token}&access_token={access-token}
	 *
	 * @param string $accessToken
	 *
	 * @return array $response
	 */
	function getDebugAccessTokenInfo( $accessToken ) {
		// endpoint for getting debug info
		$endpoint = $this->facbookConfig['facebookDomain'] . 'debug_token';

		$params = array( // params for the endpoint
			'input_token' => $accessToken,
			'access_token' => $accessToken
		);

		// make the api call
		return makeFacebookApiCall( $endpoint, $params );
	}

    public function logout() { 
        $this->session->sess_destroy();
        redirect(base_url('clients/home'), 'refresh');
    } 

}