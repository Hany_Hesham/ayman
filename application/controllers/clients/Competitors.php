<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Competitors extends HY_Controller {
   
    public function __construct(){
	    parent::__construct();
        $this->load->model('clients/Competitor_model');
	    $this->data['language'] 	= 'english';
	    if (isset($_COOKIE['language'])) {
		    $this->data['language'] = $_COOKIE['language'];
		}
		$this->data['logo']      	= $this->Home_model->get_sit_metaData('site_logo' ,'single');
	}
		 

	public function add()
    {
        $formdata = $this->input->post();
        // parse_str($this->input->post('formdata'),$formdata);
        $age = getAgeFromDate($formdata['birth_date']);
        $emailIsExist= $this->Competitor_model->checkEmailExist($formdata['reg_email']);
        if($emailIsExist == true){
            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(500)
            ->set_output(json_encode(array(
                    'text' => 'Error 500',
                    'type' => 'this email already exist'
            )));
        }
        $file_name = do_upload('profile_picture','users_profiles', 'site_assets/uploads');
        $data = [
            'name' => $formdata['reg_name'],
            'middle_name' => $formdata['reg_middle_name'],
            'last_name' => $formdata['reg_lastname'],
            'birth_date' => $formdata['birth_date'],
            'age' => $age,
            'city' => $formdata['city'],
            'school' => $formdata['school'],
            'email' => $formdata['reg_email'],
            'country_code' => $formdata['countryCode'],
            'phone' => $formdata['whatsapp'],
            'profile_picture' => $file_name,
            'password' =>  password_hash($formdata['password'], PASSWORD_BCRYPT),
            'created_at' => date('Y-m-d : h:m:s'),
            'updated_at' => date('Y-m-d : h:m:s'),
            // 'disabled' => 1,
        ];
        $uid = $this->Competitor_model->add_competitor($data);    
        // $result = mail_management($formdata);
	    echo json_encode(true);
	    exit();         
    }

    public function edit($competitor_id)
    {
        $formdata = $this->input->post();
        $age = getAgeFromDate($formdata['birth_date']);
        $file_name = do_upload('profile_picture','users_profiles', 'site_assets/uploads');
        $password = $formdata['password'];
        $fdata = [
            'name' => $formdata['reg_name'],
            'middle_name' => $formdata['reg_middle_name'],
            'last_name' => $formdata['reg_lastname'],
            'birth_date' => $formdata['birth_date'],
            'age' => $age,
            'city' => $formdata['city'],
            'school' => $formdata['school'],
            'email' => $formdata['reg_email'],
            'country_code' => $formdata['countryCode'],
            'phone' => $formdata['whatsapp'],
            'updated_at' => date('Y-m-d : h:m:s'),
        ];
        if( $file_name) $fdata['profile_picture'] = $file_name;
        if( $password) $fdata['password'] = password_hash($formdata['password'], PASSWORD_BCRYPT);
        $this->Competitor_model->update_competitor($fdata, $competitor_id);
        redirect('clients/home/pending_registrations');      
    }

    public function login(){
        $formdata=[];
        parse_str($this->input->post('formdata'),$formdata);
        if($formdata['sign_email'] == '' || $formdata['sign_password'] == '' ){
            throw new Exception('invalid email or password!');
        }

        $data = [
            'email' => trim($formdata['sign_email']),
            'password' => $formdata['sign_password']
        ];
        $result = $this->Competitor_model->login($data);
        if ($result == TRUE) {
            $competitor_data = array(
                'user_id' => $result['id'],
                'name'    => $result['name'],
                'email'    => $result['email'],
                'is_competitor' => TRUE
            );
            $this->session->set_userdata($competitor_data);
            echo json_encode('clients/home/competitions');
	        exit();    

        }else{
            throw new Exception('invalid email or password!');
        }
    }	

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url('clients/home'), 'refresh');
    }

}
?>
