<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends HY_Controller {
   
    public function __construct(){
	    parent::__construct();
	    $this->data['language'] 	= 'english';
	    if (isset($_COOKIE['language'])) {
		    $this->data['language'] = $_COOKIE['language'];
		}
		$data['isLogged'] = $this->isLogged();
		$this->load->model('clients/Project_model');
		$this->load->model('clients/Competitor_model');
		$this->data['logo']      	= $this->Home_model->get_sit_metaData('site_logo' ,'single');
		// $this->data['sliders']      = $this->Home_model->get_sit_metaData('slider_image' ,'single');
		// $this->data['sliders_data']	= $this->Home_model->get_sit_metaData('slider_data' ,'single');
		$this->data['background_slider'] = $this->Home_model->get_slider(1);
		$this->data['slider_magnified_image'] = $this->Home_model->get_slider(2);
		$this->data['inner_slider_image'] = $this->Home_model->get_slider(3);
		$this->data['social_media']	= $this->Home_model->get_sit_metaData('social_data' ,'multi');
		$this->data['contact']		= $this->Home_model->get_sit_metaData('call_us' ,'single');
		$this->data['about_us']		= $this->Home_model->get_sit_metaData('about_us' ,'single');
		$this->data['location']		= $this->Home_model->get_sit_metaData('location' ,'single');
		$this->data['email']		= $this->Home_model->get_sit_metaData('email_us' ,'single');
		$this->data['support']     	= 'clients/includes/support';
		$this->data['header']      	= 'clients/includes/header';
		$this->data['slider']     	= 'clients/includes/slider';
		$this->data['inner_slider'] = 'clients/includes/inner-slider';
		$this->data['footer']      	= 'clients/includes/footer';
	}

	public function language_converter($lang){
        setcookie('language', $lang, 2592000000, "/"); 
        redirect($this->agent->referrer());
	}
		 
	public function index(){
		$data['what_is_readysteadyshare'] = $this->Home_model->get_sit_metaData('about_us_what_is_readysteadyshare','single');
		$data['partners'] = $this->Home_model->get_sit_metaData('partners','multi');
		$data['about_us_design'] = $this->Home_model->get_sit_metaData('about_us_design','single');
		$data['about_us_coding'] = $this->Home_model->get_sit_metaData('about_us_coding','single');
		$data['about_us_voting'] = $this->Home_model->get_sit_metaData('about_us_voting','single');
		$data['winners'] = $this->Home_model->get_winners();
		$data['view']        		= 'clients/home/index';
		$this->load->view('clients/includes/layout',$data);
	}

	public function gallary(){
		$data['categories']       	= getallCategories();
		$data['images']       		= $this->Home_model->get_gallarys();
		$data['view']        		= 'clients/includes/gallary';
		$this->load->view('clients/includes/layout',$data);
	}

	public function about_us(){
		$data['what_is_readysteadyshare'] = $this->Home_model->get_sit_metaData('about_us_what_is_readysteadyshare','single');
		$data['about_us'] = $this->Home_model->get_sit_metaData('about_us_rss','single');
		$data['about_us_competition_description'] = $this->Home_model->get_sit_metaData('about_us_competition_description','single');
		$data['about_us_design'] = $this->Home_model->get_sit_metaData('about_us_design','single');
		$data['about_us_coding'] = $this->Home_model->get_sit_metaData('about_us_coding','single');
		$data['about_us_voting'] = $this->Home_model->get_sit_metaData('about_us_voting','single');
		$data['partners'] = $this->Home_model->get_sit_metaData('partners','multi');
		$data['view'] = 'clients/home/about_us';
		$this->load->view('clients/includes/layout',$data);
	}

	public function rules(){
		$data['rules'] = $this->Home_model->get_rules();
		$data['view'] = 'clients/home/rules';
		$this->load->view('clients/includes/layout',$data);
	}

	public function gallery(){
		$data['view']        		= 'clients/includes/gallery';
		$this->load->view('clients/includes/layout',$data);
	}

	public function result(){
		$data['yearly_competitions'] = $this->Home_model->get_inactive_competition('yearly');
		$data['weekly_competitions'] = $this->Home_model->get_inactive_competition('weekly');
		$data['view']        		= 'clients/home/results';
		$this->load->view('clients/includes/layout',$data);
	}

	public function competition_results($competition_id){
		$data['competition'] = $this->Home_model->get_competition($competition_id);
		$data['age_categories'] = $this->Home_model->get_sit_metaData('age_category','multi');
		$data['view'] = 'clients/home/competition_results';
		$this->load->view('clients/includes/layout',$data);
	}

	public function get_competition_results_by_category($competition_id,$category){
		$data['category'] = $this->Home_model->get_sit_metaData_by_id($category);
		$data['projects'] =  $this->Project_model->get_competition_results_by_category($competition_id, $category);
		$this->load->view('clients/home/competition_results_with_age_category',$data);
	}

	public function competition(){
		$data['yearly_competitions'] = $this->Home_model->get_active_competition('yearly');
		$data['weekly_competitions'] = $this->Home_model->get_active_competition('weekly');
		$data['view']        		= 'clients/home/competition';
		$this->load->view('clients/includes/layout',$data);
	}

	public function contact_us(){
		$data['events']       		= $this->Home_model->get_events();
		$data['view']        		= 'clients/includes/contact-us';
		$this->load->view('clients/includes/layout',$data);
	}

	public function index_services(){
		$data['services']       = $this->Home_model->get_sit_metaData('services','multi');
		$this->load->view('clients/home/index_services',$data);
	}	

	public function index_team(){
		$data['teams']           = $this->Home_model->get_sit_metaData('team','multi');
		$this->load->view('clients/home/index_team',$data);
	}	

	public function index_about(){
		$data['about']       = $this->Home_model->get_sit_metaData('about','single');
		$this->load->view('clients/home/index_about',$data);
	}	

	public function portfolio(){
		$data['portfolio']       = $this->Home_model->get_sit_metaData('portfolio','multi');
		$data['navbar']         = 'clients/includes/navbar';
		$data['view']           = 'clients/home/portfolio.php';
		$this->load->view('clients/includes/layout',$data);
	}

	public function team(){
		$data['teams']           = $this->Home_model->get_sit_metaData('team','multi');
		$data['navbar']         = 'clients/includes/navbar';
		$data['view']           = 'clients/home/team.php';
		$this->load->view('clients/includes/layout',$data);
	}


	public function about(){
		$data['about']       = $this->Home_model->get_sit_metaData('about','single');
		$arrayFilter             = \json_decode($data['about']['points'],true);
		for ($i = 1; $i < sizeof($arrayFilter); $i++) {
		    for ($j=$i+1; $j < sizeof($arrayFilter); $j++) {
		        if ($arrayFilter[$i]['rank'] > $arrayFilter[$j]['rank']) {
		            $c = $arrayFilter[$i];
		            $arrayFilter[$i] = $arrayFilter[$j];
		            $arrayFilter[$j] = $c;
		        }
		    }
		}
		$data['points']         = $arrayFilter;
		$data['navbar']         = 'clients/includes/navbar';
		$data['view']           = 'clients/home/about.php';
		$this->load->view('clients/includes/layout',$data);
	}
    

    public function services(){
		$data['services']       = $this->Home_model->get_sit_metaData('services','multi');
		$this->load->view('clients/home/services',$data);
	}

	public function competition_register($competition_id, $project_id=''){
		$this->isCompetitor() ? true : redirect('clients/home');
		$data['competition_id'] = $competition_id;
		$data['project'] = $project_id == '' ? 
		    $this->check_user_reg_in_competition($competition_id, $project_id) : 
			$this->Project_model->get_project($project_id);
		$data['competition'] = $this->Home_model->get_competition($competition_id);
		$data['view']             = 'clients/home/competition_register';
		$this->load->view('clients/includes/layout',$data);
	}

	private function check_user_reg_in_competition($competition_id){
		$user_id = $this->global_data['userData']['user_id'];
		$pre_project = $this->Project_model->get_user_competition_project($competition_id, $user_id);
		if(isset($pre_project)) 
		    redirect('clients/home/competition_register/'.$competition_id.'/'.$pre_project['id']);
	}

	public function pending_registrations(){
		$this->isCompetitor() ? true : redirect('clients/home');
		$user_id = $this->global_data['userData']['user_id'];
		$data['competitor'] = $this->Competitor_model->get_competitor_byId($user_id);
		$data['pending_projects'] =  $this->Project_model->get_user_pending_projects($user_id);
		$data['current_projects'] =  $this->Project_model->get_user_current_projects($user_id);
		$data['old_projects'] = $this->Project_model->get_user_old_projects($user_id);
		$data['view']             = 'clients/home/pending_registrations';
		$this->load->view('clients/includes/layout',$data);
	}

	public function create_update_project($competition_id, $status){
		if($this->global_data['is_competitor']){
			if ($this->input->post() && $status == 1) {
				$project_id = $this->add_project($competition_id, $status, $this->input->post());	
			}elseif ($this->input->post() && $status == 2) {
				$project_id = $this->add_project_payment($status, $this->input->post());	
			}elseif ($this->input->post() && $status == 4) {
				$project_id = $this->add_project_metaData($status, $this->input->post());	
			}		

			redirect('clients/home/competition_register/'.$competition_id.'/'.$project_id);
		}
	}

	private function add_project($competition_id, $status, $inputData){
		$competitor_id = $this->global_data['userData']['user_id'];
		$competitor = $this->Competitor_model->get_competitor_byId($competitor_id);
		$category = '';
		$age = getAgeFromDate($competitor['birth_date']);
        if($age < 9){
           $category = 46;
		}elseif($age >= 9 && $age <= 12){
			$category = 47;
		}elseif($age > 12){
			$category = 48;
		}  
		$fdata = [
			'competitor_id' => $competitor_id,
			'competition_id' => $competition_id,
			'code' => get_code_with_random('project'),
			'school' => $inputData['school'], 
			'trainer' => $inputData['trainer'], 
			'trainer_name' => $inputData['trainer_name'], 
			'status'=> $status,
			'category' => $category,
		];
		return $this->Project_model->add_project($fdata);
	}

	private function add_project_payment($status, $inputData){
		$file_name = do_upload('payment_attach','payments', 'site_assets/uploads'); 
		$fdata = [
			'payment' => $inputData['payment_method'], 
			'price' => $inputData['competition_price'], 
			'payment_attach' => $file_name, 
			'status'=> $status
		];
		$res = $this->Project_model->update_project($inputData['project_id'], $fdata); 
		return $inputData['project_id']; 	
	}

	private function add_project_metaData($status, $inputData){
		$file_name = do_upload('project_image','projects', 'site_assets/uploads'); 
		$fdata = [
			'title' => $inputData['project_title'], 
			'project_link' => $inputData['project_link'],
			'description' => $inputData['project_description'],
			'image' => $file_name,
			'started_at' => date('Y-m-d'), 
			'status'=> $status
		];
		$res = $this->Project_model->update_project($inputData['project_id'], $fdata); 
		return $inputData['project_id']; 	
	}

	public function project($code){
		$is_site_manager_user = strpos($this->agent->referrer(), base_url('/admin/site_manager/')) !== false;
		$data['project'] =  $this->Project_model->get_project_byCode($code);
		if(!isset($data['project'])) redirect('clients/home');
		if($data['project']['status'] != 5 && $is_site_manager_user == false) redirect('clients/home');
		$data['view'] = 'clients/home/project';
		$this->load->view('clients/includes/layout',$data);
	}

	public function like_project($project_id, $req=''){ 
		$data['project'] =  $this->Project_model->get_project($project_id);
		$fdata = [
			'voter_id' => $this->global_data['userData']['user_id'],
			'competitor_id' => $data['project']['competitor_id'],
			'project_id' => $project_id,
			'vote' => 1
		];
		$this->Project_model->process_vote_project($fdata); 
		if($req == ''){
			redirect('clients/home/project/'.$data['project']['code'].'/#pinned-element'); 	
		}elseif($req == 'ajax'){
			echo json_encode('true');
	        exit(); 
		}
	}

	public function projects(){
		// !$this->isLogged() ? redirect('clients/home') : true; 
		$data['age_categories'] = $this->Home_model->get_sit_metaData('age_category','multi');
		$data['view'] = 'clients/home/projects';
		$this->load->view('clients/includes/layout',$data);
	}

	public function get_projects_by_category($category){
		$data['category'] = $this->Home_model->get_sit_metaData_by_id($category);
		$data['projects'] =  $this->Project_model->get_activated_projects_by_category($category);
		$this->load->view('clients/home/projects_with_age_category',$data);
	}

	public function previous_projects(){
		// !$this->isLogged() ? redirect('clients/home') : true;
		$data['age_categories'] = $this->Home_model->get_sit_metaData('age_category','multi');
		$data['view']        		= 'clients/home/previous_projects';
		$this->load->view('clients/includes/layout',$data);
	}

	public function get_previous_projects_by_category($category){
		$data['category'] = $this->Home_model->get_sit_metaData_by_id($category);
		$data['projects'] =  $this->Project_model->get_unactivated_projects_by_category($category);
		$this->load->view('clients/home/previous_projects_with_age_category',$data);
	}

	public function delete_notify($id){
		$user_id = $this->global_data['userData']['user_id'];
        $this->db->update('notifications', [
			'readedby' => $user_id,'deleted' => 1,'status' => 1]
			, "id = ".$id);
      }
	
	// public function products(){
	// 	$data['categories']       = $this->Home_model->get_categories();
	// 	$this->load->view('clients/home/products',$data);
	// }

	// public function product($serial){
	// 	$data['serial']           = $serial;
	// 	$data['categories']       = $this->Home_model->get_categories();
	// 	$data['category']         = $this->Home_model->get_category($serial);
	// 	$data['cat_items']        = $this->Home_model->get_category_items($data['category']['id']);
	// 	$data['navbar']           = 'clients/includes/outer_navbar.php';
	// 	$data['view']             = 'clients/home/product.php';
	// 	$this->load->view('clients/includes/layout',$data);
	// }



    
	// public function get_competitions_cat(){
	// 	$data['view']             = 'clients/home/competitions_cat.php';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

	// public function get_competitions($type){
	// 	// $data['competitions']       = $this->Home_model->get_categories($type);
	// 	$data['view']             = 'clients/home/competitions.php';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

	
	// public function boat(){
	// 	$data['boat_infos']       	= $this->Home_model->get_boat_infos('1');
	// 	$data['view']        		= 'clients/includes/boat';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

	// public function packages(){
	// 	$data['packages']       	= $this->Home_model->get_boat_packages('1');
	// 	$data['view']        		= 'clients/includes/packages';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

	// public function booking(){
	// 	$data['cabins']       		= $this->Home_model->get_boat_cabins('1');
	// 	$data['packages']       	= $this->Home_model->get_boat_packages('1');
	// 	$data['view']        		= 'clients/includes/book';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

	// public function addBook(){
    //   	if ($this->input->post('submit')) {
    //     	$fdata =[
    //       		'check_in'            => $this->input->post('check_in'),
    //       		'check_out'           => $this->input->post('check_out'),
    //       		'cabins'	          => $this->input->post('cabins'),
    //       		'persons'             => $this->input->post('persons'),
    //       		'package'             => $this->input->post('package'),
    //       		'price'               => $this->input->post('price'),
    //       		'first_name'          => $this->input->post('first_name'),
    //       		'last_name'           => $this->input->post('last_name'),
    //       		'email'               => $this->input->post('email'),
    //       		'address'             => $this->input->post('address'),
    //       		'phone'               => $this->input->post('phone'),
    //       		'status'              => '1',
    //     	];      
    //     	$control_id  =  $this->Home_model->add_booking($fdata);
    //     	if ($control_id) {          
    //     		redirect('booking');
    //     	}
    //   	}       
    // }

	// public function blogs(){
	// 	$data['view']        		= 'clients/includes/blogs';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

	// public function blog($id = FALSE){
	// 	$data['view']        		= 'clients/includes/blog';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

	// public function room($id = FALSE){
	// 	$data['view']        		= 'clients/includes/room';
	// 	$this->load->view('clients/includes/layout',$data);
	// }

	// public function trip(){
	// 	$data['events']       		= $this->Home_model->get_events();
	// 	$data['images']       		= $this->Home_model->get_gallarys();
	// 	$data['view']        		= 'clients/includes/trip';
	// 	$this->load->view('clients/includes/layout',$data);
	// }
    
    // public function index_offers(){
	// 	$data['allOffers']      = get_special();
	// 	$this->load->view('clients/home/index_offers',$data);
	// }	


}
?>
