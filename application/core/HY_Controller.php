<?php

class HY_Controller extends CI_Controller{
		
		function __construct(){
			parent::__construct();
			$this->facbookConfig = [ 
				'clientId' => '264412838999374',
				'clientSecret' => 'febddf5e1baaa63d7abb8c4598c38e1d',
				'redirectUri' => base_url('clients/voters/'),
				'graphApiVersion' => 'v12.0',
				'facebookDomain' => 'https://graph.facebook.com/',
				'state' => 'eciphp'
			];
            $this->global_data['site_title'] = $this->Home_model->get_sit_metaData('site_title','single');
			$this->global_data['is_logged'] = $this->isLogged();
			$this->global_data['is_competitor'] = $this->isCompetitor();
			$this->global_data['is_voter'] = $this->isVoter();
			if($this->global_data['is_competitor']){
				$this->load->model('clients/Competitor_model');
				$this->load->model('admin/General_model');
				$this->global_data['userData'] = $_SESSION;
				$this->global_data['userData']['competitor_data'] = $this->Competitor_model
				->get_competitor_byId($this->global_data['userData']['user_id']);
				$this->global_data['userData']['liked'] = [];
				$this->global_data['notifications'] = $this->General_model->get_notifications($this->global_data['userData']['user_id'],'','');
			}elseif($this->global_data['is_voter']){
				$this->global_data['userData'] = $_SESSION['user_info'];
				$this->global_data['userData']['user_id'] = $this->global_data['userData']['id'];
				$this->global_data['userData']['picture'] = \json_decode($this->global_data['userData']['picture'])->data;
				$this->global_data['userData']['liked'] = $this->getVotersLikedProjects();
			}
			$this->global_data['face_book_auth_endPoint'] = $this->getFacebookLoginUrl();
			// require_once(APPPATH.'vendor/autoload.php');

		}

		/**
	 * Get facebook api login url that will take the user to facebook and present them with login dialog
	 *
	 * Endpoint: https://www.facebook.com/{fb-graph-api-version}/dialog/oauth?client_id={app-id}&redirect_uri={redirect-uri}&state={state}&scope={scope}&auth_type={auth-type}
	 *
	 * @param void
	 *
	 * @return string
	 */

    function getFacebookLoginUrl() {
		// endpoint for facebook login dialog
		$endpoint = 'https://www.facebook.com/' . $this->facbookConfig['graphApiVersion'] . '/dialog/oauth';

		$params = array( // login url params required to direct user to facebook and promt them with a login dialog
			'client_id' => $this->facbookConfig['clientId'],
			'redirect_uri' => $this->facbookConfig['redirectUri'],
			'state' =>$this->facbookConfig['state'],
			'scope' => 'email',
			'auth_type' => 'rerequest'
	 	);

		// return login url
		 return $endpoint . '?' . http_build_query( $params );
        // echo '<a href="'.$endpoint . '?' . http_build_query( $params ).'">Log in with Facebook!</a>';
	}

	
	function isLogged(){
		return isset($_SESSION['is_competitor']) ||
		isset($_SESSION['is_voter'])? true : false;
	}
	function isCompetitor(){
		return (isset($_SESSION['is_competitor']) 
		&&  $_SESSION['is_competitor']== true)? true : false;
	}
	
	function isVoter(){
		return (isset($_SESSION['is_voter']) 
		&&  $_SESSION['is_voter']== true)? true : false;
		
	}

	private function getVotersLikedProjects()
	{
		$this->load->model('clients/Project_model');
		return $this->Project_model->getVoterLikes($this->global_data['userData']['user_id']);
	}

		function datatables_att(){

			 $draw        = intval($this->input->post("draw"));
	         $start       = intval($this->input->post("start"));
	         $length      = intval($this->input->post("length"));
	         if ($length == '-1') {
	         	 $length ='1000000000000000000';
	         }
	         $order       = $this->input->post("order[0][dir]");
	         $order_col   = $this->input->post("order[0][column]");
	         $col_name    = $this->input->post("columns[$order_col][name]");
	         $search      = trim($this->input->post("search[value]"));

	         $dt_att = [
	         	         'draw'      => $draw,
                         'start'     => $start,
                         'length'    => $length,
                         'order'     => $order,
                         'order_col' => $order_col,
                         'col_name'  => $col_name,
                         'search'    => $search,                         
	                   ];
	               
	         return $dt_att;
		}   


	public function get_submenu($model,$func,$type_id=false){

		   if (!$type_id) {
		   	    if ($this->input->post()) {
		   	    	$type_id = $this->input->post('search');
		   	    }
		      }

			$parts = $this->$model->$func($type_id);
	      
	        $data = array();
	        
	        if ($parts) {
	        	
	          foreach ($parts as $part) {
					
				$data[] =$part;
			  }
			
	        }else{
	       
	        	$data[] =['key'=>'عذرا لا يوجد بيانات لهذا الإختيار  '];
	       
	        }

			   
		   echo json_encode($data);
	        
	       
	       exit();

		} 

     
    public function upload($module_id,$form_id,$folder) {

        $file_name = do_upload("upload",$folder);

        if (!$file_name) {

              die(json_encode($this->data['error']));

          } else {

          	if (!$this->input->post('table')) {
	              $id= $this->General_model->add_file($module_id,$form_id, $file_name,$_SESSION['user_id']);
          	   }else{
          	   	 $id= $this->General_model->add_file_intable($this->input->post('table'),$form_id, $file_name,$_SESSION['user_id'],$module_id);
          	   }


              die("{}");

            }

         }


     
    public function remove_file($form_id, $id,$module_id) {

        $file_name = $_POST['key'];

          if (!$id) {

              die(json_encode($this->data['error']));

          } else {

          	if (!$this->input->post('key')) {
                  
                  $this->General_model->remove_file($id,'',$module_id);

          	  }else{

                  $this->General_model->remove_file($id,$this->input->post('key'),$module_id);
          	  }

              die("{}");

          }

        }  	


        public function check_field($table,$col,$exist_value=''){

			 $item   = $this->General_model->search_in($table,$col,$this->input->post('name'),$exist_value); 

			 if ($item) {
	              echo json_encode('exist');
		          exit();
			   }else{
			   	  echo json_encode('not_exist');
		          exit();
			   }
            
		  }
  

 

  }

?>

    