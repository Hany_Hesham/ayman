-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 26, 2021 at 03:27 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dahabiya`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `name`, `deleted`) VALUES
(1, 'Aqua Center', 0),
(2, 'Diving', 0),
(3, 'SPA', 0);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(5) NOT NULL,
  `category` varchar(100) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `post` longblob NOT NULL,
  `status` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `category`, `title`, `description`, `image`, `post`, `status`, `deleted`, `timestamp`) VALUES
(1, 'General', 'This is a Standard post with a Preview Image', 'Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text ', '17.jpg', '', 2, 0, '2021-10-24 13:58:13'),
(2, 'Diving', 'This is a Standard post with a Preview Image 2', 'Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text ', '1.jpg', '', 2, 0, '2021-10-24 13:58:13'),
(3, 'Torusim', 'This is a Standard post with a Preview Image 3', 'Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text ', '10.jpg', '', 2, 0, '2021-10-25 16:58:13');

-- --------------------------------------------------------

--
-- Table structure for table `boats`
--

CREATE TABLE `boats` (
  `boatid` int(11) NOT NULL,
  `boat_name` varchar(100) DEFAULT NULL,
  `description` text NOT NULL,
  `facebook` mediumtext DEFAULT NULL,
  `instagram` mediumtext DEFAULT NULL,
  `twitter` mediumtext DEFAULT NULL,
  `youtube` mediumtext DEFAULT NULL,
  `boat_slug` varchar(100) NOT NULL,
  `show_mobile` int(11) NOT NULL DEFAULT 0,
  `cabins` int(11) NOT NULL DEFAULT 0,
  `color` varchar(50) DEFAULT NULL,
  `phonenumber` varchar(30) DEFAULT NULL,
  `country` int(11) NOT NULL DEFAULT 0,
  `city` varchar(100) DEFAULT NULL,
  `zip` varchar(15) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `website` varchar(150) DEFAULT NULL,
  `datecreated` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `longitude` varchar(300) DEFAULT NULL,
  `latitude` varchar(300) DEFAULT NULL,
  `default_language` varchar(40) DEFAULT NULL,
  `default_currency` int(11) NOT NULL DEFAULT 0,
  `show_primary_contact` int(11) NOT NULL DEFAULT 0,
  `addedfrom` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boats`
--

INSERT INTO `boats` (`boatid`, `boat_name`, `description`, `facebook`, `instagram`, `twitter`, `youtube`, `boat_slug`, `show_mobile`, `cabins`, `color`, `phonenumber`, `country`, `city`, `zip`, `state`, `address`, `website`, `datecreated`, `active`, `longitude`, `latitude`, `default_language`, `default_currency`, `show_primary_contact`, `addedfrom`) VALUES
(1, 'Dahabeya Queen-Farida', '', NULL, NULL, NULL, NULL, '', 0, 14, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `boats_cabins`
--

CREATE TABLE `boats_cabins` (
  `cabinid` int(11) NOT NULL,
  `cabin_no` int(11) NOT NULL,
  `cabin_type` varchar(250) DEFAULT NULL,
  `boat_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boats_cabins`
--

INSERT INTO `boats_cabins` (`cabinid`, `cabin_no`, `cabin_type`, `boat_id`) VALUES
(1, 1, 'DOUBLE CABIN', 1),
(2, 2, 'DOUBLE CABIN', 1),
(3, 3, 'DOUBLE CABIN', 1),
(4, 4, 'DOUBLE CABIN', 1),
(5, 5, 'DOUBLE CABIN', 1),
(6, 6, 'DOUBLE CABIN', 1),
(7, 7, 'DOUBLE CABIN', 1),
(8, 8, 'DOUBLE CABIN', 1),
(9, 9, 'LUXURY CABIN', 1),
(10, 10, 'LUXURY CABIN', 1),
(11, 11, 'LUXURY CABIN', 1),
(12, 12, 'LUXURY CABIN', 1),
(13, 13, 'LUXURY CABIN', 1),
(14, 14, 'LUXURY CABIN', 1);

-- --------------------------------------------------------

--
-- Table structure for table `boat_info`
--

CREATE TABLE `boat_info` (
  `id` int(5) NOT NULL,
  `boat_id` int(5) NOT NULL DEFAULT 1,
  `title` varchar(150) NOT NULL,
  `remarks` text NOT NULL,
  `icon` varchar(50) NOT NULL DEFAULT 'icon-screen',
  `image` varchar(150) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `status` int(5) NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boat_info`
--

INSERT INTO `boat_info` (`id`, `boat_id`, `title`, `remarks`, `icon`, `image`, `code`, `status`, `rank`, `deleted`, `timestamp`) VALUES
(1, 1, 'Sailing Yacht on the Nile', '<span>Step aboard the Dahabeya Queen Farida in homage to the last Queen Farida of Egypt.\r\n<br>\r\nQueen Farida often travelled the Nile with her children on a dahabeya - called the golden boat.\r\n<br>\r\nThe shallow draft of the Dahabeya allows her to pass the tributaries of the Nile and anchor on the banks.\r\n<br>\r\nWith a sailing Nile cruise far from the hotspots, you can enjoy nature up close along the Nile. Bird paradises on the small Nile islands and lush plantation oases line the banks of the Nile. Enjoy the peace and the spectacle of nature.\r\n<br>\r\nA unique way of yachting the river Nile awaits you as soon as you step onboard. A ship built from exquisite woods shimmers golden in the sun, so this justifies the traditional name Dahabeya - the golden boat.\r\n<br>\r\nThe high-quality, mosaic-laid parquet floors ensure a sustainable room climate and a feel-good experience upon arrival!</span>', 'icon-screen', '04-b_12.jpg', 'snav-content1', 2, 1, 0, '2021-07-02 15:23:45'),
(2, 1, 'Dahabeya Queen Farida–the golden boat', '<span>Go on a discovery tour along the Nile of the historical monuments and the green landscape with the spectacular desert backdrop of Egypt.\r\n						Anchoring right on the banks of the Nile offers the opportunity to hike through plantation groves for an evening stroll and enjoy tree-fresh fruit with Egyptian hospitality.</span>', 'icon-magic', '04-b_14.jpg', 'snav-content2', 2, 2, 0, '2021-07-02 15:23:48'),
(3, 1, 'Facilities Overview', '<ul>\r\n							<li>Reception with free WIFI</li>\r\n							<li>Lounge with bar, Flat-screen TV and library</li>\r\n							<li>covered upper deck - your open-air restaurant</li>\r\n							<li>Sun deck with deck chairs and parasols</li>\r\n							<li>Kitchen with hygiene certificates</li>\r\n							<li>In the corridor to the reception you can already see historical photographs of the last Queen of Egypt - Queen Farida.</li>\r\n						</ul>', 'icon-tint', NULL, 'snav-content3', 2, 3, 0, '2021-07-02 15:23:50'),
(4, 1, 'Tour with kids', '<ul>\r\n							<li>The Dahabeya Queen Farida welcomes our young guests with pleasure</li>\r\n							<li>Children under 6 years share the parent\'s cabin – free of charge</li>\r\n							<li>Children from 6 and under 11 years are accommodated in the cabin next to or opposite the parents\' cabin – charge as one single cabin</li>\r\n							<li>Children over 11 years are considered adults</li>\r\n						</ul>', 'icon-gift', NULL, 'snav-content4', 2, 4, 0, '2021-07-02 15:23:53'),
(5, 1, 'Security- and technical facilities', '<ul>\r\n							<li>Electronic fire and smoke alarms in all areas</li>\r\n							<li>Sprinkler system</li>\r\n							<li>Soundproofing of all cabins</li>\r\n							<li>2 life jackets in each cabin</li>\r\n							<li>Escape map in every cabin</li>\r\n							<li>Electricity: 220 volts / 110 volts</li>\r\n							<li>Night mode, generator switched off 12 V</li>\r\n							<li>The internal water purification system</li>\r\n							<li>the hygienic ultraviolet water cleaning system</li>\r\n						</ul>', 'icon-adjust', NULL, 'snav-content5', 2, 5, 0, '2021-07-02 15:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(5) NOT NULL,
  `boat_id` int(5) NOT NULL DEFAULT 1,
  `check_in` varchar(50) NOT NULL,
  `check_out` varchar(50) NOT NULL,
  `cabins` varchar(50) NOT NULL,
  `persons` varchar(50) NOT NULL,
  `package` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(250) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `price` double(15,2) NOT NULL,
  `payed` double(15,2) NOT NULL,
  `currency` varchar(5) NOT NULL DEFAULT 'EGP',
  `status` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `boat_id`, `check_in`, `check_out`, `cabins`, `persons`, `package`, `first_name`, `last_name`, `email`, `address`, `phone`, `price`, `payed`, `currency`, `status`, `deleted`, `timestamp`) VALUES
(1, 1, '07/10/2021', '07/21/2021', 'DOUBLE CABINS', '2 Person', 'Honeymooners Package', 'test', 'test', 'hany.hisham@sunrise-resorts.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 35, 0, '2021-07-11 18:36:35'),
(2, 1, '07/10/2021', '07/21/2021', 'DOUBLE CABINS', '1 Person', 'VIP Boat Package', 'test', 'test', 'hany-hesham@outlook.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 36, 0, '2021-07-11 18:36:49'),
(3, 1, '07/10/2021', '07/13/2021', 'LUXURY CABINS', '2 Person', 'Normal Package', 'test', 'test', 'hany.hisham@sunrise-resorts.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 35, 0, '2021-07-11 18:36:38'),
(4, 1, '07/10/2021', '07/21/2021', 'LUXURY CABINS', '2 Person', 'Honeymooners Package', 'test', 'test', 'hany.hisham@sunrise-resorts.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 37, 0, '2021-07-11 18:37:00'),
(5, 1, '07/10/2021', '07/19/2021', 'DOUBLE CABINS', '1 Person', 'Honeymooners Package', 'test', 'test', 'hany.hisham@sunrise-resorts.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 36, 0, '2021-07-11 18:36:51'),
(6, 1, '07/10/2021', '07/20/2021', 'DOUBLE CABINS', '2 Person', 'VIP Boat Package', 'test', 'test', 'hany-hesham@outlook.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 35, 0, '2021-07-11 18:36:40'),
(7, 1, '07/10/2021', '07/15/2021', 'DOUBLE CABINS', '1 Person', 'Honeymooners Package', 'test', 'test', 'mohamed.abuelwafa@sunrise-resorts.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 37, 0, '2021-07-11 18:55:29'),
(8, 1, '07/10/2021', '07/12/2021', 'LUXURY CABINS', '1 Person', 'Honeymooners Package', 'test', 'test', 'hany-hesham@outlook.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 36, 0, '2021-07-11 18:36:53'),
(9, 1, '10/25/2021', '10/26/2021', 'DOUBLE CABINS', '2 Person', 'Normal Package', 'test', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 0.00, 0.00, 'EGP', 1, 0, '2021-10-24 08:33:57'),
(10, 1, '10/25/2021', '10/26/2021', 'DOUBLE CABINS', '2 Person', 'Normal Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 0.00, 0.00, 'EGP', 1, 0, '2021-10-24 08:35:03'),
(11, 1, '10/25/2021', '10/26/2021', 'LUXURY CABINS', '2 Person', 'Honeymooners Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 200.00, 0.00, 'EGP', 1, 0, '2021-10-24 12:41:33'),
(12, 1, '10/25/2021', '10/26/2021', 'LUXURY CABINS', '1 Person', 'Honeymooners Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 200.00, 0.00, 'EGP', 1, 0, '2021-10-24 12:57:24'),
(13, 1, '10/25/2021', '10/26/2021', 'DOUBLE CABINS', '1 Person', 'Honeymooners Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 200.00, 0.00, 'EGP', 1, 0, '2021-10-24 13:01:27'),
(14, 1, '10/25/2021', '10/26/2021', 'LUXURY CABINS', '2 Person', 'Honeymooners Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 200.00, 0.00, 'EGP', 1, 0, '2021-10-24 13:03:06'),
(15, 1, '10/25/2021', '10/26/2021', 'LUXURY CABINS', '2 Person', 'Honeymooners Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 200.00, 0.00, 'EGP', 1, 0, '2021-10-24 13:06:04');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(30) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pax` int(5) NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `offer_id` int(11) NOT NULL,
  `price` decimal(20,2) NOT NULL,
  `currency` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total_cost` decimal(20,2) NOT NULL,
  `status` int(2) NOT NULL,
  `sent_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `type`, `first_name`, `last_name`, `date`, `pax`, `email`, `phone`, `offer_id`, `price`, `currency`, `total_cost`, `status`, `sent_at`, `timestamp`) VALUES
(1, '', 'Menna Allah', 'hisham', '2020-01-16', 3, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', '$', '6000.00', 37, NULL, '2020-01-10 09:09:34'),
(2, '', 'hany', 'hisham', '2020-01-16', 2, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', 'EGP', '4000.00', 35, NULL, '2020-01-10 08:55:52'),
(3, '', 'hany', 'hisham', '2020-01-17', 2, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', '$', '4000.00', 36, NULL, '2020-01-10 09:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `branch_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SUN',
  `logo` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `branch_name`, `group_id`, `code`, `logo`, `deleted`) VALUES
(1, 'Diveing Center', 1, 'DVC', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cabins`
--

CREATE TABLE `cabins` (
  `id` int(5) NOT NULL,
  `boat_id` int(5) NOT NULL DEFAULT 1,
  `title` varchar(150) NOT NULL,
  `remarks` text NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `status` int(5) NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cabins`
--

INSERT INTO `cabins` (`id`, `boat_id`, `title`, `remarks`, `image`, `status`, `rank`, `deleted`, `timestamp`) VALUES
(1, 1, 'DOUBLE CABINS', '', NULL, 2, 1, 0, '2021-07-09 11:47:21'),
(2, 1, 'LUXURY CABINS', '', NULL, 2, 2, 0, '2021-07-09 11:47:21');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`, `lang`) VALUES
('09amtclsr6qva9mnl4m9ch3rgl9hqon5', '::1', 1626102064, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('0dh68jdk8qjtsc33u1258aq4ddm8419u', '::1', 1626097264, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('0e8n20d7d4ja3d1jb7ni314qbu10nb0k', '::1', 1631634100, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633313633343130303b, ''),
('0h8tm9hibv3024mpm52f44jjracnj7ji', '::1', 1626103264, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('0k2u4j1l9d9c0lh8cbl7tfvrl0pc67ko', '::1', 1626028866, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363032383836363b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('160la1sbtgefihbd22s6v0tr5ms3mks7', '::1', 1626100264, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('1khm6f929dqtfojn32cjtgmdr1t68c5m', '::1', 1635162166, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353136323136363b, ''),
('1mb2se9f0muq4tvp1c4fvdgbei1rrohg', '127.0.0.1', 1634303561, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633343330333536303b, ''),
('1r11nmisjoeq8iu9fmu14no7s4c8t15d', '127.0.0.1', 1634303558, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633343330333535353b7265645f75726c7c733a363a226370616e656c223b, ''),
('277i3kmoo9o5u4l8sqq6ijeigdcmnb8r', '::1', 1626096964, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('2fcgmha7vh591ngupd0gusiirh1o1ntp', '::1', 1626098164, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('2idcmga2tliteqvosfcn6rdfg2k558l8', '::1', 1626441052, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363434313035323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b616c6572747c733a383a227375636373657373223b6d73677c733a33393a225265636f7264732068617665206265656e2075706461746564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('2ovi0on0tnskge5o8jin3o4di58o9mgk', '::1', 1635245163, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353234353136333b, ''),
('2slpm5j0ao45q2mkdv1jhts6npsas33c', '::1', 1627380041, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373338303034313b, ''),
('306ps9qk33jkpem7njdsi4nk99l2qimg', '::1', 1626028541, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363032383534313b, ''),
('36e8026mqjm6ck297kclhs5f1t6oqalm', '127.0.0.1', 1626007134, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363030373133343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('3c26vfj1an0vj59u71r799bcn6oik5m7', '::1', 1625836676, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353833363637363b616c6572747c733a383a227375636373657373223b6d73677c733a33353a225265636f726420686173206265656e206164646564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('3h1gavdm5ru6d552g7fjn277sj8hhcue', '::1', 1631378997, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633313337383939373b, ''),
('3lpu616mcituj5gsn98v16r97v4mlj9s', '::1', 1625840251, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353834303235313b616c6572747c733a383a227375636373657373223b6d73677c733a33353a225265636f726420686173206265656e206164646564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('3rk38bbkh4ob49vrahrqlkhva3d3t4de', '::1', 1626438105, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363433383130353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('3tsddb86ntcpu4os1n3gljh89u13hfeg', '::1', 1626100564, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('44p0phkfh94j4isac0psm19tkf9vl2od', '::1', 1626033588, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363033333538383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('4bps54p8kforla9klbur9eaqpotfk839', '::1', 1626099964, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('4dkdav408gjdnui26s44vgahpi9f8sq7', '::1', 1631710916, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633313731303931363b, ''),
('4grvcqlo8fv79o6mpsg11e41laap7dpu', '::1', 1626029404, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363032393430343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('5nmsa6fo9gvtmq61lkdv8qi7e1qphv6a', '::1', 1626101164, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('61f0cttlkrfl0um26p5efaji5pbvvjuc', '::1', 1626437664, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363433373636343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('66ip8m5hgadb3r2cgsq4tfmlr3ond34r', '::1', 1635078671, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353037383637313b, ''),
('6a8mm8n4pg8jjats2et097ir80dqclrl', '::1', 1635158413, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353135383431333b, ''),
('6g814vmqc3cth07j17ijpcq761f2675n', '::1', 1631378997, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633313337383939373b, ''),
('6i260npv1jgu6dpsb4bnsp6il0br8hlv', '::1', 1635079181, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353037393138313b, ''),
('6kq4tvag5r11rpn9hrivfn97avfn42tp', '::1', 1635158732, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353135383733323b, ''),
('6ngenm9epvittms54b6osqlupv0qpn7a', '::1', 1626440728, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363434303732383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b616c6572747c733a383a227375636373657373223b6d73677c733a33393a225265636f7264732068617665206265656e2075706461746564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('6uj9g1hten78n0m8hksqke3mfj92gs7u', '::1', 1626099664, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('719ua5hklqm1quc257dtqs2nboq6ake7', '127.0.0.1', 1626007559, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363030373535393b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('7pmdicq3gvdes66iqbuidavcv7shggj8', '127.0.0.1', 1626000246, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363030303234363b, ''),
('7u9hgf2i177ioq3n0cuflin6br4vdiu2', '::1', 1626095805, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363039353830353b, ''),
('80gagdfmp5kn4b2r69b58p2t54bla3qe', '::1', 1626443295, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363434333236343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b616c6572747c733a383a227375636373657373223b6d73677c733a33393a225265636f7264732068617665206265656e2075706461746564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('82vd85sbb9kf7eh9krqejdp3gjjv9knc', '::1', 1626104164, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('83jeoaqb8dj2vchohfit01nl5ujckchk', '::1', 1631709374, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633313730393337333b, ''),
('8gfdv2k4up2k863kvlfb87mk37tfl4pe', '::1', 1635082284, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353038323238343b, ''),
('8l8p18ng3mmap0647gcttrfpe50u5d53', '::1', 1626033241, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363033333234313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('90kj9bjl46n5nk5vhutrql7o726j9s89', '::1', 1635165286, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353136353238363b, ''),
('9f342lv0at8uehhlsesifa0p1i96a1bg', '::1', 1625839820, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353833393832303b616c6572747c733a383a227375636373657373223b6d73677c733a33353a225265636f726420686173206265656e206164646564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('9iucq8dj4d83ktildevvcg84n1r2peic', '::1', 1626441734, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363434313733343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b616c6572747c733a383a227375636373657373223b6d73677c733a33393a225265636f7264732068617665206265656e2075706461746564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('9otd1gqf3crf91hbiutj36i32ig0fjv5', '::1', 1635076338, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353037363333383b, ''),
('aeq18dfu22qpevi4sv7uk06h0d3piars', '127.0.0.1', 1626013106, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363031323438363b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('aojikp50rgb4tl1d5qd5dqbchgkgnc11', '::1', 1635158110, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353135383131303b, ''),
('apom5kn48t91nko036n21tbmfr3noqan', '::1', 1626097864, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('avecj723la3730uglmvaog3i6cmv41r6', '::1', 1626437023, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363433373032333b7265645f75726c7c733a363a226370616e656c223b, ''),
('bfauv6v74asmf4hqb5ph3d6b8cg4g4aa', '::1', 1626103864, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('bl41jvb9iul85mi1beusv602b5bg8rhr', '::1', 1626096664, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('c23bvikh8t76u16o8uodl7htr597to09', '::1', 1626439967, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363433393936373b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b616c6572747c733a383a227375636373657373223b6d73677c733a33393a225265636f7264732068617665206265656e2075706461746564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('c6m4m2mi6enhgvr1hqtq3k1cd5n0qrrg', '::1', 1625837885, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353833373838353b616c6572747c733a383a227375636373657373223b6d73677c733a33353a225265636f726420686173206265656e206164646564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('c8fgli0oqbvld4a0dca8jsl1ni4jh9b9', '::1', 1626031701, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363033313730313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('cdr3lv9d5v396mkm96g2e4l7d4q5luum', '::1', 1626102664, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('cocc3j08vn06n71pc9p3qjueivdsj4p6', '::1', 1635157808, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353135373830383b, ''),
('ebs9impcjej2vijsqkhjd9d3u6n394c8', '::1', 1626098764, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('edf2vr2fak5b01j8mgmare7hhd8bos0u', '::1', 1626102364, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('esja3d48622lkjp0h65u9blt1au43t9n', '127.0.0.1', 1626003768, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363030333736383b, ''),
('f2r59th952ohuqurcmt5lp1qbmvloit2', '::1', 1626096364, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('fjttu0rljgc7clcku535fsd1meof0no8', '::1', 1626441357, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363434313335373b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b616c6572747c733a383a227375636373657373223b6d73677c733a33393a225265636f7264732068617665206265656e2075706461746564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('fmgb1u3q7i0vcpl5lpu2k6fp77vq44sf', '::1', 1626032921, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363033323932313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('g1h0eoom61r9v68vf49jipq4nppcu5gd', '::1', 1626101464, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('giv8dj64jddq50kn87v478l3e3vrps0u', '::1', 1635159237, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353135393233373b, ''),
('goch7cekonsuu07dpsb7iv40vtmkpp9p', '::1', 1635162475, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353136323437353b, ''),
('gv25quqtgnd1oi0a0sh0dqbmubkra43e', '::1', 1626097564, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('h67041v0nl5lib922ejgobh1hdlm6iet', '::1', 1635157501, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353135373530313b, ''),
('hbq5fctm1kfsjosq531bbjkaoa2ku4ui', '::1', 1625830514, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353833303531343b, ''),
('ho8vl1msddj5ioqn0dtitbtiqt0u5odh', '127.0.0.1', 1625999942, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353939393934323b, ''),
('i2cvi89t4aomuv3b6jh5lke338p67fga', '::1', 1631710954, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633313731303931363b, ''),
('ibr9k5jiidt25iobjsuipi0d0758aia8', '::1', 1626102964, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('ich6nqk1b0bfieo4cs0i5a0io3vmqfcs', '::1', 1625837137, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353833373133373b616c6572747c733a383a227375636373657373223b6d73677c733a33353a225265636f726420686173206265656e206164646564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('jd5kc4f76plf5kvc0ufjcqv03jp9nt93', '::1', 1626099064, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('jdr0fcha7ttr9c0bk6el5sdgi0thaj73', '::1', 1635082285, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353038323238343b, ''),
('jhqdoo0d99cncohg261lu2pjqm0avj87', '::1', 1626096064, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363039363035323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('k0oqausal5m9n7k9lggc5d5vd9e872qe', '::1', 1631378473, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633313337383437333b, ''),
('k82ik06tkhf6rb80vf10t4im78rmivam', '::1', 1635076999, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353037363939393b, ''),
('khcd5j68vu26unjiuv5dq4i8ppohvkjp', '::1', 1626439666, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363433393636363b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('kjttu41hgqm695qes2rfvgb6glfsfrh3', '::1', 1635064505, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353036343437353b, ''),
('kmo4h7jfpval9fe0t7i36e5k4g2f91v2', '::1', 1625831580, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353833313538303b, ''),
('l2q6dkfbhsqot8b566570a7kebkgogc7', '::1', 1626098464, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('lt73bhve501jpk4b9blfgag2fgdmt1ua', '::1', 1627382272, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373338323237323b, ''),
('n1iobqtmddcbai4krrtaaa86fh2hpcs6', '::1', 1625836263, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353833363236333b, ''),
('njbp28rjuesd57e74j3v4u96b9ua1875', '::1', 1626438525, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363433383532353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('o0q1phfs8b2d75a5escvnbcj324e18uu', '::1', 1635080219, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353038303231393b, ''),
('o2kgsr26f7ir7mjj9sqlrvjekulp1a90', '::1', 1626033851, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363033333538383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('pd0urfnd743n7d1859oeurvb1057iev5', '::1', 1625841836, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353834313833363b616c6572747c733a383a227375636373657373223b6d73677c733a33353a225265636f726420686173206265656e206164646564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('pescgn60rgntreeeejhlgurg2odul8fd', '127.0.0.1', 1625999320, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353939393332303b, ''),
('pjvh8i351388r5vsverjqc0nkj51bo5a', '::1', 1635160558, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353136303535383b, ''),
('prfiv52mlocqfsqo1cb3jst9gskdcmq6', '::1', 1631710600, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633313731303630303b, ''),
('q1809fni3nvdte4ifoaj3n1b3lglgvvd', '::1', 1631634100, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633313633343130303b, ''),
('q68trm7i25q9hg444d5vpjcclfvt3nd7', '::1', 1626101764, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('qlssrpru7fjt5523f4kqgma7im9q0svr', '::1', 1626029742, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363032393734323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('qp57unf9lrd9fkm36mleje49pltpfia4', '::1', 1635064475, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353036343437353b, ''),
('qsv9jb76r4stl6336hllpfu4teg0jscp', '::1', 1626443264, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363434333236343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b616c6572747c733a383a227375636373657373223b6d73677c733a33393a225265636f7264732068617665206265656e2075706461746564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('qt2auast7t60atbsf6bb5n81aad89d3q', '::1', 1625837537, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353833373533373b616c6572747c733a383a227375636373657373223b6d73677c733a33353a225265636f726420686173206265656e206164646564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('qunkcovbpipgfd47qb50j3ut2il7jtbk', '::1', 1625831995, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353833313939353b, ''),
('quvmghk1tsvoli89683i925ln54k3ihb', '::1', 1635062121, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353036323131373b, ''),
('r2uq8k1tkop1pv7st6j5ikqstmpoh1he', '::1', 1625841920, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353834313833363b616c6572747c733a383a227375636373657373223b6d73677c733a33353a225265636f726420686173206265656e206164646564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('r5ctarc3v6od6e0op18ioc4ocg96r1vn', '::1', 1626028541, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363032383534313b7265645f75726c7c733a363a226370616e656c223b, ''),
('reoscgl69hi81t87ppu12jifivftobbq', '::1', 1635165445, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353136353238363b, ''),
('rgk76cko9i9qj0000qblmibvutb74tkt', '::1', 1627382297, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373338323237323b, ''),
('rs4fbk7d0ms16ic69vd6e98a77slhagf', '::1', 1635245164, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353234353136333b, ''),
('s4sl2viemqsoght2u2djtnf88lhptvug', '::1', 1635160226, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353136303232363b, ''),
('sjgcirnsonr3eaj8idr6uhl3upq3cm8v', '::1', 1626096052, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363039363035323b7265645f75726c7c733a363a226370616e656c223b, ''),
('sp1qgbf5l5cbl7oilg27kg2bfocrvboc', '127.0.0.1', 1631632735, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633313633323733353b, ''),
('t2j63ium32pm5s383ic9790rm6m87ebr', '::1', 1635080563, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353038303536333b, ''),
('t60o305hueobmrdohk82eoiog7gbdtct', '::1', 1626100864, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('t8o8m2844h89g7d9voso0at444e7ikce', '::1', 1626437023, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363433373032333b, ''),
('tboelpmboe4njal4i2086v2agbclidcq', '127.0.0.1', 1625999635, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353939393633353b, ''),
('td92ashcsrfvmaji3qgit1b3sk4ouh6o', '127.0.0.1', 1626000646, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363030303634363b, ''),
('tg4hjj2np4gm6fi2hquktr80t0b2nt17', '::1', 1625839018, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353833393031383b616c6572747c733a383a227375636373657373223b6d73677c733a33353a225265636f726420686173206265656e206164646564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('tlmgbna1sqk7ua44tbnk40hbn30krbt3', '::1', 1626440425, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363434303432353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b616c6572747c733a383a227375636373657373223b6d73677c733a33393a225265636f7264732068617665206265656e2075706461746564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('tqrc6mbidgrkgql8q2fsfbo329u8nm7f', '127.0.0.1', 1626012486, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363031323438363b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('tso446lomj3vn8hl1onl950tcmjndaoj', '::1', 1635162852, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633353136323835323b, ''),
('uc2a9v8uk65nkkulgb4ekduk8mhlibil', '127.0.0.1', 1626004637, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632363030343633373b, ''),
('uq7b3hksj6gndq8n9rnhotfq8h9ouhji', '::1', 1626103564, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('v62rkh7rf54hgtukmd9bb51uhn44k7vb', '::1', 1626099364, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('vp05t7p7dd2fik0ka5ctdfitsdcfihbq', '::1', 1625835751, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632353833353735313b, '');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `client_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cash` decimal(20,2) NOT NULL,
  `phone` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cont_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `updated` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clients_mail_queue`
--

CREATE TABLE `clients_mail_queue` (
  `id` int(30) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_info` int(11) DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(2) NOT NULL,
  `sent_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clients_mail_queue`
--

INSERT INTO `clients_mail_queue` (`id`, `type`, `client_info`, `client_name`, `email`, `subject`, `message`, `status`, `sent_at`, `timestamp`) VALUES
(1, '', NULL, '', '', '', NULL, 24, NULL, '2019-11-13 16:54:49'),
(2, '', NULL, '', '', '', NULL, 24, NULL, '2019-11-13 20:35:28'),
(3, '', NULL, '', '', '', NULL, 24, NULL, '2019-11-13 20:36:07'),
(4, '', NULL, 'محمد طرطور', '', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:11:59'),
(5, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:16:24'),
(6, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:20:31'),
(7, '', NULL, 'محمد طرطور', 'admin@admin.com', 'dsds', 'ttttttttttttt', 24, NULL, '2019-11-13 21:22:52'),
(8, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'sfdsfdsdf', 24, NULL, '2019-11-13 21:25:26'),
(9, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'sadfsdfsdf', 24, NULL, '2019-11-13 21:26:18'),
(10, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'scsdfsdf', 24, NULL, '2019-11-13 21:26:54'),
(11, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'rewrwer', 24, NULL, '2019-11-13 21:29:03'),
(12, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:30:30'),
(13, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'adsfsdfsdfsdfsdfdsf', 24, NULL, '2019-11-16 07:30:32'),
(14, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-09 07:59:43'),
(15, '', NULL, 'Johan ', 'mvmservice@hotmail.com', 'Diving center hurghada', 'Hello there. \r\nIs iT true that dive more group at the Beach of mamlouk palace and garden beach is part of your company? We Will be there in a week and going the get Some info to take diving course. We saw your website On there Facebook.   Thnx.', 24, NULL, '2019-12-11 18:00:15'),
(16, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-13 22:52:51'),
(17, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-14 16:43:48'),
(18, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-15 22:16:21'),
(19, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-18 10:11:56'),
(20, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-18 13:09:23'),
(21, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-19 04:44:36'),
(22, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-19 11:46:10'),
(23, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-20 08:43:54'),
(24, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-20 10:41:48'),
(25, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 01:21:04'),
(26, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 07:29:10'),
(27, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 10:44:01'),
(28, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 13:50:01'),
(29, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-22 02:38:28'),
(30, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-22 08:28:23'),
(31, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-22 12:11:19'),
(32, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 02:15:25'),
(33, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 09:48:54'),
(34, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 17:21:37'),
(35, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 21:50:13'),
(36, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-24 02:32:35'),
(37, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-24 23:22:45'),
(38, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-25 08:19:19'),
(39, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-25 09:41:02'),
(40, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-26 05:12:04'),
(41, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-26 08:49:29'),
(42, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 05:21:45'),
(43, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 07:57:24'),
(44, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 14:03:47'),
(45, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 16:33:17'),
(46, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-03 08:23:35'),
(47, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-03 10:48:46'),
(48, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-04 04:50:45'),
(49, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-04 08:58:09'),
(50, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-04 11:04:45'),
(51, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-05 08:17:21'),
(52, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-06 05:39:13'),
(53, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-06 07:50:32'),
(54, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 02:42:28'),
(55, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 06:18:29'),
(56, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 10:58:39'),
(57, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 13:54:56');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(20) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `rank`, `deleted`) VALUES
(1, 'عز', 1, 0),
(2, 'بشاى', 2, 0),
(3, 'سعودى سابك', 3, 0),
(4, 'سرحان', 4, 0),
(5, 'سعودى راجحى', 5, 0),
(6, 'مراكبى', 6, 0),
(7, 'بيانكو', 7, 0),
(8, 'جيوشى', 8, 0),
(9, 'عنتر', 9, 0),
(10, 'مصريين', 10, 0),
(11, 'المصرية', 11, 0),
(12, 'السويدى', 12, 0),
(13, 'الجيش بنى سويف', 13, 0),
(14, 'الجيش العريش', 14, 0),
(15, 'القومية', 15, 0),
(16, 'طرة', 16, 0),
(17, 'المسلح', 17, 0),
(18, 'السويس', 18, 0),
(19, 'سينا', 19, 0),
(20, 'رويال', 20, 0),
(21, 'الدولية', 21, 0),
(22, 'البلاح', 22, 0),
(23, 'نجمة سيناء مصيص', 23, 0),
(24, 'عثمان', 24, 0),
(25, 'بدون', 25, 0);

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `symbol`, `name`, `isdefault`) VALUES
(1, '$', 'USD', 0),
(2, '€', 'EUR', 0),
(3, 'EGP', 'EGP', 1);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `dep_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `dep_name`, `code`, `rank`) VALUES
(1, 'الإدارة', 'MG', 1),
(2, 'الحسابات', 'ACC', 2);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(5) NOT NULL,
  `location` varchar(100) NOT NULL,
  `event` varchar(250) NOT NULL,
  `describtion` text NOT NULL,
  `rank` int(5) NOT NULL,
  `image` varchar(100) NOT NULL,
  `toped` double(5,2) NOT NULL,
  `lefted` double(5,2) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `location`, `event`, `describtion`, `rank`, `image`, `toped`, `lefted`, `active`, `deleted`) VALUES
(1, 'Luxor', '', 'Is a city on the east bank of the nile river in southern Egypt.', 1, 'Pointer.png', 34.50, 54.50, 1, 0),
(2, 'El Kab', '', 'Is an upper Egyptian site on the east bank of the Nile at the mounts of the wadi hillal.', 2, 'Pointer.png', 37.00, 52.50, 1, 0),
(3, 'Edfu', '', 'Is an Egyptian city, edfu is site of the Ptolemaic temple of Horus.', 3, 'Pointer.png', 51.50, 57.50, 1, 0),
(4, 'Gebel Silsila', '', 'Is 65km of aswan in upper Egypt where the cliffs on both sides close to the narrowest point along the length of the entire nile.', 4, 'Pointer.png', 57.70, 59.30, 1, 0),
(5, 'Kom Ombo', '', 'Is an agricultural town in Egypt famous for the temple of kom ombo, meaning city of gold.', 5, 'Pointer.png', 60.50, 60.00, 1, 0),
(6, 'Aswan', '', 'A city on the Nile river. Has been southern Egypt’s strategic and commercial gateway since antiquity.', 6, 'Pointer.png', 66.00, 57.70, 1, 0),
(7, 'Philae', '', 'Is an island in the reservoir of the aswan low dam.', 7, 'Pointer.png', 67.70, 58.40, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `form_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `font_icons`
--

CREATE TABLE `font_icons` (
  `id` int(11) NOT NULL,
  `icon_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `font_icons`
--

INSERT INTO `font_icons` (`id`, `icon_name`, `rank`) VALUES
(1, 'fas fa-arrow-alt-circle-left', 1),
(2, 'fa-music', 2),
(3, 'fa-search', 3),
(4, 'fa-envelope-o', 4),
(5, 'fa-heart', 5),
(6, 'fa-star', 6),
(7, 'fa-star-o', 7),
(8, 'fa-user', 8),
(9, 'fa-film', 9),
(10, 'fa-th-large', 10),
(11, 'fa-th', 11),
(12, 'fa-th-list', 12),
(13, 'fa-check', 13),
(14, 'fa-remove', 14),
(15, 'fa-close', 15),
(16, 'fa-times', 16),
(17, 'fa-search-plus', 17),
(18, 'fa-search-minus', 18),
(19, 'fa-power-off', 19),
(20, 'fa-signal', 20),
(21, 'fa-gear', 21),
(22, 'fa-cog', 22),
(23, 'fa-trash-o', 23),
(24, 'fa-home', 24),
(25, 'fa-file-o', 25),
(26, 'fa-clock-o', 26),
(27, 'fa-road', 27),
(28, 'fa-download', 28),
(29, 'fa-arrow-circle-o-do', 29),
(30, 'fa-arrow-circle-o-up', 30),
(31, 'fa-inbox', 31),
(32, 'fa-play-circle-o', 32),
(33, 'fa-rotate-right', 33),
(34, 'fa-repeat', 34),
(35, 'fa-refresh', 35),
(36, 'fa-list-alt', 36),
(37, 'fa-lock', 37),
(38, 'fa-flag', 38),
(39, 'fa-headphones', 39),
(40, 'fa-volume-off', 40),
(41, 'fa-volume-down', 41),
(42, 'fa-volume-up', 42),
(43, 'fa-qrcode', 43),
(44, 'fa-barcode', 44),
(45, 'fa-tag', 45),
(46, 'fa-tags', 46),
(47, 'fa-book', 47),
(48, 'fa-bookmark', 48),
(49, 'fa-print f', 49),
(50, 'fa-camera', 50),
(51, 'fa-font', 51),
(52, 'fa-bold', 52),
(53, 'fa-italic', 53),
(54, 'fa-text-height', 54),
(55, 'fa-text-width', 55),
(56, 'fa-align-left', 56),
(57, 'fa-align-center', 57),
(58, 'fa-align-right', 58),
(59, 'fa-align-justify', 59),
(60, 'fa-list', 60),
(61, 'fa-dedent', 61),
(62, 'fa-outdent', 62),
(63, 'fa-indent', 63),
(64, 'fa-video-camera', 64),
(65, 'fa-photo', 65),
(66, 'fa-image', 66),
(67, 'fa-picture-o', 67),
(68, 'fa-pencil', 68),
(69, 'fa-map-marker', 69),
(70, 'fa-adjust', 70),
(71, 'fa-tint', 71),
(72, 'fa-edit', 72),
(73, 'fa-pencil-square-o', 73),
(74, 'fa-share-square-o', 74),
(75, 'fa-check-square-o', 75),
(76, 'fa-arrows', 76),
(77, 'fa-step-backward', 77),
(78, 'fa-fast-backward', 78),
(79, 'fa-backward', 79),
(80, 'fa-play', 80),
(81, 'fa-pause', 81),
(82, 'fa-stop', 82),
(83, 'fa-forward', 83),
(84, 'fa-fast-forward', 84),
(85, 'fa-step-forward', 85),
(86, 'fa-eject', 86),
(87, 'fa-chevron-left', 87),
(88, 'fa-chevron-right', 88),
(89, 'fa-plus-circle', 89),
(90, 'fa-minus-circle', 90),
(91, 'fa-times-circle', 91),
(92, 'fa-check-circle', 92),
(93, 'fa-question-circle', 93),
(94, 'fa-info-circle', 94),
(95, 'fa-crosshairs', 95),
(96, 'fa-times-circle-o', 96),
(97, 'fa-check-circle-o', 97),
(98, 'fa-ban', 98),
(99, 'fa-arrow-left', 99),
(100, 'fa-arrow-right', 100),
(101, 'fa-arrow-up', 101),
(102, 'fa-arrow-down', 102),
(103, 'fa-mail-forward', 103),
(104, 'fa-share', 104),
(105, 'fa-expand', 105),
(106, 'fa-compress', 106),
(107, 'fa-plus', 107),
(108, 'fa-minus', 108),
(109, 'fa-asterisk', 109),
(110, 'fa-exclamation-circl', 110),
(111, 'fa-gift', 111),
(112, 'fa-leaf', 112),
(113, 'fa-fire', 113),
(114, 'fa-eye', 114),
(115, 'fa-eye-slash', 115),
(116, 'fa-warning', 116),
(117, 'fa-exclamation-trian', 117),
(118, 'fa-plane', 118),
(119, 'fa-calendar', 119),
(120, 'fa-random', 120),
(121, 'fa-comment', 121),
(122, 'fa-magnet', 122),
(123, 'fa-chevron-up', 123),
(124, 'fa-chevron-down', 124),
(125, 'fa-retweet', 125),
(126, 'fa-shopping-cart', 126),
(127, 'fa-folder', 127),
(128, 'fa-folder-open', 128),
(129, 'fa-arrows-v', 129),
(130, 'fa-arrows-h', 130),
(131, 'fa-bar-chart-o', 131),
(132, 'fa-bar-chart', 132),
(133, 'fa-twitter-square', 133),
(134, 'fa-facebook-square', 134),
(135, 'fa-camera-retro', 135),
(136, 'fa-key', 136),
(137, 'fa-gears', 137),
(138, 'fa-cogs', 138),
(139, 'fa-comments', 139),
(140, 'fa-thumbs-o-up', 140),
(141, 'fa-thumbs-o-down', 141),
(142, 'fa-star-half', 142),
(143, 'fa-heart-o', 143),
(144, 'fa-sign-out', 144),
(145, 'fa-linkedin-square', 145),
(146, 'fa-thumb-tack', 146),
(147, 'fa-external-link', 147),
(148, 'fa-sign-in', 148),
(149, 'fa-trophy', 149),
(150, 'fa-github-square', 150),
(151, 'fa-upload', 151),
(152, 'fa-lemon-o', 152),
(153, 'fa-phone', 153),
(154, 'fa-square-o', 154),
(155, 'fa-bookmark-o', 155),
(156, 'fa-phone-square', 156),
(157, 'fa-twitter', 157),
(158, 'fa-facebook-f', 158),
(159, 'fa-facebook', 159),
(160, 'fa-github', 160),
(161, 'fa-unlock', 161),
(162, 'fa-credit-card', 162),
(163, 'fa-rss', 163),
(164, 'fa-hdd-o', 164),
(165, 'fa-bullhorn', 165),
(166, 'fa-bell', 166),
(167, 'fa-certificate', 167),
(168, 'fa-hand-o-right', 168),
(169, 'fa-hand-o-left', 169),
(170, 'fa-hand-o-up', 170),
(171, 'fa-hand-o-down', 171),
(172, 'fa-arrow-circle-left', 172),
(173, 'fa-arrow-circle-righ', 173),
(174, 'fa-arrow-circle-up', 174),
(175, 'fa-arrow-circle-down', 175),
(176, 'fa-globe', 176),
(177, 'fa-wrench', 177),
(178, 'fa-tasks', 178),
(179, 'fa-filter', 179),
(180, 'fa-briefcase', 180),
(181, 'fa-arrows-alt', 181),
(182, 'fa-group', 182),
(183, 'fa-users', 183),
(184, 'fa-chain', 184),
(185, 'fa-link', 185),
(186, 'fa-cloud', 186),
(187, 'fa-flask', 187),
(188, 'fa-cut', 188),
(189, 'fa-scissors', 189),
(190, 'fa-copy', 190),
(191, 'fa-files-o', 191),
(192, 'fa-paperclip', 192),
(193, 'fa-save', 193),
(194, 'fa-floppy-o', 194),
(195, 'fa-square', 195),
(196, 'fa-navicon', 196),
(197, 'fa-reorder', 197),
(198, 'fa-bars', 198),
(199, 'fa-list-ul', 199),
(200, 'fa-list-ol', 200),
(201, 'fa-strikethrough', 201),
(202, 'fa-underline', 202),
(203, 'fa-table', 203),
(204, 'fa-magic', 204),
(205, 'fa-truck', 205),
(206, 'fa-pinterest', 206),
(207, 'fa-pinterest-square', 207),
(208, 'fa-google-plus-squar', 208),
(209, 'fa-google-plus', 209),
(210, 'fa-money', 210),
(211, 'fa-caret-down', 211),
(212, 'fa-caret-up', 212),
(213, 'fa-caret-left', 213),
(214, 'fa-caret-right', 214),
(215, 'fa-columns', 215),
(216, 'fa-unsorted', 216),
(217, 'fa-sort', 217),
(218, 'fa-sort-down', 218),
(219, 'fa-sort-desc', 219),
(220, 'fa-sort-up', 220),
(221, 'fa-sort-asc', 221),
(222, 'fa-envelope', 222),
(223, 'fa-linkedin', 223),
(224, 'fa-rotate-left', 224),
(225, 'fa-undo', 225),
(226, 'fa-legal', 226),
(227, 'fa-gavel', 227),
(228, 'fa-dashboard', 228),
(229, 'fa-tachometer', 229),
(230, 'fa-comment-o', 230),
(231, 'fa-comments-o', 231),
(232, 'fa-flash', 232),
(233, 'fa-bolt', 233),
(234, 'fa-sitemap', 234),
(235, 'fa-umbrella', 235),
(236, 'fa-paste', 236),
(237, 'fa-clipboard', 237),
(238, 'fa-lightbulb-o', 238),
(239, 'fa-exchange', 239),
(240, 'fa-cloud-download', 240),
(241, 'fa-cloud-upload', 241),
(242, 'fa-user-md', 242),
(243, 'fa-stethoscope', 243),
(244, 'fa-suitcase', 244),
(245, 'fa-bell-o', 245),
(246, 'fa-coffee', 246),
(247, 'fa-cutlery', 247),
(248, 'fa-file-text-o', 248),
(249, 'fa-building-o', 249),
(250, 'fa-hospital-o', 250),
(251, 'fa-ambulance', 251),
(252, 'fa-medkit', 252),
(253, 'fa-fighter-jet', 253),
(254, 'fa-beer', 254),
(255, 'fa-h-square', 255),
(256, 'fa-plus-square', 256),
(257, 'fa-angle-double-left', 257),
(258, 'fa-angle-double-righ', 258),
(259, 'fa-angle-double-up', 259),
(260, 'fa-angle-double-down', 260),
(261, 'fa-angle-left', 261),
(262, 'fa-angle-right', 262),
(263, 'fa-angle-up', 263),
(264, 'fa-angle-down', 264),
(265, 'fa-desktop', 265),
(266, 'fa-laptop', 266),
(267, 'fa-tablet', 267),
(268, 'fa-mobile-phone', 268),
(269, 'fa-mobile', 269),
(270, 'fa-circle-o', 270),
(271, 'fa-quote-left', 271),
(272, 'fa-quote-right', 272),
(273, 'fa-spinner', 273),
(274, 'fa-circle', 274),
(275, 'fa-mail-reply', 275),
(276, 'fa-reply', 276),
(277, 'fa-github-alt', 277),
(278, 'fa-folder-o', 278),
(279, 'fa-folder-open-o', 279),
(280, 'fa-smile-o', 280),
(281, 'fa-frown-o', 281),
(282, 'fa-meh-o', 282),
(283, 'fa-gamepad', 283),
(284, 'fa-keyboard-o', 284),
(285, 'fa-flag-o', 285),
(286, 'fa-flag-checkered', 286),
(287, 'fa-terminal', 287),
(288, 'fa-code', 288),
(289, 'fa-mail-reply-all', 289),
(290, 'fa-reply-all', 290),
(291, 'fa-star-half-empty', 291),
(292, 'fa-star-half-full', 292),
(293, 'fa-star-half-o', 293),
(294, 'fa-location-arrow', 294),
(295, 'fa-crop', 295),
(296, 'fa-code-fork', 296),
(297, 'fa-unlink', 297),
(298, 'fa-chain-broken', 298),
(299, 'fa-question', 299),
(300, 'fa-info', 300),
(301, 'fa-exclamation', 301),
(302, 'fa-superscript', 302),
(303, 'fa-subscript', 303),
(304, 'fa-eraser', 304),
(305, 'fa-puzzle-piece', 305),
(306, 'fa-microphone', 306),
(307, 'fa-microphone-slash', 307),
(308, 'fa-shield', 308),
(309, 'fa-calendar-o', 309),
(310, 'fa-fire-extinguisher', 310),
(311, 'fa-rocket', 311),
(312, 'fa-maxcdn', 312),
(313, 'fa-chevron-circle-le', 313),
(314, 'fa-chevron-circle-ri', 314),
(315, 'fa-chevron-circle-up', 315),
(316, 'fa-chevron-circle-do', 316),
(317, 'fa-html5', 317),
(318, 'fa-css3', 318),
(319, 'fa-anchor', 319),
(320, 'fa-unlock-alt', 320),
(321, 'fa-bullseye', 321),
(322, 'fa-ellipsis-h', 322),
(323, 'fa-ellipsis-v', 323),
(324, 'fa-rss-square', 324),
(325, 'fa-play-circle', 325),
(326, 'fa-ticket', 326),
(327, 'fa-minus-square', 327),
(328, 'fa-minus-square-o', 328),
(329, 'fa-level-up', 329),
(330, 'fa-level-down', 330),
(331, 'fa-check-square', 331),
(332, 'fa-pencil-square', 332),
(333, 'fa-external-link-squ', 333),
(334, 'fa-share-square', 334),
(335, 'fa-compass', 335),
(336, 'fa-toggle-down', 336),
(337, 'fa-caret-square-o-do', 337),
(338, 'fa-toggle-up', 338),
(339, 'fa-caret-square-o-up', 339),
(340, 'fa-toggle-right', 340),
(341, 'fa-caret-square-o-ri', 341),
(342, 'fa-euro', 342),
(343, 'fa-eur', 343),
(344, 'fa-gbp', 344),
(345, 'fa-dollar', 345),
(346, 'fa-usd', 346),
(347, 'fa-rupee', 347),
(348, 'fa-inr', 348),
(349, 'fa-cny', 349),
(350, 'fa-rmb', 350),
(351, 'fa-yen', 351),
(352, 'fa-jpy', 352),
(353, 'fa-ruble', 353),
(354, 'fa-rouble', 354),
(355, 'fa-rub', 355),
(356, 'fa-won', 356),
(357, 'fa-krw', 357),
(358, 'fa-bitcoin', 358),
(359, 'fa-btc', 359),
(360, 'fa-file', 360),
(361, 'fa-file-text', 361),
(362, 'fa-sort-alpha-asc', 362),
(363, 'fa-sort-alpha-desc', 363),
(364, 'fa-sort-amount-asc', 364),
(365, 'fa-sort-amount-desc', 365),
(366, 'fa-sort-numeric-asc', 366),
(367, 'fa-sort-numeric-desc', 367),
(368, 'fa-thumbs-up', 368),
(369, 'fa-thumbs-down', 369),
(370, 'fa-youtube-square', 370),
(371, 'fa-youtube', 371),
(372, 'fa-xing', 372),
(373, 'fa-xing-square', 373),
(374, 'fa-youtube-play', 374),
(375, 'fa-dropbox', 375),
(376, 'fa-stack-overflow', 376),
(377, 'fa-instagram', 377),
(378, 'fa-flickr', 378),
(379, 'fa-adn', 379),
(380, 'fa-bitbucket', 380),
(381, 'fa-bitbucket-square', 381),
(382, 'fa-tumblr', 382),
(383, 'fa-tumblr-square', 383),
(384, 'fa-long-arrow-down', 384),
(385, 'fa-long-arrow-up', 385),
(386, 'fa-long-arrow-left', 386),
(387, 'fa-long-arrow-right', 387),
(388, 'fa-apple', 388),
(389, 'fa-windows', 389),
(390, 'fa-android', 390),
(391, 'fa-linux', 391),
(392, 'fa-dribbble', 392),
(393, 'fa-skype', 393),
(394, 'fa-foursquare', 394),
(395, 'fa-trello', 395),
(396, 'fa-female', 396),
(397, 'fa-male', 397),
(398, 'fa-gittip', 398),
(399, 'fa-gratipay', 399),
(400, 'fa-sun-o', 400),
(401, 'fa-moon-o', 401),
(402, 'fa-archive', 402),
(403, 'fa-bug', 403),
(404, 'fa-vk', 404),
(405, 'fa-weibo', 405),
(406, 'fa-renren', 406),
(407, 'fa-pagelines', 407),
(408, 'fa-stack-exchange', 408),
(409, 'fa-arrow-circle-o-ri', 409),
(410, 'fa-arrow-circle-o-le', 410),
(411, 'fa-toggle-left', 411),
(412, 'fa-caret-square-o-le', 412),
(413, 'fa-dot-circle-o', 413),
(414, 'fa-wheelchair', 414),
(415, 'fa-vimeo-square', 415),
(416, 'fa-turkish-lira', 416),
(417, 'fa-try', 417),
(418, 'fa-plus-square-o', 418),
(419, 'fa-space-shuttle', 419),
(420, 'fa-slack', 420),
(421, 'fa-envelope-square', 421),
(422, 'fa-wordpress', 422),
(423, 'fa-openid', 423),
(424, 'fa-institution', 424),
(425, 'fa-bank', 425),
(426, 'fa-university', 426),
(427, 'fa-mortar-board', 427),
(428, 'fa-graduation-cap', 428),
(429, 'fa-yahoo', 429),
(430, 'fa-google', 430),
(431, 'fa-reddit', 431),
(432, 'fa-reddit-square', 432),
(433, 'fa-stumbleupon-circl', 433),
(434, 'fa-stumbleupon', 434),
(435, 'fa-delicious', 435),
(436, 'fa-digg', 436),
(437, 'fa-pied-piper', 437),
(438, 'fa-pied-piper-alt', 438),
(439, 'fa-drupal', 439),
(440, 'fa-joomla', 440),
(441, 'fa-language', 441),
(442, 'fa-fax', 442),
(443, 'fa-building', 443),
(444, 'fa-child', 444),
(445, 'fa-paw', 445),
(446, 'fa-spoon', 446),
(447, 'fa-cube', 447),
(448, 'fa-cubes', 448),
(449, 'fa-behance', 449),
(450, 'fa-behance-square', 450),
(451, 'fa-steam', 451),
(452, 'fa-steam-square', 452),
(453, 'fa-recycle', 453),
(454, 'fa-automobile', 454),
(455, 'fa-car', 455),
(456, 'fa-cab', 456),
(457, 'fa-taxi', 457),
(458, 'fa-tree', 458),
(459, 'fa-spotify', 459),
(460, 'fa-deviantart', 460),
(461, 'fa-soundcloud', 461),
(462, 'fa-database', 462),
(463, 'fa-file-pdf-o', 463),
(464, 'fa-file-word-o', 464),
(465, 'fa-file-excel-o', 465),
(466, 'fa-file-powerpoint-o', 466),
(467, 'fa-file-photo-o', 467),
(468, 'fa-file-picture-o', 468),
(469, 'fa-file-image-o', 469),
(470, 'fa-file-zip-o', 470),
(471, 'fa-file-archive-o', 471),
(472, 'fa-file-sound-o', 472),
(473, 'fa-file-audio-o', 473),
(474, 'fa-file-movie-o', 474),
(475, 'fa-file-video-o', 475),
(476, 'fa-file-code-o', 476),
(477, 'fa-vine', 477),
(478, 'fa-codepen', 478),
(479, 'fa-jsfiddle', 479),
(480, 'fa-life-bouy', 480),
(481, 'fa-life-buoy', 481),
(482, 'fa-life-saver', 482),
(483, 'fa-support', 483),
(484, 'fa-life-ring', 484),
(485, 'fa-circle-o-notch', 485),
(486, 'fa-ra', 486),
(487, 'fa-rebel', 487),
(488, 'fa-ge', 488),
(489, 'fa-empire', 489),
(490, 'fa-git-square', 490),
(491, 'fa-git', 491),
(492, 'fa-hacker-news', 492),
(493, 'fa-tencent-weibo', 493),
(494, 'fa-qq', 494),
(495, 'fa-wechat', 495),
(496, 'fa-weixin', 496),
(497, 'fa-send', 497),
(498, 'fa-paper-plane', 498),
(499, 'fa-send-o', 499),
(500, 'fa-paper-plane-o', 500),
(501, 'fa-history', 501),
(502, 'fa-genderless', 502),
(503, 'fa-circle-thin', 503),
(504, 'fa-header', 504),
(505, 'fa-paragraph', 505),
(506, 'fa-sliders', 506),
(507, 'fa-share-alt', 507),
(508, 'fa-share-alt-square', 508),
(509, 'fa-bomb', 509),
(510, 'fa-soccer-ball-o', 510),
(511, 'fa-futbol-o', 511),
(512, 'fa-tty', 512),
(513, 'fa-binoculars', 513),
(514, 'fa-plug', 514),
(515, 'fa-slideshare', 515),
(516, 'fa-twitch', 516),
(517, 'fa-yelp', 517),
(518, 'fa-newspaper-o', 518),
(519, 'fa-wifi', 519),
(520, 'fa-calculator', 520),
(521, 'fa-paypal', 521),
(522, 'fa-google-wallet', 522),
(523, 'fa-cc-visa', 523),
(524, 'fa-cc-mastercard', 524),
(525, 'fa-cc-discover', 525),
(526, 'fa-cc-amex', 526),
(527, 'fa-cc-paypal', 527),
(528, 'fa-cc-stripe', 528),
(529, 'fa-bell-slash', 529),
(530, 'fa-bell-slash-o', 530),
(531, 'fa-trash', 531),
(532, 'fa-copyright', 532),
(533, 'fa-at', 533),
(534, 'fa-eyedropper', 534),
(535, 'fa-paint-brush', 535),
(536, 'fa-birthday-cake', 536),
(537, 'fa-area-chart', 537),
(538, 'fa-pie-chart', 538),
(539, 'fa-line-chart', 539),
(540, 'fa-lastfm', 540),
(541, 'fa-lastfm-square', 541),
(542, 'fa-toggle-off', 542),
(543, 'fa-toggle-on', 543),
(544, 'fa-bicycle', 544),
(545, 'fa-bus', 545),
(546, 'fa-ioxhost', 546),
(547, 'fa-angellist', 547),
(548, 'fa-cc za', 548),
(549, 'fa-shekel', 549),
(550, 'fa-sheqel', 550),
(551, 'fa-ils', 551),
(552, 'fa-meanpath', 552),
(553, 'fa-buysellads', 553),
(554, 'fa-connectdevelop', 554),
(555, 'fa-dashcube', 555),
(556, 'fa-forumbee', 556),
(557, 'fa-leanpub', 557),
(558, 'fa-sellsy', 558),
(559, 'fa-shirtsinbulk', 559),
(560, 'fa-simplybuilt', 560),
(561, 'fa-skyatlas', 561),
(562, 'fa-cart-plus', 562),
(563, 'fa-cart-arrow-down', 563),
(564, 'fa-diamond', 564),
(565, 'fa-ship', 565),
(566, 'fa-user-secret', 566),
(567, 'fa-motorcycle', 567),
(568, 'fa-street-view', 568),
(569, 'fa-heartbeat', 569),
(570, 'fa-venus', 570),
(571, 'fa-mars', 571),
(572, 'fa-mercury', 572),
(573, 'fa-transgender', 573),
(574, 'fa-transgender-alt', 574),
(575, 'fa-venus-double', 575),
(576, 'fa-mars-double', 576),
(577, 'fa-venus-mars', 577),
(578, 'fa-mars-stroke', 578),
(579, 'fa-mars-stroke-v', 579),
(580, 'fa-mars-stroke-h', 580),
(581, 'fa-neuter', 581),
(582, 'fa-facebook-official', 582),
(583, 'fa-pinterest-p', 583),
(584, 'fa-whatsapp', 584),
(585, 'fa-server', 585),
(586, 'fa-user-plus', 586),
(587, 'fa-user-times', 587),
(588, 'fa-hotel', 588),
(589, 'fa-bed', 589),
(590, 'fa-viacoin', 590),
(591, 'fa-train', 591),
(592, 'fa-subway', 592),
(593, 'fa-medium', 593),
(594, 'fa-snowflake', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gallary`
--

CREATE TABLE `gallary` (
  `id` int(11) NOT NULL,
  `thumb` varchar(150) NOT NULL,
  `img` varchar(150) NOT NULL,
  `categories` text DEFAULT NULL,
  `title` varchar(150) NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallary`
--

INSERT INTO `gallary` (`id`, `thumb`, `img`, `categories`, `title`, `rank`, `deleted`) VALUES
(1, '1.png', '1.jpg', '{\"pf-icons\":\"Aswan\",\"pf-media\":\"Hurghada\"}', 'Dahabya', 1, 0),
(2, '2.png', '2.jpg', '{\"pf-illustrations\":\"Luxor\",\"pf-uielements\":\"Cairo\"}', 'Dahabya', 2, 0),
(3, '3.png', '3.jpg', '{\"pf-uielements\":\"Cairo\",\"pf-graphics\":\"Sharm El-Shekh\"}', 'Dahabya', 3, 0),
(4, '4.png', '4.jpg', '{\"pf-illustrations\":\"Luxor\",\"pf-media\":\"Hurghada\"}', 'Dahabya', 4, 0),
(5, '5.png', '5.jpg', '{\"pf-graphics\":\"Sharm El-Shekh\"}', 'Dahabya', 5, 0),
(6, '6.png', '6.jpg', '{\"pf-icons\":\"Aswan\",\"pf-uielements\":\"Cairo\"}', 'Dahabya', 6, 0),
(7, '7.png', '7.jpg', '{\"pf-icons\":\"Aswan\",\"pf-media\":\"Hurghada\"}', 'Dahabya', 7, 0),
(8, '8.png', '8.jpg', '{\"pf-icons\":\"Aswan\",\"pf-graphics\":\"Sharm El-Shekh\"}', 'Dahabya', 8, 0),
(9, '9.png', '9.jpg', '{\"pf-icons\":\"Aswan\",\"pf-media\":\"Hurghada\"}', 'Dahabya', 9, 0),
(10, '10.png', '10.jpg', '{\"pf-illustrations\":\"Luxor\",\"pf-icons\":\"Aswan\"}', 'Dahabya', 10, 0),
(11, '11.png', '11.jpg', '{\"pf-icons\":\"Aswan\",\"pf-media\":\"Hurghada\"}', 'Dahabya', 11, 0),
(12, '12.png', '12.jpg', '{\"pf-illustrations\":\"Luxor\",\"pf-icons\":\"Aswan\"}', 'Dahabya', 12, 0),
(13, '13.png', '13.jpg', '{\"pf-icons\":\"Aswan\",\"pf-uielements\":\"Cairo\"}', 'Dahabya', 13, 0),
(14, '14.png', '14.jpg', '{\"pf-icons\":\"Aswan\",\"pf-graphics\":\"Sharm El-Shekh\"}', 'Dahabya', 14, 0),
(15, '15.png', '15.jpg', '{\"pf-icons\":\"Aswan\",\"pf-graphics\":\"Sharm El-Shekh\"}', 'Dahabya', 15, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`id`, `name`, `deleted`) VALUES
(1, 'Pharaoh Azur Beach Resort', 0),
(2, 'SUNRISE Garden Beach Resort -Select-', 0),
(3, 'SENTIDO Mamlouk Palace', 0),
(4, 'SUNRISE Crystal Bay Resort- Grand Select-', 0),
(5, 'SUNRISE Royal Makadi Aqua Resort', 0);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `english` text COLLATE utf8_unicode_ci NOT NULL,
  `german` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `french` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `arabic` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `english`, `german`, `french`, `arabic`, `updated_at`, `timestamp`) VALUES
(1, 'Dahabeya', 'Dahabeya', 'Dahabeya', 'دهبية', '', '2021-06-25 13:51:59'),
(2, 'Welcome To Queen-Farida', 'Willkommen bei Queen-Farida', 'Bienvenue à Reine-Farida', 'مرحبًا بكم في الملكة فريدة', '', '2021-06-25 13:53:48'),
(3, 'Queen Farida', 'Königin Farida', 'Reine Farida', 'الملكة فريدة', '', '2021-06-25 14:00:47'),
(4, 'English', 'Englisch', 'Anglais', 'الإنجليزية', '', '2021-06-25 14:00:47'),
(5, 'French', 'Französisch', 'Français', 'الفرنسية', '', '2021-06-25 14:00:47'),
(6, 'German', 'Deutsche', 'Allemand', 'الألمانية', '', '2021-06-25 14:00:47'),
(7, 'Arabic', 'Arabisch', 'Arabe', 'العربى', '', '2021-06-25 14:00:47'),
(8, 'Home', 'Zuhause', 'Domicile', 'الصفحة الرئيسية', '', '2021-06-25 14:00:47');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_id` int(11) NOT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mini_target_id` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_data` text COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `new_items` text COLLATE utf8_unicode_ci NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `user_id`, `action`, `module_id`, `target`, `target_id`, `mini_target_id`, `data`, `new_data`, `items`, `new_items`, `comments`, `ip`, `log_time`) VALUES
(1, 1, 'Create', 1, 'Sliders', '11', 0, '0', '{\"first_title\":\"Enter Title\",\"second_title\":\"Enter Text\",\"rank\":0}', '0', '0', 'added slider: 11', '172.50.28.253()', '2021-07-07 15:15:21'),
(2, 1, 'Delete', 1, 'Sliders', '11', 0, '{\"id\":\"11\",\"thumb\":\"\",\"img\":\"\",\"video\":null,\"first_title\":\"Enter Title\",\"second_title\":\"Enter Text\",\"rank\":\"0\",\"deleted\":\"0\"}', '0', '0', '0', 'Deleted slider:11', '172.50.28.253()', '2021-07-07 15:16:51'),
(3, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:37:22'),
(4, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:37:30'),
(5, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:37:39'),
(6, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:37:46'),
(7, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:39:18'),
(8, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:40:00'),
(9, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:40:09'),
(10, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:40:13'),
(11, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:41:06'),
(12, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:41:25'),
(13, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:41:31'),
(14, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:55:26'),
(15, 1, 'Status Change', 5, 'الموردين', '7', 0, '0', '0', '0', '0', 'Changed booking status:7', '::1()', '2021-07-11 18:55:29'),
(16, 1, 'Create', 6, 'العملاء', '8', 0, '0', '{\"location\":\"Enter Location\",\"describtion\":\"Enter Describtion\",\"rank\":0}', '0', '0', 'added Event: 8', '::1()', '2021-07-16 12:47:51'),
(17, 1, 'update', 6, 'العملاء', '8', 0, '{\"id\":\"8\",\"location\":\"Enter Location\",\"event\":\"\",\"describtion\":\"Enter Describtion\",\"rank\":\"0\",\"image\":\"\",\"toped\":\"0\",\"lefted\":\"0\",\"active\":\"0\",\"deleted\":\"0\"}', '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"toped\":\"0\",\"lefted\":\"0\",\"rank\":\"9\"}', '0', '0', 'updated Event: 8', '::1()', '2021-07-16 12:48:58'),
(18, 1, 'update', 6, 'العملاء', '8', 0, '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"rank\":\"9\",\"image\":\"\",\"toped\":\"0\",\"lefted\":\"0\",\"active\":\"0\",\"deleted\":\"0\"}', '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"toped\":\"0\",\"lefted\":\"0\",\"rank\":\"9\",\"image\":\"New_Project.png\"}', '0', '0', 'updated Event: 8', '::1()', '2021-07-16 12:49:38'),
(19, 1, 'update', 6, 'العملاء', '8', 0, '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"rank\":\"9\",\"image\":\"New_Project.png\",\"toped\":\"0\",\"lefted\":\"0\",\"active\":\"0\",\"deleted\":\"0\"}', '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"toped\":\"650\",\"lefted\":\"400\",\"rank\":\"9\"}', '0', '0', 'updated Event: 8', '::1()', '2021-07-16 12:50:19'),
(20, 1, 'Delete', 6, 'العملاء', '8', 0, '{\"id\":\"8\",\"location\":\"test\",\"event\":\"\",\"describtion\":\"test\",\"rank\":\"9\",\"image\":\"New_Project.png\",\"toped\":\"650\",\"lefted\":\"400\",\"active\":\"1\",\"deleted\":\"0\"}', '0', '0', '0', 'Deleted Event:8', '::1()', '2021-07-16 12:53:01');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(5) NOT NULL,
  `activity_id` int(5) NOT NULL,
  `hotel_id` int(5) NOT NULL,
  `sub_id` int(5) NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `activity_id`, `hotel_id`, `sub_id`, `rank`, `deleted`) VALUES
(1, 1, 1, 6, 1, 0),
(2, 3, 1, 7, 2, 0),
(3, 2, 1, 1, 3, 0),
(4, 2, 2, 1, 4, 0),
(5, 2, 2, 2, 5, 0),
(6, 2, 2, 3, 6, 0),
(7, 2, 2, 4, 7, 0),
(8, 2, 2, 5, 8, 0),
(9, 2, 3, 1, 9, 0),
(10, 2, 3, 2, 10, 0),
(11, 2, 3, 3, 11, 0),
(12, 2, 3, 4, 12, 0),
(13, 2, 3, 5, 13, 0),
(14, 2, 4, 1, 14, 0),
(15, 2, 4, 2, 15, 0),
(16, 2, 4, 3, 16, 0),
(17, 2, 4, 4, 17, 0),
(18, 2, 4, 5, 18, 0),
(19, 2, 5, 1, 19, 0),
(20, 2, 5, 2, 20, 0),
(21, 2, 5, 3, 21, 0),
(22, 2, 5, 4, 22, 0),
(23, 2, 5, 5, 23, 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creat` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `view` tinyint(1) NOT NULL,
  `remove` tinyint(1) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `creat`, `edit`, `view`, `remove`, `link`, `type`, `rank`, `deleted`) VALUES
(1, 'Sliders', 1, 1, 1, 1, '', 'site', 1, 0),
(2, 'Boat Info', 1, 1, 1, 1, '', 'site', 2, 0),
(3, 'Packages', 1, 1, 1, 1, '', 'site', 3, 0),
(4, 'Gallery', 1, 1, 1, 1, '', 'site', 4, 0),
(5, 'الموردين', 1, 1, 1, 1, 'admin/suppliers', 'store', 5, 0),
(6, 'العملاء', 1, 1, 1, 1, 'admin/clients', 'store', 5, 0),
(7, 'المخزن', 1, 1, 1, 0, 'admin/items/groups', 'store', 4, 0),
(8, 'التقارير', 1, 1, 1, 1, 'reports', 'store', 6, 0),
(11, 'التسجيل', 1, 1, 1, 1, 'admin/log_activity', 'store', 6, 0),
(13, 'Site manager', 1, 1, 1, 1, 'admin/site_manager', 'store', 10, 0),
(17, 'portfolio', 1, 1, 1, 1, '', 'site', 12, 0),
(18, 'blog', 1, 1, 1, 1, '', 'site', 13, 0),
(19, 'contact', 1, 1, 1, 1, '', 'site', 14, 0),
(20, 'Footer', 1, 1, 1, 1, '', 'site', 14, 0),
(21, 'Client Requests', 1, 1, 1, 1, '', 'site', 15, 0),
(22, 'gallery', 1, 1, 1, 1, '', 'site', 12, 0),
(23, 'team', 1, 1, 1, 1, '', 'site', 13, 0),
(24, 'Offers', 1, 1, 1, 1, '', 'site', 14, 0),
(25, 'language', 1, 1, 1, 1, '', 'site', 15, 0),
(26, 'Bookings', 1, 1, 1, 1, '', 'site', 16, 0);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `to_branch_id` int(11) DEFAULT NULL,
  `to_dep_id` int(11) DEFAULT NULL,
  `readedby` int(11) DEFAULT NULL,
  `head` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(3) NOT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(5) NOT NULL,
  `activity_id` int(5) NOT NULL,
  `hotel_id` int(5) NOT NULL,
  `sub_id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `data` text NOT NULL,
  `image` varchar(150) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `special` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `activity_id`, `hotel_id`, `sub_id`, `name`, `data`, `image`, `price`, `currency`, `special`, `deleted`, `timestamp`) VALUES
(1, 1, 1, 6, 'Test', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(2, 3, 1, 7, 'Test 1', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(3, 2, 1, 1, 'Test 2', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(4, 2, 2, 1, 'Test 3', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(5, 2, 2, 2, 'Test 4', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(6, 2, 2, 3, 'Test 5', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(7, 2, 2, 4, 'Test 6', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(8, 2, 2, 5, 'Test 7', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(9, 2, 3, 1, 'Test 8', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(10, 2, 3, 2, 'Test 9', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(11, 2, 3, 3, 'Test 10', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(12, 2, 3, 4, 'Test 11', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(13, 2, 3, 5, 'Test 12', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(14, 2, 4, 1, 'Test 13', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(15, 2, 4, 2, 'Test 14', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(16, 2, 4, 3, 'Test 15', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(17, 2, 4, 4, 'Test 16', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(18, 2, 4, 5, 'Test 17', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(19, 2, 5, 1, 'Test 18', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(20, 2, 5, 2, 'Test 19', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(21, 2, 5, 3, 'Test 20', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(22, 2, 5, 4, 'Test 21', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(24, 1, 4, 6, 'test 23', 'Test Test Test Test Test55', 'IMG_3782.JPG', '10000', '$', 1, 0, '2020-01-02 14:01:53');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(5) NOT NULL,
  `boat_id` int(5) NOT NULL DEFAULT 1,
  `title` varchar(150) NOT NULL,
  `remarks` text NOT NULL,
  `time` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `location` varchar(250) NOT NULL,
  `price` double(15,2) NOT NULL,
  `status` int(5) NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `boat_id`, `title`, `remarks`, `time`, `date`, `image`, `location`, `price`, `status`, `rank`, `deleted`, `timestamp`) VALUES
(1, 1, 'Normal Package', 'Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo.', '09:00', '2021-08-01', '3.png', 'Aswan', 100.00, 2, 1, 0, '2021-07-09 11:50:37'),
(2, 1, 'Honeymooners Package', 'Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo.', '09:00', '2021-08-01', '1.png', 'Aswan', 200.00, 2, 2, 0, '2021-07-09 11:50:40'),
(3, 1, 'VIP Boat Package', 'Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo.', '09:00', '2021-08-01', '2.png', 'Aswan', 500.00, 2, 3, 0, '2021-07-09 11:50:42');

-- --------------------------------------------------------

--
-- Table structure for table `site_files`
--

CREATE TABLE `site_files` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `form_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `site_groups`
--

CREATE TABLE `site_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_groups`
--

INSERT INTO `site_groups` (`id`, `group_name`, `serial`, `img`, `rank`, `deleted`) VALUES
(1, 'HVAC ACCESSORIES', 'hva', 'hvac_accessories.jpg', 1, 0),
(2, 'VENTILATION FANS', 'ven', 'ventilation fans.jpg', 2, 0),
(3, 'ELECTRICAL MATERIALS', 'elec', 'ELECTRICAL MATERIALS.jpg', 3, 0),
(4, 'GRILLES & DIFFUSERS', 'gril', 'grilles diffusers.jpg', 4, 0),
(5, 'G.I. & ACCESSORIES', 'g.i.', 'G.I. & ACCESSORIES.jpg', 5, 0),
(6, 'P.I. & ACCESSORIES', 'p.i.', 'P.I. & ACCESSORIES.jpg', 6, 0),
(7, 'AIR CONDITION UNITS', 'air', 'AIR CONDITION UNITS.jpg', 7, 0),
(8, 'THERMOSTAT & CONTROL VALVES', 'ther', 'THERMOSTAT & CONTROL VALVES.jpg', 8, 0),
(9, 'AC FILTERS', 'ac', 'AC FILTERS.jpg', 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_group_items`
--

CREATE TABLE `site_group_items` (
  `id` int(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_group_items`
--

INSERT INTO `site_group_items` (`id`, `group_id`, `item_name`, `serial`, `company`, `description`, `img`, `file`, `rank`, `timestamp`, `deleted`) VALUES
(1, 1, 'وش مبنى 100 سم', '00133E8', '1', 'Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit.', '1.jpg', '1.pdf', 1, '2019-04-18 14:46:10', 0),
(2, 1, 'حديد 4 لنية ', '002225B', '4', 'حديد 4 لنية ', '', '', 2, '2019-04-24 14:50:10', 0),
(3, 1, 'test', '009B11E', '4', 'حديد 3 لنية', '', '', 3, '2019-04-24 15:22:01', 0),
(4, 3, 'حديد 4 لنية', '00269BB', '1', 'حديد 4 لنية', '', '', 0, '2019-04-24 15:23:21', 0),
(5, 3, 'حديد 4 لنية', '00F436C', '6', 'حديد 4 لنية', '', '', 0, '2019-04-24 15:24:17', 0),
(6, 3, 'حديد 2.5 لنية أطوال', '00A6820', '7', 'حديد 2.5 لنية', '', '', 0, '2019-04-24 15:26:18', 0),
(7, 3, 'حديد 2 لنية لفف', '003988F', '1', 'حديد 2 لنية لفف', '', '', 0, '2019-04-24 15:29:51', 0),
(8, 7, 'أسمنت الجيش رتبة 52.5 بورتلاندى', '0091BED', '13', 'أسمنت الجيش رتبة 52.5 بورتلاندى', '', '', 0, '2019-04-24 15:36:18', 0),
(9, 7, 'أسمنت السويدى', '0009D57', '12', 'أسمنت السويدى', '', '', 0, '2019-04-24 16:10:52', 0),
(10, 7, 'أسمنت المصرية', '00C27C9', '11', 'أسمنت المصرية', '', '', 0, '2019-04-24 16:11:38', 0),
(11, 7, 'أسمنت سويتر', '00D982E', '25', 'أسمنت سويتر', '', '', 0, '2019-04-24 16:15:22', 0),
(12, 7, 'أسمنت الجيش رتبة 42.5 بورتلاندى العريش', '00B3C18', '14', 'أسمنت الجيش رتبة 42.5 بورتلاندى العريش', '', '', 0, '2019-04-24 16:18:53', 0),
(14, 1, 'test2', '008C60E', 'test2', 'test2 test2 test2 test2', '556a0152-fab8-428f-9010-215a63d9fa00.png', '710b7b44-f1e3-4ecf-bf81-37ca8927664c1.png', 0, '2019-10-04 20:23:52', 0),
(15, 1, 'test2w', '00A31BA', 'test2w', 'test2wtest2wtest2w test2wtest2w', '556a0152-fab8-428f-9010-215a63d9fa00.png', 'Book121.csv', 1, '2019-10-04 20:27:44', 0),
(16, 2, 'test2ww', '00F9BB8', 'test2w', 'test 2uhsd', '6bc837d5-1df8-4527-8e1a-61ba7a17c5a8.png', '710b7b44-f1e3-4ecf-bf81-37ca8927664c.png', 5, '2019-10-04 22:38:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_meta_data`
--

CREATE TABLE `site_meta_data` (
  `id` int(11) NOT NULL,
  `header` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paragraph` text COLLATE utf8_unicode_ci NOT NULL,
  `points` text COLLATE utf8_unicode_ci NOT NULL,
  `meta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_meta_data`
--

INSERT INTO `site_meta_data` (`id`, `header`, `title`, `paragraph`, `points`, `meta`, `img`, `link`, `rank`) VALUES
(1, 'Queen Farida', ' ', '', '', 'site_title', 'Queen-Farida-Final.png', '', 1),
(2, 'Our Services', 'Central Ventilation System', 'will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype looks finished by.', '', 'services', 'fa fa-sun-o', '', 1),
(3, 'Our Services', 'HVAC Contractor', 'will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype looks finished by.', '', 'services', 'fa fa-adjust', '', 2),
(4, 'Our Services', 'HVAC Maintenance', 'will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype looks finished by.', '', 'services', 'fa fa-fire', '', 3),
(5, 'Our Services', 'HVAC Technical Support', 'will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype looks finished by.', '', 'services', 'fa fa-cogs', '', 4),
(8, 'Call', '01002741794', '9am-5pm', '', 'contact_us', 'fa fa-mobile', '', 1),
(9, 'Email', 'info@divemore-group.com', 'Web: divemore-group.com', '', 'contact_us', 'fa fa-envelope-o', '', 2),
(10, 'Location', 'Sheraton Street, Hurghada, Egypt', 'Sheraton Street, Hurghada, Egypt', '', 'contact_us', 'fa fa-map-marker', '', 3),
(11, '', '', 'Our purpose is to positively impact lifestyles of the community. We serve through consistent, timely, quality,  efficient and  value  added  delivery of  innovative joiness that  constantly exceed expectations.', '', 'footer_about', '', '', 1),
(12, '', '', 'Don\'t hesitate to contact us with any questions or inquiries.', '', 'footer_info', '', '', 1),
(13, 'Tel', '01002741794', '', '', 'footer_tel', '', '', 1),
(14, 'Email', 'info@divemore-group.com', '', '', 'footer_email', '', '', 1),
(15, 'Working Hours', '9am-5pm', '', '', 'footer_working', '', '', 1),
(16, 'Hurghada', 'Diving', '', 'Trips', 'gallery', '21.jpg', '', 1),
(17, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', '13620906_976161869163509_1200008273719622505_n.jpg', '', 1),
(18, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', '13731617_980013822111647_805224201259891546_n.jpg', '', 1),
(19, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', '14102607_1012532422193120_5040987907399577544_n.jpg', '', 2),
(20, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0027.JPG', '', 3),
(21, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0174.JPG', '', 4),
(22, 'Sent Successfully', 'Sent', '#000000', '', 'clients_req_status', '', '', 1),
(23, 'Failed to sent', 'Failed', '#bd1212', '', 'clients_req_status', '', '', 1),
(24, 'Waiting to send', 'Waiting', '#467b2b', '', 'clients_req_status', '', '', 1),
(25, 'Business City', '3.5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'cabo-pulmo-diving-11.jpg', '', 1),
(26, 'Business City', '4.5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'Discover-Scuba-Diving1.jpg', '', 3),
(28, 'Business City', '5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'scuba-diving2-960x11491.jpg', '', 2),
(29, 'Julien Miro', 'Software Developer', '', '', 'team', '11.jpg', '', 1),
(30, 'Joan Williams', 'Support Operator', '', '', 'team', '21.jpg', '', 2),
(32, 'Benedict Smith', 'Creative Director', '', '', 'team', '31.jpg', '', 3),
(33, 'Madlen Green', 'Sales Manager', '', '', 'team', '41.jpg', '', 4),
(34, 'Julien Miro', 'Julien Miro', '', '', 'team', 'Discover-Scuba-Diving.jpg', '', 5),
(35, 'Waiting to send', 'Waiting', '#bd1212', '', 'booking_status', '', '', 1),
(36, 'Pending', 'Pending', '#ffa922', '', 'booking_status', '', '', 1),
(37, 'Confirmed', 'Confirmed', '#1f8d5d', '', 'booking_status', '', '', 1),
(38, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0179.JPG', '', 5),
(39, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0216.JPG', '', 6),
(40, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_1324.JPG', '', 7),
(41, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_1417.JPG', '', 8),
(42, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', 'IMG_1624.JPG', '', 2),
(43, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', 'IMG_1629.JPG', '', 3),
(44, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', 'IMG_1632.JPG', '', 4),
(45, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_1924.JPG', '', 9);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `thumb` varchar(150) NOT NULL,
  `img` varchar(150) NOT NULL,
  `video` varchar(150) DEFAULT NULL,
  `first_title` text NOT NULL,
  `second_title` text NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `thumb`, `img`, `video`, `first_title`, `second_title`, `rank`, `deleted`) VALUES
(1, 's1-thumb.jpg', 'explore-poster.jpg', 'explore.mp4', 'Dahabeya', 'Welcome To Queen-Farida', 1, 0),
(2, '04-b_14.jpg', '04-b_14.jpg', NULL, 'Dahabeya', 'Welcome To Queen-Farida', 2, 0),
(3, '04-b_08.jpg', '04-b_08.jpg', NULL, 'Dahabeya', 'Welcome To Queen-Farida', 3, 0),
(4, '04-b_12.jpg', '04-b_12.jpg', NULL, 'Dahabeya', 'Welcome To Queen-Farida', 4, 0),
(5, 'DSC_3396.jpg', 'DSC_3396.jpg', NULL, 'Dahabeya', 'Welcome To Queen-Farida', 5, 0),
(6, 'DSC_3436.jpg', 'DSC_3436.jpg', NULL, 'Dahabeya', 'Welcome To Queen-Farida', 6, 0),
(7, 'DSC_2623.jpg', 'DSC_2623.jpg', NULL, 'Dahabeya', 'Welcome To Queen-Farida', 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `text_color` varchar(150) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `rank` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_name`, `status_color`, `text_color`, `rank`) VALUES
(1, 'Draft', '#000000', '#FFF', 2),
(2, 'Avalible', '#b88121', '#FFF', 4),
(3, 'Cancelled', '#0088cc', '#FFF', 6),
(4, 'Unavalible', '#1f8d5d', '#FFF', 8);

-- --------------------------------------------------------

--
-- Table structure for table `sub_activity`
--

CREATE TABLE `sub_activity` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_activity`
--

INSERT INTO `sub_activity` (`id`, `name`, `deleted`) VALUES
(1, 'Diving', 0),
(2, 'Courses', 0),
(3, 'Intro', 0),
(4, 'Daily Dive', 0),
(5, 'Speciality', 0),
(6, 'Aqua Center', 0),
(7, 'SPA', 0);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(150) NOT NULL,
  `possion` varchar(100) NOT NULL,
  `info` text NOT NULL,
  `contact` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `id` int(5) NOT NULL,
  `boat_id` int(5) NOT NULL,
  `title` varchar(150) NOT NULL,
  `remarks` text NOT NULL,
  `from_location` varchar(250) NOT NULL,
  `to_location` varchar(250) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `status` int(5) NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `role_id` int(5) NOT NULL,
  `department` int(11) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `permission` tinyint(1) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `fullname`, `email`, `role_id`, `department`, `mobile_no`, `password`, `is_admin`, `permission`, `disabled`, `last_ip`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'hany.hisham', 'admin', 'info@alamtc.com', 1, 1, '12345', '$2y$10$1jn497ev2euAuPFua8V8RunCu7kZlbH2I4GYNSxtnlLpQkpx57j1m', 1, 1, 0, '', '2021-07-04 00:00:00', '2021-07-04 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_branches`
--

CREATE TABLE `user_branches` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(6) NOT NULL,
  `role_id` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_branches`
--

INSERT INTO `user_branches` (`id`, `user_id`, `branch_id`, `role_id`) VALUES
(317, 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `permission`) VALUES
(1, 'Admin', 1),
(2, 'IT', 1),
(3, 'GMs', 0),
(4, 'Staff', 0),
(5, 'Managers', 0),
(6, 'test', 1),
(7, 'test2', 0),
(8, 'test22', 0),
(9, 'test221', 0),
(10, 'test22', 0),
(11, 'test23', 0),
(12, 'test22', 0),
(13, 'test23', 0),
(14, 'test23', 0),
(15, 'test221', 0),
(16, 'test221', 0),
(17, 'test221', 0),
(18, 'test22', 0),
(19, 'test', 0),
(20, 'test22', 0),
(21, 'test', 0),
(22, 'test2', 0),
(23, 'test22', 0),
(24, 'test2', 0),
(25, 'cost', 1),
(26, 'recieving', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups_permission`
--

CREATE TABLE `user_groups_permission` (
  `id` int(11) NOT NULL,
  `role_id` int(6) NOT NULL,
  `module_id` int(11) NOT NULL,
  `g_view` tinyint(1) NOT NULL,
  `creat` tinyint(2) NOT NULL,
  `edit` tinyint(2) NOT NULL,
  `view` tinyint(2) NOT NULL,
  `remove` tinyint(2) NOT NULL,
  `pr_edit` tinyint(1) NOT NULL,
  `pr_approve` tinyint(1) NOT NULL,
  `recieve` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups_permission`
--

INSERT INTO `user_groups_permission` (`id`, `role_id`, `module_id`, `g_view`, `creat`, `edit`, `view`, `remove`, `pr_edit`, `pr_approve`, `recieve`) VALUES
(1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(2, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0),
(3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(12, 0, 2, 1, 0, 0, 0, 1, 1, 0, 0),
(13, 0, 3, 1, 1, 1, 1, 1, 0, 0, 0),
(14, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(17, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(18, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(19, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(21, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0),
(22, 1, 2, 1, 1, 1, 1, 1, 1, 0, 0),
(23, 1, 3, 1, 0, 0, 0, 1, 0, 0, 0),
(24, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(27, 1, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(28, 1, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 1, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(31, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(32, 2, 2, 1, 0, 0, 0, 1, 1, 0, 0),
(33, 2, 3, 1, 0, 0, 0, 1, 0, 0, 0),
(34, 2, 4, 1, 0, 0, 0, 0, 0, 0, 0),
(35, 2, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(36, 2, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(37, 2, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(38, 2, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(39, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(40, 2, 10, 1, 1, 1, 1, 0, 0, 0, 0),
(41, 6, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(42, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0),
(43, 6, 3, 0, 1, 0, 0, 0, 0, 0, 0),
(44, 6, 4, 0, 0, 1, 0, 0, 0, 0, 0),
(45, 6, 5, 0, 1, 0, 0, 0, 0, 0, 0),
(46, 6, 6, 0, 1, 0, 1, 0, 0, 0, 0),
(47, 6, 7, 1, 1, 0, 0, 0, 0, 0, 0),
(48, 6, 8, 0, 1, 0, 1, 0, 0, 0, 0),
(49, 6, 9, 0, 1, 1, 0, 0, 0, 0, 0),
(50, 6, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(51, 25, 1, 1, 0, 0, 1, 0, 0, 0, 0),
(52, 25, 2, 1, 0, 0, 1, 0, 0, 0, 0),
(53, 25, 3, 1, 1, 0, 0, 0, 0, 0, 0),
(54, 25, 4, 1, 0, 1, 0, 0, 0, 0, 0),
(55, 26, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(56, 26, 2, 1, 1, 1, 1, 0, 1, 0, 1),
(57, 26, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(58, 26, 4, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE `user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `creat` tinyint(4) NOT NULL,
  `edit` tinyint(4) NOT NULL,
  `view` tinyint(4) NOT NULL,
  `remove` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`id`, `user_id`, `module_id`, `creat`, `edit`, `view`, `remove`, `timestamp`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(2, 1, 2, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(3, 1, 3, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(4, 1, 4, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(5, 1, 5, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(6, 1, 6, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(7, 1, 7, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(8, 1, 8, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(9, 1, 9, 1, 0, 0, 0, '2019-01-15 13:55:20'),
(10, 1, 11, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(11, 1, 12, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(12, 1, 13, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(13, 1, 14, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(14, 1, 15, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(15, 1, 16, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(16, 1, 17, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(17, 1, 18, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(18, 1, 19, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(19, 1, 20, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(20, 1, 21, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(21, 1, 22, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(22, 1, 23, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(23, 1, 24, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(24, 1, 25, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(25, 1, 26, 1, 1, 1, 1, '2019-01-15 13:55:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boats`
--
ALTER TABLE `boats`
  ADD PRIMARY KEY (`boatid`),
  ADD KEY `country` (`country`),
  ADD KEY `company` (`boat_name`);

--
-- Indexes for table `boats_cabins`
--
ALTER TABLE `boats_cabins`
  ADD PRIMARY KEY (`cabinid`),
  ADD KEY `room_no` (`cabin_no`);

--
-- Indexes for table `boat_info`
--
ALTER TABLE `boat_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `cabins`
--
ALTER TABLE `cabins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `supp_name` (`client_name`);

--
-- Indexes for table `clients_mail_queue`
--
ALTER TABLE `clients_mail_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `font_icons`
--
ALTER TABLE `font_icons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallary`
--
ALTER TABLE `gallary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_files`
--
ALTER TABLE `site_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_groups`
--
ALTER TABLE `site_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_name` (`group_name`),
  ADD UNIQUE KEY `serial` (`serial`);

--
-- Indexes for table `site_group_items`
--
ALTER TABLE `site_group_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `serial` (`serial`);

--
-- Indexes for table `site_meta_data`
--
ALTER TABLE `site_meta_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_activity`
--
ALTER TABLE `sub_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_branches`
--
ALTER TABLE `user_branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_groups_permission`
--
ALTER TABLE `user_groups_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `boats`
--
ALTER TABLE `boats`
  MODIFY `boatid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `boats_cabins`
--
ALTER TABLE `boats_cabins`
  MODIFY `cabinid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `boat_info`
--
ALTER TABLE `boat_info`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cabins`
--
ALTER TABLE `cabins`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clients_mail_queue`
--
ALTER TABLE `clients_mail_queue`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `font_icons`
--
ALTER TABLE `font_icons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=595;

--
-- AUTO_INCREMENT for table `gallary`
--
ALTER TABLE `gallary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `site_files`
--
ALTER TABLE `site_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `site_groups`
--
ALTER TABLE `site_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `site_group_items`
--
ALTER TABLE `site_group_items`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `site_meta_data`
--
ALTER TABLE `site_meta_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sub_activity`
--
ALTER TABLE `sub_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trips`
--
ALTER TABLE `trips`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `user_branches`
--
ALTER TABLE `user_branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=318;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_groups_permission`
--
ALTER TABLE `user_groups_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `user_permissions`
--
ALTER TABLE `user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
