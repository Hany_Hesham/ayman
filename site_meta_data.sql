
-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2022 at 06:03 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kids`
--

-- --------------------------------------------------------

--
-- Table structure for table `site_meta_data`
--

CREATE TABLE `site_meta_data` (
  `id` int(11) NOT NULL,
  `header` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paragraph` text COLLATE utf8_unicode_ci NOT NULL,
  `points` text COLLATE utf8_unicode_ci NOT NULL,
  `meta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_meta_data`
--

INSERT INTO `site_meta_data` (`id`, `header`, `title`, `paragraph`, `points`, `meta`, `img`, `link`, `rank`) VALUES
(1, 'logo', ' ', '', '', 'site_logo', 'ISCPO-2021_logo_en_1.png', '', 1),
(2, 'slider', '', '', '', 'slider_image', '0_Tm5YhkSDJAbXX6FL.png', '', 1),
(3, 'slider', 'Scratch Game-Script Olympiad', 'Contestants will create NEW and original projects (story, animation, game, mixed, etc...) with the main subject: ... ! All projects must be written in Scratch 3.0 language (in your Scratch account on https://scratch.mit.edu) ! Having a nice intro, menus, storyline, animation, music and games inside will be a plus! This is an INDIVIDUAL competition, no TEAMS allowed!', '', 'slider_data', 'scratch-8815-7.jpg', '', 1),
(4, 'social', 'Facebook', 'https://web.facebook.com/hany.godzila/', '', 'social_data', 'fab fa-facebook-f', '', 1),
(5, 'social', 'Twitter', '', '', 'social_data', 'fab fa-twitter', '', 2),
(8, 'social', 'Google Plus', '', '', 'social_data', 'fab fa-google-plus-g', '', 3),
(9, 'contact', 'Phone', '+20 122 774 3419', '', 'call_us', 'phone-header fas fa-phone ml-5 visible-xs visible-sm visible-md', '', 1),
(10, 'about', 'Smart Goals Academy', 'At the initiative of NGO \"Asociatia SASORYCODE\" (non-governmental and non-profit organization) and with exceptional support of many partners and software companies, we proudly created this page to support children who love the world of programming and computers. IKCC is the starting point for these wonderful, passionate and inventive children.', '', 'about_us', '', '', 1),
(11, 'about', 'Location', 'Alkawthar disstrict، 147													Mohamed Saied st.(Metro st، Red Sea Governorate)', '', 'location', 'https://goo.gl/maps/i4Rr2VnTvW9MQgFu7', '', 1),
(12, 'contact', 'Email', 'info@smartgoals.academy', '', 'email_us', '', '', 1),
(13, 'Tel', '01002741794', '', '', 'footer_tel', '', '', 1),
(14, 'Email', 'info@divemore-group.com', '', '', 'footer_email', '', '', 1),
(15, 'Working Hours', '9am-5pm', '', '', 'footer_working', '', '', 1),
(16, 'Hurghada', 'Diving', '', 'Trips', 'gallery', '21.jpg', '', 1),
(17, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', '13620906_976161869163509_1200008273719622505_n.jpg', '', 1),
(18, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', '13731617_980013822111647_805224201259891546_n.jpg', '', 1),
(19, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', '14102607_1012532422193120_5040987907399577544_n.jpg', '', 2),
(20, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0027.JPG', '', 3),
(21, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0174.JPG', '', 4),
(22, 'Sent Successfully', 'Sent', '#000000', '', 'clients_req_status', '', '', 1),
(23, 'Failed to sent', 'Failed', '#bd1212', '', 'clients_req_status', '', '', 1),
(24, 'Waiting to send', 'Waiting', '#467b2b', '', 'clients_req_status', '', '', 1),
(25, 'Business City', '3.5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'cabo-pulmo-diving-11.jpg', '', 1),
(26, 'Business City', '4.5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'Discover-Scuba-Diving1.jpg', '', 3),
(28, 'Business City', '5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'scuba-diving2-960x11491.jpg', '', 2),
(29, 'Julien Miro', 'Software Developer', '', '', 'team', '11.jpg', '', 1),
(30, 'Joan Williams', 'Support Operator', '', '', 'team', '21.jpg', '', 2),
(32, 'Benedict Smith', 'Creative Director', '', '', 'team', '31.jpg', '', 3),
(33, 'Madlen Green', 'Sales Manager', '', '', 'team', '41.jpg', '', 4),
(34, 'Julien Miro', 'Julien Miro', '', '', 'team', 'Discover-Scuba-Diving.jpg', '', 5),
(35, 'Waiting payment', 'Waiting payment', '#bd1212', '', 'project_status', '', '2', 2),
(36, 'Ready to publish', 'published project', '#27a9e3', '', 'project_status', '', '4', 4),
(37, 'Payment Confirm', 'Payment Confirm', '#14d707', '', 'project_status', '', '3', 3),
(38, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0179.JPG', '', 5),
(39, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0216.JPG', '', 6),
(40, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_1324.JPG', '', 7),
(41, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_1417.JPG', '', 8),
(42, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', 'IMG_1624.JPG', '', 2),
(43, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', 'IMG_1629.JPG', '', 3),
(44, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', 'IMG_1632.JPG', '', 4),
(45, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_1924.JPG', '', 9),
(46, 'Age Category less than 8', '< 8', 'less-than-8-category', '', 'age_category', '', '', 0),
(47, 'Age Category from 8 to 12', 'BETWEEN 8 AND 12', 'between-8-and-12-category', '', 'age_category', '', '', 0),
(48, 'Age Category more than 12', '> 12', 'more-than-12-category', '', 'age_category', '', '', 0),
(49, 'What is ReadySteadyShare? (Logo of RSS)', '', 'ReadySteadyShare is an international students coding competition organized by Smart Goals Academy for elementary and middle school students. RSS is uses Scratch as competitive programming, Scratch is a high-level block-based visual programming language and website targeted primarily at children 8–16 as an educational tool for programming.', '', 'about_us_what_is_readysteadyshare', '', '', 0),
(50, 'About us?', '', 'RSS is coding competition for School students,    we care about our children! We care about the future! And the future is digital! We support children to perform in software programming, advanced technologies and computer science.\r\nRSS operates on, is measured on and strives towards the following values:\r\nDIVERSITY, PASSION, INNOVATION.\r\n', '', 'about_us_rss', '', '', 0),
(51, 'Description of competition?', '', 'ReadySteadyShare is to develop children programming skills by creating games, stories or animations. Children will be attracted to these creative projects and will unleash their imagination.\r\nThe contests will help children develop their coding skills, meet new challenges and achieve the highest level of software programming performance.', '', 'about_us_competition_description', '', '', 0),
(52, 'Design', '', 'Add backdrop, sprite, diffculty, sound ,scoreThe contests will help children develop their coding skills, meet new challenges and achieve the highest level of software programming performance.', '', 'about_us_design', '', '', 0),
(53, 'Coding', '', 'Project will become a bit more specialized through the code, or, the set of instructions we provide in order for the game to carry out how we’d like it to', '', 'about_us_coding', '', '', 0),
(54, 'Voting', '', 'Submit and share your project to get more votes', '', 'about_us_voting', '', '', 0),
(55, 'smart goals', 'smart goals', '', '', 'partners', 'partner_1.png', '', 1),
(56, 'Just Created', 'Just Created', '#3e5569', '', 'project_status', '', '1', 1),
(58, 'Published Project', 'Published Project', '#371ebf', '', 'project_status', '', '5', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `site_meta_data`
--
ALTER TABLE `site_meta_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `site_meta_data`
--
ALTER TABLE `site_meta_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
