-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2021 at 10:17 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kids`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `name`, `deleted`) VALUES
(1, 'Aqua Center', 0),
(2, 'Diving', 0),
(3, 'SPA', 0);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(5) NOT NULL,
  `category` varchar(100) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `post` longblob NOT NULL,
  `status` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `category`, `title`, `description`, `image`, `post`, `status`, `deleted`, `timestamp`) VALUES
(1, 'General', 'This is a Standard post with a Preview Image', 'Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text ', '17.jpg', '', 2, 0, '2021-10-24 13:58:13'),
(2, 'Diving', 'This is a Standard post with a Preview Image 2', 'Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text ', '1.jpg', '', 2, 0, '2021-10-24 13:58:13'),
(3, 'Torusim', 'This is a Standard post with a Preview Image 3', 'Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text ', '10.jpg', '', 2, 0, '2021-10-25 16:58:13');

-- --------------------------------------------------------

--
-- Table structure for table `boats`
--

CREATE TABLE `boats` (
  `boatid` int(11) NOT NULL,
  `boat_name` varchar(100) DEFAULT NULL,
  `description` text NOT NULL,
  `facebook` mediumtext DEFAULT NULL,
  `instagram` mediumtext DEFAULT NULL,
  `twitter` mediumtext DEFAULT NULL,
  `youtube` mediumtext DEFAULT NULL,
  `boat_slug` varchar(100) NOT NULL,
  `show_mobile` int(11) NOT NULL DEFAULT 0,
  `cabins` int(11) NOT NULL DEFAULT 0,
  `color` varchar(50) DEFAULT NULL,
  `phonenumber` varchar(30) DEFAULT NULL,
  `country` int(11) NOT NULL DEFAULT 0,
  `city` varchar(100) DEFAULT NULL,
  `zip` varchar(15) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `website` varchar(150) DEFAULT NULL,
  `datecreated` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `longitude` varchar(300) DEFAULT NULL,
  `latitude` varchar(300) DEFAULT NULL,
  `default_language` varchar(40) DEFAULT NULL,
  `default_currency` int(11) NOT NULL DEFAULT 0,
  `show_primary_contact` int(11) NOT NULL DEFAULT 0,
  `addedfrom` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boats`
--

INSERT INTO `boats` (`boatid`, `boat_name`, `description`, `facebook`, `instagram`, `twitter`, `youtube`, `boat_slug`, `show_mobile`, `cabins`, `color`, `phonenumber`, `country`, `city`, `zip`, `state`, `address`, `website`, `datecreated`, `active`, `longitude`, `latitude`, `default_language`, `default_currency`, `show_primary_contact`, `addedfrom`) VALUES
(1, 'Dahabeya Queen-Farida', '', NULL, NULL, NULL, NULL, '', 0, 14, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `boats_cabins`
--

CREATE TABLE `boats_cabins` (
  `cabinid` int(11) NOT NULL,
  `cabin_no` int(11) NOT NULL,
  `cabin_type` varchar(250) DEFAULT NULL,
  `boat_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boats_cabins`
--

INSERT INTO `boats_cabins` (`cabinid`, `cabin_no`, `cabin_type`, `boat_id`) VALUES
(1, 1, 'DOUBLE CABIN', 1),
(2, 2, 'DOUBLE CABIN', 1),
(3, 3, 'DOUBLE CABIN', 1),
(4, 4, 'DOUBLE CABIN', 1),
(5, 5, 'DOUBLE CABIN', 1),
(6, 6, 'DOUBLE CABIN', 1),
(7, 7, 'DOUBLE CABIN', 1),
(8, 8, 'DOUBLE CABIN', 1),
(9, 9, 'LUXURY CABIN', 1),
(10, 10, 'LUXURY CABIN', 1),
(11, 11, 'LUXURY CABIN', 1),
(12, 12, 'LUXURY CABIN', 1),
(13, 13, 'LUXURY CABIN', 1),
(14, 14, 'LUXURY CABIN', 1);

-- --------------------------------------------------------

--
-- Table structure for table `boat_info`
--

CREATE TABLE `boat_info` (
  `id` int(5) NOT NULL,
  `boat_id` int(5) NOT NULL DEFAULT 1,
  `title` varchar(150) NOT NULL,
  `remarks` text NOT NULL,
  `icon` varchar(50) NOT NULL DEFAULT 'icon-screen',
  `image` varchar(150) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `status` int(5) NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boat_info`
--

INSERT INTO `boat_info` (`id`, `boat_id`, `title`, `remarks`, `icon`, `image`, `code`, `status`, `rank`, `deleted`, `timestamp`) VALUES
(1, 1, 'Sailing Yacht on the Nile', '<span>Step aboard the Dahabeya Queen Farida in homage to the last Queen Farida of Egypt.\r\n<br>\r\nQueen Farida often travelled the Nile with her children on a dahabeya - called the golden boat.\r\n<br>\r\nThe shallow draft of the Dahabeya allows her to pass the tributaries of the Nile and anchor on the banks.\r\n<br>\r\nWith a sailing Nile cruise far from the hotspots, you can enjoy nature up close along the Nile. Bird paradises on the small Nile islands and lush plantation oases line the banks of the Nile. Enjoy the peace and the spectacle of nature.\r\n<br>\r\nA unique way of yachting the river Nile awaits you as soon as you step onboard. A ship built from exquisite woods shimmers golden in the sun, so this justifies the traditional name Dahabeya - the golden boat.\r\n<br>\r\nThe high-quality, mosaic-laid parquet floors ensure a sustainable room climate and a feel-good experience upon arrival!</span>', 'icon-screen', '04-b_12.jpg', 'snav-content1', 2, 1, 0, '2021-07-02 15:23:45'),
(2, 1, 'Dahabeya Queen Farida–the golden boat', '<span>Go on a discovery tour along the Nile of the historical monuments and the green landscape with the spectacular desert backdrop of Egypt.\r\n						Anchoring right on the banks of the Nile offers the opportunity to hike through plantation groves for an evening stroll and enjoy tree-fresh fruit with Egyptian hospitality.</span>', 'icon-magic', '04-b_14.jpg', 'snav-content2', 2, 2, 0, '2021-07-02 15:23:48'),
(3, 1, 'Facilities Overview', '<ul>\r\n							<li>Reception with free WIFI</li>\r\n							<li>Lounge with bar, Flat-screen TV and library</li>\r\n							<li>covered upper deck - your open-air restaurant</li>\r\n							<li>Sun deck with deck chairs and parasols</li>\r\n							<li>Kitchen with hygiene certificates</li>\r\n							<li>In the corridor to the reception you can already see historical photographs of the last Queen of Egypt - Queen Farida.</li>\r\n						</ul>', 'icon-tint', NULL, 'snav-content3', 2, 3, 0, '2021-07-02 15:23:50'),
(4, 1, 'Tour with kids', '<ul>\r\n							<li>The Dahabeya Queen Farida welcomes our young guests with pleasure</li>\r\n							<li>Children under 6 years share the parent\'s cabin – free of charge</li>\r\n							<li>Children from 6 and under 11 years are accommodated in the cabin next to or opposite the parents\' cabin – charge as one single cabin</li>\r\n							<li>Children over 11 years are considered adults</li>\r\n						</ul>', 'icon-gift', NULL, 'snav-content4', 2, 4, 0, '2021-07-02 15:23:53'),
(5, 1, 'Security- and technical facilities', '<ul>\r\n							<li>Electronic fire and smoke alarms in all areas</li>\r\n							<li>Sprinkler system</li>\r\n							<li>Soundproofing of all cabins</li>\r\n							<li>2 life jackets in each cabin</li>\r\n							<li>Escape map in every cabin</li>\r\n							<li>Electricity: 220 volts / 110 volts</li>\r\n							<li>Night mode, generator switched off 12 V</li>\r\n							<li>The internal water purification system</li>\r\n							<li>the hygienic ultraviolet water cleaning system</li>\r\n						</ul>', 'icon-adjust', NULL, 'snav-content5', 2, 5, 0, '2021-07-02 15:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(5) NOT NULL,
  `boat_id` int(5) NOT NULL DEFAULT 1,
  `check_in` varchar(50) NOT NULL,
  `check_out` varchar(50) NOT NULL,
  `cabins` varchar(50) NOT NULL,
  `persons` varchar(50) NOT NULL,
  `package` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(250) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `price` double(15,2) NOT NULL,
  `payed` double(15,2) NOT NULL,
  `currency` varchar(5) NOT NULL DEFAULT 'EGP',
  `status` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `boat_id`, `check_in`, `check_out`, `cabins`, `persons`, `package`, `first_name`, `last_name`, `email`, `address`, `phone`, `price`, `payed`, `currency`, `status`, `deleted`, `timestamp`) VALUES
(1, 1, '07/10/2021', '07/21/2021', 'DOUBLE CABINS', '2 Person', 'Honeymooners Package', 'test', 'test', 'hany.hisham@sunrise-resorts.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 35, 0, '2021-07-11 18:36:35'),
(2, 1, '07/10/2021', '07/21/2021', 'DOUBLE CABINS', '1 Person', 'VIP Boat Package', 'test', 'test', 'hany-hesham@outlook.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 36, 0, '2021-07-11 18:36:49'),
(3, 1, '07/10/2021', '07/13/2021', 'LUXURY CABINS', '2 Person', 'Normal Package', 'test', 'test', 'hany.hisham@sunrise-resorts.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 35, 0, '2021-07-11 18:36:38'),
(4, 1, '07/10/2021', '07/21/2021', 'LUXURY CABINS', '2 Person', 'Honeymooners Package', 'test', 'test', 'hany.hisham@sunrise-resorts.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 37, 0, '2021-07-11 18:37:00'),
(5, 1, '07/10/2021', '07/19/2021', 'DOUBLE CABINS', '1 Person', 'Honeymooners Package', 'test', 'test', 'hany.hisham@sunrise-resorts.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 36, 0, '2021-07-11 18:36:51'),
(6, 1, '07/10/2021', '07/20/2021', 'DOUBLE CABINS', '2 Person', 'VIP Boat Package', 'test', 'test', 'hany-hesham@outlook.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 35, 0, '2021-07-11 18:36:40'),
(7, 1, '07/10/2021', '07/15/2021', 'DOUBLE CABINS', '1 Person', 'Honeymooners Package', 'test', 'test', 'mohamed.abuelwafa@sunrise-resorts.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 37, 0, '2021-07-11 18:55:29'),
(8, 1, '07/10/2021', '07/12/2021', 'LUXURY CABINS', '1 Person', 'Honeymooners Package', 'test', 'test', 'hany-hesham@outlook.com', '29 giza, giza', '01068267949', 0.00, 0.00, 'EGP', 36, 0, '2021-07-11 18:36:53'),
(9, 1, '10/25/2021', '10/26/2021', 'DOUBLE CABINS', '2 Person', 'Normal Package', 'test', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 0.00, 0.00, 'EGP', 1, 0, '2021-10-24 08:33:57'),
(10, 1, '10/25/2021', '10/26/2021', 'DOUBLE CABINS', '2 Person', 'Normal Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 0.00, 0.00, 'EGP', 1, 0, '2021-10-24 08:35:03'),
(11, 1, '10/25/2021', '10/26/2021', 'LUXURY CABINS', '2 Person', 'Honeymooners Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 200.00, 0.00, 'EGP', 1, 0, '2021-10-24 12:41:33'),
(12, 1, '10/25/2021', '10/26/2021', 'LUXURY CABINS', '1 Person', 'Honeymooners Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 200.00, 0.00, 'EGP', 1, 0, '2021-12-15 00:34:56'),
(13, 1, '10/25/2021', '10/26/2021', 'DOUBLE CABINS', '1 Person', 'Honeymooners Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 200.00, 0.00, 'EGP', 1, 0, '2021-10-24 13:01:27'),
(14, 1, '10/25/2021', '10/26/2021', 'LUXURY CABINS', '2 Person', 'Honeymooners Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 200.00, 0.00, 'EGP', 35, 0, '2021-12-14 15:59:09'),
(15, 1, '10/25/2021', '10/26/2021', 'LUXURY CABINS', '2 Person', 'Honeymooners Package', 'Hany', 'Hisham', 'hany.hisham@sunrise-resorts.com', '21 Intercontinintal Hurghada', '01068267949', 200.00, 0.00, 'EGP', 35, 0, '2021-12-14 23:07:05');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(30) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pax` int(5) NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `offer_id` int(11) NOT NULL,
  `price` decimal(20,2) NOT NULL,
  `currency` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total_cost` decimal(20,2) NOT NULL,
  `status` int(2) NOT NULL,
  `sent_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `type`, `first_name`, `last_name`, `date`, `pax`, `email`, `phone`, `offer_id`, `price`, `currency`, `total_cost`, `status`, `sent_at`, `timestamp`) VALUES
(1, '', 'Menna Allah', 'hisham', '2020-01-16', 3, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', '$', '6000.00', 37, NULL, '2020-01-10 09:09:34'),
(2, '', 'hany', 'hisham', '2020-01-16', 2, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', 'EGP', '4000.00', 35, NULL, '2020-01-10 08:55:52'),
(3, '', 'hany', 'hisham', '2020-01-17', 2, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', '$', '4000.00', 36, NULL, '2020-01-10 09:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `branch_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SUN',
  `logo` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `branch_name`, `group_id`, `code`, `logo`, `deleted`) VALUES
(1, 'Diveing Center', 1, 'DVC', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cabins`
--

CREATE TABLE `cabins` (
  `id` int(5) NOT NULL,
  `boat_id` int(5) NOT NULL DEFAULT 1,
  `title` varchar(150) NOT NULL,
  `remarks` text NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `status` int(5) NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cabins`
--

INSERT INTO `cabins` (`id`, `boat_id`, `title`, `remarks`, `image`, `status`, `rank`, `deleted`, `timestamp`) VALUES
(1, 1, 'DOUBLE CABINS', '', NULL, 2, 1, 0, '2021-07-09 11:47:21'),
(2, 1, 'LUXURY CABINS', '', NULL, 2, 2, 0, '2021-07-09 11:47:21');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`, `lang`) VALUES
('00vnm6obndtsa7emrnu0jmd5ejrntjvq', '::1', 1639949847, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393934393834373b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('053ku0rgsgio4ld0u0v0k9i2r2kt91iv', '::1', 1640637926, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633373932363b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('0g0dfu4hmta7523d1jn8b78ah43de134', '::1', 1639531595, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533313539353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('0g0h7inmp9qr2drnft0ud4q1o28g3qa4', '::1', 1639513738, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('0pll6ddu42jsv47eamhd92el1sk0j96g', '::1', 1640638638, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633383633383b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('0tjuvgtfkfk77n6cp8fk7g13a319oehn', '::1', 1639491994, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439313939343b, ''),
('0tn87bloflqb0kqraknf8qab4rea2teh', '::1', 1640547402, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303534373430323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('11gu6douo8iisrmrpj26sqq524hud9is', '::1', 1640380488, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('127hb9bsfggjr49923oa1b5tr3lunb8k', '::1', 1639528901, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393532383930313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('14kh1qf0ktpcbgko5iucfb6rg43tel1t', '::1', 1639620173, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393632303137333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('16d0bao4jr7vdgk3hheag3262maktmhe', '::1', 1639928246, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393932383234363b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('181lhma6auhvu161lb01ti95covij7i8', '::1', 1639537953, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533373935333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('1b3655b5i7h2bj6d1kvpq2j03uhj5e9i', '::1', 1639494555, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439343535353b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b7265645f75726c7c733a363a226370616e656c223b, ''),
('1pr1vvuolbdpv9lk11hlo6ksf0bigd77', '::1', 1639790823, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393739303832333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('213igpif1kk7kjso745phm14kolvao7s', '::1', 1640123763, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303132333736333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('22g3i107s7f1qpgt6sg22p7jcti604u9', '::1', 1639621111, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393632313131313b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('2puv7qcv9rt69ognqhp9tbrgr9c677ef', '::1', 1639491230, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439313233303b, ''),
('2r46pimh2vcted0ovem1ej01ul4ajit0', '::1', 1640123432, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303132333433323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('333ubtlocoatehndpki3np4rpsjqqlhh', '::1', 1639943385, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393934333338353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('3f7kiedlui3chhj8bg1nh1h7v8hnbn88', '::1', 1639785765, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738353736353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('3li8ah4ms0p246966cgd0ei2k74km378', '::1', 1640554792, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303535343735353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('3o8t7g0junghj7dv60n70uk36ktmc65u', '::1', 1639496618, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439363631383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('3piken5jv01pfum1ducp1qj9p2akhtdl', '::1', 1639533781, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533333738313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('40g68p2l0fujn0vdkbftvt0u4m4qnqiq', '::1', 1639496212, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439363231323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('4586fcpcv20fsa2h82n8ei3tivi59bpp', '::1', 1639614757, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631343735373b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('46bs61lj4cvt5ct0bjl8nsmolm9sa4ol', '::1', 1640554317, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303535343331373b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('48k0bsvbtuhe8bceve6aelfi6uhrpm18', '::1', 1639617198, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631373139383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('4iaui96vh4lhh180sisfovseh2c736vn', '::1', 1640639817, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633393536353b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('4utkpufd28r1nigiori4nueur12i6ne4', '::1', 1639528330, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393532383333303b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('51ogo76gmg9il22sqp95c62lug1fdnma', '::1', 1639786914, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738363931343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('546fd3p2gia4hq5cieas5nc4ivr8s4la', '::1', 1639791581, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393739313132383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('54g2qb3tumk8187m7blqndv4v2rtfim6', '::1', 1639491644, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439313634343b, ''),
('57okia0sa5g5t1gi413midb040vfmb6f', '::1', 1639619840, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631393834303b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('59gfte5nvvl22lqsshgg8toq29qs1dt0', '::1', 1640639565, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633393536353b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('5fnqjuetei594n927v1qml114g9e2hf8', '::1', 1639618070, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631383037303b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('5lrd3v0rc10s7s58s49e656s6cifbg60', '::1', 1639788270, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738383237303b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('5mi7rodgd04hpiag8i4e81d5om2nvvj7', '::1', 1639495021, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439353032313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('5qs4pnhujakc4p0dbfjq7uanji6scj23', '::1', 1639944923, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393934343932333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('5vhac02jfsmh9jt4n3icji5ddakmnjcg', '::1', 1639945998, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393934353939383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('61hri4trpaia61a6q9bjub7pkkloqaj4', '::1', 1640549801, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303534393830313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('6ffgalm49c7g1l8v5qkqht1t22jc09uc', '::1', 1640301112, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('6gknuavcq8vqltv387pd3d2q8l2ol928', '::1', 1639788584, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738383538343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('6ita24digf37ftv8ohs32kniv61ck4eh', '::1', 1639495865, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439353836353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('6k313vlkt24ljq6et400s3dbdpeo16au', '::1', 1639536219, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533363231393b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('6leqfmu98ak1110dd3bt42kspmrg5rpd', '::1', 1640638943, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633383934333b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('6n2apgjjakrl7i8ppoelc47nse0au48i', '::1', 1639789866, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738393836363b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('7ltapetbcp2pfo2bdnnqork9dgj30og4', '::1', 1639928861, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393932383832333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('7u4ingvf6ulkuegal4f2liifc7dn8j0c', '::1', 1639492416, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439323431363b, ''),
('8ihpvsaf5qk6o6scqcrj90jgng1ivibs', '::1', 1639747914, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('8oamvk2usp9gj6ja49tnq7gsb0gi1u3i', '::1', 1639537562, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533373536323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('8q6p7g4u0h20tg4u9o3f4iudcb4rjkj1', '::1', 1640617592, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303631373539323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b616c6572747c733a383a227375636373657373223b6d73677c733a33393a225265636f7264732068617665206265656e2075706461746564205375636365737366756c6c7921223b5f5f63695f766172737c613a323a7b733a353a22616c657274223b733a333a226f6c64223b733a333a226d7367223b733a333a226f6c64223b7d, ''),
('8qcqi44qnv95c62n2tesl5mkolh6nuvb', '::1', 1639747868, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('9b5h3bu20cqtgobv0qcil65ap6d7vm46', '::1', 1640121118, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303132313131383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('9cl8d7c1o0gm9ughn60bp6e8fi5ort34', '::1', 1640454274, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('9npsa220nkch7lkqvrpbnvo01ipvr8tc', '::1', 1639788901, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738383930313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('9t8e758701vaiook0inc9e66oguh1tef', '::1', 1639945557, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393934353535373b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('adv6bgu6h68cndr8ml6mmunpa5hd4v5t', '::1', 1640124100, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303132343130303b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('afrebvk9ckve3chkfi663811lofmd5vb', '::1', 1640630777, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633303737373b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('aie2u2hq808hpo7qadsoanp9cv63ukfi', '::1', 1640116977, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303131363937373b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('aj6fm3rjjrsdru4kdm95dktlklmkf2dg', '::1', 1640593204, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('ajobb4iekmtkbel0ktc82nkbuf0nrtqe', '::1', 1639616870, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631363837303b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('akg4ilmfp0lsun63b2p10t1lh3dauqll', '::1', 1639680161, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393637383037323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('anbgkhkcacba4v4rsd8e37vhqtfbbo57', '::1', 1640554755, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303535343735353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('bjdip23mgo9i24mdcvsc0r0jlf4jf2s7', '::1', 1639620789, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393632303738393b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('bl3f4n6ts1t86l5scblee6iaur2app9b', '::1', 1639678072, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393637383037323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('cc4punnmjfohq2hhcobjoj9rbdtc8060', '::1', 1639784296, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738343239363b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('cpt52ri3eh1ri62rtbutg5k71r4av89s', '::1', 1639530203, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533303230333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('d00u2n06doa20g2r72a8u6kbuucop9cc', '::1', 1640618262, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303631383236323b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('df3fe5sunlhn65j28f113cne2h4d5j7i', '::1', 1639490903, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439303930333b, ''),
('dg0sa5fgdn5j2mn3796lfop6bmb4746c', '::1', 1640124856, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303132343535353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('domd3eeuj03d8th25igap873pnoedk1u', '::1', 1639621974, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393632313131313b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('eb7pmkl0hfnd4gm85g0eihpkj07ac9mk', '::1', 1640292706, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303239323730363b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('ec29hcf48fr4id0s3qtgcc7prnpn2ndj', '::1', 1639951377, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393935303938373b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('ed5no7rnl9qms87077d6l81gkdj0hkfp', '::1', 1640635608, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633353630383b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('f7pie25kc1rjc625ihgciobmkm4q3g1l', '::1', 1640511559, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('f9sokpu4ae37ua4pcssggekp7n4lacib', '::1', 1639786125, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738363132353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('fcv19gkbj47q1lfhf6ricbfrkuu3j3al', '::1', 1640629706, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303632393730363b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('fe3deh8kors9ni29h1gobmd2al99fccu', '::1', 1639786500, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738363530303b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('fh457m84skhghfkl9r2sbc0nusvtamn5', '::1', 1639600533, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('fjo93l725khl4srkdh269oh8oud8rv3i', '::1', 1639928823, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393932383832333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('fopidb04dlcl50j694l9l9s6opldvgk3', '::1', 1639538448, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533383331383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('gbdstg6j4fclj3mvhl4sp3ohq5hrt863', '::1', 1640552553, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303535323535333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('ghkr6ffn2n88rmjcptb0jei1jp5ssr69', '::1', 1639618808, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631383830383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('h67ac0iens8oqkg9m6al0n63nu8hclak', '::1', 1639613906, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631333930363b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('hb102sb7ls3cmu2aorhi73qbpk6vmmeo', '::1', 1639496999, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439363939393b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('hcu8i8hr5rf8qlqv27klbd43gdbibdrv', '::1', 1640124555, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303132343535353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('hi667gra9v7fkie0o3pfb6fhn31hai1r', '::1', 1640121431, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303132313433313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('i5jv8ahmjc50rpgre2saivsgf7t05jdg', '::1', 1639618438, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631383433383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('in1vt6t44g89a7eeh793dcti4bto1tpn', '::1', 1639787951, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738373935313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('j51m5gu0ndqe4f320dc0i1s8i3helesc', '::1', 1639531268, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533313236383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('j963b76d4i52qmd8kb8q8qu9i7ck78ko', '::1', 1640116649, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303131363634393b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('jok0nbcrrs1rvagb8h9kdbkog1e69fhn', '::1', 1640117486, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303131373438363b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('jrm870c495kpn4vc68amb1dnocq9tjtt', '::1', 1640550476, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303535303437363b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('js6lutknmi03k7u581hq5ga79udl8oef', '::1', 1639946554, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393934363535343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('k10ss72e9einlf2u78i462eefnp2b2q5', '::1', 1639530521, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533303532313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('l8vnr3rshnmj89c3t0qi9vngi1hjor9r', '::1', 1639945254, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393934353235343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('m1bdnc8qe7rmms4uegu4k8ikl7knt8e9', '::1', 1640292373, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303239323337333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('m2ci0abfjuljem4m6s6st6fgscufcctl', '::1', 1639497535, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439373533353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('m5qrejipnp9v6q8qe01q2areiob13pm5', '::1', 1640635145, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633353134353b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('mcdqbscfpjrnijpqktmkbhi95vdtlta8', '::1', 1639791128, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393739313132383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('meqjl207lt78pmufdsgevaj365qopc60', '::1', 1639947288, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393934373238383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('ml5hdrad5cvpvj68pic2j0gvftr07gse', '::1', 1640292905, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303239323730363b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('mqhnb644bbei5nd6vj85tskevv6imf15', '::1', 1639996106, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('msap49gm831g2mpgeuujg6ed2bet4oh1', '::1', 1640636680, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633363638303b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('n6ad38kfh551uh6oq5580qk556fuj53k', '::1', 1639782572, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738323537323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('n7mrp4cksflbt8c5i787rlr5donjjkl8', '::1', 1640634772, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633343737323b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('n8eqioa94d57idlpsdop5nveqstkiac4', '::1', 1639950212, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393935303231323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('n8h4125al8d6eop68kcaqml74e38l8oo', '::1', 1640116338, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303131363333383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('o923s94tr3riu239auneg2o9ook7dko2', '::1', 1640635995, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633353939353b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('oarvv4ueunlp4psolkrm1d40u7r0q8va', '::1', 1640550103, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303535303130333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('od27hle0bbajkgl8dacf3mn3m1jsjs3u', '::1', 1639538318, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533383331383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('oj32kp3v2aq2nt2dhi2bmrpq9bhn6shf', '::1', 1639530893, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533303839333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('ojpclrvusmimlp90o5a08o1bp2ra7gjk', '::1', 1639620479, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393632303437393b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('opul0dia8oaf61m80942kjobaju2m0cb', '::1', 1640546798, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303534363739383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('p9a2bmvup7bvrpfb1rno52dmsa3v6a8b', '::1', 1639783789, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738333738393b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('pb3698ouam2u6pn09e411b0kb6q34htj', '::1', 1639529869, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393532393836393b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('pi7nuubpmepba16b3on1gq6lssdigatq', '::1', 1639535250, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533353235303b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('pk4q1uupsbtlkh9bcau5neh78uoa340h', '::1', 1639950892, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393935303839323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('q6blkcqr8celi2et1i96upf625oqbj2m', '::1', 1639529538, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393532393533383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('q76or3nlabiqfask7dom4ko1tmji2f18', '::1', 1640630286, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633303238363b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('qvfpmnmrh0e961cjnhe32ucdkucmjs60', '::1', 1639499785, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439373533353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('rfnmmd2i4qufs2nk8hdklth7h70fnfjg', '::1', 1639944528, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393934343532383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('rghibshv9d85v3pt0ncd6nkqqmuli266', '::1', 1640637317, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633373331373b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('s01lerkog6b2f4lburmnq79hvk0bhush', '::1', 1640638243, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633383234333b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('s3a155bhts3s4d87srjuukqnlj00f1ed', '::1', 1640165955, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('sggblllrvb3vjd0keu5dqo0idtsal4a7', '::1', 1640115042, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303131353034323b7265645f75726c7c733a33383a2261646d696e2f736974655f6d616e616765722f636f6d70657469746f72735f6d616e61676572223b, ''),
('t67simnhv08eumb06pelbg2cf05t2qqa', '::1', 1639787648, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738373634383b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('tdr2595n9fmvt9to80s9q91hko4482ld', '::1', 1640117951, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303131373935313b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('te35u3jj6197tsi2o1t139sup6eovaup', '::1', 1640118260, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303131383236303b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('tfpf240i0t7lgp60hg8s85q2m6cp0o6n', '::1', 1640628891, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303632383839313b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('tjg8rir9qp7v5djomgptusmh5tu8jmfh', '::1', 1639677004, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('u44dgc06u7f3cglrpqd334h23ojmsttd', '::1', 1639789263, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738393236333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('u8nv1tcm9smh4p0fg4fl9sbnk6vscn3k', '::1', 1639617714, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631373731343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('u8uko48ttvrqd4jpoata06819mpu3ron', '::1', 1639523030, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393532333033303b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('uc5rjofh9e1pputrjgerg9ba0iobpodr', '::1', 1639619229, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631393232393b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('um4vv838u7n26ps1pkckdlcnh5ipaplb', '::1', 1640636352, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633363335323b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('uo2vmls1untfakrhsttcs931rblg5slu', '::1', 1639619534, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631393533343b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('upu8hbnt27nr1ivcrf45fvbju34lvkrn', '::1', 1639522655, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393532323635353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('uq2o376kommboar6gk7oup1k0rn2pmg3', '::1', 1639616556, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393631363535363b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('ut4omgc2eqrdn8o8n3b09ud58il0f2r5', '::1', 1639537175, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393533373137353b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('uu44f8bku1uhi7o7tsd0nrcdbgd2qisj', '::1', 1639782259, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738323235393b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('uvk71b28b831s6nftklur9di4q490ia6', '::1', 1640547787, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303534373738373b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('v82db3g6bndm9k4conau1pe5honotahd', '::1', 1639492729, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393439323732393b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('vgti5aq47ia1b5o88sc5e00c0mkmukss', '::1', 1640631276, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634303633313237363b757365725f69647c733a313a2231223b6e616d657c733a373a226d61686d6f7564223b69735f61646d696e5f6c6f67696e7c623a313b656d61696c7c733a32343a226d61686d6f75642e6d6f6869657240676d61696c2e636f6d223b69735f636f6d70657469746f727c623a313b, ''),
('vi32qck1tivgt3883thagtfviedg5ffr', '::1', 1639783322, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393738333332323b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('vohec4gb702gnpju7k5j54tl274jvbhs', '::1', 1639790513, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393739303531333b757365725f69647c733a313a2231223b6e616d657c733a31313a2268616e792e68697368616d223b69735f61646d696e5f6c6f67696e7c623a313b, '');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `client_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cash` decimal(20,2) NOT NULL,
  `phone` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cont_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `updated` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clients_mail_queue`
--

CREATE TABLE `clients_mail_queue` (
  `id` int(30) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_info` int(11) DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(2) NOT NULL,
  `sent_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clients_mail_queue`
--

INSERT INTO `clients_mail_queue` (`id`, `type`, `client_info`, `client_name`, `email`, `subject`, `message`, `status`, `sent_at`, `timestamp`) VALUES
(1, '', NULL, '', '', '', NULL, 24, NULL, '2019-11-13 16:54:49'),
(2, '', NULL, '', '', '', NULL, 24, NULL, '2019-11-13 20:35:28'),
(3, '', NULL, '', '', '', NULL, 24, NULL, '2019-11-13 20:36:07'),
(4, '', NULL, 'محمد طرطور', '', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:11:59'),
(5, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:16:24'),
(6, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:20:31'),
(7, '', NULL, 'محمد طرطور', 'admin@admin.com', 'dsds', 'ttttttttttttt', 24, NULL, '2019-11-13 21:22:52'),
(8, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'sfdsfdsdf', 24, NULL, '2019-11-13 21:25:26'),
(9, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'sadfsdfsdf', 24, NULL, '2019-11-13 21:26:18'),
(10, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'scsdfsdf', 24, NULL, '2019-11-13 21:26:54'),
(11, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'rewrwer', 24, NULL, '2019-11-13 21:29:03'),
(12, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:30:30'),
(13, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'adsfsdfsdfsdfsdfdsf', 24, NULL, '2019-11-16 07:30:32'),
(14, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-09 07:59:43'),
(15, '', NULL, 'Johan ', 'mvmservice@hotmail.com', 'Diving center hurghada', 'Hello there. \r\nIs iT true that dive more group at the Beach of mamlouk palace and garden beach is part of your company? We Will be there in a week and going the get Some info to take diving course. We saw your website On there Facebook.   Thnx.', 24, NULL, '2019-12-11 18:00:15'),
(16, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-13 22:52:51'),
(17, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-14 16:43:48'),
(18, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-15 22:16:21'),
(19, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-18 10:11:56'),
(20, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-18 13:09:23'),
(21, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-19 04:44:36'),
(22, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-19 11:46:10'),
(23, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-20 08:43:54'),
(24, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-20 10:41:48'),
(25, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 01:21:04'),
(26, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 07:29:10'),
(27, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 10:44:01'),
(28, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 13:50:01'),
(29, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-22 02:38:28'),
(30, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-22 08:28:23'),
(31, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-22 12:11:19'),
(32, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 02:15:25'),
(33, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 09:48:54'),
(34, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 17:21:37'),
(35, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 21:50:13'),
(36, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-24 02:32:35'),
(37, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-24 23:22:45'),
(38, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-25 08:19:19'),
(39, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-25 09:41:02'),
(40, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-26 05:12:04'),
(41, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-26 08:49:29'),
(42, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 05:21:45'),
(43, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 07:57:24'),
(44, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 14:03:47'),
(45, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 16:33:17'),
(46, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-03 08:23:35'),
(47, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-03 10:48:46'),
(48, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-04 04:50:45'),
(49, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-04 08:58:09'),
(50, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-04 11:04:45'),
(51, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-05 08:17:21'),
(52, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-06 05:39:13'),
(53, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-06 07:50:32'),
(54, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 02:42:28'),
(55, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 06:18:29'),
(56, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 10:58:39'),
(57, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 13:54:56');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(20) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `rank`, `deleted`) VALUES
(1, 'عز', 1, 0),
(2, 'بشاى', 2, 0),
(3, 'سعودى سابك', 3, 0),
(4, 'سرحان', 4, 0),
(5, 'سعودى راجحى', 5, 0),
(6, 'مراكبى', 6, 0),
(7, 'بيانكو', 7, 0),
(8, 'جيوشى', 8, 0),
(9, 'عنتر', 9, 0),
(10, 'مصريين', 10, 0),
(11, 'المصرية', 11, 0),
(12, 'السويدى', 12, 0),
(13, 'الجيش بنى سويف', 13, 0),
(14, 'الجيش العريش', 14, 0),
(15, 'القومية', 15, 0),
(16, 'طرة', 16, 0),
(17, 'المسلح', 17, 0),
(18, 'السويس', 18, 0),
(19, 'سينا', 19, 0),
(20, 'رويال', 20, 0),
(21, 'الدولية', 21, 0),
(22, 'البلاح', 22, 0),
(23, 'نجمة سيناء مصيص', 23, 0),
(24, 'عثمان', 24, 0),
(25, 'بدون', 25, 0);

-- --------------------------------------------------------

--
-- Table structure for table `competitions`
--

CREATE TABLE `competitions` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `start_date` varchar(100) NOT NULL,
  `end_date` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `closed_at` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `price` decimal(5,2) NOT NULL,
  `currency` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `competitions`
--

INSERT INTO `competitions` (`id`, `code`, `type`, `title`, `description`, `image`, `start_date`, `end_date`, `active`, `closed_at`, `rank`, `deleted`, `timestamp`, `price`, `currency`) VALUES
(1, 'comptest', 'yearly', 'World Robot OlympiadTM', ' International Final\r\n SMART Cities\r\n 8-10 November, 2019 - Győr, Hungary', 'world-robot-olympiad_slider2.jpg', '2021-10-27', '2021-12-30', 1, '', 1, 0, '2021-12-02 21:12:34', '500.00', 'EGP'),
(2, 'comptest', 'weekly', 'weekly Robot OlympiadTM', ' International Final\r\n SMART Cities\r\n 8-10 November, 2019 - Győr, Hungary  International Final\r\n SMART Cities\r\n 8-10 Novembe...', 'world-robot-olympiad_slider2.jpg', '2021-10-27', '2021-12-28', 1, '', 1, 0, '2021-12-13 16:20:39', '300.00', 'EGP'),
(3, 'comptest', 'weekly', 'World Robot OlympiadTM', ' International Final\r\n SMART Cities\r\n 8-10 November, 2019 - Győr, Hungary', 'world-robot-olympiad_slider2.jpg', '2021-10-27', '2021-10-28', 0, '', 1, 0, '2021-12-10 00:14:10', '0.00', ''),
(4, 'comptest', 'yearly', 'World Robot OlympiadTM', ' International Final\r\n SMART Cities\r\n 8-10 November, 2019 - Győr, Hungary', 'world-robot-olympiad_slider2.jpg', '2021-10-27', '2021-12-30', 0, '', 1, 0, '2021-12-10 00:14:03', '500.00', 'EGP'),
(5, 'comptest', 'yearly', 'World Robot OlympiadTM', ' International Final\r\n SMART Cities\r\n 8-10 November, 2019 - Győr, Hungary', 'world-robot-olympiad_slider2.jpg', '2021-10-27', '2021-12-30', 0, '2021-12-18 01:09:15', 1, 0, '2021-12-17 23:09:15', '500.00', 'EGP'),
(6, 'COM20216', 'weekly', 'new weekly competition', 'new weekly competition new weekly competition', '4096.jpg', '2021-12-17', '2022-01-09', 1, '', 0, 0, '2021-12-16 01:58:38', '20.00', 'EGP'),
(7, 'COM20217', 'yearly', 'test adding competition update', 'test adding competition', 'Robotics-for-Kids.jpg', '2021-12-01', '2022-01-01', 1, '', 0, 0, '2021-12-18 01:33:47', '400.00', 'EGP'),
(8, 'COM20218', 'yearly', 'season competition', 'season competition season competition', 'banner-competitions.png', '2021-12-14', '2021-12-06', 1, '', 0, 0, '2021-12-18 01:34:36', '200.00', 'EGP');

-- --------------------------------------------------------

--
-- Table structure for table `competitors`
--

CREATE TABLE `competitors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `phone` varchar(150) NOT NULL,
  `age` int(3) NOT NULL,
  `school` varchar(255) NOT NULL,
  `language` varchar(50) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `city` varchar(255) NOT NULL,
  `birth_date` varchar(150) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `profile_picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `competitors`
--

INSERT INTO `competitors` (`id`, `name`, `email`, `password`, `phone`, `age`, `school`, `language`, `disabled`, `created_at`, `updated_at`, `city`, `birth_date`, `middle_name`, `last_name`, `country_code`, `profile_picture`) VALUES
(1, 'mahmoud', 'mahmoud.mohier@gmail.com', '$2y$10$8dM..K/IxTJS87BnavjArey7vIVfLz.sLrmmGPTIQJlNRtJEdZC.C', '01002273237', 17, 'rejac school', '', 0, '2021-12-19 21:56:18', '2021-11-18 22:00:00', 'sadasd', '2021-11-22', 'ibrahem', 'ismail', '20', 'full-nao.png'),
(2, 'hany', 'mahmoud@stalion.com', '$2y$10$1OYo78hFe9KcaqEzNU4F2u1l3Q2mFQA4UoWwDbM1xANW3Ef75ZiGu', '01002273237', 0, 'thanaweya askria', '', 0, '2021-12-12 12:18:46', '2021-12-10 22:00:00', 'sadas', '2021-12-22', 'hesham', 'ismail', '20', '4096.jpg'),
(3, 'omar', 'hany.hisham@sunrise-resorts.com', '$2y$10$NZC4vc9nbaQsZzBbKpQ5buFwe8JxsIAAukcZuOAYeSNSEcKrH.XfG', '01002273237', 0, 'asdD', '', 0, '2021-12-19 21:49:55', '2021-12-10 22:00:00', 'sadas', '2021-12-16', 'mahmoud', 'ismail', '20', 'banner-competitions.png'),
(4, 'shahd', 'shahd.mohier@gmail.com', '$2y$10$NZC4vc9nbaQsZzBbKpQ5buFwe8JxsIAAukcZuOAYeSNSEcKrH.XfG', '01002273237', 0, 'asdD', '', 0, '2021-12-19 21:49:55', '2021-12-10 22:00:00', 'sadas', '2021-12-16', 'mahmoud', 'ismail', '20', 'banner-competitions.png');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `symbol`, `name`, `isdefault`) VALUES
(1, '$', 'USD', 0),
(2, '€', 'EUR', 0),
(3, 'EGP', 'EGP', 1);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `dep_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `dep_name`, `code`, `rank`) VALUES
(1, 'الإدارة', 'MG', 1),
(2, 'الحسابات', 'ACC', 2);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(5) NOT NULL,
  `location` varchar(100) NOT NULL,
  `event` varchar(250) NOT NULL,
  `describtion` text NOT NULL,
  `rank` int(5) NOT NULL,
  `image` varchar(100) NOT NULL,
  `toped` double(5,2) NOT NULL,
  `lefted` double(5,2) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `location`, `event`, `describtion`, `rank`, `image`, `toped`, `lefted`, `active`, `deleted`) VALUES
(1, 'Luxor', '', 'Is a city on the east bank of the nile river in southern Egypt.', 1, 'Pointer.png', 34.50, 54.50, 1, 0),
(2, 'El Kab', '', 'Is an upper Egyptian site on the east bank of the Nile at the mounts of the wadi hillal.', 2, 'Pointer.png', 37.00, 52.50, 1, 0),
(3, 'Edfu', '', 'Is an Egyptian city, edfu is site of the Ptolemaic temple of Horus.', 3, 'Pointer.png', 51.50, 57.50, 1, 0),
(4, 'Gebel Silsila', '', 'Is 65km of aswan in upper Egypt where the cliffs on both sides close to the narrowest point along the length of the entire nile.', 4, 'Pointer.png', 57.70, 59.30, 1, 0),
(5, 'Kom Ombo', '', 'Is an agricultural town in Egypt famous for the temple of kom ombo, meaning city of gold.', 5, 'Pointer.png', 60.50, 60.00, 1, 0),
(6, 'Aswan', '', 'A city on the Nile river. Has been southern Egypt’s strategic and commercial gateway since antiquity.', 6, 'Pointer.png', 66.00, 57.70, 1, 0),
(7, 'Philae', '', 'Is an island in the reservoir of the aswan low dam.', 7, 'Pointer.png', 67.70, 58.40, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `form_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `font_icons`
--

CREATE TABLE `font_icons` (
  `id` int(11) NOT NULL,
  `icon_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `font_icons`
--

INSERT INTO `font_icons` (`id`, `icon_name`, `rank`) VALUES
(1, 'fas fa-arrow-alt-circle-left', 1),
(2, 'fa-music', 2),
(3, 'fa-search', 3),
(4, 'fa-envelope-o', 4),
(5, 'fa-heart', 5),
(6, 'fa-star', 6),
(7, 'fa-star-o', 7),
(8, 'fa-user', 8),
(9, 'fa-film', 9),
(10, 'fa-th-large', 10),
(11, 'fa-th', 11),
(12, 'fa-th-list', 12),
(13, 'fa-check', 13),
(14, 'fa-remove', 14),
(15, 'fa-close', 15),
(16, 'fa-times', 16),
(17, 'fa-search-plus', 17),
(18, 'fa-search-minus', 18),
(19, 'fa-power-off', 19),
(20, 'fa-signal', 20),
(21, 'fa-gear', 21),
(22, 'fa-cog', 22),
(23, 'fa-trash-o', 23),
(24, 'fa-home', 24),
(25, 'fa-file-o', 25),
(26, 'fa-clock-o', 26),
(27, 'fa-road', 27),
(28, 'fa-download', 28),
(29, 'fa-arrow-circle-o-do', 29),
(30, 'fa-arrow-circle-o-up', 30),
(31, 'fa-inbox', 31),
(32, 'fa-play-circle-o', 32),
(33, 'fa-rotate-right', 33),
(34, 'fa-repeat', 34),
(35, 'fa-refresh', 35),
(36, 'fa-list-alt', 36),
(37, 'fa-lock', 37),
(38, 'fa-flag', 38),
(39, 'fa-headphones', 39),
(40, 'fa-volume-off', 40),
(41, 'fa-volume-down', 41),
(42, 'fa-volume-up', 42),
(43, 'fa-qrcode', 43),
(44, 'fa-barcode', 44),
(45, 'fa-tag', 45),
(46, 'fa-tags', 46),
(47, 'fa-book', 47),
(48, 'fa-bookmark', 48),
(49, 'fa-print f', 49),
(50, 'fa-camera', 50),
(51, 'fa-font', 51),
(52, 'fa-bold', 52),
(53, 'fa-italic', 53),
(54, 'fa-text-height', 54),
(55, 'fa-text-width', 55),
(56, 'fa-align-left', 56),
(57, 'fa-align-center', 57),
(58, 'fa-align-right', 58),
(59, 'fa-align-justify', 59),
(60, 'fa-list', 60),
(61, 'fa-dedent', 61),
(62, 'fa-outdent', 62),
(63, 'fa-indent', 63),
(64, 'fa-video-camera', 64),
(65, 'fa-photo', 65),
(66, 'fa-image', 66),
(67, 'fa-picture-o', 67),
(68, 'fa-pencil', 68),
(69, 'fa-map-marker', 69),
(70, 'fa-adjust', 70),
(71, 'fa-tint', 71),
(72, 'fa-edit', 72),
(73, 'fa-pencil-square-o', 73),
(74, 'fa-share-square-o', 74),
(75, 'fa-check-square-o', 75),
(76, 'fa-arrows', 76),
(77, 'fa-step-backward', 77),
(78, 'fa-fast-backward', 78),
(79, 'fa-backward', 79),
(80, 'fa-play', 80),
(81, 'fa-pause', 81),
(82, 'fa-stop', 82),
(83, 'fa-forward', 83),
(84, 'fa-fast-forward', 84),
(85, 'fa-step-forward', 85),
(86, 'fa-eject', 86),
(87, 'fa-chevron-left', 87),
(88, 'fa-chevron-right', 88),
(89, 'fa-plus-circle', 89),
(90, 'fa-minus-circle', 90),
(91, 'fa-times-circle', 91),
(92, 'fa-check-circle', 92),
(93, 'fa-question-circle', 93),
(94, 'fa-info-circle', 94),
(95, 'fa-crosshairs', 95),
(96, 'fa-times-circle-o', 96),
(97, 'fa-check-circle-o', 97),
(98, 'fa-ban', 98),
(99, 'fa-arrow-left', 99),
(100, 'fa-arrow-right', 100),
(101, 'fa-arrow-up', 101),
(102, 'fa-arrow-down', 102),
(103, 'fa-mail-forward', 103),
(104, 'fa-share', 104),
(105, 'fa-expand', 105),
(106, 'fa-compress', 106),
(107, 'fa-plus', 107),
(108, 'fa-minus', 108),
(109, 'fa-asterisk', 109),
(110, 'fa-exclamation-circl', 110),
(111, 'fa-gift', 111),
(112, 'fa-leaf', 112),
(113, 'fa-fire', 113),
(114, 'fa-eye', 114),
(115, 'fa-eye-slash', 115),
(116, 'fa-warning', 116),
(117, 'fa-exclamation-trian', 117),
(118, 'fa-plane', 118),
(119, 'fa-calendar', 119),
(120, 'fa-random', 120),
(121, 'fa-comment', 121),
(122, 'fa-magnet', 122),
(123, 'fa-chevron-up', 123),
(124, 'fa-chevron-down', 124),
(125, 'fa-retweet', 125),
(126, 'fa-shopping-cart', 126),
(127, 'fa-folder', 127),
(128, 'fa-folder-open', 128),
(129, 'fa-arrows-v', 129),
(130, 'fa-arrows-h', 130),
(131, 'fa-bar-chart-o', 131),
(132, 'fa-bar-chart', 132),
(133, 'fa-twitter-square', 133),
(134, 'fa-facebook-square', 134),
(135, 'fa-camera-retro', 135),
(136, 'fa-key', 136),
(137, 'fa-gears', 137),
(138, 'fa-cogs', 138),
(139, 'fa-comments', 139),
(140, 'fa-thumbs-o-up', 140),
(141, 'fa-thumbs-o-down', 141),
(142, 'fa-star-half', 142),
(143, 'fa-heart-o', 143),
(144, 'fa-sign-out', 144),
(145, 'fa-linkedin-square', 145),
(146, 'fa-thumb-tack', 146),
(147, 'fa-external-link', 147),
(148, 'fa-sign-in', 148),
(149, 'fa-trophy', 149),
(150, 'fa-github-square', 150),
(151, 'fa-upload', 151),
(152, 'fa-lemon-o', 152),
(153, 'fa-phone', 153),
(154, 'fa-square-o', 154),
(155, 'fa-bookmark-o', 155),
(156, 'fa-phone-square', 156),
(157, 'fa-twitter', 157),
(158, 'fa-facebook-f', 158),
(159, 'fa-facebook', 159),
(160, 'fa-github', 160),
(161, 'fa-unlock', 161),
(162, 'fa-credit-card', 162),
(163, 'fa-rss', 163),
(164, 'fa-hdd-o', 164),
(165, 'fa-bullhorn', 165),
(166, 'fa-bell', 166),
(167, 'fa-certificate', 167),
(168, 'fa-hand-o-right', 168),
(169, 'fa-hand-o-left', 169),
(170, 'fa-hand-o-up', 170),
(171, 'fa-hand-o-down', 171),
(172, 'fa-arrow-circle-left', 172),
(173, 'fa-arrow-circle-righ', 173),
(174, 'fa-arrow-circle-up', 174),
(175, 'fa-arrow-circle-down', 175),
(176, 'fa-globe', 176),
(177, 'fa-wrench', 177),
(178, 'fa-tasks', 178),
(179, 'fa-filter', 179),
(180, 'fa-briefcase', 180),
(181, 'fa-arrows-alt', 181),
(182, 'fa-group', 182),
(183, 'fa-users', 183),
(184, 'fa-chain', 184),
(185, 'fa-link', 185),
(186, 'fa-cloud', 186),
(187, 'fa-flask', 187),
(188, 'fa-cut', 188),
(189, 'fa-scissors', 189),
(190, 'fa-copy', 190),
(191, 'fa-files-o', 191),
(192, 'fa-paperclip', 192),
(193, 'fa-save', 193),
(194, 'fa-floppy-o', 194),
(195, 'fa-square', 195),
(196, 'fa-navicon', 196),
(197, 'fa-reorder', 197),
(198, 'fa-bars', 198),
(199, 'fa-list-ul', 199),
(200, 'fa-list-ol', 200),
(201, 'fa-strikethrough', 201),
(202, 'fa-underline', 202),
(203, 'fa-table', 203),
(204, 'fa-magic', 204),
(205, 'fa-truck', 205),
(206, 'fa-pinterest', 206),
(207, 'fa-pinterest-square', 207),
(208, 'fa-google-plus-squar', 208),
(209, 'fa-google-plus', 209),
(210, 'fa-money', 210),
(211, 'fa-caret-down', 211),
(212, 'fa-caret-up', 212),
(213, 'fa-caret-left', 213),
(214, 'fa-caret-right', 214),
(215, 'fa-columns', 215),
(216, 'fa-unsorted', 216),
(217, 'fa-sort', 217),
(218, 'fa-sort-down', 218),
(219, 'fa-sort-desc', 219),
(220, 'fa-sort-up', 220),
(221, 'fa-sort-asc', 221),
(222, 'fa-envelope', 222),
(223, 'fa-linkedin', 223),
(224, 'fa-rotate-left', 224),
(225, 'fa-undo', 225),
(226, 'fa-legal', 226),
(227, 'fa-gavel', 227),
(228, 'fa-dashboard', 228),
(229, 'fa-tachometer', 229),
(230, 'fa-comment-o', 230),
(231, 'fa-comments-o', 231),
(232, 'fa-flash', 232),
(233, 'fa-bolt', 233),
(234, 'fa-sitemap', 234),
(235, 'fa-umbrella', 235),
(236, 'fa-paste', 236),
(237, 'fa-clipboard', 237),
(238, 'fa-lightbulb-o', 238),
(239, 'fa-exchange', 239),
(240, 'fa-cloud-download', 240),
(241, 'fa-cloud-upload', 241),
(242, 'fa-user-md', 242),
(243, 'fa-stethoscope', 243),
(244, 'fa-suitcase', 244),
(245, 'fa-bell-o', 245),
(246, 'fa-coffee', 246),
(247, 'fa-cutlery', 247),
(248, 'fa-file-text-o', 248),
(249, 'fa-building-o', 249),
(250, 'fa-hospital-o', 250),
(251, 'fa-ambulance', 251),
(252, 'fa-medkit', 252),
(253, 'fa-fighter-jet', 253),
(254, 'fa-beer', 254),
(255, 'fa-h-square', 255),
(256, 'fa-plus-square', 256),
(257, 'fa-angle-double-left', 257),
(258, 'fa-angle-double-righ', 258),
(259, 'fa-angle-double-up', 259),
(260, 'fa-angle-double-down', 260),
(261, 'fa-angle-left', 261),
(262, 'fa-angle-right', 262),
(263, 'fa-angle-up', 263),
(264, 'fa-angle-down', 264),
(265, 'fa-desktop', 265),
(266, 'fa-laptop', 266),
(267, 'fa-tablet', 267),
(268, 'fa-mobile-phone', 268),
(269, 'fa-mobile', 269),
(270, 'fa-circle-o', 270),
(271, 'fa-quote-left', 271),
(272, 'fa-quote-right', 272),
(273, 'fa-spinner', 273),
(274, 'fa-circle', 274),
(275, 'fa-mail-reply', 275),
(276, 'fa-reply', 276),
(277, 'fa-github-alt', 277),
(278, 'fa-folder-o', 278),
(279, 'fa-folder-open-o', 279),
(280, 'fa-smile-o', 280),
(281, 'fa-frown-o', 281),
(282, 'fa-meh-o', 282),
(283, 'fa-gamepad', 283),
(284, 'fa-keyboard-o', 284),
(285, 'fa-flag-o', 285),
(286, 'fa-flag-checkered', 286),
(287, 'fa-terminal', 287),
(288, 'fa-code', 288),
(289, 'fa-mail-reply-all', 289),
(290, 'fa-reply-all', 290),
(291, 'fa-star-half-empty', 291),
(292, 'fa-star-half-full', 292),
(293, 'fa-star-half-o', 293),
(294, 'fa-location-arrow', 294),
(295, 'fa-crop', 295),
(296, 'fa-code-fork', 296),
(297, 'fa-unlink', 297),
(298, 'fa-chain-broken', 298),
(299, 'fa-question', 299),
(300, 'fa-info', 300),
(301, 'fa-exclamation', 301),
(302, 'fa-superscript', 302),
(303, 'fa-subscript', 303),
(304, 'fa-eraser', 304),
(305, 'fa-puzzle-piece', 305),
(306, 'fa-microphone', 306),
(307, 'fa-microphone-slash', 307),
(308, 'fa-shield', 308),
(309, 'fa-calendar-o', 309),
(310, 'fa-fire-extinguisher', 310),
(311, 'fa-rocket', 311),
(312, 'fa-maxcdn', 312),
(313, 'fa-chevron-circle-le', 313),
(314, 'fa-chevron-circle-ri', 314),
(315, 'fa-chevron-circle-up', 315),
(316, 'fa-chevron-circle-do', 316),
(317, 'fa-html5', 317),
(318, 'fa-css3', 318),
(319, 'fa-anchor', 319),
(320, 'fa-unlock-alt', 320),
(321, 'fa-bullseye', 321),
(322, 'fa-ellipsis-h', 322),
(323, 'fa-ellipsis-v', 323),
(324, 'fa-rss-square', 324),
(325, 'fa-play-circle', 325),
(326, 'fa-ticket', 326),
(327, 'fa-minus-square', 327),
(328, 'fa-minus-square-o', 328),
(329, 'fa-level-up', 329),
(330, 'fa-level-down', 330),
(331, 'fa-check-square', 331),
(332, 'fa-pencil-square', 332),
(333, 'fa-external-link-squ', 333),
(334, 'fa-share-square', 334),
(335, 'fa-compass', 335),
(336, 'fa-toggle-down', 336),
(337, 'fa-caret-square-o-do', 337),
(338, 'fa-toggle-up', 338),
(339, 'fa-caret-square-o-up', 339),
(340, 'fa-toggle-right', 340),
(341, 'fa-caret-square-o-ri', 341),
(342, 'fa-euro', 342),
(343, 'fa-eur', 343),
(344, 'fa-gbp', 344),
(345, 'fa-dollar', 345),
(346, 'fa-usd', 346),
(347, 'fa-rupee', 347),
(348, 'fa-inr', 348),
(349, 'fa-cny', 349),
(350, 'fa-rmb', 350),
(351, 'fa-yen', 351),
(352, 'fa-jpy', 352),
(353, 'fa-ruble', 353),
(354, 'fa-rouble', 354),
(355, 'fa-rub', 355),
(356, 'fa-won', 356),
(357, 'fa-krw', 357),
(358, 'fa-bitcoin', 358),
(359, 'fa-btc', 359),
(360, 'fa-file', 360),
(361, 'fa-file-text', 361),
(362, 'fa-sort-alpha-asc', 362),
(363, 'fa-sort-alpha-desc', 363),
(364, 'fa-sort-amount-asc', 364),
(365, 'fa-sort-amount-desc', 365),
(366, 'fa-sort-numeric-asc', 366),
(367, 'fa-sort-numeric-desc', 367),
(368, 'fa-thumbs-up', 368),
(369, 'fa-thumbs-down', 369),
(370, 'fa-youtube-square', 370),
(371, 'fa-youtube', 371),
(372, 'fa-xing', 372),
(373, 'fa-xing-square', 373),
(374, 'fa-youtube-play', 374),
(375, 'fa-dropbox', 375),
(376, 'fa-stack-overflow', 376),
(377, 'fa-instagram', 377),
(378, 'fa-flickr', 378),
(379, 'fa-adn', 379),
(380, 'fa-bitbucket', 380),
(381, 'fa-bitbucket-square', 381),
(382, 'fa-tumblr', 382),
(383, 'fa-tumblr-square', 383),
(384, 'fa-long-arrow-down', 384),
(385, 'fa-long-arrow-up', 385),
(386, 'fa-long-arrow-left', 386),
(387, 'fa-long-arrow-right', 387),
(388, 'fa-apple', 388),
(389, 'fa-windows', 389),
(390, 'fa-android', 390),
(391, 'fa-linux', 391),
(392, 'fa-dribbble', 392),
(393, 'fa-skype', 393),
(394, 'fa-foursquare', 394),
(395, 'fa-trello', 395),
(396, 'fa-female', 396),
(397, 'fa-male', 397),
(398, 'fa-gittip', 398),
(399, 'fa-gratipay', 399),
(400, 'fa-sun-o', 400),
(401, 'fa-moon-o', 401),
(402, 'fa-archive', 402),
(403, 'fa-bug', 403),
(404, 'fa-vk', 404),
(405, 'fa-weibo', 405),
(406, 'fa-renren', 406),
(407, 'fa-pagelines', 407),
(408, 'fa-stack-exchange', 408),
(409, 'fa-arrow-circle-o-ri', 409),
(410, 'fa-arrow-circle-o-le', 410),
(411, 'fa-toggle-left', 411),
(412, 'fa-caret-square-o-le', 412),
(413, 'fa-dot-circle-o', 413),
(414, 'fa-wheelchair', 414),
(415, 'fa-vimeo-square', 415),
(416, 'fa-turkish-lira', 416),
(417, 'fa-try', 417),
(418, 'fa-plus-square-o', 418),
(419, 'fa-space-shuttle', 419),
(420, 'fa-slack', 420),
(421, 'fa-envelope-square', 421),
(422, 'fa-wordpress', 422),
(423, 'fa-openid', 423),
(424, 'fa-institution', 424),
(425, 'fa-bank', 425),
(426, 'fa-university', 426),
(427, 'fa-mortar-board', 427),
(428, 'fa-graduation-cap', 428),
(429, 'fa-yahoo', 429),
(430, 'fa-google', 430),
(431, 'fa-reddit', 431),
(432, 'fa-reddit-square', 432),
(433, 'fa-stumbleupon-circl', 433),
(434, 'fa-stumbleupon', 434),
(435, 'fa-delicious', 435),
(436, 'fa-digg', 436),
(437, 'fa-pied-piper', 437),
(438, 'fa-pied-piper-alt', 438),
(439, 'fa-drupal', 439),
(440, 'fa-joomla', 440),
(441, 'fa-language', 441),
(442, 'fa-fax', 442),
(443, 'fa-building', 443),
(444, 'fa-child', 444),
(445, 'fa-paw', 445),
(446, 'fa-spoon', 446),
(447, 'fa-cube', 447),
(448, 'fa-cubes', 448),
(449, 'fa-behance', 449),
(450, 'fa-behance-square', 450),
(451, 'fa-steam', 451),
(452, 'fa-steam-square', 452),
(453, 'fa-recycle', 453),
(454, 'fa-automobile', 454),
(455, 'fa-car', 455),
(456, 'fa-cab', 456),
(457, 'fa-taxi', 457),
(458, 'fa-tree', 458),
(459, 'fa-spotify', 459),
(460, 'fa-deviantart', 460),
(461, 'fa-soundcloud', 461),
(462, 'fa-database', 462),
(463, 'fa-file-pdf-o', 463),
(464, 'fa-file-word-o', 464),
(465, 'fa-file-excel-o', 465),
(466, 'fa-file-powerpoint-o', 466),
(467, 'fa-file-photo-o', 467),
(468, 'fa-file-picture-o', 468),
(469, 'fa-file-image-o', 469),
(470, 'fa-file-zip-o', 470),
(471, 'fa-file-archive-o', 471),
(472, 'fa-file-sound-o', 472),
(473, 'fa-file-audio-o', 473),
(474, 'fa-file-movie-o', 474),
(475, 'fa-file-video-o', 475),
(476, 'fa-file-code-o', 476),
(477, 'fa-vine', 477),
(478, 'fa-codepen', 478),
(479, 'fa-jsfiddle', 479),
(480, 'fa-life-bouy', 480),
(481, 'fa-life-buoy', 481),
(482, 'fa-life-saver', 482),
(483, 'fa-support', 483),
(484, 'fa-life-ring', 484),
(485, 'fa-circle-o-notch', 485),
(486, 'fa-ra', 486),
(487, 'fa-rebel', 487),
(488, 'fa-ge', 488),
(489, 'fa-empire', 489),
(490, 'fa-git-square', 490),
(491, 'fa-git', 491),
(492, 'fa-hacker-news', 492),
(493, 'fa-tencent-weibo', 493),
(494, 'fa-qq', 494),
(495, 'fa-wechat', 495),
(496, 'fa-weixin', 496),
(497, 'fa-send', 497),
(498, 'fa-paper-plane', 498),
(499, 'fa-send-o', 499),
(500, 'fa-paper-plane-o', 500),
(501, 'fa-history', 501),
(502, 'fa-genderless', 502),
(503, 'fa-circle-thin', 503),
(504, 'fa-header', 504),
(505, 'fa-paragraph', 505),
(506, 'fa-sliders', 506),
(507, 'fa-share-alt', 507),
(508, 'fa-share-alt-square', 508),
(509, 'fa-bomb', 509),
(510, 'fa-soccer-ball-o', 510),
(511, 'fa-futbol-o', 511),
(512, 'fa-tty', 512),
(513, 'fa-binoculars', 513),
(514, 'fa-plug', 514),
(515, 'fa-slideshare', 515),
(516, 'fa-twitch', 516),
(517, 'fa-yelp', 517),
(518, 'fa-newspaper-o', 518),
(519, 'fa-wifi', 519),
(520, 'fa-calculator', 520),
(521, 'fa-paypal', 521),
(522, 'fa-google-wallet', 522),
(523, 'fa-cc-visa', 523),
(524, 'fa-cc-mastercard', 524),
(525, 'fa-cc-discover', 525),
(526, 'fa-cc-amex', 526),
(527, 'fa-cc-paypal', 527),
(528, 'fa-cc-stripe', 528),
(529, 'fa-bell-slash', 529),
(530, 'fa-bell-slash-o', 530),
(531, 'fa-trash', 531),
(532, 'fa-copyright', 532),
(533, 'fa-at', 533),
(534, 'fa-eyedropper', 534),
(535, 'fa-paint-brush', 535),
(536, 'fa-birthday-cake', 536),
(537, 'fa-area-chart', 537),
(538, 'fa-pie-chart', 538),
(539, 'fa-line-chart', 539),
(540, 'fa-lastfm', 540),
(541, 'fa-lastfm-square', 541),
(542, 'fa-toggle-off', 542),
(543, 'fa-toggle-on', 543),
(544, 'fa-bicycle', 544),
(545, 'fa-bus', 545),
(546, 'fa-ioxhost', 546),
(547, 'fa-angellist', 547),
(548, 'fa-cc za', 548),
(549, 'fa-shekel', 549),
(550, 'fa-sheqel', 550),
(551, 'fa-ils', 551),
(552, 'fa-meanpath', 552),
(553, 'fa-buysellads', 553),
(554, 'fa-connectdevelop', 554),
(555, 'fa-dashcube', 555),
(556, 'fa-forumbee', 556),
(557, 'fa-leanpub', 557),
(558, 'fa-sellsy', 558),
(559, 'fa-shirtsinbulk', 559),
(560, 'fa-simplybuilt', 560),
(561, 'fa-skyatlas', 561),
(562, 'fa-cart-plus', 562),
(563, 'fa-cart-arrow-down', 563),
(564, 'fa-diamond', 564),
(565, 'fa-ship', 565),
(566, 'fa-user-secret', 566),
(567, 'fa-motorcycle', 567),
(568, 'fa-street-view', 568),
(569, 'fa-heartbeat', 569),
(570, 'fa-venus', 570),
(571, 'fa-mars', 571),
(572, 'fa-mercury', 572),
(573, 'fa-transgender', 573),
(574, 'fa-transgender-alt', 574),
(575, 'fa-venus-double', 575),
(576, 'fa-mars-double', 576),
(577, 'fa-venus-mars', 577),
(578, 'fa-mars-stroke', 578),
(579, 'fa-mars-stroke-v', 579),
(580, 'fa-mars-stroke-h', 580),
(581, 'fa-neuter', 581),
(582, 'fa-facebook-official', 582),
(583, 'fa-pinterest-p', 583),
(584, 'fa-whatsapp', 584),
(585, 'fa-server', 585),
(586, 'fa-user-plus', 586),
(587, 'fa-user-times', 587),
(588, 'fa-hotel', 588),
(589, 'fa-bed', 589),
(590, 'fa-viacoin', 590),
(591, 'fa-train', 591),
(592, 'fa-subway', 592),
(593, 'fa-medium', 593),
(594, 'fa-snowflake', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gallary`
--

CREATE TABLE `gallary` (
  `id` int(11) NOT NULL,
  `thumb` varchar(150) NOT NULL,
  `img` varchar(150) NOT NULL,
  `categories` text DEFAULT NULL,
  `title` varchar(150) NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallary`
--

INSERT INTO `gallary` (`id`, `thumb`, `img`, `categories`, `title`, `rank`, `deleted`) VALUES
(1, '1.png', '1.jpg', '{\"pf-icons\":\"Aswan\",\"pf-media\":\"Hurghada\"}', 'Dahabya', 1, 0),
(2, '2.png', '2.jpg', '{\"pf-illustrations\":\"Luxor\",\"pf-uielements\":\"Cairo\"}', 'Dahabya', 2, 0),
(3, '3.png', '3.jpg', '{\"pf-uielements\":\"Cairo\",\"pf-graphics\":\"Sharm El-Shekh\"}', 'Dahabya', 3, 0),
(4, '4.png', '4.jpg', '{\"pf-illustrations\":\"Luxor\",\"pf-media\":\"Hurghada\"}', 'Dahabya', 4, 0),
(5, '5.png', '5.jpg', '{\"pf-graphics\":\"Sharm El-Shekh\"}', 'Dahabya', 5, 0),
(6, '6.png', '6.jpg', '{\"pf-icons\":\"Aswan\",\"pf-uielements\":\"Cairo\"}', 'Dahabya', 6, 0),
(7, '7.png', '7.jpg', '{\"pf-icons\":\"Aswan\",\"pf-media\":\"Hurghada\"}', 'Dahabya', 7, 0),
(8, '8.png', '8.jpg', '{\"pf-icons\":\"Aswan\",\"pf-graphics\":\"Sharm El-Shekh\"}', 'Dahabya', 8, 0),
(9, '9.png', '9.jpg', '{\"pf-icons\":\"Aswan\",\"pf-media\":\"Hurghada\"}', 'Dahabya', 9, 0),
(10, '10.png', '10.jpg', '{\"pf-illustrations\":\"Luxor\",\"pf-icons\":\"Aswan\"}', 'Dahabya', 10, 0),
(11, '11.png', '11.jpg', '{\"pf-icons\":\"Aswan\",\"pf-media\":\"Hurghada\"}', 'Dahabya', 11, 0),
(12, '12.png', '12.jpg', '{\"pf-illustrations\":\"Luxor\",\"pf-icons\":\"Aswan\"}', 'Dahabya', 12, 0),
(13, '13.png', '13.jpg', '{\"pf-icons\":\"Aswan\",\"pf-uielements\":\"Cairo\"}', 'Dahabya', 13, 0),
(14, '14.png', '14.jpg', '{\"pf-icons\":\"Aswan\",\"pf-graphics\":\"Sharm El-Shekh\"}', 'Dahabya', 14, 0),
(15, '15.png', '15.jpg', '{\"pf-icons\":\"Aswan\",\"pf-graphics\":\"Sharm El-Shekh\"}', 'Dahabya', 15, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`id`, `name`, `deleted`) VALUES
(1, 'Pharaoh Azur Beach Resort', 0),
(2, 'SUNRISE Garden Beach Resort -Select-', 0),
(3, 'SENTIDO Mamlouk Palace', 0),
(4, 'SUNRISE Crystal Bay Resort- Grand Select-', 0),
(5, 'SUNRISE Royal Makadi Aqua Resort', 0);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `english` text COLLATE utf8_unicode_ci NOT NULL,
  `german` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `french` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `arabic` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `english`, `german`, `french`, `arabic`, `updated_at`, `timestamp`) VALUES
(1, 'Dahabeya', 'Dahabeya', 'Dahabeya', 'دهبية', '', '2021-06-25 13:51:59'),
(2, 'Welcome To Queen-Farida', 'Willkommen bei Queen-Farida', 'Bienvenue à Reine-Farida', 'مرحبًا بكم في الملكة فريدة', '', '2021-06-25 13:53:48'),
(3, 'Queen Farida', 'Königin Farida', 'Reine Farida', 'الملكة فريدة', '', '2021-06-25 14:00:47'),
(4, 'English', 'Englisch', 'Anglais', 'الإنجليزية', '', '2021-06-25 14:00:47'),
(5, 'French', 'Französisch', 'Français', 'الفرنسية', '', '2021-06-25 14:00:47'),
(6, 'German', 'Deutsche', 'Allemand', 'الألمانية', '', '2021-06-25 14:00:47'),
(7, 'Arabic', 'Arabisch', 'Arabe', 'العربى', '', '2021-06-25 14:00:47'),
(8, 'Home', 'Zuhause', 'Domicile', 'الصفحة الرئيسية', '', '2021-06-25 14:00:47'),
(9, 'about us', '', '', 'من نحن', '', '2021-12-14 15:20:41'),
(10, 'test test', NULL, NULL, NULL, '', '2021-12-18 01:32:28'),
(11, 'sadadsa\r\nsdadsasd asdasdasd', NULL, NULL, NULL, '', '2021-12-18 01:32:28'),
(12, 'test adding competition update', NULL, NULL, NULL, '', '2021-12-18 01:33:47'),
(13, 'test adding competition', NULL, NULL, NULL, '', '2021-12-18 01:33:47'),
(14, 'season competition', NULL, NULL, NULL, '', '2021-12-18 01:34:36'),
(15, 'season competition season competition', NULL, NULL, NULL, '', '2021-12-18 01:34:36'),
(16, 'new competion rule', NULL, NULL, NULL, '', '2021-12-19 20:20:54'),
(17, 'new competion rule new competion rule', NULL, NULL, NULL, '', '2021-12-19 20:20:54'),
(18, 'Competition Registration', NULL, NULL, NULL, '', '2021-12-19 20:35:24'),
(19, 'Customer support is a range of customer services to assist customers in making cost effective and correct use of a product. It includes assistance in planning, installation, training, troubleshooting, maintenance, upgrading, and disposal of a product.                            ', NULL, NULL, NULL, '', '2021-12-19 20:35:24'),
(20, 'Voters Registration', NULL, NULL, NULL, '', '2021-12-19 20:35:37'),
(21, 'Competition Rules', NULL, NULL, NULL, '', '2021-12-19 20:36:00'),
(22, 'Sign Up', NULL, NULL, NULL, '', '2021-12-19 20:42:48'),
(23, 'Voting', NULL, NULL, NULL, '', '2021-12-27 15:08:39'),
(24, 'Submit and share your project to get more votes', NULL, NULL, NULL, '', '2021-12-27 15:08:39');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_id` int(11) NOT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mini_target_id` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_data` text COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `new_items` text COLLATE utf8_unicode_ci NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `user_id`, `action`, `module_id`, `target`, `target_id`, `mini_target_id`, `data`, `new_data`, `items`, `new_items`, `comments`, `ip`, `log_time`) VALUES
(1, 1, 'Status Change', 5, 'Projects', '14', 0, '0', '0', '0', '0', 'Changed booking status:14', '::1()', '2021-12-14 15:59:09'),
(2, 1, 'Status Change', 5, 'Projects', '15', 0, '0', '0', '0', '0', 'Changed booking status:15', '::1()', '2021-12-14 16:00:58'),
(3, 1, 'Status Change', 5, 'Projects', '15', 0, '0', '0', '0', '0', 'Changed booking status:15', '::1()', '2021-12-14 16:01:15'),
(4, 1, 'Status Change', 5, 'Projects', '15', 0, '0', '0', '0', '0', 'Changed booking status:15', '::1()', '2021-12-14 22:53:07'),
(5, 1, 'Status Change', 5, 'Projects', '15', 0, '0', '0', '0', '0', 'Changed booking status:15', '::1()', '2021-12-14 23:00:30'),
(6, 1, 'Status Change', 5, 'Projects', '15', 0, '0', '0', '0', '0', 'Changed booking status:15', '::1()', '2021-12-14 23:06:11'),
(7, 1, 'Status Change', 5, 'Projects', '15', 0, '0', '0', '0', '0', 'Changed booking status:15', '::1()', '2021-12-14 23:07:05'),
(8, 1, 'Status Change', 5, 'Projects', '12', 0, '0', '0', '0', '0', 'Changed booking status:12', '::1()', '2021-12-15 00:33:40'),
(9, 1, 'Status Change', 5, 'Projects', '12', 0, '0', '0', '0', '0', 'Changed booking status:12', '::1()', '2021-12-15 00:34:03'),
(10, 1, 'Status Change', 5, 'Projects', '12', 0, '0', '0', '0', '0', 'Changed booking status:12', '::1()', '2021-12-15 00:34:56'),
(11, 1, 'Status Change', 5, 'Projects', '12', 0, '0', '0', '0', '0', 'Changed project status:12', '::1()', '2021-12-15 00:39:27'),
(12, 1, 'Status Change', 5, 'Projects', '12', 0, '0', '0', '0', '0', 'Changed project status:12', '::1()', '2021-12-15 00:44:26'),
(13, 1, 'Status Change', 5, 'Projects', '11', 0, '0', '0', '0', '0', 'Changed project status:11', '::1()', '2021-12-15 00:44:32'),
(14, 1, 'Status Change', 5, 'Projects', '12', 0, '0', '0', '0', '0', 'Changed project status:12', '::1()', '2021-12-15 01:18:33'),
(15, 1, 'Status Change', 5, 'Projects', '12', 0, '0', '0', '0', '0', 'Changed project status:12', '::1()', '2021-12-15 02:59:45'),
(16, 1, 'Status Change', 5, 'Projects', '12', 0, '0', '0', '0', '0', 'Changed project status:12', '::1()', '2021-12-15 02:59:56'),
(17, 1, 'Active Change', 2, 'Competitions', '5', 0, '0', '0', '0', '0', 'Changed competition active:5', '::1()', '2021-12-15 03:12:43'),
(18, 1, 'Active Change', 2, 'Competitions', '5', 0, '0', '0', '0', '0', 'Changed competition active:5', '::1()', '2021-12-15 03:12:49'),
(19, 1, 'Active Change', 2, 'Competitions', '5', 0, '0', '0', '0', '0', 'Changed competition active:5', '::1()', '2021-12-15 03:12:57'),
(20, 1, 'Active Change', 2, 'Competitions', '5', 0, '0', '0', '0', '0', 'Changed competition active:5', '::1()', '2021-12-15 03:13:01'),
(21, 1, 'Active Change', 2, 'Competitions', '5', 0, '0', '0', '0', '0', 'Changed competition active:5', '::1()', '2021-12-16 00:19:33'),
(22, 1, 'Active Change', 2, 'Competitions', '5', 0, '0', '0', '0', '0', 'Changed competition active:5', '::1()', '2021-12-16 00:19:40'),
(23, 1, 'Active Change', 2, 'Competitions', '5', 0, '0', '0', '0', '0', 'Changed competition active:5', '::1()', '2021-12-16 00:19:48'),
(24, 1, 'Active Change', 2, 'Competitions', '5', 0, '0', '0', '0', '0', 'Changed competition active:5', '::1()', '2021-12-16 00:19:56'),
(25, 1, 'Active Change', 2, 'Competitions', '5', 0, '0', '0', '0', '0', 'Changed competition active:5', '::1()', '2021-12-16 00:35:13'),
(26, 1, 'Create', 2, 'Competitions', '6', 0, '0', '{\"code\":\"COM20216\",\"type\":\"weekly\",\"title\":\"new weekly competition\",\"description\":\"new weekly competition new weekly competition\\r\\nnew weekly competitionnew weekly competition\",\"image\":false,\"start_date\":\"2021-12-16\",\"end_date\":\"2022-01-08\",\"price\":\"20\",\"currency\":\"$\",\"active\":1}', '0', '0', 'added Info: 6', '::1()', '2021-12-16 01:31:07'),
(27, 1, 'Create', 2, 'Competitions', '7', 0, '0', '{\"code\":\"COM20217\",\"type\":\"yearly\",\"title\":\"new yearly competition\",\"description\":\"test\",\"image\":\"kids-learn-robotics1.webp\",\"start_date\":\"2021-12-16\",\"end_date\":\"2022-01-08\",\"price\":\"20.5\",\"currency\":\"EGP\",\"active\":1}', '0', '0', 'added Info: 7', '::1()', '2021-12-16 01:41:08'),
(28, 1, 'Active Change', 2, 'Competitions', '7', 0, '0', '0', '0', '0', 'Changed competition active:7', '::1()', '2021-12-16 01:41:47'),
(29, 1, 'Active Change', 2, 'Competitions', '7', 0, '0', '0', '0', '0', 'Changed competition active:7', '::1()', '2021-12-16 01:41:51'),
(30, 1, 'Updated', 2, 'Competitions', '6', 0, '0', '0', '0', '0', 'Changed competition:6', '::1()', '2021-12-16 01:57:20'),
(31, 1, 'Updated', 2, 'Competitions', '6', 0, '0', '0', '0', '0', 'Changed competition:6', '::1()', '2021-12-16 01:58:38'),
(32, 1, 'Updated', 2, 'Competitions', '7', 0, '0', '0', '0', '0', 'Changed competition:7', '::1()', '2021-12-16 02:07:38'),
(33, 1, 'Updated', 2, 'Competitions', '7', 0, '0', '0', '0', '0', 'Changed competition:7', '::1()', '2021-12-16 02:07:59'),
(34, 1, 'Updated', 2, 'Competitions', '7', 0, '0', '0', '0', '0', 'Changed competition:7', '::1()', '2021-12-16 02:08:18'),
(35, 1, 'Active Change', 15, 'Rules', '4', 0, '0', '0', '0', '0', 'Changed rule active:4', '::1()', '2021-12-17 23:07:46'),
(36, 1, 'Active Change', 15, 'Rules', '4', 0, '0', '0', '0', '0', 'Changed rule active:4', '::1()', '2021-12-17 23:07:53'),
(37, 1, 'Active Change', 15, 'Rules', '4', 0, '0', '0', '0', '0', 'Changed rule active:4', '::1()', '2021-12-17 23:08:19'),
(38, 1, 'Active Change', 2, 'Competitions', '5', 0, '0', '0', '0', '0', 'Changed competition active:5', '::1()', '2021-12-17 23:09:15'),
(39, 1, 'Active Change', 15, 'Rules', '4', 0, '0', '0', '0', '0', 'Changed rule active:4', '::1()', '2021-12-17 23:09:30'),
(40, 1, 'Create', 15, 'Files', 'I20211', 0, '{\"table\":\"site_files\",\"file_name\":\"4096.jpg\"}', '0', '0', '0', 'added file:4096.jpg', '::1()', '2021-12-17 23:37:55'),
(41, 1, 'Create', 15, 'Files', 'I20211', 0, '{\"table\":\"site_files\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f.jpg\"}', '0', '0', '0', 'added file:af8d63a477078732b79ff9d9fc60873f.jpg', '::1()', '2021-12-17 23:38:21'),
(42, 1, 'Create', 15, 'Files', 'I20211', 0, '{\"table\":\"site_files\",\"file_name\":\"40961.jpg\"}', '0', '0', '0', 'added file:40961.jpg', '::1()', '2021-12-17 23:43:20'),
(43, 1, 'Create', 15, 'Files', 'I20211', 0, '{\"table\":\"site_files\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f1.jpg\"}', '0', '0', '0', 'added file:af8d63a477078732b79ff9d9fc60873f1.jpg', '::1()', '2021-12-17 23:47:09'),
(44, 1, 'Create', 15, 'Files', 'I20211', 0, '{\"table\":\"site_files\",\"file_name\":\"banner-competitions.png\"}', '0', '0', '0', 'added file:banner-competitions.png', '::1()', '2021-12-18 00:28:17'),
(45, 1, 'Create', 15, 'Files', 'I20211', 0, '{\"table\":\"site_files\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f2.jpg\"}', '0', '0', '0', 'added file:af8d63a477078732b79ff9d9fc60873f2.jpg', '::1()', '2021-12-18 00:48:55'),
(46, 1, 'Create', 15, 'Files', 'I20211', 0, '{\"table\":\"site_files\",\"file_name\":\"40962.jpg\"}', '0', '0', '0', 'added file:40962.jpg', '::1()', '2021-12-18 00:54:46'),
(47, 1, 'Update', 15, 'Files', '6', 0, '[{\"id\":\"4\",\"module_id\":\"0\",\"form_id\":\"I20211\",\"user_id\":\"1\",\"file_name\":\"4096.jpg\",\"caption\":\"\",\"timestamp\":\"2021-12-18 01:37:55\"},{\"id\":\"5\",\"module_id\":\"0\",\"form_id\":\"I20211\",\"user_id\":\"1\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f.jpg\",\"caption\":\"\",\"timestamp\":\"2021-12-18 01:38:21\"},{\"id\":\"6\",\"module_id\":\"0\",\"form_id\":\"I20211\",\"user_id\":\"1\",\"file_name\":\"40961.jpg\",\"caption\":\"\",\"timestamp\":\"2021-12-18 01:43:20\"},{\"id\":\"7\",\"module_id\":\"15\",\"form_id\":\"I20211\",\"user_id\":\"1\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f1.jpg\",\"caption\":\"\",\"timestamp\":\"2021-12-18 01:47:09\"},{\"id\":\"8\",\"module_id\":\"15\",\"form_id\":\"I20211\",\"user_id\":\"1\",\"file_name\":\"banner-competitions.png\",\"caption\":\"\",\"timestamp\":\"2021-12-18 02:28:17\"},{\"id\":\"9\",\"module_id\":\"15\",\"form_id\":\"I20211\",\"user_id\":\"1\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f2.jpg\",\"caption\":\"\",\"timestamp\":\"2021-12-18 02:48:55\"},{\"id\":\"10\",\"module_id\":\"15\",\"form_id\":\"I20211\",\"user_id\":\"1\",\"file_name\":\"40962.jpg\",\"caption\":\"\",\"timestamp\":\"2021-12-18 02:54:46\"}]', '{\"table\":\"files\"}', '0', '0', 'updated files of form No#. : 6', '::1()', '2021-12-18 00:56:06');
INSERT INTO `log` (`id`, `user_id`, `action`, `module_id`, `target`, `target_id`, `mini_target_id`, `data`, `new_data`, `items`, `new_items`, `comments`, `ip`, `log_time`) VALUES
(48, 1, 'Create', 15, 'Rules', '6', 0, '0', '{\"title\":\"tets;asd;la;sd\",\"content\":\"<p><img src=\\\"data:image\\/jpeg;base64,\\/9j\\/4AAQSkZJRgABAQAAAQABAAD\\/4gIcSUNDX1BST0ZJTEUAAQEAAAIMbGNtcwIQAABtbnRyUkdCIFhZWiAH3AABABkAAwApADlhY3NwQVBQTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLWxjbXMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApkZXNjAAAA\\/AAAAF5jcHJ0AAABXAAAAAt3dHB0AAABaAAAABRia3B0AAABfAAAABRyWFlaAAABkAAAABRnWFlaAAABpAAAABRiWFlaAAABuAAAABRyVFJDAAABzAAAAEBnVFJDAAABzAAAAEBiVFJDAAABzAAAAEBkZXNjAAAAAAAAAANjMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB0ZXh0AAAAAEZCAABYWVogAAAAAAAA9tYAAQAAAADTLVhZWiAAAAAAAAADFgAAAzMAAAKkWFlaIAAAAAAAAG+iAAA49QAAA5BYWVogAAAAAAAAYpkAALeFAAAY2lhZWiAAAAAAAAAkoAAAD4QAALbPY3VydgAAAAAAAAAaAAAAywHJA2MFkghrC\\/YQPxVRGzQh8SmQMhg7kkYFUXdd7WtwegWJsZp8rGm\\/fdPD6TD\\/\\/\\/\\/bAIQABQUFBQUFBQYGBQgIBwgICwoJCQoLEQwNDA0MERoQExAQExAaFxsWFRYbFykgHBwgKS8nJScvOTMzOUdER11dfQEFBQUFBQUFBgYFCAgHCAgLCgkJCgsRDA0MDQwRGhATEBATEBoXGxYVFhsXKSAcHCApLyclJy85MzM5R0RHXV19\\/8IAEQgDhASwAwEiAAIRAQMRAf\\/EAB0AAAIDAQEBAQEAAAAAAAAAAAECAAMEBQYHCAn\\/2gAIAQEAAAAA8dXXTnrEtkKVUYc+U6rpfuua2+yy26Lr9n7Dv9ToaXFVVNVaBZCYsEEBkMrRTJXnw87mcvzHznwGSuiy22M6F1UgMJCIUcoYpIKwwSAqIqkrDAJJIpifc6koooBtJkrqw5caPc9m7Zbbpveyyyuep9v6TsdLVbKaaaEREhMigGKokgFawxM+PDzOZ5r514DPVRZdZC6kiLDBDAQYwIWSarcQWAgKViwxTAJIIIU+41V0UZqZfY8gqyY8dIaxt+7Vbo02vay19j3nqu509t0qooopFNZaEKqsAslci1qpldWTJzeZ5r574PPXUbrIbJFIhCkGAnt\\/o\\/6InK\\/InGV\\/0R9418PwX5TyoYBEgkhACmSCFR9sWiinLnW29nauunFjoSWNr37btWnQ7WLRv9v7D0HV26WTPny0pTXGIUJIATEqC11rGWjLmwczzXz7wtC0Pe62MGUSGAQRhf8AtL2tOjr8z8UeF+q\\/q95d1fk\\/4+xFIAFghgAIAMIWfbKqc+fNnqbQxtiVZseepTZfv3ade79B+j6Hk\\/ifIs9d7f0fV6Gm1aM+TPXmQFkrCsYCzJVWlVKl1oz5sPN834DxGdKnusEtEgkgIhWT336z5t3T3+85n5O630utdnse18S\\/JnOAACiSSRZBAIIPta56MtGSlL7Gd1FGTNWtVl+vfr636j9dzrNnk\\/zbV6H3vpuvv13ynNjzJlpkFaVxyWNjCumiumsWpnoow8\\/zvgvD0rTZc8eEqRJIJDF9j+r+QvoaPonVs8t82waOl6r1mvwHyN8HmvilMSQQhSskggX7bVVmzZ82So3Pa7KmTPWqXey7GvJ+kfe8Hijo2\\/nvy3T956rtdHZpajLjypmorC1pWtjtY17RKc9FFYYZ6M+LBwPB+KqFL22wtCpBUqSIRq\\/VtydGdT6XdPjHGs7Oz6N082N2zfnb830KVMiSCRTAJ9pWnNmzZ8lCO1tlsWjNSeh+jvtF9g00eX5Gbfv\\/ADxxNvsvY93p7NbU5seWrNmzBakrWyx7br7CtVOfPUgWnNmyYuB4fxtIqtteFhIII0EPb+xInK+m5h1uH9H99q+X+Cs06fond83TU\\/o+l+Y\\/zvUkAkisFkgg+0ivLRlz5c9UY2eo6Fvn\\/O1dj9TfTPP5ulv3c3xdQsHwGu30PufSdTbrevNjyZ82HLEqrRGsv06b7bHKV1V0Z82LJmyY+J4nx1Irua0MyiQFSI3rv21r09T5P5Tmivo8TqfdKvimfN0+x7nz2XPZ7H1ef4X+aeKgBCiGCMhH2cpRRnzZaK6mn1r9LbX5X59yfoT0WbwdG\\/sbvM87l9QeW8H9Q6vN5fpOtt2WDPlx5MeDGqVUorXbNOzXv03OoCVU48HK5fNw8PxXkq5Xa9ywqpgkEJ\\/Wnubup0PV+R+RcXHi9B82+8\\/SPF\\/Ns1NG\\/wBXhp29\\/wBt0NXk\\/wAnfKFEAAIMgg+y2pVRTnz0VIn1f9MJy9+27cuDmeAy9btZvNUs\\/S8p95103+O8L2OhuuFOTHjw8\\/FUqVVq2jb1Ov1tWndq03uaqaKMeLByPN+F8B5nDW9jgxCsAkhn7bOf0lfuuhm+DcBhwPS\\/WPSfNPBpiX1VIjfQfQ69nB\\/FfglKiQQRlZPsV8SqqvJnqv8AsH3LpeS850e\\/r7+bD4nzuzodHzvl7ezp9T9Lfy2abfmFvS3apTmxY8XP5+dErAs6ff8AX9rtdO8yFoItYiKlWXmeb8t8k+Y89DFiwqVIn6Z+h87Xu3+32\\/M\\/k2Dlekb2H0XD8l49NXqDXzh6H3foduzwn4UxgQGLIZJ9fteVpXTl9v8Aofv05vFee6vc7nf5Pk\\/DHq9XTyM1Ha+pdqjkcLkX9Pw+Hp79TZ82THj5+DNRWh0e3+kex1aSWorMgVkSAABVRF5vyT4T8+iBZABIPoX6R4lHUw++9vb8b8rg6fKn0H3vg\\/CeH9kejl5+ev230HrdBfxt8kiwQEQQT6\\/bcAq0+k\\/TfY5mXk+DXq7vV6vF\\/PF6XWvxcfie29p9IzZvA35m6Hyrq9XdplGbNjy4MOXPVPd\\/X\\/UA77K6a5JBIVkggWKFoSU\\/O\\/zp8wQKAyQS39WPmz6\\/N937F3\\/CfPuddmy+w+m+P8AOe\\/b5XPy5u99V7fX8n+KOEkEkkAE+uW3tAuv9N92zi8fyfB1elt7fzLzVmju9DP0vE9j0\\/wBT6XI+b0Yuucfz\\/wBD199spzUZ8WTFlz9X7v7StdFtckkhgkIkjlUUAKtVS1\\/K\\/wAveSKQQAT2H6SwYuX6nxfs\\/rHa8R8oWscWe9Ty+3h+h04+dix\\/R\\/Xeg7vy38gcCJACJAPrN19plfvvvIu4fgPHbu10Oo3zedLtdTm8Tp6\\/Xezq8h8rfo77ez8ty+l7GuwV15s+XHmr+ifaSl10AMeEvRICb2zyO8WutIqpXVwvzp8IzxQJFnufv2bTlrr+iem9J4\\/4\\/wAzNoz+H\\/Q\\/iOcM\\/f4vB0b\\/AEfovUem6PjPzx8i4ggiyKPrL36LB0fvHd5afOPF2+i7Wy3mcbkZ\\/Xdrg6m9p7tuN8eTFZ39\\/j\\/F9fu9fa8WqjPmy9D7N9H5tVhkawmFlOeQE2PKACCzyuuStK8fyv8AK\\/mwoClR9P8AuvD8r3Ozs9b7Pt4viHKw4OB9O9Z5XynP0ez8u4z+g9V6fp7ujwvjnw355UggA+stfffu+6eo4XO4Xz2ek6V2ng870VnQ5fldHS+hfRcnnuX4fm+grv8Al\\/n93T7fU22QVU5s3pPuXo+Q7yS+01R7c1Mkgj3MMwkkglhRWWuqjyn5U+WiKsVdn6L6nJ7tNfV932fQfEvM+K6bdL6Ht8Vx+Zu7WAc2z2HpO33OtqmXyvnfAfE\\/CVqfrMv9Z9g+mZuX47k4fH0afSdDy9XT16U5XoM\\/rPop4\\/lvOaPAcr6Zq\\/PejZu7fW12SV0Ue2+x7ct1ih9EINdSgiCFjdJnEkkBMjKYlWfkfmz4HUAsC7v0j7XzgTMv0v2uD4jxOB67j9P33A+fUt2OpTXzR6z3\\/pOl06M2d7X+VfnD5sv1v3\\/2X3+jh8\\/m+Q4nP6PGt63lD0dWmnzx1+u+vZuLzvCDb1LeF8k427Xt6\\/V2WSsJ9D+oXlrJLNmeqIIBIZI1qM5zAGSBpCGKstWfnfBvzNlWKsXV+iPWcwDkc677r0fBeNqqwH2Vfgulj2dKjNzA33DsdbVw00716+X4j+Xv3j7DZzsfK8\\/g8v5oek1Txt3THY4p7HjfXfQ\\/X87514Dsbrr+z+evMa9+zb1OnrsCn6b9F1FnJli1wQCGQ7Ux3asTXpnEhMkkMdWVhVnwfGfyvzVKhA33P6Jj5unreA+le96HzvxUx87L6Pdjw8n0Xdw4MfF+g+57vrOF87Hpe3PSp5H116+ffned8R5W7vbbfHUdXV6XgcP1b9X2vR4f537W\\/sab58H4t+zbs19ro6GRvqX0LVYWYwQACQQxmtzoSdi0VSQwyQyXyt6yqUYfkn5M5axIgPufs3cpw0dH0PpfSfNfEYa6HbppysHr8Xn7T6L2Ho\\/WdXj\\/AAHb9E0eo6s4zDkVcz49z9Pa3ba+Rr8l7WcbsXWe99JwvnPmfY0ee7mX4fzNF2rd0Ox2NVyz6j9D2PGLxYBBBG1Zq5IohNpFckBMMkNltN1QEWnD8n\\/I3MCRVC\\/X\\/tXiuR6XrWdf2vf8R4DJxcOnr1U87L7Xi4Oxl7vp\\/W91PKfmn6V73b7mzxXX5+bh+X+ceg73Uv4\\/A9Pvto8N1ujZ731PA53m+Ds9Bf8ACvBZ7b9Grqd\\/tb72T330zoXMXBAggi3XLmrggkhOuzIkkhkMMNltFypI2THl\\/OX5yUBQq9X9I9LzvqMCafQH1\\/P+L+J0es4u70qecu9JlTnn1\\/q+n3qviHzT6d2fqfY8n6HzXE5fnfL7t3er8T3+rUcHZ6eD6T1uZ5LBg4fnut7\\/APLHGsuu19Tu93q6bjv+udxr7r2WCCQ3LXUABJBIG0imQGEyEyaDRa1TLmpvz+U\\/HHiJXAqzr\\/qHrcQ0ZqfW+xyfBOb63j4MnsMGfl7fVUU4z6n3Xct5H56y+t+kfTzk8z5HLxDwNnf8z1ejQnPtPW+kcXtZ\\/Ic3q3+P9H82+K33aOj1ep1+z0dL6fq3rNEy5elcYsBkCgCCSASAmyswwkEwxtNddltFvOt2JVk8T+OvJLWIqnf+k\\/Q8uc7k87y3ofbd3x\\/Ko53RnhPR9dO2tXMS30v07p+F+Mdnufb9efg+T4g9MLfJ6uzh8nm9v8uf6P8AXV8d5JdvVp9H8b+EW69u\\/f0ul1exsv0fT\\/Y6GLJxrenaJAIogEkBCwKbNOaGQmNAYW15nTKnQaNXVk8J+N\\/NKqRYl36H9bi53Xf53Vj7nqeThx5Po\\/Q8z4\\/Fn9d3sNHL7ntu\\/t+dfPO51\\/svluB5fB6DfX5mbt2HzfW1+2A8\\/wACnb3ti6flnxe7Xu179\\/R6PU6ey76X7a+x4SuDndbXABABFBhdqF+bfmHr\\/rzqEkwyG9azCSuazU8ZmoqyfOvx1wIiiJJ9D+x9zPzKbfN+M63vebz+Nf8AQOJyOfz9voeJq6XW3el9FT83w2fRPJ+MX0Wunj9LyvP9lZme7qVczJxPfry+rp+U\\/L0u27NvQ6G\\/ob+jr+g\\/QdNjOZFbLxt+6wACAARtTWZcn5F+Wz9R\\/eMUJMN1uaSzVmrTFr1tC80plqyfLfyBykUKsU7\\/ANPcHz\\/sLqOF47v+55uTsbU4PP5i9nidnt8vp+h9J1+Z4v0nxOj0fV7HN80Ox7ru8jx1d2zlec9V08V\\/o93zv5Z52iuzdu3dDf0N2vX7\\/wCm7HZnsqAGjDxJ1dRCyQCanptXF8p\\/LvS\\/Y\\/cYljITqsFCJmydS97EjG\\/oc3NXh+P\\/AJJwJFCqJ9O+q+T91zCc3QswzVotw83lcL6F6TzvmG29fv8Ao+hwNnhuJZ0+x5DjdfT0e7hxpq5nlfSarrfN2+w+G87DVXZr6G7ob9u672\\/1vpSwteM8geVcjkdLoaGgkEuguTIkmqlSYY8uqDVY8HS61THTSWjdI86ujB8Q\\/KuRQqiL1\\/14PHUm7p4cVcutvKUVdKzz\\/Ml27rej76z5wfIV+qty6Nff6t3Fw9Cjk1Unu+G9NxPAZOfSV09Dd0du\\/R6z7N2ktjaZnSRZdVKeDy9HR1O0jRSa0iGx88khWCvPlnR7+rMSxMZ9+PVXRXVzfgf5izhFCxfRfpbJiS9cywDRbbIj7udycUv29f0XWPybre0dvF16Ol1KMmdH7tXmKBr9x0PgHGxYc8Wzfu6Wvb6r7b6GmywaGzIJBHAkTl8fBnqrVJQioHRr2Rnsttu2aJv7VlqyFiY5mnbgZUSrl\\/nv82UCsAVns\\/qDHkqWsSWTRYwQBqebml2zt9\\/T8xt7\\/p5XkEWqmuupujl5d27r\\/Mublw48Zrm7fv2eq+7+pzy8XW5qhBCDozySALIbqK0vyQSNYIWJZrDVGs14YzSFiTqOayItXK\\/PX5voVAqEei\\/TXNyUQM976Lq0SpFfNgzvZu9B0vBbu162nDnAWumupKWJ29D51gmXHiyGqq7fu9b9+9lXRa1l9GYSSQg7MZggEgWSqKRGViTGJ305zCZIxjEkltddFoVKuZ8A\\/NOdAEAX236L42VJZp16NLV1110UqMOCttXotnnep17qJLGZKcueqoW7NvieHYKcWbItVS7fY\\/oj2bYnss0ZsoaQQwgwgEQCK1agCTVmJJMYnZTUZGhkLkgkuQXAROb8O\\/MGBUVYF+jfeONQmjrdMEwRUrz5sfOyTrdlc\\/YZbLL73IrxcvJUdO3z3kLLEGbNkqTPPcfor1t2C7oc+2ZVhIkhMkkkgkVAAIokLEkkl4pMkJMYxoYWjEtAteL5R+VvOJWBAPq\\/2nmZtnbvSMVZZBXTmot15c3Q2HVqsLgM2XgcsXaOb4Gy2EpnxZq6Ppn3z0ejnn0nIx1CSSSSRpJIZBLKVCwKsjiFiSS7JIAWaFoTCSSzByAszeH\\/ACx80RIAh\\/V3ZxWdeyowgmGFlIw87N6LXu1XmASyrn8DmnZV8oF1hsZ8uLDZ9l+v9yzn169XKRQQSAZCUxORWu+wrAAqiAusjFoWLJzhZosShd7ltbZYWZzI0Rlari\\/m\\/wDPdKhZP0P9WyvuoCuiWNYxEgo5eDJ9E1OxAgLNl4OPV0vkXmbb49t12Tnt9\\/8AofTGHObKqlAhIEkavP1O\\/qerPm4fO2uyhVAJLKJC0YzHf29jX22V83zlO8m57KC7tIrqGJHP\\/JfwtQsn6r95XY+etrKQ9ll9Ehr5\\/M5mf6uSSAA9srqw37\\/zlmujvde9HN+4\\/V9lOPK0bMiyGEAyZ+h6jS8srqWrN5vL086ACMYwAhJMy9\\/o2Wl7rUSrh+d2x9VcdncGApZCZ89\\/CGYAfW\\/01hQ2JXYFllthrkrxczl81PrzPJBCzodWSzT8K82xNmm6Zj+le6tGYiLSAsNtcACen7LhKbrgKq6vO87QsUyMYVEkeZ\\/SbrjKfGc\\/L3fZ6xh8dc7tYxuvqDAKXjTh\\/wA+uVWOt+2a6kjSX1kvbbTEz4+dzOXiu+zsxKgCx0Ntb28n4fiNl2lpz9P6dtpraLWgUKbPQedChPW+iXyfwr5vTo6n0r7l0K6qfMZnkkJIdIJDX2utbYflH5z4fXwY+\\/8Af\\/t1vK8VdYzu1vUxpGRYSx4\\/8+uMg9x+nunWIY+uslrzmz5cWHl83Ct33R2gSQOa5K3a3y\\/xF9F91i82\\/wDUMqaVqigKJZECjre+Hzn8deWyybO7679bekpqzeSthhJgsrggGn0V1lvxH8x8DBb9W+RJ1f1D+lG8dx3exn19jFnWw5wSZxf58cxen+ivrOCoSPdrkNoz4cvP5\\/O53PFr\\/eb7DXFKkVI1UfT5f4YdNlrznH9L9UrWK0EiIzPWij6N0vDfiLyfS9x4vkWek4X1b9f7lTh8e0EtCLaZAF9Lrc+A\\/HHVxeO2\\/VPhen6N4z9l\\/b8\\/zx316c3Y6eDLk09Hn0Qwed\\/nxn3\\/AHzT1vb1qXt2XNBMmLDzefg5+JGss+2bddsIWBaqVkr06Pjng2JeYqf0x6YKK6RAoUvqyIOx73J+FvmvN919u+V\\/POj6H5n3P0p+mkpz+VshJJj1SAaPR3On4q8X9O8T5Dodf5fYna9R\\/QDt+N472Xns6Zjxn1XDxpIvivxn9T+v68vy\\/wBn0492joyV1ZcnP53Nw4stZMt+t7t+3TYUkXPVnV1bznl93zjlSLmxfp32Ct0eXmkioJd3\\/OovtfRfH\\/xFyKvR9jzN\\/c5vleh6b+g+6tPPZSY0jPWBF6\\/XazyP4HwX4LOtx+ekXoftP7\\/xvFWvsbbfKWyaLJlgHnMHL5C+L+WfZPQyy7bvSujPjwYOdhwU1wk2++6nR6GzXpaNXmryZHPJ5fr9LeV+c+BTNh\\/Tntm6\\/V8lUrKK4O\\/x6wv1C78cfnBW5\\/d4c7nC6Nm\\/9y\\/TlTk8dyTIWVYB6PbG+K\\/hxNPHt6nI6HT5maz9J\\/tFfm5bqOWzTtctLGQcnncjzzTkde7zWprdWzclGTDix4MWKkKjWM3oun1Orv2bb7bDTjzc6luB8W+x+tyd\\/reS+T\\/O+N+hfpt7vhgAUCen4uUb\\/qFH4A+OthxdWxt3P09bF+xvvQryeatcwmQQLPVWw\\/nb8SbNZsbPN1GKn7Z+\\/L\\/nuNrX2LmfdlZp5\\/5x7Xz9CZOrMHC8n6tdWrXvXLzsOPLjz1oqyFrNe\\/pdbq9Dbr122V87PzcVfQaZ9U2b+wvxu36TdFggUAS+3PO99Dr\\/AJ2\\/NObV093puXV9d+M8zr\\/rX9ComfzVrQkzfgUK\\/prI359\\/E+zR6v0XzD9D\\/EPJ9WV\\/W\\/31p8NyLHmyzHHd38L8f+kLlr1brcvl\\/A+K+vLZs3bVxYsWXJSqIjSQ2u+zq9PpdLbs13JzsnO5tbVdvnW9UXdbp+E5n0SuCQALFDXLPUe8T8F\\/NPH8+zb7Dnd+eFfR+7PsaLn84XjGSCKtnpLAflH88r0l2j1PitDNu+8\\/tW\\/xfFd2uetHa3P5q3meQ09HTn53neXbxOwXs19Y83Nky1JWqgiEuzvp6HQ6XT37dNHPwYOYPLZe77TRobW2vqcJ+9mySAAAA2FfW+6q\\/Jf5Q52Uban2HTR1f6OenUZvPMx0Ct4ltNbekcTmfzQ82UG\\/YQnA+h\\/q79FN4\\/hXO+muVvXlw8TD8ayfcTi87z\\/SJj8P29Almrp8nLVTVXUsVY0nQwtZddr29TqbteXnc7DiFHmer0+p6To5+r57q+i2v08nByAQACO6et9vPmH89\\/PVo11h9Hzj9l\\/drxMvBNjNJILpknorTB+RfypS6Tv46Ods+n\\/vf1c8b5+97rkiY+X57rYcmrqeX4\\/G9Dtx835l8c+7+3KqNNQTNmoqRZBOr630Pxtnutuu6HS3aaOXgpzJzfR83r8TT6Ts9nzfyr70\\/Ut3b+LwSCK5HK+o92mX8QfnegJfnt06+5+\\/fo5iZeJaWMIIdaF9DewTifzy+dF5fRS2n9t\\/pC6eJ4dz6DJyvM83Zsu4ufk+dwfVePm+RfKMoFvX9F6Hv+o1SVZs9CqNvoPR38Tg+ed7bLLtF+rdix1ivH880e49Dvnnr\\/Q9XvPi2c\\/rr1186sNQjFfQfRgfL\\/z1+e1o112w\\/tP9EmCvDg2pQ0Mgi1J2d1hRPCfhPwGezVinZ\\/R\\/7H2QfPuXbZoA5HlKOjfj10fLuDwvsOnP+ffCwgCARvZfWfS58WZ+n2+0\\/N42CtcJayy66+yHHn5PH4+Frbej7Huer834\\/Z3\\/AEvpxcKF6GzzxCJHU9T6vYU8d+JPjOZbur7n9Z\\/bSSp5eHRZQxjB6q0r6nVD1qvnvzB8Ax5U9l+qfv21jPl+W2zQMXikOtuZh89Vo7HQ8l8b8AARDAqme+\\/QXJ6HXvrx5KaskSZmZrbr7a8\\/lfMc1nax7r3bve07nivF7vovouj0XszW7eSkrWMsu+z6oqZvmPxjznZ+lfW+yQSqcOyytySQRXXU3b0PXBE4\\/kcvo\\/X6iWNHymWXXpwfK3UvxKMXqKeJ2PlnxfMIDAwWBk7\\/AOmOv393C5eLHeKsyr7Xk4M8u0NzflWd3sd2e6+5jv73rZyuR0Nr+x0amyZjErliSfTPXRFlZCqqsGmLjrba7EkrFFczr0OhokgjC1xA88L5myy858ni0zebx9D0nO3\\/AE\\/8xfCyhgkBixlA0fV\\/s\\/0zV5Hz2HDfKso9ns1Wlas\\/kvjdrGy02Pbda5CbO9y\\/U9ngZdPp\\/StjyORWWSDX9H9G4hVREqyY8WKX3W22O0JACSqquiqaNWrVotdpVnw8zhcx3eylLPK+e85iu9Zo+g96fi\\/5aJIokgIIiE+r+4fQ\\/S87zvP59wry90btnR1cjzfzblWMS9tpussd4Ksy6On3en5Pqdj23FaxylhSAWad+zXfBXnz5rNNt9111tz7JWsZFC1pVTVnzZs9YMrVKxH0XMy1yU\\/OvOZfd+29UzJ+FPGhgpAgkkgAdNf2H6x6teJg51sp6W\\/XrfzHivHJba7iNdc9l1jGLVRTPo9uDh9zq6WeyyWSSRwqSMzvdov13WW3W2O9lkkixVRK66M9GequqsKIpLW6CyIqtMPX7FrOcP4F5ipJCAjrAyFWWT3n3L3FvIwc6yd3Zrp8j4Lzsd7bHkjW2W2WtY7VU5qfoXdFWexpLrWskYmwqsLEu9tt+vRcXeE2GyXZVCha66qqakrRAsAkay13SutWdun0HZ281+DKosEkiySLIskWd77v9L3cvnc2z1GzD4v57zGjPba7Msj2XO9ttsqzZvY+laVCLGve2RzLSQTGjmy+xmZ7CjEmF0UBmRQErVFUIVgEsuZxnqjO3U6rsx+afi8RSFKmKIRArqsK6\\/tv1\\/tcrncz3fF8L4LPA0a225ysSWWWWtfeRmyej9ghYCVl7L2BeWF3MJMZ7TCFMhMhhAkLsTIEihAsipLbnjZ6C9j9ze5LfCPyzEDrAJBBBIYFWGH659w9Hy+bo+d+MUARy73GyKkZntttvulGXp+\\/St2MrBl16mx2ZncloSzQCBFMhjRhGDMzMxAWBUiwVm2xnsOBmss9JexNn5X+DKJJIokEMUiACGRfpP331nL+SeYAgkMLWEgCs22XXW2X2U0P9KggtZFU6tFAeyxnjuzQhoADKophJZ1hBdmZ4VECpFBSWuzb25TNZ0u05py7\\/wAV\\/NgFJAkVojCSRYsYQD3X6H9j+b4FEgkEIKosLXX3WaGcV1fSNqFUusRBp6+HI7XWuxckloohAqkkMZ4DIzOWYyRQAiSBnZt2yjHHfv25edlq7f4T4iKYJIIIYsIEiOJAB7X9F\\/JgsCiCBCFCwSXW6bdN5FeX3PqVroiaDWs9A\\/LzPdbY5ZmLSACRK5ITBYyxgbYbGMEACqgjOx17HwKTdry0Ip434vqgkhCxosKsIFIIkEWey9oIqgACRBFEUl7tF+m6xa83oPoFq0VKljIvS6aczNZfY7O5cyQCLWskhJYiGF2LO0kACosLFr9l9eAlzFrRT8u\\/M5CkwRYGCtIBCFghihq\\/tASKIAIqqZIpZrbrrb72qz7ff9hUSkI8Szuvly53tstNjkkgBQqiGEwkNJLHktLACIFWMXt06LMFbEla0qHP\\/OPzwLIRCsMQyFSsIghUAr9mCqIAAsUGSCO1lllt+h1oX1XrbM11tKpbWOh1qMtWey93d2JkUAVyCFoWEMItMLsYAqgLHezRfoqwOTAlObPwvjny6uCAiFSVAhIBKMpggEr+zJFkiiKojSSFmttssvvdc1T9jvdGaNiVx6j3jRTRXdY9jMSRCEqhBhhJkJjsSWMAWIpZ20aL7uWrEhM\\/O8z82+bcaKQpggEeLBFdTBBJIAE+zpJCFCrJC0MjvY919ll1ebNTjy9j1nX3b5Uxr1dvLXnrS57HZ40WIqKzQCQvIxJsIYkqsCB7WbXfoyY2aRMnn\\/lfyjlyCKQIwgWMpimSQqFYyRV+zrAVgCgBpLJIXe2y62568+WnFy+QPee26nSsUNT2tGammuXWPHjQgVqGilVJYk2EuYSZFVQbrZou1PySxleLxvxHwgBBUhWAkBAhBEhkWBlJAH2ISEQxVimOWkjtZbbZe6Z8WfFy+Py8vtvqPf6WhVWehoopqre2wksSAikUci13pR1oz2+sLNCYIFEd30X36udQxAweA+B8CFIwAgkggDKShKtBIJATIn2KEkmLFBjQsDC723NqtqzZMeLkcbl4B6b6\\/wCp6OpVr29bPnorWyxnJIkQFfnv5igESaI\\/ov18WJMMULCbb9GjVXzWMqx\\/OPgPLKyQAqyMBIAymCCBgRAIZAPs7RySIICJCZCWd7NGizPmx4sfH4vO5SafRfW\\/XdLQhXutmypWlzs4eQQKF+RfEsiI9ew+2\\/Q3aZmIJAQktdp1ab+MrxcPzj4BzA6SCSQAFIZAZAIRIFJUyAj7QXsEYQJDFkZYSXsssssz0ZcuTlcjmcnNr6Hb+rev6VtV59Dly0JSbLWhJjVCItHz3x\\/Gp3+p+hekZmhkMitC76NOzXzsTMuL5z+fOeDFhWSRQIXrkEUyKwIlbxYYVH2MxyQYoWAwSRXLuz2iqqjNn53L5nIxaOl1Ov8ATPY9Al+h2cWGmqs3WQuSqoVkVTIzs6ywGRpCGfRp2bquKSMvgPzvyysKyAiLIAXrhSCQoZGSBhFaJPq7GMYrIohiiASOXZmKLWuTDz8XF5t\\/T6nS6X0j1vQhPotODJRmDO7PCogEYCMCzFS0khYwtfr16buHW60+L\\/OHGhUSAqYquoEgkEEiySGCCGRFn0ksYSCqiSSBYAWcxnrJmXJkw8Xl29Pp9Tb0\\/ovq9xru7y10YcrNY0JgkVmhjAGNASwkaF7tmq6\\/DkEr8x+bvMEQMoKyCKRCFkgEiiGQqATFMVfehmMiiuIZIAFCktCYbdVNVFOHj8sdLq9XobOn7\\/1ewV7uxnqpw0M1ktVTBIYzLGEjCNCrMWt2ardC8wBfPfnTxywrBBAIIIskIkWRRAVhCiyKjgp6kQwisKFhIVQAoBWEtt2gJVk5nKXd1un09uzo+\\/8AT6pO7WmejEjS6QgiRpGAJgLBgGuDHZr26r8\\/BC8j8+\\/PBIBJFMURSBAYAQBBJAABGiBwa9oEMgSsSM5QKFEgkYtt32Oas\\/P51WnqdfodHZq6Xu\\/SbLu6udKMuKo7FocggOYYDIZI0fVnNm3dq1XXcvhjm\\/C\\/lTKpIWEAApBJBIVilYJAYBFBiMpFEJeMK0ksa5lUIqkq0Ytq2WvKs+LAr9LrdHp79ujoe39Dt7vZlNHOx48q33Z7JFUtCCwjtYabLM12rdp63R03Z\\/CZ8Pxb4+sIkEUxGQQQAwQoRAFZYIpZSAsgmdo9kIrrkse5wtYVAxhYNbptdUz5sizb1ul1ejr16NnsvQ+g+gcjLRkyJXmyjVnlpVRAxjM1q0aQmrXfbdq1X7uf4nN8c+N1mKRIpKEQKrJICCBIoEkWRZGRkkiIxayIAiS2y51CoqQNGMZrmeV05a1W\\/q9Pq9Lds1Pq9f3\\/AGG3LRSikLVmJzPZDDCI61W6qr77LLrbdj2Tx\\/P+RfG0AgMgEkEWCBRDBBCFWCCLARCK2CspJZlWoQXWWsyIFqeEEFmZmCUU1yHo9XqdTft2Xm\\/0\\/p\\/ZtRVXWqGRaWpzhnJCqt2h7LWe622zTrj+V4Pxz5RWyFWEkEiiQRYBJABJFMUQLCqxgGrAJIeFK0jPbdYItaLASYQ7wCtKBCNfS63V6G\\/bouJ9B7L2CZakigSMAlSIBHdpZY8tstsey7Tr4PjfinzmLCrLJIokkUgKICIAVEgKgCCRYJIq2AliqIhltt7sK61rIYNIxaKFSpRDdv6vV6PQ369Bg6\\/tvaZMlaCSSQrWkkAZ7HjtYbbLLb9HO+ffDvHmSAAyKVIAKQMkBggWSIQyAKQICkMCuwLLK6lD233tAK1qVoYzAsIBXWojW6el1Op0Oht1OyzX7D3QopREiyQwCSNGJaxrbLb7tFvm\\/lnxjkQGSRYDFDJFKQwIVdSsgAgAiwrACoVljNLAAlSBrbdFrLWqVAF4wjsBK0USR7eh1en0t+7Za5Et7v0Ho5qa1UKAIRBHaM7my66\\/RoTwXxj53VIJCAyxTBEEiyQCQBlWSKQIFAkVlQGERoWi1osl1t9hAqWqBo8kdgayiLIxfZ0Or0uhv23WsQx2e69TRRWihAoBMMYtZZdfddfo5Hxr5Bx3YQBoICsiwQLFIkUSK6iRYAAVECyRBDA4hYqlVZa269mauta4ISTGaMgArSO7aNnU6XR37dVzFmj2d323Rz01JUsSQwNYzWatmnTZh+V\\/E\\/KPazBoJBIIAJFgEESQQgCCKICoERlWEKUktKlzWtdZNl19xiVylGBJkdyRWFCSWWWauh0ej0duu64uzPLL\\/W7dFoqNYAkLO119+m6634v+clsaywgyFTBIoICQCAgRZCFiwpAsiwpFBAgU3SCwIErjPddeVCLUshYRizEoihVdrLLtnQ6G\\/bs0XWvY9rt0PYYMVE1a77HkJZoFmnpdXpeS\\/HXNNljNIQCQJFBSRVIIEixq5AIUAWRYpAD112Vh7GBaIKlVrLNF1iIFqUQsVZ2YRJErJaxrtW3b0NmvTouusssts9J6rHnz4sOGhC9lpa7Rt6vV6W3VrT8j\\/NI1rsJJDIAJCggQR0IiyLIBFECMoAgEiI4Q2sYQakUM199jqFSpWWFo0eyRYiLGZrLL9m\\/ft16NF99t9tt3t+xRStNdSSt30sxe7Tq1a9W\\/b8B\\/MossLAgwgiSCKkEWEAhZIpQQLIsUKIAyrCpTQTACtaSXW3WuFRUUAFmYWOVEQJIzvbd0elv16tei++6+63V7nRVSlddZkL6LTFfRo2buhtt8H+K6HdjCGggkiyARZABAsMAig1ghYa4IkEQNAulQ7LERVa2261wqrXW0UsWjmyKAqqzF9Ozp9Lft17L9Ft9ui3Z61q6qalVZJZbpWR9Gnb1ehc\\/I\\/D\\/Ae0yNFcSQAgLCoEAgiSFZFAUqFKhZK2AEkS9WYmUhQbLb7WUKqIITGLl3ECqEZ5o6HU6G\\/fv26tGi\\/TfZbq7prxcS6QSSyx2Dvfr1W+vtsz\\/i7wTvYCCIZIIYkAEkCyBIVkUACBYEIrKhq4ygXGNJK1SM9uix1ipUshYFnL2ArFUrbo6HR6W\\/obt27Zqvvv0W3N0QnC02LBGWy+xg1t\\/S6HG9Vc4\\/IvyVrCxgkJ7foZxeVirWAwKRAhUCFIFDVFVEEVQYIFTUrEgLUIbLdNjBQiVmSF5YzMXiRHv7vpfW+v5\\/Aya9+7fr1aLrGcXCnibiYoLNfYyG7Xs14vSWu\\/xP59zsdFdKU00p9B+t+su5vB8x5PyXA5eYgKyRIBEiiFQsESKrJJAsVNSswCytQ1t1tlgCoirYpljF4zk39v1PsfU9\\/t+u5\\/iDyE6fR36L3kRObjSvg9G4CKS9jmNe+3Vk9Rcz8nIiKBEqzp067vYYBXm5\\/M854fxnM8pkVCIEkVVkSQKAAjVsEhiJpMcCV1qxuttewoqrU0jRnLNq9r3+x7n1fd3NNXZ8v4L1+uxmaCipJXy8KVcLpXgQwMWMstt068XqrnsUVoJGIAbj5PZUNJHlOfueZ+FfI\\/OZ0AiiKAFiQBYqlAyqQqag1krautS1lttloCBK1YsXZun7H9GfWudy9lxiN0x855fqdF0rC0U1x6edXXXwOpesgIMjFrb9GjD6nTZawrrUF4JBxl9MxLM7R\\/QdvzvzbxHzD535utJEESBViiKsCGBAVr2hmERUWNZfbYzoqqgDFtXsPpP0j3X1jX4zmnVt1W30cf5\\/w+9dbAi0UiPTybQvmtOsiBGjQlrb9Ozi+3vvnP61ddcZiQJwdvqabCGscWdD1ubxVGTzvz35J885YACgKpQBRFEiQKg2oXihUWNbZoexYoRI07H0f63733HpLuhk8rs6NmbHixZc\\/J8z1nseoLUqxx4ruvPOdDpQJUAWhsd9OjTwPYX38Tz30SupA0cg18Dtd60R7Gkuu9k3jsZWnm+K+P8AyPgKgVYEACxRIAgNabkYuEStWa19DWFQFSd36z9h9r09u3k9\\/wBZy\\/M589ZkWqnB5bXbGClEVnngZ6aee2dcxVUSEu9t2m7zvsbX+OfQvX11qosYSUcb0faLWGMZc3sNXmeRJCMXjPz38XqAQBQFWCLAgioNwDMakrV5bdcbGArj\\/X\\/vntOg611z2fofNePAEJMTL5nLqQs0VUkPO+JfUenwru2xIrWR2drLrrvNewnlPmn6IsVUIaAtjwem6tjkRibm9L0+LwTDC489+TvjKBIiQAKoUkVmsLtRo6qiKWsuuaxgsX7R+m+9YAbFr9z1fHeUBJLMBXxvO64I8iIqsfA\\/O\\/p2dvRsWgghZ2eyw+a9J5b5l987zRZJI5mBPS9KyRYS1zdrvc3y7sSTD4D8S+brgQJFAVYRECKNqtGUVKkssuvd2WV+g\\/ZntzJHsWe53eI88jPdcFzpM3kyQGhgRSE+c\\/PfSdf1d9zwEwlmYxfIcjgfbfUQSSAsxnL3el0R9hRKxZOp6fL5IkuRJR84\\/O\\/xqmKqrEgUCRYiNqV49LJSQ9p0s5I6f6B\\/Qe+BZZEt95o8Py7iNmvPlwZ2TzmWuCCPEVpKvG\\/Dvqvt7QJASTY99l1fyzl\\/eOiQAGCsHZeV6HttJZCztYL\\/AFtXk4TCRFx+b\\/HfzNWREEUBWjVBV+t\\/\\/8QAGgEAAwEBAQEAAAAAAAAAAAAAAAECAwQFBv\\/aAAgBAhAAAAD6+JNERmtK0qk+b53glvWqGTM5wkwd6A6TKoTCLjRQrBkK4dL62ZTaMoq9auiPn\\/LyHrowCJzmQB1oDopFFRzY9PTx82voTnVMU2TL+tmYdN55j2vwPK9P6fx\\/F5k9dKpqc4mZQN1YFFsinHILoxJ16ctKZMjvM+uU5zVNRPm8sefev2WPgccF6a0GcZSpARpQNsYrWnK4w02jPJb9yFA7J+tUxAvFfpeN51W9un6Tl8\\/z8R67U1GeMTMg6rQmrQBHJPZ0mHJ6K5BK+pwKgX1wpnH5jm0U3u9tF50+nXLF666NZ48+UgkJ6XTbpCxWa6OvPhvu5hxkd+VUiK+taXlfOLbfmvouN+HF69vRxZVtro45ufOEgAAd6bVI840nm6dZ5NrnoXNPXMtaQ\\/rGY\\/Fz1bvPfTk3x5ei37nJw5VpqY8uMAAMAQVW10LPd5cvR04c+9zvlzlakaQfXD+Y83fsrG6l4c3Tu\\/U9LyuDF3PFi0DTkpoSADfdNZdU58fT0YY9I7nlyetbJfU+X42G\\/TvIY64cnTtWv0d+ZwYGHJDAE0A0ACBbdQGfQp4ex5HRldxxac+T6+nkzXR1bJvC1x7dD29jfTzuLm4uUaAGAgAYgQtethGtZ8PVtitc+hYYrlm9cr27dpTilx9Gx1endYcGPDwsYCZIDABpiQtupp5dETxV6HHtvlU5YZ1DNOrouKw2jzuzS36958vJlnxJgAIAAYAMBJderIrYy4evQRGhhy6PLTbsee2Frl06Gusw5+Xn0hMTEAANDAAG0kuzQBbOJ1MLqaz5c9Vt065mW+mUXW3Rxc3NhibDaEmAAd2POwABgnJ2WNTq7y2yd453GGddPUtXzbyLPq9rj8fhwzXQFCQxAADAAYhtMldltJdSGIzjfPHnn0oOlinOn7HXy+N53NnuFCQwTabYMSQIYNkydlgjpliTAzwx9aFpUzIez3LDyPJwcjAAEwAGNMkAAqhTJ16MitSYRTZlh6MhLGen6ws\\/O8bkhAMAAABgDEMQnTAmV0bgnecAJErqhIq30fSjUeV89EgFAdWtPDjGJjBV3dBllxpsAiV09Ac+qhAhBYxkv0\\/oQmPjsE0FMfeEXrHmoBgV6Ytp564JHSJlHeRm3MgpLabGSj6vsRl8SgAoOnX6Lo35fnKy89Nph6Ol\\/R8WevhYc6eiJiTuWcWSpmnWsYjpiJ9P6VKfiYGwT039jy\\/M7ez1\\/G18mAGX6a9n5cro7\\/MzzWlZ1nJ1ZBUpJ3panFAxDPQ9fuj4\\/LXAAro66wwendMcXIwDq69zgnWtZ5MkXpkQndGkSy9bWc6ZxAFJQur1PM5CQHWvbWOOwdvNhwDTOzo6ubmes3XHADzuQN61mKqqFEdEznMvVZYpQ6aBht1XGdLa8+fhBh19OpjNVKxxblZvSK2dbkttBOXS3OeeumWJlkRLsYPo2shVvgc\\/G0B1dJ04pVXMsSozmq2bZdOmhZ4r0QSnTbtjzOWDmkpti6Hvrma5ZvkwYI16jXfNGOLlMxL3mwTTrWUazNXT17t6dHkeQRGdXDHTq9ahLPkBMF0a26nJJpGkaLLdAAN965+qeXo51P0OxYD5+Pg5pymXTTbaU5SAAJmtsbBZx07Yc2feAAA9g6Jzy29ndtjlhHyQs6nQEwzBJJFJFVRToAyilkr7UAAABr1cfZ7F0qAYE\\/L8JVzoJNpJACGmNsGxinFrM165AAABLX0\\/TaoBsoU+B5Sm3bEwSY2AAACYUiM3nL13AAAAFH14wTclNk+R4CRbq5BgBVUpQgAGwkJzqqtNAAACX1rABJ02TyfKwDtatAAVbaCUJAUyExDdoEAAAB9YDlMZQycfB4sobq7EAOtGIJBJNtEADtyVIIAATPrQBFAxk5+ZwY82Tp6tgBdjaSQIGlMsL1SENARcAAfWgAwYwWfn+bhXNi6u3cq26phJIkAlDFe8DQgJlggA+tGmigYOY4fMwDnwL0rRFjpIJoBDlRNq9KlCYClIAaD6tNANNg1nwcGLRzZUa3TZVAxIQJKM7b6BJCaAUjgAF9IMSYgoDLh5s6RHNFPXSmVTYnKSJIhXW9KQQBIgJTlo9UJQCEwqM4zoULmitL1bdUAJKZImXXS00CASSAkJqTpaGMAYrJmRSkubu1ywq70pJpmWcxptpx72CGiWgUA5SVJdIMY0DZTmEJRIY41dPS9boic84kzW3VQAJgkImWIJA3YDABholAiVCFjDqndO0lMwkV12CAAE5SSaliEbsGABadBMiUxIpnGnTqgJlTB0ddjSQyQBJCFIioN6BUDluhhMppTmplZNNtgkkr7N2NAClAAKREggW1CCilNjYIzaM5mVGJugUsbutuhDATFAgAJSJBAtXUuk6JtgNKUiYhRONb2xjbFnHoNCYAlLSARIQCEtqE2DqbGOlKhEYpRnGu11TdErLKPUuBoAEEIBSBAJwdDQMoGxtaETJllEZTM9O106oUzlE+lrDHnldsCXmBLSlASugYxMV0GgyMMcocIoVVpZVNRGcqW2zJ4Pbbv0zSlASKBCXQMYqbGyjS\\/EzT0Y2Cg0dDCVmpgCStuRE+h6PldXQQiW8xSC3BsHQwz5Y1jjzK06EDcg04LY4CJBFRmC06OfF9vXQCzQErYdDCwz8\\/Arrx5wd7NMDWKElQUp15EAVnmAdMYJHr6BMBDS3Y2FNz42YV2Y4iqr0YBWxKQBVPmzQA85kHucwF+lqKAgJ3VMY6fm8DVX04ZE1q9aACurIkAp88JAIlSJ7nMAV7AZpwLzP\\/\\/EABsBAAMBAQEBAQAAAAAAAAAAAAABAgMEBQYH\\/9oACAEDEAAAAPyfTe8lpr11jjhlg76PvPV1cwhAN1VCQs5kBJtgACuUAMQCD8r13uZddnXnjw4c6a+59\\/opRMAN1ZYIiIQ0hqkzouMNdpwzE2IYk\\/y3TfpWMvq6tM\\/Pw+v9vwPhfo\\/sOuyIQh07bYKJzTBAA76CFpWcCiQBMR+W109W0Zq+u\\/a129NYflm\\/3HqbOJUp03VMAWeQMQAW9mIam3GUAk2L8srp26a1+hnzfoe5Za5ef8H1e\\/7fTROYh27bAFlmwBAadMkmkBckCxlgC\\/Li+jbv+z6snosoccvoV4unoaqYJZduhNhM5oqUBvo5FV5rSRKc4GhH5dnpf032NGGlRhcdFLPyM\\/W6FEkJ6PSmwSYoiBgPpQBTkcMyhCGj8thd\\/wCoUY5aTG2OtzMfC+77PWpzcaa1UgxIaYpWYC0tNpaXIZ08NJyGj8wyn9D9+M8dsylV5S\\/kPm\\/rvd6iR7NpA2RF0OaEsoA6EUC1ebhtE3E5H519D9H13zZGkWquIng+B5vqfoe9rTRAACczohJ0IzgT0ody7UaTLOfaycdKonlLjfF6xM8Hx\\/Hy\\/Se\\/6OnQ0JgkmktCUAUzPIZpU1oikPJ3nn1SRo1llNTSNcpPM+d5ubr+i9Hq6xIARQyFYpTEUzPIZsDusdVF5akWVld545aTpmunFLP5fn19H1uo3aAEUAEjBCQlTrCQNAe0OsygGCZjhpnq8tlM5eY+zt7OrHmoEwACahsABJSynggLcmuWt50Z1olU55UiRgvN8r6D0Ono3nzATQwAE+a9QEAhCVmKE6shlIspgpxhpyN15nxHrfX+r07LzIcjaAGACQDEm5FBdYIT1UjczdK7FGKmVboj4HzvT+y97rfnQ5GAmDAAloAASEDeABYAAFaNc0Ejps+F8Ndv131HXxy0qSaG5bAFIAMASkGzJAaCYAO7XISMLPlPkoW3031\\/RSBgAAJgkgAAASQDdYIHoFIHQPlGA35f5w8VX1P22zAEJ45I23kaABLDEu9mkIbp5QGybbAGc6ALPl\\/iYTr9a6mAJC5ZTSvsBMYRxp8q630sSabo5reqTbYmzECSiPzXymb\\/AK+wAhvmw8XDl3+jevSDBrkzy+d73h7PTuLPSKpvnvWknSbBmQkhj+f\\/AD6Xt+v0CaFPN5np9\\/N53nexPfQDI4a8X6NZx5\\/obWubas9KI2uRNgJhMNA2jwPl\\/L3\\/AFHXLcBPDhvTSVyPXr3QMw5+aum84mevWcXTNLtEgNmY7eIgHSafm+F9N1jAHzcb3rPHTl6d+oQPmy5ejU5dpy69pi1dWwSzGAhu8pkHculTpgABy8y0qCI3160hPnxzjolK432mdC6VCazQm0MvGUnQLcYAUxBy453ROHSbbpgscjl3bidb1SvUADKKYNJu65kAR53z1\\/V98tgWIObLGdHFaG+qAIwIx01yrR2zShwVzITFs6S1fNIeb8z5cTjH3H1caJJ6JGOJGa1ovcEDeOaWZqy6HVzZhmxSM1zfRkb8msnwPjzmkl6fufR+rK0uUGUIbq2kADEpSQUOk6J5mAJjq3eNFcfwnnZQnJA9f2HQtPIAAAQITE0mpCUWxjXMAANt7WsNfF+IyicptOIK\\/VPZaghMQAAhNMRIhCkKY6IwAAGNrXZfLfIpzIgmJh\\/oX1dOc5ltCAASZN5ACBSDodGWQAAMaNd\\/yNoFT0rGMpn679BVExM24SAFKKHJICkctzbFlIAADEPX8qaHa1Zlnkl6v6xZDziG0SATADbUggCKkpt8wAAAAN\\/lwF6MlQZZvf8AR\\/ctuc85dQgFAkUAkAnDEWjORMAAAY\\/y8Q6JAjPNa\\/V\\/S+j1WLGYpICJHLAEBJU1BRlDYmAA0AP8wBMAFnEq\\/o\\/qOvfu1M8oAQZrIRc6iASaAMGmS6AAaGg\\/MVCbrNpSsrv3\\/q+t69+yxiAGs1Gcl6WkCcWpZhnTbmhgAA0H5oIpDblKHp7v0fSr179p55lhMQxpp0kgEg51LZU2NMATaH8EQ6bVNpJb+76dD009DXLGEk4kYAnRICRzogBtOhsAGh\\/PjdsbbYLfoqktnr31zzKRBLE2ikCBc6TgAB1TEwVSyQhphIhK6csu3t3cMVtKjNUCRV0s856OaKlptS0UW5bABqXAyRCCXbQG7enUIhKM1I3o6SFPPhbBpUNCV0hooAlAmkEAhspram9OhMmSZE2OkRHNkUNBQ05YMbExgSSxBJNQxjZrVO3pq1MpDAAjHmgm2BSVggCk6CKaSAlJMUqlQ09NKqyrdgpAQQuPGWBSboEOgQFOaBkkslpEhDGwW1Xdj0AoQKZwyxkBopUFNFNCBuhyypUsQZtCQrEG109CtBZyiZSmWYgNA6lllTUjQUFy2JSxCgEgljKu9nTqlM5ylIBT4qAKTbkZRUjKmqQrEhJyKGJIlsb130dWmspkiApFPgLBVcghtN0DabKixEiJaQpQ86K120WySC1EJShsGITWj1nPLmHc2TdIHTAQkpBwiUpR6GmihiALqJkECB26ltZdI3y8XXlkUUqbVVLqGQAKAl6s06qAyBJg5aEkKRltjm6Yp5+mo5sDQG2NNzNQkxSV0dDXNtuEvOoEGWsOaJQSZb1TBW6EufTVmfE0ymMGpEkEo9LQDh6dpE1CQE5OgAUxG2lMA0GC562YsuaKYqpNgplJk79wqjj67ZITm0BHPsJ0ZzN7psFWiYTidABzctU0UPT\\/xABBEAACAQIEAwYDBwIGAQQCAwAAAQIDEQQSITEQIEEFEyIwQFEyYXEUQlBSYIGRIzMVQ1NiobEkNHLB0QYWgpLw\\/9oACAEBAAE\\/AvOfJcuX4J+ZFlNkWIQvWX4X52MYyoVh8H+lrlxC8tFNkGREL1l+W\\/KxjKhWH+CKEn91ndz\\/ACMt+AvzH5CF5cGQZEQvwC5cvyMYyZVH6qhh6mInlgih2JSVu8dxdm4Rf5MRdm4SMs3dIxnZtKvS8MbS6FWhVoaTg1xScnZGC7KWkqhHDU4r4UdxT9h4Cg3fu0YzsunODcI2kSi4Npr8BfJfnfOhcV5ESmyLEIX4MxkyqP1KTbSRgMNHDUV79R1EjvkZynK5icHTxNNxaMZgauEnqvD0fDsuhnqZ2XUUd4d4d4fEjH9nQrQbS8RODhJxkrNfhb50Lkim9ErmFwEFTTnG7FgqCd8iPstD8iK3Z1NrwaFWlOi8slwRTZBkRehuX9IxjJlUfqezqXeYiPyJNly6RfQoy1FIr0adWNmr3Md2NOF50dV7HZmkbEn8y4i5SY9TtDs1V1mj8ZUpTpSyyVn+FPyFxpwlUkox3MHhI0afz6jdkZzvEZkYuiq1N+49HwpsgyIud+Rf1DGTKo+D9Pgqjp14jbaQ3ZEXbVkW5GbJIhLMuGhOjCztEluOQmXKEuG5jezqOIXzF2LTXU\\/wWkf4JH8zMR2Q4K8GNWdn+FpNuy3FgMS18BHs3EvoPs3Eroing6855chhcFCgvmLYqMvwTXDFpd6+EHqU2RYvMvxuX5L+jYyZVH6mEnCSaKNRTpxs9Rs0WrIVirrqYat0M4hq6KyyzZvwTVinOzIsQ5ly5dEoqRj+zL5pw3Gmnb1D9DSpTrTUYLUwXZvdSzS1ZlijwloijFcastR3QpXIs2T1K7vVlwiU2RYheXcuX4XLly5cv6NkyoP01GhWryy06bZT7Arv46sUVOwa6tkqJmL7DnRo54VM9tzsyqo3i9yasLXczHeXVh3g8yMPWU0KStwxcdbnXhGRezKM7ocyVQ7w7w7wpu5KN0do9nb1Ka1Grb+nflQwteorqB9hxH5T\\/D8R7FTD1ae8eNGhUrytFGBwMaH1HoSmd4RqGYuTdkXuzM0aCK0kqbHu+CKbIMQn5TZfhcuXLly5fnsWLFjKZTKZRrg+SZVH6XBYWWLrKC26spU6WHpqEFlijvY+5nKdpJmJ7Jp\\/3aCtU\\/7M2aPiXiW6L34LQzK2pOpk1iYftHxZZEJ5irTU0S0k+F9RMpzyszpokzMX4UWXHBSMf2Uqt5Q+Iq4erS+OD9fg8BKcoynt7EKUUjLEyRJ0IyWxi+zpX\\/pRKHZNST\\/qbFDCwoxskbEib1LkS4mVal3YzZUXbLivcx81ZIo0p1pqESXZE9LVT\\/CI\\/wCqyfZtWnrCWYjdaMixMXkMZcuXLly5cuX4oQkWLFixYsWLFhxHEcRoYyZUH6Xsql3GFzW8dQv1kXS1f8C1RRlwr4SNXxLSfuVaUoOzVixJ2FFvcyRylWnlqJowtW8UJ3K+HzXaGrcM1hzbKdZrczX1Gy5cpSFIRoythoVE00Y\\/s94d5o\\/D63A4JzkpzRGCiSnY7wVQzCszTgyctB6suJiJ1baHzJPNwuZ8kbspYeeNq5npTKeGo0PggkOWpnXuKafUxVDN\\/UjuiLExPlfF8bly5fhfihIjESNBK+yFTn+U7mXyO4\\/3HcL3O4gdzT9juKfsfZ6fsfZoH2Wn7DwlP2PscB4GBLs9e5PsyX5ip2XX6E8BiYf5Y6VSO8H6ShVUqFKUV0JOyb6kdPFMhKUvoZ8kiMr8J04VFaSMTRUJWTMnDNYmsxQll0ITFqjFpJmUy8OpH4S4zMRlZlOVzMJiMRSVSLRjcHPDyf5fTW5FCUtlcwnZ\\/wB6ZCCiMqsuRZcjvwuNlWoai14IqxzHjiKQtiKv0IYN1Hert+UUYwVkiTL3uO2zFZaMzW32Kke7qSXQTExPlfBjHwuXL8iRCF+hDDzfQWGl7iwy6ndRj086xYsZTu17EsPB\\/dP8Nw0nrSRW7Ew8\\/h0K3YuIh8HiKlGrSfjg16DsqrfD5Ir4d2Sd9TSOsnoQrTk9PhJ+JXMPW+6zOIx1By8cSFT7sitGW8WRxP3ZLUhG+o\\/CyjUISMRS7yJfLoypNxZdSLlPYe4y5cpVSEriEWuY3DqpSmicckpRfT1GEwTru7+Ep4WnBaRLJDZcrMuJiZDhIq1bGa7LkOF8qJSFqd2mUaCckmylThDZW4MqTsrCHO2kthP7u6EzEyTqWXQQhcr4MY+ZFLCVJ9LFPAwW+pGnGOy4R0LNmT5lojt6WdGnNWcSv2NQn8HhZiOzMTQ+7mXy87s6vOnXhBbSexPeSLZjPdaaRFVei6EvD4kYepGa31MxujGYL78CMHKFup3EIO9hy9hkXYo1kKSaMTQzeJFSneJ3U7kKdlqbM34PhmsylXRTmpFyMip4kdqYfJPOvT4bDOtLbwlGmoIbLjJSsTmLUQimSqKJVxKJVczERWvBMnJmeK3Y68pO0CD01MPBuWYaMxKVhzzyGn72M19Jb8HPJFvohyUpNoQhcl+LGPloYCrU1l4UUcJSpbI0Rc1YkkXRmL+mtxaTMT2ZQr\\/d19zE9l16GsfFEa+XlpuLutzC1FWw0LS1S8RddCy\\/Y2M9lqSxNn\\/STZDtRxyqcSnVzK58RWw+TWJK\\/Xhp7D3HeJQr\\/mIuMkVqeXYluZhkZlroY0ZSSa2MNXfUU7iZHUxmFjVgyvQlQm4y9LTh3k4xKVKMIpJCYyQ5lWoXI7iFuXsYurJ7EU3uZPYUWiFx6FyOVqxWwmd+GRRwuSJRoyk9SKUUXKjKtdyeVEZZVc1nuyN0tdUX8UTFVXSjb8xBiF5D5aVCpV2WnuYfBU6Svu\\/c0Rc3LeksW8i3GyZjOzaVbVKzMRhKuHbutPfy8HiXhqn+17jUXFTp63LfMei1Mne\\/FsQhFKyRisPnXzMFVeRJ9CEjcr4f70UZWXsIbtuOquhHGVaX0MNjFidyvGwmbj3Kciw7IfCOjKctiMiDPiRjcDGvH5lfD1KE8sl6TA0\\/FnOhcuTZVqGa74RRcTJS0J6y4RFlZGKWpUd2ZW3wiQWYhFJDZiMQqKuVMfUl9x5SFSMtUKV9COisxXV0Ryv4lYxFXvJvXRbECIuF+Z8UnJ2SMPgb\\/FqQoKES\\/C3l250rkjTjcuaFkW5rFixUoQqLVGM7JteVL+CUZQdpKz8rDY+rh9PuFOpTqwzwf7Dg3rLhew5XKXgf1FU0RCXDE4VSWaC8RKLTs0OVttzJKW5GEUVIJplC9Ct8jSrAqQylyxfKKWYqJneW0NxRKbIyISIyNzE4GliFaSK\\/YtSH9t3KtCtS+ODXoaVN1ZKKKVJQii45HeaMr4gcmyNyImJFyczLmJPJ0FOU9ilFrccxtsWxYoUs+thRSM1io7GL8c4ohBJEqGt46MjLo9zWxCN\\/oYnE3\\/px2ERIiflU6Uqr02MNg0i0aaHJvnty24X50O5+3kXNC3O43MZ2fTrrbUxGFqYeXiWnv5XZP\\/qleWltjOm3B7ldVLeB6kMWvgqrLIjao\\/C9Bq1iEiEiEhMnTpT+JFWlGEnY2JGYmrtGHnoiVONQq4bIrpmpKJT0ElJE6KIwSJCdiDIMixSEzRoqYeE+hiex6U\\/hVvoV+zcRR2WZDTW\\/mwpzm7RiYbDKkvmSRckyT0Jq7EiEEZDLqSeg5G5DQlGMinRgis7bCz9SM7iRQw7lZy2NIqyGTkZ7oqK\\/iEyI6UZipy162RWxLnpHRC4RExeTRw7qNX2KOHUVqOSib81i3G3G\\/kpDuXfomivhoVYtNGM7OnQ8UFp5MJunJSi9UU6sMbC6dqiKU5uXdzWpWwqrPxqxDLShaK2L9Zbl7kKttyFRe4qt9iOxiFKnVbezKsc8dGKs6by1DNB7SIx6lOWVlKoVpNxGZk9DYjUsXzcZEZWKdQUyJHkcYsqYHD1N6aJdlYV\\/5SK3YcPuOxX7OxFD7t0Wtz0sJWq\\/dMP2ZGOtTUyQXQ0JEmN3JEoiaR3pCQ5JIlO4hR4IdUvmZZWO6juUaLU4zjZwtqKS4SZNkp9ERjdalef2beN\\/YpKvVnnlKy9jPkXzO97mm6kv2L3bfuIQhC8jD4Vy1ZThGA5vn24X4XL+Yr9B3Lv0k6amtUY\\/szedIas7PyKdSdKSlF2ZhMTHFRT2qIqX6Iemv8nhWpLa50ISlF67FGrD3FNFaEasbFSlUoy+RXod6inhI03wd7kKmUjNTRUVvoVI5HmQq0Jbm+xCVi1+DGhSaKdVEJpiFIuXMxc1M3uOEJrVGK7Ko1fumJ7MrUNldcUnJ2SKPZ9SfxaFDs+nT+6KEYjehKRmJMmy45XIxTRUoFLD2JJQQ5ti4Ji1PkWaep30af1KGdvNJivUaiilDu4WJRO9cdydaJKo5M1Wor\\/Enr7ElGejjcjQjC7v+woxpJ1arMRiXXn\\/ALeiELgiIubfQw+G6silHnXB+fZ8L2L63G7+llG5j+zVU8UNyUJQbUlr5EZyhLNF6mD7QjW8FXSRVjZaI1XQlrZ9DfV7E6qX\\/wBD796rwlDHVYVY05kKlyUVURVp92PgxojNwITVRak6KsSwyI01FEtCEzLmGsozKWaKVeUZ2IVLxQmJjWg3YzGYvcvYTuSpxl0Mb2TCrdx0ZXw1XDytNFHBU6WyIxUeEpFyQ5WJTJyHMSI6FyVTIh1HNmUVI7tigstyU\\/YikTpxqRI4C073JUZaRRh6KpobJTSKtePQanKWr0Iq2pHXUbW9hEqqpQvUZisS8RP\\/AG9OK4oXKrt2Rh8PbViVvS5GSVuCdjPxVuo7dPOt5DVzH9nxrK63KlOVKTjJeRqYLH5rUqr+jJLS+\\/0NtRyc9I\\/yKCjwxNC9prdFCpeEWQmNKotSvQyD0ZuMZ3n5SniXtIlrrwkjYo1Bq+o7IfCS1KMtCDExPQqQuTm47ohXIzL3RTnrxr4WnWi00Zy42OWvCRMnOw5tkURXBaFZuTI6F0tyNa7LlSp0QtxLqXIkIdS9ipVtCX0FXxFTMlPS4pVIbwI1FIv1YvDf59DLmHWVClep+xXxE68rt6dOKFwQuVJt2Rh8Pl1e4l6bM+aKTMu\\/n6+RYaMdgI14\\/Mq0pUZZZLycPj6tDTeJRr0MXpF2fsZculjT3L2JO5S8PhM5TmaTVmV8LJeJaoaaM3sOF9xJIlC5Tk4+Fklbg0bMhK6KlzPl3MyZa5TIyISISNypTi0YqDoSzLYoV7lOoN2ZTlfjdGYcxszleplRLFuRmbIiEInJWIK7J0X0O5quRGhaxOSSsJX1Ipexe2iOpRpWV2J2KjsYuTyZV1KFHIjQdCMvkxOdN2n\\/ACQ1b9yeIo4e+f4ytXnXnmlyIQiKEuSzk7Iw+Hy6vcjG3osvuP5eXf0Gj8lox2BhXj8yrSlSm4y8nseC76pPrGPhM3ef+4qwnF54bkcbSlpPwsp+PxLY+Ze5CViEi5iFRnpbxDhlG0tySMxDxO\\/sZsw+DRFtMhaSKuHuRo2LWE9SLIMixSG7mIpZ6ckPNQqWKVbYzZolGfTjnuRRJjdxsxEm42I09RIhA7sjCxUnYXiZFWHKxFlWpoZal7kZZfiRmX3SKbZRoW1lwnIbzIqrN+xmI6mZLS+plUt0VsdZuNJfuTqSqPNJ3fMiKELkw1O2pH0FjwozF\\/LUrCa1v59zR+TclsdqYfMs3XyaNadCanHoQnHFQ7yk7T6opt1cycfEiWCpynnmhaJWJe\\/88IT9zvcp3tSrpHRFGmoL5mIhGzZWgqi3PtDo+GYqkaztAnanHIt+opCmZU0ZSWhCplFPMbcVKxCZGYpXIktTHYfMrlKbjoylVFO0rlKV1wp0bbliZckVdeETvLaIhK5VqWRrIjAubjkkbkRwizuYJ72RSp04rw8JMmzNlIk492\\/E9CWMhHw09ZFClJy7yfxEZeLfRatmKlCWIquHwt8qEiKIi5KUHORCmKNhsjr5lvcuX85K\\/USb89WLcd+Ow5Fmyw0V6KqRaMZhJ4eb\\/L5NGvOhPNBmFrwxMVNaT6oqXu0uP\\/QxNr4tSjUiZ0TkpK1yaq0p6JuJLC1K\\/QpYenhIafES1Y1woTuiq5XujvIzXzJOxSb6l8y4sU7EKtyDMxcqxzIxFLJK5TmKZhqoi42VGSZcb1O6uThNFKjJ7j8CHLMRSNuGdIqRcXfoInXVNafEYdza8RGmsuo4TpO8NvYjXUvkyciczdizfdJZKkbSX7FPARhO8EKEnJxWy3Zj8YoruKP7vksJCQkJCFxjFzdijSUUJcXH2Ltbia9Xe3n29jVcd+Cl0sO7FG3I0YnDxrRaaMXhZ4afy8mlVnRlmg7MwuNp4tZZaTJQ6WE+DujO3pCNyUK++axLFV72zGGnLxyc76FPER2kOenhJ3fB8KJrFuMicKmbwlHDVd5lR9EQnYzLgxo8SPtkqRQxcqrFI3MRSzE4OEiDuQnkZQq5kN5TMTZJjmR1IsujMoolLMyMEzu0TjliZ2blO0492\\/2MSsVQe3h9zDRv4nqYefeVbLZCGVcpKpLZFrmWS1N0nsy92QO0cd\\/lUX9eKRYSEhIREXFJydkUKOVCXK1ccXHYUxP1FxWa9Be+\\/I2K752jFYWNeDTRicNPDzs9vJjJxd0zB4\\/vo93Udp+5b2HaPUSdTpaJZRHqYjD5tUUItUX9SRRr20kPUa4NCeV3Id3VXzMlOJVqX0Q0Muyi8xlQ1wqUlIw1NUxSIsauYihcSystcoVXCViWpK6JVCcxJsgX4VZO5EdRR23Kc29yvJ7CiJWEQmpq00PC02tNCKp0LRiVsTWozl7H+JSn4cup3lV6ygRnGWzEvCLRXTE7kUv29zG4+\\/8ATpbe\\/KkJCQkIQuCV3YoUcol5Dp31QpCl6CxlfHtHHxwUPeb2RPHY+tL+\\/YodrYvDTSqvPEo144ilCpF6Py8xpz34Wv5LRjMJGvBpor0J0JuL8rD9oToq0vFH2KE6OK1jLX2LWHZ7Fy5lsspUpkkUcRk8MjSWqGhjKedM3WpJDRYaKTsyU7iq20ZnRDUvbQiyDEySuVqRTKlPqPEQ9ypiF0JTuLViXGdQilUWu5VoV4\\/CU4VL+MpRlOe3hRUj4uKETrZNLkG3qzGK8YS9yjh7PMxJHcKZepRlrt7nxNGiXidjGYzP4Kfw8VxQkLihcMPQ6sS8qcUxaOzFIv5trGvB8O2X\\/wCdG\\/toVHJbEo5qep2Ff7I7\\/mEo3fkJ2LX25cvzLW4XL+wo+\\/Oi3FoxuDjXgyrSlRm4y8qlUlSmpRKNeGJp5lv1RVz0vFDX5Cx1OW+jKGq73p0L8J0yUDDua06DRlMhCKLXZKI0NEkQ+IlSlvElhpzKWCcdWSahoi5EjIi+E43HGzEs0TFKpGXhKUZdTKQpmUqPIOo2JCuilVX3jLTZUqQgrIepbgkVquTRblO89WISzxcf3XBEWkWUivWp0HaGr9irVnVd2x8qEIQhccPR+8yMfLsNJklKPzQpil5a5Htw7TwH2yCcPjiTjUpPLUpTjL2aKGExeLnaNK0esmYehHDUY049Ovl5vc8LHEs+FxysayErcWrcqLD4tGPwKrx+ZUhKnJxktfK7Kl\\/XkveLM7qKWmq3RDA9\\/UzZcsOpNx0jH4VtyOkmQpqJJ8UXLjRYlFlpXKE9NTPFE6ze3FMiyMiL4TiRJxUy1hHeKJGVyu7sjEtwREdBt3MjRYsVqmRWW5CDk7sUbcE7O5WWW0\\/useLpU+t2U+8xDTnpHoirWVGHz6EtW2xj5UIQhccPQzeJkI+ZuO3CdK+q0Zdx+IUxSL+Y+W+lvK09+NzMORmb2IRtqx25L80WPklE7QwCrK63JQlB2ktV5NCq6NWFRdCj9lr\\/ANaC1K1W\\/hWw+KNhvlvy5UJJDQ1yJkWRkJkiJGfQnC+pKpOH3SDlLcp3lLTZE46luCRCBYchy4QsToocMvJScWnTn8MifZro1M1s8eg26eSK+J9DH\\/3UvaPBj5ELghC4Yehnd3sQhY281Li0nuSo\\/lZmqQ+KIqyFUXuZzMZi5mMxnMxm+Zn+ZmXuZl7mZe5mXuZ17oz\\/ADHJWM8fczx9zPH3M8fzHeR\\/Md5H8x3kfzHex\\/Md7H8x30fc76J3xepMVH8zIqK6DXlJJo2L8sonaOA7zxx3GnF2fk4fE1MNPNB\\/sUa9PFwzQ36okuS\\/m5iQ1yJkZEZFyPCnUtoxQp1EfZoDjGmtCV2yxlEjNYcuROwpXJRuh8UUazjpLYqTw9P+plWYqzdScpvqMY+VCELhQouo\\/kU6dtEbedfmlRpy+6PDe02dxV\\/Mju6\\/t\\/yZK\\/5GZa35GZa3+nIy1v8ATkZaq\\/y5Fqn+nI8f5JfweP8AJL+Dxfkl\\/Br+WX8Hi\\/JL+Dxfll\\/B4vyS\\/gSqPanIy1LX7uRap\\/pyLVP9NmWr\\/psyVfyHdVfync1fZHcVfkLDTe8kfZZfnPs\\/+4VCH1EopaRXC3mJ2PiHpzSjc7R7Pz3nDcaadn5NCtOhNTiylUhiaSqR\\/dDXk28ljXImRkJkXrwRBtEZyPiMqLeSnYUyWvFIzRpxuypUlVd3yPlQhCMPRdWXyKVNRSSEregb8zbksregTsPXkTsXTXmR8iULnaHZ2bxw3GnF2e\\/k4bEzw01JfuiMoV6aqQ2Y15KRbyGh8iZFifBIihJWG7Dfm2LGkVmexVqOrLkfMhCKFGVV\\/IpUlBJJEY2G+Zvy2kvXJ33GvSPnlG5j+zs\\/jhuSi4NprycFi3hp\\/wCx7oeWcVOGsWPmsZS3CxYsWLFuL5EREJCQi\\/LYsWLDXOkV6ud2Xw8z5kYbDyrP5FGioJJEY2G+a\\/4ApaW8tSHbp6uUbmP7OVXVblSnKlLLLycDje4lkn\\/bZJJq61T68iIrybFjKZRxLFixGPBCQlyWLFi3CxYaGuVIxFX7kf35mW5sNhJVXd7FKkoJJIirF+CVxxyrhccv0S43MZgIVlsYjDzw8rNfv5OAxmT+jU+B7EoGUsRI+ZYymQ7sVMtYfBLhYUTKZSxbjbg0S5EitVyLKt\\/IsPkwmBc\\/FMp01FWSFG3JFZUTld\\/il\\/UtGIwsKyaaMX2fOg7x1j5OCqd\\/hYPqtGNFhIS8y3Cxbgx8EhRFEsW52hkuFhRKslRhfr0G8zu\\/JfCMZTdorUwmBy+KW5CAlbg+CHLp6C53i6an9R\\/cLVfkWq\\/IvUX3DvPfQUk\\/VXsZ\\/bUtUfyO6k\\/vM+z\\/AF\\/k+z\\/X+TuZLabLVY\\/M7z30L8i1GregsVKSmtjH9mbzpjVnZ8\\/Yv9quvmhosKJbyrczGPhbzJIlEyigKJi55qr+XksZTpSqyyow2EjSWxGHFv0TdhZ57L9yNBddRQSLIsWMqHBEqC6aHjhvsZr+ncui3FSv8QoJFuWxKmmOi4\\/D\\/Ap9Ho+S4iWj9BOCkjtbB92+8jz9lUe6wt3998681jH57RlEhIqfHP6lvIfDBUMsRLi36Jvp1IUeshR8lq5OlbWIpXL6W9JrN2RCCj5VidNSNYO0v55E\\/RY+lnoyJK0mvny9ndmyxLVSelJf8jtstlzrnfK2NjfnrhYsIxdLJWl8y3k01eoigrJFy5f0XyW5TpZfqW43R3tNffQqkHtJc9Sn1W4n6P4nZEY25XJLdk+0sJT\\/AMy\\/01F2pGV3CjNpH+L0utORDtLCz+\\/b6kZxls+MoKSGnTdn+z8hFreZX1pyK392f15KEO8rUoe8hSTilHZaW9FcuNjkN+kr0Y14W69GTpypyyyXkMo\\/3EUfgH51JpQly3KNLLq93xr4qlQV5ysV+16sv7Syr3ZKvWqfHVk\\/3MyE0RxeIpfDVl\\/2YftjpXj+6KdWFWKlCSa5akLeJC9CyEbLlxnacKHgh4plbE167\\/qT\\/boKSRHFKA8RBnexfUo4mrRd6dSxg+1FVahU8MhcKlNTVjVaPdc6H5lX4GV\\/71T68nZyvi6f8lCnWjmlKNovb0LY2NjkN+mxkVOm11WvkMof3YlN+Afo6EM0s3Rccbjo4ZW++V8V3k3Ju79x15dEZqrP63ud7VQsQ+pCopdTDYqphp3g\\/wBvcw2JhiYZo\\/uuRq5bLK3oYK+vL2l2hkvRpPxdX7Epdbjq30R4urIwu0Tw+CU5fHG0PbqbsvJEas4nZfad7Uqj+gteFeH310350W08yt8Eir\\/cn9eNGjOvUjTjuzBfYMHOyeer+Z\\/\\/AASnm1bHzLnfBschsbHIv6VElLvHdaMeja5b8JFJ\\/wBWJTfhXo\\/kU45YpcMbi44am316FerVxMpy1fuZSHilGEY6sn2bjLfCv5GnHR7kviQ7afXoVoYNd86Tqwy2yqUSnVvozD4n7POE6ctfvIo1Y1qcZxej5KsboXoIqy5O0cX9mo6fHLYnPdtmFwbxd3KeSP8A2V8LLCzyP9ie6E3dWdtdyvVr5MZP7XQn4YxZd+4p9JCxalCEKtKMlGNl7kW4u52Vj+\\/hkk\\/EuElctlbj7cY7k9+EOD4ocLD5sQ7Up\\/Qlq5fXhSo1K01CnBtmF7Pnhs+acc8o2Vil2dVi3OpJL2KbdJ93KV\\/Z865Wy42XHIbGxvmuJl\\/PQ2oq72KkcLiJPuZ2n\\/wzZ24XLly42Rfjj9Si\\/CvPyu1+ShG87+3CcsqbO0sY69aX5ehGVVKUc9oy3HL2OzrKvfK5Stp8jCVK9SVS5j6ve15NQtYadyh\\/fo\\/1Mni+Lexia8vs+X7Vnzzbtl1PYhU\\/4Ox8VafdPaW3IzZtefBXfL2jie+rzfRaIw8YVqnjmkkYV0qc5OU1YxdbvqrtNuK2uZbpu2hCE1KLy\\/yYqPgu6dPfdcNRNkXcw1d4erCcX1KFRVIRkuq4Yhaxl+3MuRiIu6HzY55cPV+hhsHXxb8C06yexQ7E8X9ar4f9pCFOiu7pQURKNK9tZdWZrmOqQpavfoYep3tGEuVC5LjZcbGxsbL86kJiZfyXzTvWatOyR9gnCrni\\/CVMGq8pzhU19ipSqUn44Ncb8Gy+phn4FwvwyD08rMsnJQVofXh2viO5w711loNjbfUWuxRl3Tm41LOwsZK0F3krN+NInV8TyfD0HVi6VOKh4l8UvcWJVLEd8qOWOR5UYnE9\\/wB0rWUY8JZFaz1MLVcJq261RSmqlOE11V+SotU\\/Pprkx1XucNVl8iq9RVEpLTQk6ipK8NJPwyKtSVaV5e1iNSUYSgnox51CMnU+iJ1Zz0lK51NTcTdzM\\/Y7Er56GT8vCtG8Jcli1uLZctwvz1qcK0ckvh6nghHLCKsui2HN+43bUvcxterRiu7W\\/UWExWI8c9PqYSLpUsje3KuNy5cbGxsbGxvyFIUhMTEy\\/kPhfhXqKlScmUXeKcWRmOlCbz\\/eE1O8JxJ9m0G7+JfQxGAq0dY+OPI2YJ3oQfGG43oN38nJ\\/TT5NyKshnb1X+rCHsNv3F8xyIyuVaynGnakllVvqZn+U8b6FerXmqed3yxsiMne1hK7stxxs7PRkWk4s7JnnwiX5XbkqLR+fDbk7anahBe8irU1sjMrDqVJpQz6R2RlfWRFa\\/GPJl+I\\/cehVioWtUUrq50VxidjsCf9aa\\/2iGWs2vnxiN343EdB78b8XV9kOfu\\/2JVCTne0S2Xc3Qrx8P8AAoola2pCd2y5cvwT4XLjZcbGxvy1IUhSExMuJl+R8GMuXMZGU6GWKFLFYH7rt\\/wYTE\\/aYXy2LuJGTbbIzMyK+BoV9bWfuir2ZiIfB418icJwdpRa+ozs2X\\/jr6i4Zhy8qnK8LEuNJXqR4S2O2ZXxk+LdyN+iEqnseP3FGfuZJ\\/mFCWZF5KpFx0fRmSVXvZyqK8fc9zsJ\\/wBKqvmuSQvRduvSj+49Wd2\\/c7vpmO5+p3SW6MlP2Z3VtdSfQg6SnHNrHqh63tx7Ef8A5kfoyOwyrpUlzPjm5auJhTW48c6s1ClBzf8AwN20W45+2rL3H7CUr6stcnC6HOxKoUZ\\/1C5cuXEy5cbGxsuXL+XcTExSFIUhMuX4vgxly5J6I0asQhCmrRjZEpq9ris47m1hSd37kanvoy6e5KOZWksy+ZW7Ii9aVW3ykYShXw141IfuLzU7EuOG\\/uftwqbHa\\/8A62sJOTRLcsktdyFugtylTlVeWnByZWi6crSjZkNWl77GMwuCpQw0Yzl3v+ZfoTVrnU0svmdgrwVn9OR+TmjbblW65e3V\\/Tov5szfIvYbRhoyxE4wTSv7mOwn2WMZKtGa+Qm2Yp\\/ZezKFCcYS73xfNFSUZu6ilpwj8TTLrMNHYv8A62H0ZHbhX+NfTlQ+fFVZx0RKNSrLrJlOl9mh8H1Y5GaMBSvsKNi0voXHIqq+sSdS25Cs++j7XLly4mJly42Nj9AmJikJikJly\\/B8GxsuX1KsrZSNQ7xCV22+pkW97Fnl1N99fmeLqsyIvTw\\/wRnf\\/wCjE4arCTrYeb+cSjjqi+LUhOnWXh0focL8UuE9jtqn\\/wCY\\/miGSMXqXs7n7kWSuyhjq+FUu7nuVsVUxUs9R6mDq0oYihOrFuEPymPxrxWInU\\/i\\/sZ3rcWquLdHYsLYW\\/vLkZ7+bHdcvbEM+Eb\\/ACu5LSTRca6me2wpPqQd5RMfi62KqXquN0suhG5ET1L5n8xo7Ch\\/5En\\/ALCO3DEbx8xyJZ6j3Sj\\/AMmaMFaKJSbFNZpRvdo33I\\/7f5E\\/y\\/yOSW2rJyt11HN+46lju51vufux4SnS+bOi5ExDG\\/RpiYmKQmJly42NjY2XE9TFVstS1tBVUU25\\/QTVi2fS4lKOjZmV9v3LX2Zvvo\\/cuyFnuTwNNtyiyklTeV02pDp+zMkvy+fhd5cGdvUvgn+xfLcevGLvoMT01ITavZ77lru5GK1Hf4ehRjmmvqYOl3OHpQ+XIz35FbqPgh8LabluC3XLWp97TnB\\/eVjF0nSqyTWp90TJfI2IamV3tIikmVNbex4Mr9+FPVQTj+52HQy0JVPzvT6LjX+75Waw2\\/cz5hzexe2pWx9areFCNv8Asw+ExdOWfL9dSOqH\\/uf7Emrb2RKr+UjGdR+FEcKlrUkZKMHenFZiVR\\/Nn2qlOeTvFcitLX1LciZuh8LebGnKXTmuJiYmXLjY2NjY2R3JUnVctju1S+KNhVCFW3QUru8ZfsKXuJ6meUJbGr\\/cxFeGHheRSrU6qvTmmKVhyTi7kZeEUkShGXyZKEo+bht3x7Rod\\/QmuvQrq0+XfoPREF1JWSjYzNtsvnaOxsL32IX5Vryvy9LbHTfyO38H4u+it\\/8AsdhX3La6jf8ABF2ZKrmViK6jbegykryRSg604U47z0MNSVKnCC2S44jdci5bkprXKRz3vN2QrXKv5hEIQTbUUmzQlPLLQlVjv1Lym\\/chh1vUY6ttILQt1mypXhSXSK+ZV7T37uN\\/my+tzCYqFSHjaU1\\/yaPbhYtwTHxflRozl0IYZLc8EOdCYmJlxsbL8dhVcj1Z311qzuqNV\\/l+cT7Jp4Zkqc4bojUZGals9RVXtJCfUrYWOJ+KbRPsvFUmnQnf\\/ghmjGKnvY624NPe5GV1rwnT6x8zDfFxkro7awXcz7xLwS5b8EdBT0sUqeeWVfudm4P7JQX5nvyvy3b2GvnxW3LXoxr0pU5bMx+Cnhqk01wV09B3b8XuJXvqZRSfDX2ILU7GwGSPf1F45bfQS44j4l5E5Jbk594nYgnFPqxRvq3cSdtjc+F2O9sSrE6mqM12kRjCmiUnNjkoGJ7R1tT1fuSlKbvKV3yJtdSOJrw2qsj2jXW6TF2mvvUiOPw0urX1I1Kc\\/hmn5apTl0I4X3FShEc4xJVyU2\\/JuXLjfLJxtqSd3chJLcp1obCa6Owqso\\/Ero7qlV1joydKpD\\/7IVns9RVdCFYUkxuxT2v7m9yHeX1enC0t0yNXpLRlSnfVeXh\\/j5MTh4V6coSWjMfgamDquL+Ho+WVuhT+LexLZop0pVX\\/APJ2R2X3SVWa\\/wDauaWzEi+hbTyXxhtzY3BQxkLP4ujMXgKmHm1l\\/Yjo9rjbYiV8ty2lywtjsnshyar146dIijyV\\/wC5zyqWRPxxM9OKUUj2kbP5MlOa8KQuhVjmVr6k5OLs9GSq2E6jnG+xh6PdLvZ\\/E9h5pak5qCev1MVi3V8MPg\\/78u7WxSx9anu8yKGKp19tH7cblzLJ9BUJsjhl1FCETPFDrDqNl\\/NbLjkluyWIprqPFe0R1qj6l2+vJGpKJTr30\\/4F7xKdZSVpbmLhltOK06neeEVQjVI1M\\/hLaCtsxpo+RJJ21sOVtKi09xScPnEnFSWZeVRdqi5cVhKWJpuFSN0Y7savhruCzwGuLjZRd1r0Ixc+mpRwc6zSs2\\/ZGA7JjRtKrv0j7c8+CQ+dXRp15KfPiMNSxMctSP7mM7FnG7is6+W5PBzi3YdOcd4ktjI2lZMw\\/Z2KxPwUjA9h0qFp1PHMjG3LUd5y5puyM7lmT\\/Yvs\\/5O7jF5mLZ+xfwtDr2+7c7+bvdaHf6al3X0yZhYJ\\/QpUIQ8ch+LxS26FbFU6coZ52MbjO\\/bjD4P+\\/O7OhmrEsLNbVP5O4fWRGhEyQXQujvDvGXb6+gbtuTxMVtqSr1JdTfhbksWLcIVWhWlG+xCpbwT1iYnC5dvgZkq09tUQlOT0RTap9dTviGIhNWIxl92Q9OpaLjoW8OU+FKy0Iu38j38mLsyOqXK4oxPZODxG9Oz90Vf\\/wAb\\/JM\\/\\/Xq\\/\\/wDmQ7Aq+0f3ZR7Epx+Of8FKhSoK1OCXkN3Zcv5uxF38ieHo1fjppj7Kwj+61+5\\/g+F\\/3FPs7C0\\/8oUEtlzTeWLfPW+Evf8AfVGj\\/wD5f9mVQ1n\\/AAXv4p6RJ1nPRLQdoazf7C7yt8oip0IR92Z8q3svZEq2bZEpaK\\/8GJx6hdQ8UiU5TblJ3fn4au8PVUyGPhXWhnuQouavc+zfMlDKWGjbnlhY9B4aQ6U10LP25ajcYSl7EpSnuyxYtwsW4242LCm1GxTqp6MhUVsr1iSo0lvsz7PC3gqWPs9e\\/wB1r3ud5l0q0\\/3RFX1g7iqzjIjVjLSQoRUtGe7\\/AIP\\/AIL7eXhql45fQucUOV\\/Q3sKp7inH38+5Xq5tFtzXJq8WW10Hk3v+w7R8U2TqObuzvGtiFO\\/imdNXZHeRjsiEcRUtanp\\/BCmo08uVfM7XlklGEXvv6KMpRd0zDY\\/pMo17axZTrwn9RqEipRyjQ0bc3eSFUM6LxZkgzuIH2aA8KvdmIiqVGcVs\\/IsW57Eako7kqsZQTTI1JPoU5ZdXLUzZt0n9CVHrB2Z3rvaqv3HG+sXdFOeVfNiy5txfMm\\/LjNxd0U8VF\\/EZk9n5V0d5BdR14jrMzN+mzNHfSO\\/+R30DvIe5mj7l0XLl0Zo+53sF94eIpoeL9kSrSn15r8akbP67E5pPxx1HKVSWpJroUlm1ZKWVfMp4epW8T0XuU8PSp7R193x7WlfFft6ShipUnrsUcTCpsyFf838imTgnqhoaNuL4NiQlwjbqeH85Ktb4XcnjHEq1pVnr6TPJEaoptDSqI8VOVr2Kcnn1IVJQl7indZpaLohyu7ly\\/lXM7Qq8194+11D7ZI+2P2Ptj9j7W\\/ZH2qR9pn7nezf3jMy\\/kX4X9JczP3M79zPL3M79zMy\\/kPkklNWZVzw0krobvolYtsinQlNZY6LqyGGineTzckvhZjpZsTU9LCcqbvFmGxylpIhUt9CMk1oNXGho24tCQlwQ2orUnNy+hUr20iN39N1ElKOpkkk0mSWdWa8Qr\\/uKpP2X1E5Pfjf08Vc257ly5cuXL8Lly\\/qXyuz3R3NL8h3VOVrw2Fpy1XanIrPNVqP\\/AHenw+NlT0lsUqylrFkZqX1GhoaHpxS4JE5qBOfWTKtdy0XqaT8C\\/AMxoWaEXLly5cuXLl+FzMXLly5cuX9I+ePNjJZaE\\/oPr6ijXnRej0MPio1VvqQqX0Y0NDRtwsJE6ltFuVKihq9ydRzfn38vDvw\\/gVy5cuXLlzMZi5mMxmLmYuZi5cuX9XDm7Wnlw0\\/VQnKDvFmFxqn4ZbkKvuNDQ0WsJE6vSJVrKG245OW\\/qWMw7\\/Ar8Lly5cuXLly5cuXLly\\/r4LlR25P+kl6vYwmO+5UKdW30NxoaKlS+i2Kte2kTf1lF+L9FIa5ULlR23PxwXrcJjXDwz2KVX22NJIaKta+kfPvwvwvz34sp\\/EhbeWkW\\/HUPlhHkuZxbHak8+Kfy9dhcY6TtLYo1uq2NJL163RTenlx4NfjaXB8sdi45jmX4ZrU2YmWevUfz9fhcU6Ls\\/hKNbqtvwCi\\/MXBr8ZXF8iMxflxNTJRkN3bf4BhcU6Ls\\/h9c+FFienlwfBlvxp+Z2hUtTf6Bg9Sm\\/LQuNvxl+XOaSMfXzyt+gqcxSvxv5EH+Ny8m45FSvGPUxONvpEeu\\/wCgr2I1hVTMnwT8hO6\\/GnztjkVK6iVcd7FSvOfX9CsztEMQRq36mYTL80Xb8Zk+dsnUSMRjbaInWnPr+h2MZGtKBSxCZGdxMT5oMf4s9OdsrV1Ar4xz0j+hnxZIYy9ijivcp1FITE+VOxuvSykoq7dkfaKH+rH+Tv6P+rH+TvqX+pH+Tvaf51\\/J31L\\/AFY\\/yd\\/Q\\/wBWH8nf0f8AVj\\/J9oof6sf5PteH\\/wBaJ9twv+siGNw05KMaqv6Zu\\/M5GIxKgtytXlUfy\\/RD4MYx8aNdxKVZSExPlgx+k7Tpznhnk6O74W45TKzbkwlCWIrwivq37elk+ZsxOJUEyrVlUev6LYxjHwRTqODKNZSExPli7ofpMX2Wp3nQ0f5P\\/onCdN5Zxafs+FhZo7XHOo\\/vMjHh9DD9n16+r8EPdlChTw8MlNfX5+k2HrytmKxKgmVKkqju\\/wBB3L8L8L8jGMY+CEQm4u5RrZhMT5E7M3H6ScYVFlqQUl8yp2Vh5fBKUP8AkfZNZfDUg\\/8Ag\\/w3GL\\/K\\/ho\\/w7Gv\\/J\\/5F2Vi3vkX7kOyF\\/mV\\/wD+qKWEw1D4KWvu9X6NcZvlbMViVBMqVJVHd\\/h1+F+Fy\\/G5cuXLl+Fy5cuXLly5cuXLl+Fy4xjGPghCIScSjVuJl+SnLpwa\\/DVwk7crZicQqaZVqupJt\\/hty5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly\\/Bj4MfBCEIjJplGrcT5YyuuDX4SuS43d8uIrqCK1Z1Zfity5cvwuXLly5cuXLly5cuXLly5fhcvwfBjGIQhCISyspVLifInZm\\/F\\/g6XJJ8taqoJmIrurJ+34fcuXLly5cuXLly5cuXLly5cuXLmYzGYzGYuZjMXL8L8WMYhCELhCWUpVLifJCVuVr8EXFD5atRRRisS6rt0\\/EMxmLmYzGYzGYzGYzGYzGYuXMxmMxmMxmMxmMxmMxmMxmEy5fkYx8EIQuMJZSnMXFIRYt+DJCRbhLjcqTyoxmKc3lX4jcuZjMXLmYuXLmYuXLly5fhcuXLly5fkvwXC\\/Ix8UIQhcYSsU5ifBb8ti3ImNenSubciRblqPjOVjGYu\\/hj+kFzvkQhCFxjKxTmJ8Iands7o7pndMasWMpbgnYtf0VuFyxfikJcFAUDIjIiUbIk7vhKVjGYv7sWb\\/AKXfIhCELkjKxCYmUZeIzWQ6p3sjvZDnfkyljY0Zb0FzctYuWLFuS7LszMUivU0twlKxjMXbSJ8W\\/wCnEIQmIQuMZZSExMhUurD8mxYuWNi\\/l3LiRsblue3FvKiUrslKyMXjLaRG7u7\\/AE8hCELlTsQqEZEJXQ\\/KtxsW4XLly5cuX4W428hF78NipO7JTUTFYz7sTff9PoQhCFyrQhUIVBO4\\/O0LGUsWZYsaF+N+W5cuXLl+FWp0ROoooxOMcrqL\\/USEIQuC5YzKVWxuP0F2XZr51iwipUtoVaygtyvipVNF+o0IQhC54ysUq9txNSLfgNSskV8UodSrWlVe\\/wCgH+CoQhCF5FOq4FOrGfrrDkolbElfG9Ikm5at\\/qZCEIQvJjJrYpYhPSRoy3p7clSvCmYnHfMqV51Pp+qUIQhC8qnWlAhWjPyLeVYsK3JYnVhTWrMR2kl8JVxc6mxvv+q1wQvMW5GUkjvDMi\\/oFwuX4dowlZ\\/o5fgCEIXmQV2W0HEs\\/czTR3x3yO8RnRmRfnujPE72J3onJiixRLGKp5oMqxyTkv1auC4Ly6USw0WMo4GQdMyy9y1T3L1DNVM9UzVS9U\\/qmWq+p3UvcVEVJCihcko5kdo0slS\\/6tQhC4LyqS057FjKjKjKjKvYsjKWLeQlw7VpeBv9WoQuSxYsW42LEVqR29KhCXHHQzU2SVpNfqxISEhISLFuFixYsWLEVqLyr8LifC\\/MhcldXgzExy1p\\/qpISEhIsJFixYsWLFixYsRWvJJ5ULNIyy9zK\\/cy\\/MyfMyfMyGRGRGRGQ7v5mT5nd\\/M7v5nd\\/MVN+53UukilN5ssuSfws7Rjav8AqlISEhISEhISLFixYsWLFiwuSv8ACQ+FegQi39bkex2rH+qvM3I0JyFgpDwUh4Woh05roWft+lVCT6EcPN9CGEkU8DfoLApdCWFsOjYyiQkJGUymUymUyliwuSt8JH4V56EIf9xcuNw\\/eSHgvkfYvkfYmfYpH2OR9jmfZJn2SZ9mqH2eoKhU9ihhPkU8PFCpxHRiPDRZLBr2JYBexPAfIlgpIeHqLoOEl0\\/RqhJ9COFnIhgH1IYBexHBr2FhokaHyI0LFVZUOVzu8w8OLDioConcndHdoyIyoyoyoyokuSv8KI\\/CvQLg\\/wC4uWUEzuondR9juoncxO5idxE7iJ9nifZon2WJ9liKkoj0KesrDjpxsZEOjF9B4WLJ4GL6E+zvkS7PmtiWArpbEqco7x\\/QsYuWyKWArVOlh9mSXUp4D5EMEl0I4ZIVJexkMpYTsd9Ar1I5WRV2JcLc1y5fjLkr7Ij8K9AuD\\/uL0UynpO45305rFjKd1EWHiVcBCX3TFdlZdYk8JUiOLXT9ARpTlsingZSsYTs2EFsQw8Y9CpTQqRl5NTJcVInSsVUU+a5cuX5Hy1\\/hRH4V6Jv+ovRSIFvKplirTTRLDRfQq4CL6FXs32J4WrDoNNfjSjKWyKeDnPcpdnIp4NLoU8LazsQgkhlRozFmxUjuTujIiyHNLqTqX2KhHfzWX5MT8BTTyLUtI8Rd+xf5GYzmZGYzGYuX+Rr7HiNS3zEh\\/wB6PI3ZEJ5n5sikvES8qnvwkhrUsOmmTw0ZdCt2fF9Ct2dKOxOlOG6\\/FoUZz2RR7Ob3KWAiuhDDLoiGGRFJdDQRM7vW4oJCymZI72J33yO8kNt9eM0deW\\/k1JZZid+OJ\\/tlH4Fz28uX96PJiJ5KbMC80b+Yx7lPQ38qO4tuE1ryWJUosq4KMuhX7M9iphqlPp+JU8PUqdCh2b7opYNR6EaaRYjLId+\\/yolUm+pRjwqOyO9+R3jMz9\\/IY9xebjFpmKFXNHjif7ZR\\/tr0Uv7y43O0K9\\/Aupg6eSkvMZ1ILzIbcKq5rDpplbCRktjGdn5dYjVtH+HJOT0MLgM1myjhIx6CppFuaitOFfby2iohebUjni0KToVbPYhPMuGJ\\/tlD+2vRS\\/vLhcr4hQizDU5YmtnewtF5kiJHzKb04VfIsVaKkjtLB5HmX4dgMJfxNFKkoryaW3DEeZNefi8P3iuijWdN5ZEKiZiH\\/SZh3\\/TXobkn\\/VQ5FWuoo8eKqWWxh6KowS81iI+ZS4VNvKxlBTgyvh505S00\\/DMNS7yqjC0VCK8qG3Ctvy2MpYtyMktfPxWEzeKJGpOk7SJ1lKkzDPwFy\\/nX4VXlncqYgjTqYiRh8NGivn5zIIXNYsWLcaW\\/Ce3lSV0VMJGV9DGdnOGsB6P8JpRUpJMwlGnGSsiG3krcjtwq\\/FwsWLEeLHyT9BiqNNq9ip4djC\\/20JFi3l2LLgzGPUoU4zl4inTjBeFeczqQ82O4h7eZXhFrY7QpxhU09JgsPTnuf\\/\\/EACoQAAIBAwQCAgIDAAMBAAAAAAABERAhMSBBUWEwcUCBkaGxwdHh8PFQ\\/9oACAEBAAE\\/IS4x0nRJNHTAYxsUBUJopiZInoTJ1LONqOjHpkmkkkkkkkkkkkk0N0SSSSNjY1L07jfor0WjaiWvunOjHjgjyOn1p30c14rjejo\\/Yx0nlkkk6HRjGOkioMMJ0QhaWNeuMOMKrHR1kkkkkkkkkkkkkkkkkmiSSSaJJGxuh6NxuM2WGRpWl\\/BiNSCUxehrV\\/wlmV4LebfwOnNEjJJE6TWR0MdUyRO4wwqInXfVJxxhVYxjJJGTSaSSSSSSSSSTSSSSSSdAkbGxqjm8zdGfQq5OfNvoR5754E7PcVwEayjVGvJDz7TvVIVLbhEVOWKiQNyxJsl9C5KsQR1mttVqc\\/DY6MY3QyROiSSaNjoY6SSIYYQhhVUc1Y10WFScYarGMdWySaSSSSSSSTSSSaSSSSSSSSSNjY3Q9O4ydHReT7r96ZM6TcItzfkbkJjiiNBaPjApqGzFlgS5IgRF2FBRmtrN7gPL3Nd\\/I2O4xsYySRMkkkb0GOsiY1CYngTG8puEJhNI+JuX9Y3QmfkI5GPDrDDCYmSMYx0Y2SSSSSTUkkkknSyRskkkkkbGxhq+4yox12otvD9GNaJeLyFCskJF2xX7yJngXKFWTwPy6JdUm9kvFrYuDAaHkRmSESQPltBxbR8tjGMYxkiZJJJOgx6EKhMTEcSwhyr8hNqlBMR1gK2Jq6yIvKkwwmJ1Y6GxjZJJJNEkkkkkki0ukjZJJJI2NjYw1O4y0ODNVVeaIdzayUPZGRDSyxNMWFFuLW5ENOroVgE\\/Rkoc1YmJK1l9L3RkGY+yhss\\/oYxENO5nSs+Z0dGMYx0TJJEyasY9C0iWwkTw7fQhyF4y5bP5nGwIlFFplDSFMPgRjpAMFBxMTq2MbGSNk0STUkkmiSSSdDGMZNJGxskbGGHybjKjV6IVVp58jxsMe0kC9c3iRzcJWLEDfMSQNNyYIJQrO7GBaSVIaKUSNJJin9yZQ1Xb4Dqx1MeiRMTJJox1kCsLmzSGBISbDyyGTBIXVMUEo2ugJDM0aIrzjCZNGMYxjY3UYkekCEiepjGOkkjY2NjY1OLMnR0VhUjUvDKk\\/wACGlve4vxwSbuHSkhcw4YshJSWLMu44NhIJsNcsgsWDQlBAhknse5IshTrDm+4IYzSX3pHw2OhjGhojTjC7p+iZfGMUPv8CeY9yC2I2KBLSkaJwk5jsmcouPDJYcMaZL1XKT0ExMkYxjY2N1E6wEExMTE9ZsMvoOgc0NjY3TuM6Oirzbzf0vpCl\\/7Bfihe2Ca7\\/noKdKrbCbDuuhmxODbEZDKLROR7JqcDbJKwmVxqriyREyCWJLya5RggHAZtIDHS3ht4GhjQ0NDRBF6JC4wQlGKfQOygOTuE9t4IRE1BYPZkoQeGMRoswYpcyKCuWLg5FEuy3vwSIXcoR+MAmQQ1s6gwmSSMbGNjDDoPUAgmJ0JRWvuy1EaLUaGGHMtCPr4F87z6Gkts3wYTDeAzkiC9A2O\\/+85P+VpQlIV2M5OJB2ypOJBznwNaGhxQxQ7gWEgy4rkJhaJoGHAIYtMfV+PGjak0uNUgaGNEEEFoRshQrUiKnTKwQmw2MRPcaQkmYpGlC0vgwIlhYS3E3JcOL\\/0vPNIm+IMjABCtHN2tBCZJI2MNjYw2N1HQmhMTGZNUUk3GZD+hj\\/Qn7oJ9\\/wBDnc9v5rC3HrHQA+Me5Gt45g70mb0foxP+iCBfAUKCfg2qgbA2IjfTwXWOhZsWqYmMKDRiZkgmhwiQmwTUJ0iN4xcbsjBhkWDSMhqGjGQqFh3FkY0Z51TTfW6KsDQw0NEDmHsX0u4lUIaxDInHzkTwONqYzaTJyHOCcSXd7jk6F\\/Y0hzWZvZIXscKCSSN4vMSvC9mTsD2YmtuHcCV96IJJGxsYbGGGJodCSRD7DLFL0cFJML9DnH7YtnLLYlly5BBBBBBBBFRlrwNoy6DlC7lczNKQL63Q15EOaTvNcW3yNZCmBRhCDyobwJESd2JXsZPZFviCGC1pobdO4xEzGdhSOmjNTuZQNFzIINYtwMw2mgcYQNisoflDRX+a821yvA0NDQi2ytJCFSjtLpM5zuLybDwQ3cd2N5g7jh7nIECLIbUlhgbiqMLurmEDma5\\/gNlM5FZkMSm0JDJgOMMJjY2NjDYww1GySRMRtwi\\/Wu6TYMRKRKVhtTg7CI4V4HpikEEEUsxiSa7J1mkGdDVnt5eQyD1UCSEu0DZqeD9jywoO3NZYIOIGTSEpOEFIbyJJDDFxrpoehN3M4JjNnahJzZCMTgMZGIOwhLRLE0p1tyNjrPOp6Ps21TV1VW7G4qpIiQyxEZM6SbDlhmmI4dKcyCYILs2+B3iRbxkCsKb4JEKFnkWrY4YlpjZfYTScr0h2wlzC+3gcYYTJGxhjdDUOt24W5\\/yKiVZeWJAwkGaIsDYk9\\/iQQOrKok248RPIJm+BHiUGtJho2Ogw5EsVuNix\\/ghuQ4Iy7vKwWNmyKkWWGMmqz2FQJw2mLgFWAjpWTChthl68CwNmoWSSucgr4kNZ2JNhFw1d8ot4yLq9PXvqsbidd696siAJIgGLSAltNE3kYCi0MIYTXjegpGlAsrjkXgXdiHOkleItJCtERkYHEnuJbMKLF7Y8zEimqlLQcYTJJGxsbG6GMbGNVa5BSh2qDCTCRZuS\\/hRRIjVFEOjRhoke6onbfF411+qWUF6G65PcvnghNcvoQcCrOF0O52yjKW5n+kNTcsUkEk3aRCSO6Y2uhLhN6xThIW2FlIajl1G00fuliihY4gH+3Rjl9sfj3otTHMvQrCHJchakgmGNwuyJXpiRCw4JkcswRwuAh3ArAyY9jvIxIgQVGBKJbZjewK5ZZeu4kS1Xmw7UO24ysFbsYsqJYcTokkkbG6GMTGGxrprLg7dTdiRE6odUvB5hZsLkPomrLEhMh6xhxS3Q40xDmu9FRjpEzngXNueA7TvS2IhE3IwnoKrH2TwKGeyV2NkMjxcEnLmy0m94Yl9QY8CfBgKS7FpRpr1xPaiC5DVLAoS9JmfcPsa1SPEmKv22CESwYkZaD5hUeQbEowkFNqEN0lDMgxhKUVETYmYwMo8k4kasoIi0OSNj6AsxsYAI\\/SBDtx\\/ImyzIdkLv7dViwSSSSNjY2MZZD2EKt9ish60S1oJgmkiw+OtG3YVcktLUkmi7QgijQrJDRhzH5VDbXwxXfZitZTBL2EJIKwSbmiLL6IWdhIhDyJXCxJBtZOCy4wgVsEFHHoNY00qFnToBsOhWRjVwhG01LvJE4wW0I0+y8UjWhUS2H2LNuzIFCLXFK5MyKIQiodUJVGnK42BsxeYMQsilJEYZHNyhd9IRABooOSmsF\\/wNgsVy6NXFYuyavyQ9BhMkkkbGNjY+F9BeafQshDbaWRpXMQagiaEiUh+G5uwtkjTnxTSDFIIIJBCNj5yKXqyQXGShRhO6HK1qzMRS\\/IuKdhm1TsJexNgxZDrfzEuuXdGUQ21cjXlAyGGQM7CBIVZHbMjuGhRjs8kJiQ5okPcmGYwriksZVIYS+\\/VLW5b3HTnvodxNX0oX7MFFcsjyx1CocB7F6ixgc3I0uxM3CwMdqSe4jeRTuOyMZbIVCYmJWhxidYWXlMw1YdO5SWNWDQ1rktgqvbSjS5fYhz7fYOzMm6mdDiZJJIxsy4i4wSROuRiysqJaEponkkeyAzPjkDTvkblHmgmkDQ0OiCFpe0NciGsrwQpwZmBsjSnbyx5w+griJt8DTahjUzaGbIv4DGGNjljXKcORPctjSSigY1yIyNnyGMBLDQyVkkwRwyS0Xlk30nXYmUMelehxGJsnkLgIwhkjg+h02G007qGqLjbfRHu2XvLliyyIYIyDlWLHLJ1BS4RlMtIcERKSCwkG2NymgwDTs5MvkDYZ3FIM90x2ePIspks4iOA8PaCw7oKMRIetWibbKgqGoTpJI2Q2hkw5xJCWtJLIZEkk+NXOqibAmHG806WhoWsNChqgxFEyhxrVmEhH+5HxWpuNLTdg0034oh7RMIYxEvgLbl\\/IZwRFcj7Q2aiScy6ISextYTFkOOosLusDZFNKGs0I2hs2O+TF1j2iuschrQxKIpFEz9CEFrTEEzaHY5ESQ4ZEqg5I3kgkQpEzHSM4CdiOykkkYFRJHuLqdkINNkEZasj9iiDyIiRVdjzi9kzi4ISksDOUcYEljYrhXhu8jvg\\/st6yYCFQhVkSTREkXO2i00LQnA235FuWE76TJJcUkeTF67zSzraFoNq4MfZql6OnFE0aacNG\\/M47r\\/IJYLcoa7O4v0XLNQSLF56UuLSljYkyZESnuIyYXDHvLsSWFyLCWIEySDTENmKfsJkRHKJF3ofGjeQpiJYZ3BD3IdxiggyU0NJikbkSMUiBGIJjDK5PsOMjcsUPhGZlgCwFesWvYSNdHrBrD30PCLDuyRZmMrtwHM6m4VZu0OlF+mLs9F9voJI7qeB+Z6nryKKoi4UVZIESxAEa+JHYnGBv3G6zSRlkm0TwNNOH5lC5Z+xDUaWJx3aXuMsZrRjRgvX1M+8lOsgulQngk5gieotaSJyGUD1\\/4heHYuskjVYt8CFk4wJw7qm3eRbxQsWBsmTuGhwQ1CwerG\\/GQYkU7SRTISJqKkicivyIe5HDSdPdsOkNwSmWLQM9uRJOUdiH+xEBeMRIYhwCJsIJAux4ZKaOeyrueSIQJG10NEJ7bczdOhwlVkPDPSEIQwxJ1BKqgAUgEJ8FJvCO6CCfiKSSwxufMm0fkpfa+pqmx26MVtDX78N7du7YllsmUPfvI+9lEP8AosaiEYvsGMTiWLluz6CWsrFuaDeRaKmvgQKQWaOo6cJdCHlE65FCASmOuOC3CJN8TYmlJEI3cJyiaFiWRMgDE6YRMxIkiIZibUM7tBAglMBDbckhwFmWFqFLZ3YdiMguUIVsKAzYTyi6DXiSw2LErD2dghMQhCzFNRNCbyuxUYFWPGnfQlkuPioGzy\\/FI+Zig4m2PMoEYKKdh30QNjMGygtNWeFx8MJMQBF\\/dJkBWxC2qBG7foNOzIHAUEzY\\/YAwaeQwWwfQlRyuRRzMi8gMWC4pYkDQ0EkMCVG2w3I1cewp7iFuNYwLDJhUO5C3EkEmCGo+DuHBC5LbDS0WCYLhkOQsSZDZIboUFDHLCSEZCplhaIhgn1w2Rl0ij5Yzc2cPIqSDY6eUwZb9FpH8FCP7loVEIR11BBIiiRCsRpDL0P3Q0HckUDfhgsiCwhu\\/MtHgJG159hoe9Xop2JXtWU1ycWYRVFsEW0uedEaVaP8A2MXRUqCFuz0Xd1nD7MY+nAv\\/AAydwCe4nKzOqDGmQ8Drg92M2TknNkA5JBMVR9hXBwLy4tfAhNRiDnFQlij2EEtQ9qDluMZiTY0oSxXIhbicuCTBFwpzmwRCeZm5PXG4g7u48kReWJfmxlI3ZJQLU\\/QdglexNqncZG+GUI1YvLD9In9Fi8mWxoQhaS0olVlBKsQUybrQySGtyfBPwk+D88WribGKJwxtXbkiIaSbJDQih6BvNCa6s318iK6gmL\\/eXnZ4ZZuWylPoRF7bjcmvzsLY24RL24JzahJeARktjQkjRc2htQWeGPm80+whdY21slvJInCGSRcRuC08GKWTAhOw\\/WICHpcil3HCLM9DiFlJAsjY8KmPJxgdiC9SN1hS5+9Q1xPgJ91hsX3mCHUCZNk9XFyJm5ZacSbCKmvgezbsoyJS+7Rm7yJUFoAIIIKiwgQrkg0oS5eLkTEi+QoSoEiYfnTaIpFCcbDdBWThGNDWgpz2vq6b60WI1hievnIbqyzO5El2oZy\\/YyzaIRKJbGxstXcCSopVesU2qDlh0pgcEZtVSQa9xSXZAKFApwJCZ0UoXLHRstE6MtDindkroKRkdghOBEWhfi8wxkQknKYmqDkmg+DsRcx09oivJTXRZLnDyOvX2XllumNa+C6bcLdh83xvDuxISEtIAopiIYpCNLyQLwJkyGOTh5JCfOme2gT7Hp5u7WRKd2SalW\\/zxyG2wo1YGW+RZHjjVA1Sah4Un6fNXql7C4UQYhZ7UGW5DgOdtyCoe6D5HNsFjIi+IIKjBdO6qOmYENpZRPYcTJXs2mhFaEblITZj1aYTG9EvIoRjasoJi7ZJZUG8tQtJCb2AoRGCBBR6wuLnXYxFfA57FkNqYvyWC\\/b\\/ALJsMPgzo6zI3bfzGiKEJCUEEhISlXwQQq5EvClLF9lD5JTMcPmhB+VZHknAngXej48TuUfwMjsbU\\/dN2ELi3g4UoaaFRuQasG1DDk4uZFe567hhqk5by\\/Iuu60W1NWaaYgvFCaIC1oyg9sfzZdcYlptDkc6YsjkZCxyKcdUjYycTyyaLoireFcNQxicojDTSF4B\\/MjRTqsma7HZYaSQ6VNbXOhLqilRo87EI3BBMDSK1LN4sjP\\/AEQfxINECEhBBBBBBCJYSQLStUEpYthou6\\/sQp4ZIT4ki7GyRYgxU+u3iSEg7tSxRfO3xF+zxSLikSDhY1bCcbjd7iezFPwuReKohmk9xyQhA0Tjq0rcMhaIbaI0xTdsLo\\/QOvys3uWziyhMVuIixc2kTRhQHMUxngEUkYRaOC0BtzcT0zLui4LYXT0QtLmOx3FpI6iqCEuhZEfJekeyoQsZdQmi5Kj1JsKRP+Aq\\/ERgFOUGc3qwZuF2IIQQQJVNggghDmSbbEMWHayFVUb0pSOCiAk+Ejsmw6MNao6Jwh1zWXglili+xKeNcPgUbkf+FJPdjZ7iVlk2FJmVjNhaFbDxq2RR0ZIh1XaOMTYGPcei9N9yOhK23GLhFlciyU26JiEJoxoXAJLDWqTELQNKJKoiuwOSGSwSHsoNbqEQSq9olImHdklmPuYqHb1RK8\\/qI6duY3WT\\/CPr5MgSiCNAqNlTogKRJIwhaVYbG9MkqnYdEEJKIbx9DyCOR3eipOdNe4uxHlEVsO6D4h1DqHXoySISCSS1+UdU6p1TqHSOodY6R0hjfcx8ULAT\\/g3vwlpCYleLvA02FNQx0YxTQlGm0eltNZ8P3w8iC1ptTGqJwPTkTJJ0JkBE6bEJ6AsBrjOuF2JEeEPsC6QJ+BMRigGMbY3SULSBVgspHAsiRXgnb8C1zeYCCCEUgWkEyftWiFSCy0VUqNzrlEEPRBkE+hO093GrDP0NW1j\\/ANA\\/9+mTA84P\\/FIpgSBP\\/OCngE\\/8pSLrYdHCHqob\\/wBhH\\/e0S\\/8AMX\\/LUOIX7Hto\\/oT7\\/qJZkR5L6omeBysonWqtwJVwkrCc6GhaFkC9Kaaz4YXUfsyo\\/iYxDWuBBEa1nQJkVKzWJJgWQLkPMathpR1dWNoY3KiVJtfrXIwv9IaGNCEXOSKNoCJawSgIUbnQlRvw2YXklJ2N1aOxBBBBFE4UEEammhoZNjSH5Ma48wYeloUmC9WXhEJmkH3oije7fkHwdHFBrSkQQZhiCBoaI0CEUTIy2i8JU3igRgSkzoY9UiCCT+ESh42QqNCDRBA1oGP52p8EKXqKiVSSfCg8rtrhxO3iRYI8Y+FMCc67QtWmhUkGGs14eXhIwNixKRRISE72FSiR17LrO4g1REhIQ960kEyZqlobpGh1SkSMzbhLc4aaGNCDo0NCGEGxRxqmiIGJ+OnA3rgfxuUufCSTNE9LQlIgRNZdUw1SJ1uzH6RIfClBoirlIIIIIrFDLDomGwmp4MqcVIIoRVCKH0pwVSEiZm7+2pBhoggiiumv4JwRkc0IR2USImJ+M\\/iIj5kCkHPcYavCnpnI4I2x9KEEI0QQQQQQRS5DntQkjQ3rKoyqQyBMRXMQkSHPfPR2IR91aGEpBDbshsl24EigjsNyxKRfygMfGnRJAkn46WTkxsUEa91BOtOXTQKsEaIIIIojxEqqUEEVSJDEQaFQ8wQ1jLsQhURBA0IMSJgTvSlWSEh2GmixcjUPgNFlkmD9EcUfbOun7qXpkVi\\/ZGAdMeReORqg5uFfodP9hZn8Ak5CPIf+4j2i\\/ViROFb9iR6FWXwc5DYmMeuOciGs67qgdyiCCLjVYpBcQRSCCwYaiMeOXAzgVJfGrrFqokRoRDgSoam7hCEoG9BLpPlSl2cCXIyr\\/Zsh1EBhs2GkZF7oexnkhItn5F4pGTDIfe89bGwHqJCRBA1ZkkXp47YLkfQJ0mCbFliKx4npgYE0WeHqZu8wyBKjIEuNEEECRBtWCBj0Mb+GBeDOjvcxCCGQJCRFTTbSW4tTi7yLUDhYJ0mEN+W6kUthDG1dfGyFIisUgaIEhrO0+NmYjs90TTRpPgQ\\/BJ1DdiSyIIpAiCKsLLr\\/AILbBsJrANt\\/BUvGx0zDSis2NJMIhLgtRVgQZBBBAlVBA3oibE\\/KhFqEEgZx71RBBCEhKxA0MWX60O6hjOhvWk3hTomjbbSZbCFrLu+XVJFub5P2ZNfZKrFGpJ+BuT5ys+JD8EP8kxapJEaFcoS7LRF+CU5nXgixT\\/jeJZS11SB4TUpmUyqTpvcDweRIHQsLHNOziRSEHJWybD1rBBBFEhVkYeg3ImQQR4IGIkkkTJQspO0N1QhKjxVEsZlSReNmt7jehwIXvv8ACBkH\\/wDJkyvzgOZ9zYQbJmyt6MVum4DbF3ZVuqOjUjm\\/J6GlSvCh624yxYKuCb\\/o0PTa0W6CFpYauTOEMRKa6bfg\\/ox2MmiBhYUj4x0WnJF0PS9f6J+5OTanocjtMbI9cWIIIEqMbG6j30JRu9JIIIIpBBBBFHmjehENLUCRCEKjHGCJBqIb8K0zS5On2JUiq\\/Ae13\\/9QMSD3xPKSYkUxNiH2y7Qvsv+B1ZEH0juvE9c79BWFod9BLB9jI27wCEHLlrAgbE9xabIUdCiZREM\\/LsxlcMij\\/gNCvVZYgaGJ0idbR6mPL+5zRQpouVku541k0pUYb0A9tJ0LYkTERSCKxV6ZMx178SZ6c2hMTJJobGIdDk1fi3rdwmXZCVOyo3r6CzyVx2MiHwkuS6rFLDJpaTKZ+mSRacQnJ\\/Q4yvNf2+RjLLySWuCn3JcyZ0YHOw0pefNluJQiK\\/TQt\\/IPkfxzjkW64lluqJTZb07OJSCu0LZupjs\\/H4uF5Mqk\\/mRCNMu7db1VrCGFEsJ2FuOlw5JE0smHYeVy1HIHsh+7kPL3PtV4JWuQNykTpSkVgVQ9MVbbFSaKgVUEEapMkUcaWRuk9PhuENjKGsk40F0kb+hNpHonUmUJpNbf7iEvHguLYZXYLTmDYkXdMbf5HDfCIWMAwj4GyPFHFYOiXVVeWgsonBCkn9i+e\\/70YxJQ1+cXn+hFV2THTv9ecUmncUBR3ZtEB8ia1muXsjfMxsYuapvuofREJd1yJPIuE\\/Z6jj+hnFpFSH7CqJEPCJGOhoElk0NaGSruNf66DL1NNt5D0df2TBm7LkbBQn9Ryq0SSSLQSTUvTVGGyfAKgnSKxoMToxWRHnu\\/kmVr5hbCmS5yLfxLGyaJoWI+0SP6JoQni4viNcOqyRz9iS2g2S29x60rETSz2xu8tvm+BP6loh7xpZZL8GbkE7bE+3ctZ4IsLfjhfiRytd\\/EY2Fh1i+l57U6Lr3tX2IcZsjNKN0Mlim+nYXsySy2B0Oa6Py\\/QlkJMx2ZCuhDjoxAYoF+POkE6lfQnMVUoGzCYmRCkORgVD0vd1sFuKMmx2EP8oey4k7jK7+AvvB7mMty2JJExMcTJGGHrqozo+jjXiiE6WqpokxmCEK07yQ5EVFC7q35I8HyndEHtfGBJfW5X0TkbG6Ho6iFWQtMTNM6I8mJG6xJLlkIuCxE\\/CFkMJxQxzDhJF3IiEeN+zJ8BMqogI9AzOHLQ2ziFyOcho2YpjfZ3lUuk3nkhNH\\/f8Agv8AkuK4gkI+qQ5cAlXBWV5DympH4B+qlMltOzGrm7BI41FrkhR9OZVcU1GxypUBrqSIOilKm7HkmV8GOIJJdJFgl\\/s6GFET7uKjhSJnFCCdCbUOheio2N6ULVd20VQISJ1TQ1VulNsUL80IffJwxY91wckWNnOKEoX7Q\\/MJ0\\/hIonRwGJE0j34rp2LHWK9yJGcj\\/FjfBMZJDl2L2QRX9yOJDe1fY2q6sQcxPjC5RmUJPKJrJiV\\/Q+12hJTMFqgjUsoSjRYTNnMsyqAkj7S97hLXI4kbF\\/cPDjtIjagMRXtxHIhY9ENrYXojTyd6UJ7UOqcIkWDETExWE1bgZG0XbG9C97Ij+WC+13GzpuI7SuMzlQvUyEh3QhSdMv1yOiiqBF6CjLDDfiWv8CghJI9hsYYYyEKQhpkmnsyJeaYQtFdEwPYJyN7m9xJCj2ETCfxH+gKYg3a4k\\/0kILZNkuhiSSfE9rDS6pPo1MokFGDFc0rn8VIsBklnB6cQhteVs1BaCbeBbiRx8STmw8k7uBPttqZJZ9+BWAfWhZ9gtOVpPbIsGworuB5u3CViuOjIz2bKss0CwdkNNtjuA6ZYKRc2YheMejFRzCq0MGn74Iek6WtZe8c7\\/kWb9DJuyTdCsY4rcsXaIS7Z7RBeBYgOghhoIOoGGxt+VStbyKCEjDdQZVgtlFMQ\\/Mv4DuLwDUuS5QpSE6bhQt\\/IITddAXhbli4o3n\\/gxH+5JPQx4J0pjr+pRpYvOyyRZLPY2eXe7Dk8jOzCF3BbJerLf1y91i5XPqEiybnoCfqZJaU39vL+0LR0theuYrcCGkFBFjI97GIJxfPBJMjmsYIm2uBG5baRGrQ0U1sGWcZR6eLEGZXToqobHpgUoS43F7vRZgl0KY2I9XJXsUJFi2YkK325kV3Ljb8sQin0l9ckcsSPH0CjPp2P4KSTQexjQbGx\\/GFEQWkjs2A1bNYrkLJ0wSCwjnuQXR6E2m9yhJvCcKiyN3IM\\/UgZt7PAmrTfb6Z\\/gMaU3Lz\\/AKRJiXV9m4jux5Nn2IgyYkW7sIlgRtygl6CeMhiLsyNqXUbEVkORks\\/ek8+2hWdwimzIIbqUQmyolcgmeFWLQnHmi1eNr7Q2ocy5I3ghPZEtBsd2O1Gkhh4Haeq3HoTXNdCJcyiWdF9FSnKGZ\\/YVUbaWlmWZ7+qGiQKLmjcDdjtE8xvlkYrWMEIlSWyDFwNlYXJcA+z9GRtHXKJLm3dsejBcSRs7NHeaYIpEWBCBhojwRXA1KiYtA1aQSSgwfiEq99BGzGWuSMa3yHy0EOGbCtrNE3FZi6J3wuORXUeh9HSC64x7ijmRVufEq\\/qk0jfv9CWtkdjBN8iyTIIuwamniMjTlQ1Iu1RrcR2Nl84VXRzSCNc2MuRWZ\\/jRZFphU+ogs2IP9kCpsuhEvgh2u0Kctf7EDZ4ICJWNzNTQ9cmP1JUdSqnoc94XIgka4zwIIuWdxIsyso5Mz9waWRxFflImwTfAe\\/NwLL8QjW9CS1vkBKhLNs7l25IdZJyE8hP7II0QQb0QNCDpFYILEN2SI3YR5UFoqqKjaBUYqLDYxPKSIaFPYktDXd0PGSe9B3\\/sZfvRZS2yDtpsbwvlEgmPoj+ZrnAyTfLG5X8xOMkBJgP5Jfa64Nq9rwqr3eqTTG5ekDIxRvEIuJLsYa2SJ4XGujvEo\\/SFpNb+izBiI8MkRAQkmpVvSsUx6v47MHYgZB5Dsu5uT2OBPgbqYzkSUJIW5DNCVfdxvXnwYFVrXQqodcpstiMPA5\\/yjcsLHKNmKz+RqZsbDTcMe5LdJJVLbgU7JM6twIrRPOyEJvfSf5nOjCOvTP7ozRb\\/AM4zOeqP1PMaEMY6OuXZTSj8ubWYYZsM3fUhUQTispzNbciDStBP7NjCrEeCXp2Gbvbgawc05SurUrgJWf7F28cCDdyA5wWSXfLgbScCKAE7t7IUucX4wQ2+9uR761V6YIJXyxON5tGNiGKJhjtFyQihKpm5JRJTeSOpW34CPd8RVjRYCIuS3fRme6rGjfEjhvhCJOzmtyi0Mx0U5L5X\\/UE5Yw2YxuSTVthchjVIm+RcjNnqn5yktDSwhUQyYUj3N4eB82xElIoSV+KI\\/wDyBXdPs2gTA6p6P8ImG2NZIlgQ9PA4QrZcimsSVw\\/dpRGj61JiWa9Fi9gWv2RA3A15OosM5xwI5SbMh48HGb0NnvRkEEEECQlW5ZGMII5V6Hv+44X6GxdmKu\\/yuGK\\/uHLwYIoQ\\/Tx\\/IOG8lo15IvPA05o4MErcjtHIgWLcZnNikbeckb4woggaJBIO3owynMzwdf8ApG5dWvsd1HJERMcN5zkPk19GtjBDMEj7J67hpWFwfjRdK13KONyPfemuLunZ\\/syL+BXbD\\/BC\\/wAAaKT28H+BRCljQyRdiFRUhO2S27yLmlWdkE0g9jqXeKDG2DBvLciwrNthvZksMSF7siddpOxrmSwuxm2LYGIi+xIihnXOmT9CVzxwgm5zdi2ojHFWQ3bjIZA1osJECRAkJUYlW2SROZjgy6L3NtkUQJCWgRuPVL+xrjdZkWCSwznZZm6dTFJfaL4ufLIK3BJ6Ul5b6FaS2kd3CyN8DbvBHBcpYNje6vVAvseXytDUjCholXV0rOxfabv9Wkdr1rbSRfhXdE9reCXiS0NR96E8lIldbP3fox\\/rhf8AuMEr93FaSEuiNK+AQ3Lboqtk4QQZpRN13wJs6DIH0JJNFhDlQv5jqyHhciMhLqRAnH5THcPbgwIl+WRse8W\\/0SS\\/hQ7kzfVx4UDWNhc\\/USZCYloze8DGwXsdWkRBFGshLDoyPLnsQkQIOpxf2P5d6p9KIFQggXQgip+iSwX0TV3\\/AEKc9ke4faLjYYBOhZdv\\/kP38MfJmS+qGTPJgY\\/7uxJHRS\\/bJq9Ui5juvgNpG+SPK8rGkl3TgOEMeBek6WiWSXttMjDI2VdC6W6\\/wW63P2kZEx8J7kkbbLguYKeR4IgG91Eq9hqH7J8ubOSbtLyG1XWyky8rTGPcLgQ8cBbDGXK6obbRf1JTo1VIgbuJxwjhob9jsZmlyWP2JEEEEEEVEQQQQQbBisl+yJDGxh\\/uZx+pFbIFTOBIuvTIqHdFlDkpvYRA0ShSTh7E5IElvlkkkkk0bJqpMuKUrGLGGT4WjLQyUMKRvFh5joqJ+CxaKyl7G6skWMxK7Eu4TtxO2idtO5EOSHJ3Ii2j2Q3SRA56zgnRNSYiHCaW4ZdzHdbkw\\/DhDFkxvyTmqywQWHs6H8ve\\/pF3l9h0eGW7jWnyryaStTu\\/sZZBKr4oDBRhuJzRKQDN6CQj5NDSKZIU8Dz7J89uKIgjQqLQqQNDQpTyxMZJGlBvD\\/snsFyjN\\/I3JpcOBpbV2Ux6kAMYzqSSSIb0oLAbRu0SN0LhR0zo0z1jaXvBPuJj7kQhCYmTRIhKew7EkkkkkjckkjZIxsbGSSyfNJ3juHaOwkSSJiFSRiSSR5JHxdpjEsQ3cO4jWn\\/uhJtmFtoaGdE37j4spiZsOyMoc8CRMK9yGg026E5qbjOgSEJEx0oWZjyxCrtoTohMWKLS1RYCsk2EhxzF8GGyYYm5NIxeJa+SuXR5JJEJJJEySfMmJlhmLL2MTJJEySanvpGdAkkkkbGxsb8qZOk6SImENCQ5gXNHARIklCW2madHfzeH68Wa3UDpPkqEuW3HAmIqCO5CcodBKgi7M\\/1iVsqqohakyREkn2ToaGdkwJjSeUNIiNqSSSJkkkkkk0nypiYn3uJtvA7jYadxax7hv+yI1GXfdeK+SSOj8a0pG9EjapsGlt29NiDevGvkdHonUm5CM1BBfN50INZITkVAsqUbIvZ28UVkkQ+iSRMmrGhksCSdaYmTes1kknxJklqS6Yi7eDyh7eJUT4p8Lxpn5Kzqq\\/VUOIxGrBxJ5WzENWqjktQ\\/2hI00lxLeHmm2mRMkTomTpMirOh0RJImTqnySSSMT8AAISSSST5H4HqsansbqxP46bZNO5gZ6ZEV5\\/iNJZVcumRyFy\\/MNtpfhmkk0kmkkkkiZMCZP5JoyAcOqehiqn4V4ppNZJJJJJJJJpJJNJJ8b8C0XEBGhZYsLUnrd3S\\/jenI9DpwM072EpJtLmQqfWvLJNE0TRImJidExB1NBPImkEk6lMcB1nzSTWfCtSF8ZXEFtogvSQQShqhoNe9GTpb4+aKzpKIT5fKMhMggggggjzpiExUvVjR7CRRojQnRqr3I0Ui+BI\\/FtpXxkQUfQrssDVCEMY3dGjvWhE9L+Ou3giE+X2+BOpCExZERTFGW0RI1A1SKJ6E4Y8oakjpIhaF5H4VqXxUJVr6LWOI30\\/WDuZyLXaj8O3hutC6674yEIQiSdCGCZCBoargmuTYokodConVfFWpfFSFR4XhmmBaNbU+zjw8a7\\/8AwlWRMmjGRIWRsklDVcVY0MaUMaLmNQJ\\/AflXxkIVGl+CSSRquWKzjw2p9U6+Mx\\/CQqIQtDE4cixCDuOzFS1odNujGO9ExaZ823y1RCEIaLapo2MIW4lYdJwzZvJ+Ga8anW9e\\/BJj4caEhIgQqsaomwIssW9xbgmjka0pxFIao6MXlfgkn4k6IokISMIaX4CKRe7i7yJfZ4F5bVeh2J8cEfBQhIRAlRjGNjG3ZstigSOikY1Vqk6jQxjMCfhmr+ZOhKiEIRsamRi2WxF5cY3EeL60fWhV+vHN63IXyVVUY2MkkY0MMkapEbqMpFXSdQxKOsi0T450zSSSfhJE0QhDQM6WLSEzuMmyxLbc67k6eB13XmifjRqiiEIVONBjoaoneUxicOJFehLpkJoTUg0RR0TJ8zIujLdLf+AP\\/AH\\/AIYa8\\/hjV\\/lJhNAGn\\/UjNvU2BaJ8ciEIQiZpbIh+DNfU38GdP3ozp3PqnXheiKRWCCCCCCKQRRKiQiBCQkJEEDoe9XIvGyRnd2EKucGrjcCED0T5uj1rgg5UVst\\/RZENNPDHDdLAkoTnga3IMXMXKa4xeN6UISIEiBRpkjRfYY21tcr4r8v2RSCC1IIIIIII8C1p\\/ijJGPR99E3HLYdhKrkmiyRDpyDHR0TrPilq4xh7+xhvX+xBFN1JPRLJjuMhbnoRq\\/8Av2Rh53bt38BCEhIcJI0p0Ni0i7xOwel15030MnRAtPFfrRNXRPGiTms1dZJrJNOSSSROonRIw2NjY78HikghM3JKDpkcWAg1pmk+TpY6z\\/4XP9Lp33yguR\\/CkH\\/W\\/wDYvCD0tuXn4CQhBISJXGhsjLnDSy2pnOh6ZrzoWjmtyR121TSSSaJPYTqSTrAnwAPY9qVTYbGGHG0ORxqGEpiVRNgQYho3A1NBqjVU9c\\/NQhIQSI+iaF+hmFtj70TV0xonRNfvVOvnRz4vHz8AE9Y\\/vo+VZsahmLpeR6GomSEKrk4naknDQruHdUGhoarNZJJ8jF8FBISEOBeNDY13Hdt28DF4ERSdEk0+qcn0W07URavFOaSSSSSSTpCfEAIVEPbTU91GxhmNMFsMONUYhohImEQKw3qE4WHNEQ0YqvnTVKghUktobSJyZdrxZ0X8MGCR6szenA9e\\/wA9L\\/3w9tEUyEDYxOhyoaNGQxqJTEyKSIGOKOPET4dvgpCCFQ8ImrcDU2x1R6W2l0Xjfj5L0mvJfmnBJwMjDo9tE9vAA9qffyAF99S8okbG016i2EMbolGkV8jRMxHA6DQxrRdE+Odc6lcaiqEhlFBIVJVYSxtjC23ii1OTny517D18130b+PN5a+Jq7jdUkSSyXQmxxMlSRuoy+hNQhDXyYSQTmgsDU1YYaiqMMjxWfPiuaiV7maJN0UEhKkEaijcCVbY6TDfwPxW8LXem\\/n3NiSWTeimr1JaGQQPT1qSJkkkjodEONQ1CEPcli5uCcjYCcNxk0j8hyGGyqmlyLoyQQRSfDBFFkPgLex7FRJuqJUkipUkycJFq23RDbZt7+N+PYxov5F4EOK2nW\\/Hv40Jkq9JGxuh1cYejYIQh7nfRSlncaWBmw+Kk7NUggYboTYnLkbIT5JrBFblz70tUXCS5jlgTMRQSorC5BcguQfJDFlkiElnahts21\\/hzpzp30Wvpm5zozrdFofgQtf140STRsbd9DDjUmqKjWEuDvLsyIMaIpJNWrGUyyNXgh0LafrTBDY5cEpISIYQhKiohEomiFyDHNiZmy4dcY5kt+bnRenGi9Nzmm+q5OubkkDNxU+jsehVepSI+6Orp1Tc28E0dHoQ442BhhMVXvQXZrUR1nQhonka7DZbEtV4ECNMi7E5CRdiFqIVUWNMlnimEssSwKG2zMy422nJ\\/I212toeCNLwQKnRyRrvqYydCF4MRp38LPvUfA9LUJiIozax3mIUogySSVWayixCJguGvJLknSme5CEUSxOiaSiSGgIII3Qem2OBs7\\/8Ayvvw30qvFW9KFq2pfxc6opE0Q4w41C0sMemTlexKWUJS\\/kiCXzUJ5FyCNCpBBBAkyQmLBNhj82HLbY717fJnwWgZ9U9qkU+x021LQxjtSRU+6Xo6sxqcaM6b80ZfScYeqhPSzcPtwF6aYw0RrkkknzoQkJCQ4SuSKTFz3FyWceHge\\/i+9c+OdEytK8Dp90W9eCxZp+heK\\/mdNtCGGHPYYYWiKJsbZEubjQ0R8dCQkJCCqWxam8IY5FtkU+j7I+PbzO\\/h+q709j0KmJPvWhCM+GSRUknVg2OfAcYcahPwMU2ucoHAMNaWSqySSSSfZJKJRKohKhISEKHe4n\\/A9d4C+Fv4ePFvpnV96OqcU+yU9fY\\/Q9KFXnQ6IYte1bj3q6wYIFQ4wwysMIWuTcpQuzccDQ6yQQIRFbaIXBB7EFsIwkJCVEwWiTTDZygbbS1VTjStSNmbeZY+D969\\/B3R6YptRanbTtotV14t4P4omSgToYQn4y\\/GRE7ch5kKhHtS22iTJsbcy7uiF61W0xoZer8cHNI8XNLV3p9UivFGf7Veh1Yx6MiFksej68PHwVsIcYYYTExMms1VIARKGrA9kksoQJ+5LudhAlFq2JR3EG5AXYRs4\\/lilQu2H9prtW+m9L6GfXjvS+l6npvXK8OXiuCLG\\/hQtEU+6W88aUIQwqEMITEJ0Wm8MMofQ6Bq9jhIsMQHtodB001PegpuXEbsV2FthOhCETqMdrj4fOn7INvFfVamxeuZGWI70Wrev3SDBPgQjbU1q\\/zzbGaIRloCoQhUVIIEsEAYx0gaXBDgjwdB0HQIC4iHB6UQIQhCFS2JqD7pHw7UdII8C82+h1e+h1TN6ZpwPR0ciotdjc2ONO1OKOdDIIrxXIQQSEhIQVBBIihCwFhaPxIWpC0Aq76Ob0Y+DyY89yNH14PrS67aHmiptV0jQs0Wl6dqbqlq9HFYIpBFYoloAwUUEIEUEEVQtDBVYyaSWJRDklciQTySuSHJKJXImuRNcijkYUQqQ\\/oiFcmddjbyx4OaxocU2rmR40\\/4Tamzq6ssc09ozT6IM6Vp5rfik1+tWNtcaYJNHiokEFRRRXUWvpcg19YrDPmewjyI8s9n5PcLu\\/J7s9h7j2C7SXMY3kKCezCqk6A4zrjW\\/Dai1\\/VPunOrcwvJanNi3l+hRY2FV6XOqPDFII6Iqr7W1IgggiiitALfRh9iwT1oS0oSFihxRirkImEcrwqiTayNuGxM2gyrjRnUto+tEeJm+l1jTcRsfWjIqXOD6Hp5ExUt4dj68duKQRwPrPRDWw8EAXsHbSyoN4E3An4E\\/AnJkuCfBMSHWR7PZ+p4VpRYMNSKrwbDQMnx6rNfENWw0xikKVKLewxsbUPh2nYqmsj5\\/wBFtOdG+jNiLa0Om1PrRbTFFS2l+CKRNERowLG1DW4SK5U2ETCMaNxrhrRYaJg6xvB0nWJOBLrqgQh1\\/Y8gmiEKhMYVocXR0UnUdJ1nSdZ0j4R8KFNgSWQjDsCVMggasbtqI26kF04YneM4cqWrYtr5rOjYtvTc+zGi3F6caWLIkfdOD7H6pmtxYotcaMiFWDYZJMZB4qKTuIobULCKIkELdDy39QMQOW6SpJEIhxplDqJGxh1\\/a8MWpVIcUxVgjynahFSKEJVggVBq9iRqw4sC92C7cc4Hbl9Mae6Pqm9OTbQ68U96GMnFd6cjscDgf4JqtxUWvbVsIhmVR1IQd4tPHItiCISJRL4IcT8jrEIZDIQmJjZNDqJJJGG50fueOToQmJiYvKJ6Jq9M6cxZcd6EEanlDRj1akpMvu1OcjWGqrzMdHS1z7GcRV6I5MOrz4EqRW1eD3TmtskHyCmUY4z3R4MaFSRIuxpspF1xjyIQQlbERsYWkBcloTJEySSSRus1RmNH8450CDdHpRT5HqQ4pRIEOD1JciSXQSfcThPLFSJURIMW6NM0nSx7kyG1EEaI0NEBYJkRMiBtVeLlC3v+J58fN6xR6FkkljJp96ELyQP0ES1IkqNK6IK92JoSIl0lKlPAzglA5SGcIB5TQ19j4Bs6Mow6TSKwTJEyaJJHpdy18kBPXuKQRRGpCEKkVEyc9D3PvV1nU1i+i5aI0osWk8EVMEDVi3BJHeUduZjL7V+9V6c030S1XnSiNXrRkQhaMaL8DBQcTJUhj0KJMlKR7AMdnqxakgvA9gbRtqkmrJlpa1Z8T0ibClE0zDjnRHhQhCp0ItLDO1SvxYivRKkEalS5KXFogguNpGoMmsYzyLwcUZk+6Z0fY9X3p+67aI3M0W5tpWEg+BE6CiyFoQQWtGupJJOp0Lsj+OSRYjtmMSi4mZaBaYqtKFocjiOSSI9jESkifGwlxRIgjUhUmSiW0xWDHexJLptpe48CpnRbTzW1VRUY6Om9Ez0LTyQT0ZCsJaYIIEto22uCKtE6FZ+J6OZlQshJqMSTrjSqTUUI8XHEmCIK5JPkqLYVYZek0VXyqL4WSw5cTeP3pwbU5o6Qe6c1to9U+i00+6M+z60IWuCxablghKNKrkWFpeo0QQQJ3Q40NVWUQBeWJEzTekq8g2AghPjkkYvwQ7HGyORYwfymxjCJVCaLENQalJYjwqcjv8kFCOJ512o9H0WsM+6WsOj\\/AKG8G69G4thuKRR4kVxYdMUQv6Jrh4okoknhxGKpAlakSDRFw0kO4mg8C317eBm6UyprDghkSBCIII1JZEoihLYeMDgvXMR1DXnxKh4CC0TSSWTosUa1J5fhZuMcierk3OSBjsjI6O0iNjJFx2cEYIuvqmZFdobJq2f\\/xAAvEQACAgAFAwIGAwADAQEAAAAAAQIRAxASITEgMEFAUQQTIjJQYUJxgRRSkSNg\\/9oACAECAQE\\/Ae2n1Yq2Jof4jfqXT4zYuy80+qXBiIfr76HiPwap+7IT2p\\/+jVEpeEbilJPk2atDzXW++izH+JvaH\\/p83Ef8mYPxMotKbtZY0SS7b9Es1lP7WJZqdf0UrdFZRbjwOcK5\\/wANUPc5W3psT4nQ6jT\\/AGf8vE9kYnxE8RVwsqyw9oR\\/oxVsSQ12X6d0v0UpL74lb0NFDIFFZ0QbiftcegclBW2f8yV7RQvi5eYo+bh6dWqjFx3PZbLKiihIhHVOKJYuHClKVH\\/IwZbayaGs1kup5X0X3m6GcMUr5yaJJNEdhbqxoaKKyUnEi1P9PvTnHDVsxcV4jKEhrJCWWko+YsO9P3e\\/sSbk7bKMF39L\\/wAJIazQulm4+xbNX6LXTXRLwUVlCXgW5Lgoi6eTQihrKJzvku1j42jZcjk5csoSKHyKIoj2Erym3fOVCRgRtuXsTHmhZtpDmORZfcsT6KyatCNI4WU0xSv+yihojIdsWwiiURbERqhD7GJLRBsbvJRFEewll4LchbEpUVYoUacsPD0YX7ZMeSyQ2kaxyL9BYpNF30vZ2hZPeh7EJ++UkUv9IuyhZMosU9qZzw76H0\\/EY2t0uFkoiWXJRwciROdbIW4tstjCwd9UiXBND6HL2HL0yl0u0xO0bsaKaIN8ZNUfsi20MvJoaHkpvzua4\\/sjT4ZWUpxjyyfxdfaieNOfMihIrp5IoxHSEiKyuiGH8178CGTGs3Icr9RGXv01QsnGynFkZXlRHZ5MTyaGmmIltnHEfnc2lwOcny80RVZMWajQ3Q7luRj75ww9b\\/RFKOyzmMZOfherjLp4E08pDTRGafOUkKWayllJ2Vki64zqxUii8uDkolJRN5M4zjht88CVKsrJTHIsxJ+F62MvHTxIvJocWiE6dPgY1uJ1mnlLNq1ZWaEhdEmJGJLSv2JOW5FFZVq4Fskix4hLEHM1DlXr4SvsSjqHBoVs00UJjZHNxPIijhiEislvk9ytjgknJkYvOtWwvpHMcxzHM1CZz30pPhG6dNegjK+m2hO8mrOC7KGRRWb2KyXGSiJZtUWVkx5xw3L+hpRWw2OQ5FliV+hg1pMTevQxlfS9tzULKsnESrJm5pfkaHmtsuSst7ylnwYSU5pDJkhjySv1VdrgUr6HuiOz37DQlWbQ8qHFryR27FXsYOD8v+xk0TQxlHHessssssssssvu8Cd9MXWz7jWSGVnfTg4end85smiRRsuPw\\/BF30NEX4ed9N5vKyyyy+nBwv5v\\/OhmIiaL\\/FRl79DRF7F5XnZZZedl9NlmDH5k68Lqx5qP9jk5PtrCb52NGGvdlYf\\/AEPl4b90SwZLjfuJXwRwV\\/JlQX8Uav0j6HzBDwk\\/tf8Ag01s+w84v36F59HZ8JL65L9dN0YktUm+0lZFKH9l2U\\/Y3XgsTJQU\\/wCyq7KTk6QkoLYvL5GM\\/wCDH8PjL+DN0OpKmSWl12lne\\/Q\\/QqTi01yjAxfmwvz0S+2X9D57S+lftkIynKkQhgYfO7P+VhIXxGDPkxfhcOavD2Y04umt8sWNrV2cONK\\/Lyw8OWLLShLCwI\\/sn8ZH2F8bfgeJhY33R39zFw3hv9eGfcuhDyfU2u1Xc+GxY4bafnofDHy+zFWz7mWsGGmP3PlmJJ03ZGUm6PB8PjTj5MSKxoak\\/qWSJKm11xVtLPUvhsL98seLiYs3fAzDWxpsUta0SGtLolznfUuUWi7K7CRRWddmqMP4qUNnuiPxOFLzX95MxFU2iKtk0l1x2TMLnV7DGhRS8FeBKjBnpktycdM5LLGW6fXhct5YSvEiYrcpMSKOBZY3KfuPjoRJbWWPpsjxlRRRRRXTRRRXTRfOeHj4kPNoj8VGX3KjGac3WT36fOXhGH9ss26I7nKFsY33\\/wCLLF4XXh8PLB+\\/\\/GMboi9SsjvG8kzE+2J4fS3eVdMUhVlRRRXYoorNIooa6V2fYg6THJusmk9mLZVlFamqMR3N5Yv2r++vD4eUXUkxqm0NCGJWUS4R4ebzvLhZJWJJZRVdt9FFFCWTT8ZUhxzfZXCI7FdDRH6d\\/wDzPF8deH5z5V+VznQsm7PDzfSkaemyy+pySNTfBpb560m+ELDmyENJPAjLfhk8OWHyrRZsxxK7K4LIyvnoS8y4HK88R79cHvmnRtLjn2HtnKWXh9LEKLEx9hWN02iz6hQb8nykVlrRecYaxYcVlWdGJ8O7uA00\\/wB5aTTm+lZps1fo1v2SOc3JdlS6NUvccpe5v1J7jNNlVlqp9lFkIwa\\/ZPDfgTouxrKUY+xSNL8MqdqmRjVv37E8OM+UP4ZadnuSwZx8HI4mkfXZaLRqRqNTHfc3LZqL\\/XU8k7yckOTfc4FiS9xyUv0xOhOxobyhBzexDDUO2yVxlIUzV+u\\/RXRT6K7b6lyu9YmJ3lh4Tlu+BJJUu7jqsSXoKKyorpoortPN5w+4fe4EzDwfMu\\/8UvqT\\/EPOHoOPQfFL6U+tfgaKKI+fwGMrgxr\\/APFS4J4fsaRrpXZXqK\\/BMmMaHH8Gl+FmPLklHvbGxsbG3sbd5L8NiEuhrv2htH+Fd1L09lllllllllllllljZJj6WvWpeosssssssssss1GosskyT62s16mq9RrNZrNZrNZrNZrNZrNRqNTNRqH2Gq9LeayS\\/EvscjRhYcZRtn\\/wJfK\\/i98r7d5XlFxXKPmYf\\/U\\/+ckxIS\\/Kbxuuuyy87LL60mJV+Lfba7Nl9lIUa\\/G11PpfasvqjASr8K+++q+1fQotkcOvzb6a6qKNJRH88sqKKNJpNJpKXQnv+RfU+iy8kLurhflWxvO+iPHab6IcdTaXk+ZE1R9\\/wK7LmkPENTPq9ijcpmk0mg0I47T6NTNcjXI+ZI+bI+bItscW8tUl5FiyQsf3RB6\\/tTf4K0iFTvekvJL5Wl1bfuO2f6WkfMrwLEvrfefWhuo9OBiOFpHzvmKpc+GfOa5QsWL9a5pDxX4KkzdRoctEf7HOTzoXc39z6vc+r3Nzf36n1Il0x2ki6ZNfU8tUo8Mjjf8AY1J8P1EpKJLEbLLo1N+R+F+jE\\/j3UUeO2tlfYfUyfj+uhXHdEZal6ZukSlb6ZfcT56l2OSuzRwOV97wiXEelScCM1LursY3THlD5ZP7n332p\\/d2H1rgl9q\\/vqw\\/v6H1Y7dcn\\/8QANhEAAQMBBwMDAgUEAQUAAAAAAQACEQMEEBIgITAxQEFREyJQMmEFQnGBkRRSobFy', '0', '0', 'added Info: 6', '::1()', '2021-12-18 00:56:06');
INSERT INTO `log` (`id`, `user_id`, `action`, `module_id`, `target`, `target_id`, `mini_target_id`, `data`, `new_data`, `items`, `new_items`, `comments`, `ip`, `log_time`) VALUES
(49, 1, 'Create', 15, 'Files', 'I20211', 0, '{\"table\":\"site_files\",\"file_name\":\"40964.jpg\"}', '0', '0', '0', 'added file:40964.jpg', '::1()', '2021-12-18 01:24:19'),
(50, 1, 'Create', 15, 'Files', 'I20211', 0, '{\"table\":\"site_files\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f3.jpg\"}', '0', '0', '0', 'added file:af8d63a477078732b79ff9d9fc60873f3.jpg', '::1()', '2021-12-18 01:24:24'),
(51, 1, 'Update', 15, 'Files', '7', 0, '[{\"id\":\"11\",\"module_id\":\"15\",\"form_id\":\"I20211\",\"user_id\":\"1\",\"file_name\":\"40964.jpg\",\"caption\":\"\",\"timestamp\":\"2021-12-18 03:24:19\"},{\"id\":\"12\",\"module_id\":\"15\",\"form_id\":\"I20211\",\"user_id\":\"1\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f3.jpg\",\"caption\":\"\",\"timestamp\":\"2021-12-18 03:24:23\"}]', '{\"table\":\"files\"}', '0', '0', 'updated files of form No#. : 7', '::1()', '2021-12-18 01:24:25'),
(52, 1, 'Create', 15, 'Rules', '7', 0, '0', '{\"title\":\"test\",\"content\":\"sadads\\r\\nsadads\\r\\nsadads\\r\\nsadads\",\"file_name\":\"40965.jpg\",\"video_link\":\"sadasd\"}', '0', '0', 'added Info: 7', '::1()', '2021-12-18 01:24:26'),
(53, 1, 'Create', 27, 'Files', 'I20211', 0, '{\"table\":\"site_files\",\"file_name\":\"40966.jpg\"}', '0', '0', '0', 'added file:40966.jpg', '::1()', '2021-12-18 01:28:39'),
(54, 1, 'Create', 27, 'Files', 'I20211', 0, '{\"table\":\"site_files\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f4.jpg\"}', '0', '0', '0', 'added file:af8d63a477078732b79ff9d9fc60873f4.jpg', '::1()', '2021-12-18 01:28:39'),
(55, 1, 'Update', 15, 'Files', '8', 0, '[{\"id\":\"13\",\"module_id\":\"27\",\"form_id\":\"I20211\",\"user_id\":\"1\",\"file_name\":\"40966.jpg\",\"caption\":\"\",\"timestamp\":\"2021-12-18 03:28:39\"},{\"id\":\"14\",\"module_id\":\"27\",\"form_id\":\"I20211\",\"user_id\":\"1\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f4.jpg\",\"caption\":\"\",\"timestamp\":\"2021-12-18 03:28:39\"}]', '{\"table\":\"files\"}', '0', '0', 'updated files of form No#. : 8', '::1()', '2021-12-18 01:28:41'),
(56, 1, 'Create', 27, 'rules', '8', 0, '0', '{\"title\":\"comp rules\",\"icon\":\"fas fa-pencil-alt\",\"content\":\"sadasd\\r\\nasdas\\r\\ndas\\r\\ndasd\",\"file_name\":\"af8d63a477078732b79ff9d9fc60873f5.jpg\",\"video_link\":\"sadasdasd\"}', '0', '0', 'added Info: 8', '::1()', '2021-12-18 01:28:41'),
(57, 1, 'Active Change', 27, 'rules', '8', 0, '0', '0', '0', '0', 'Changed rule active:8', '::1()', '2021-12-18 01:29:30'),
(58, 1, 'Active Change', 27, 'rules', '8', 0, '0', '0', '0', '0', 'Changed rule active:8', '::1()', '2021-12-18 01:29:40'),
(59, 1, 'Update', 15, 'Files', '9', 0, '[]', '{\"table\":\"files\"}', '0', '0', 'updated files of form No#. : 9', '::1()', '2021-12-18 01:32:28'),
(60, 1, 'Create', 27, 'rules', '9', 0, '0', '{\"title\":\"test test\",\"icon\":\"fas fa-pencil-alt\",\"content\":\"sadadsa\\r\\nsdadsasd asdasdasd\",\"file_name\":\"40967.jpg\",\"video_link\":\"Sdasdasd\"}', '0', '0', 'added Info: 9', '::1()', '2021-12-18 01:32:28'),
(61, 1, 'Updated', 2, 'Competitions', '7', 0, '0', '0', '0', '0', 'Changed competition:7', '::1()', '2021-12-18 01:33:48'),
(62, 1, 'Create', 2, 'Competitions', '8', 0, '0', '{\"code\":\"COM20218\",\"type\":\"yearly\",\"title\":\"season competition\",\"description\":\"season competition season competition\",\"image\":\"banner-competitions.png\",\"start_date\":\"2021-12-14\",\"end_date\":\"2021-12-06\",\"price\":\"200\",\"currency\":\"EGP\",\"active\":1}', '0', '0', 'added Info: 8', '::1()', '2021-12-18 01:34:37'),
(63, 1, 'Delete', 27, 'Files', '1', 1, '{\"id\":\"1\",\"module_id\":\"27\",\"form_id\":\"1\",\"user_id\":\"0\",\"file_name\":\"Screenshot 2021-12-09 222942.png\",\"caption\":\"Sign Up\",\"timestamp\":\"2021-12-09 22:30:28\"}', '{\"table\":\"site_files\"}', '0', '0', 'Deleted file No#.1from  site_files', '::1()', '2021-12-19 19:49:42'),
(64, 1, 'Delete', 27, 'Files', '1', 2, '{\"id\":\"2\",\"module_id\":\"27\",\"form_id\":\"1\",\"user_id\":\"0\",\"file_name\":\"Screenshot 2021-12-09 224827.png\",\"caption\":\"Sign Up\",\"timestamp\":\"2021-12-09 22:30:28\"}', '{\"table\":\"site_files\"}', '0', '0', 'Deleted file No#.2from  site_files', '::1()', '2021-12-19 19:49:52'),
(65, 1, 'Create', 27, 'Files', '9', 0, '{\"table\":\"site_files\",\"file_name\":\"4096.jpg\"}', '0', '0', '0', 'added file:4096.jpg', '::1()', '2021-12-19 20:20:39'),
(66, 1, 'Create', 27, 'Files', '9', 0, '{\"table\":\"site_files\",\"file_name\":\"childrens-robotics-classes.webp\"}', '0', '0', '0', 'added file:childrens-robotics-classes.webp', '::1()', '2021-12-19 20:20:48'),
(67, 1, 'Create', 27, 'Files', '9', 0, '{\"table\":\"site_files\",\"file_name\":\"banner-competitions.png\"}', '0', '0', '0', 'added file:banner-competitions.png', '::1()', '2021-12-19 20:20:48'),
(68, 1, 'Updated', 27, 'rules', '9', 0, '0', '0', '0', '0', 'Changed rule:9', '::1()', '2021-12-19 20:20:54'),
(69, 1, 'Updated', 27, 'rules', '9', 0, '0', '0', '0', '0', 'Changed rule:9', '::1()', '2021-12-19 20:22:44'),
(70, 1, 'Updated', 27, 'rules', '9', 0, '0', '0', '0', '0', 'Changed rule:9', '::1()', '2021-12-19 20:25:01'),
(71, 1, 'Updated', 27, 'rules', '9', 0, '0', '0', '0', '0', 'Changed rule:9', '::1()', '2021-12-19 20:25:57'),
(72, 1, 'Updated', 27, 'rules', '9', 0, '0', '0', '0', '0', 'Changed rule:9', '::1()', '2021-12-19 20:34:53'),
(73, 1, 'Updated', 27, 'rules', '2', 0, '0', '0', '0', '0', 'Changed rule:2', '::1()', '2021-12-19 20:35:24'),
(74, 1, 'Updated', 27, 'rules', '3', 0, '0', '0', '0', '0', 'Changed rule:3', '::1()', '2021-12-19 20:35:37'),
(75, 1, 'Updated', 27, 'rules', '4', 0, '0', '0', '0', '0', 'Changed rule:4', '::1()', '2021-12-19 20:36:00'),
(76, 1, 'Updated', 27, 'rules', '1', 0, '0', '0', '0', '0', 'Changed rule:1', '::1()', '2021-12-19 20:42:48'),
(77, 1, 'Updated', 27, 'rules', '2', 0, '0', '0', '0', '0', 'Changed rule:2', '::1()', '2021-12-19 20:43:07'),
(78, 1, 'Active Change', 27, 'rules', '3', 0, '0', '0', '0', '0', 'Changed rule active:3', '::1()', '2021-12-19 21:40:33'),
(79, 1, 'Active Change', 18, 'competitors', '3', 0, '0', '0', '0', '0', 'Changed competitor active:3', '::1()', '2021-12-19 21:45:51'),
(80, 1, 'Active Change', 18, 'competitors', '3', 0, '0', '0', '0', '0', 'Changed competitor active:3', '::1()', '2021-12-19 21:49:55'),
(81, 1, 'Active Change', 18, 'competitors', '1', 0, '0', '0', '0', '0', 'Changed competitor active:1', '::1()', '2021-12-19 21:55:59'),
(82, 1, 'Active Change', 18, 'competitors', '1', 0, '0', '0', '0', '0', 'Changed competitor active:1', '::1()', '2021-12-19 21:56:18'),
(83, 1, 'Active Change', 27, 'rules', '3', 0, '0', '0', '0', '0', 'Changed rule active:3', '::1()', '2021-12-21 19:54:55'),
(84, 1, 'Active Change', 24, 'Winners', '3', 0, '0', '0', '0', '0', 'Changed winner active:3', '::1()', '2021-12-21 21:16:58'),
(85, 1, 'Active Change', 24, 'Winners', '3', 0, '0', '0', '0', '0', 'Changed winner active:3', '::1()', '2021-12-21 21:17:23'),
(86, 1, 'Create', 24, 'Winners', '4', 0, '0', '{\"rank\":\"1\",\"competitor_id\":\"4\"}', '0', '0', 'added Info: 4', '::1()', '2021-12-21 22:02:53'),
(87, 1, 'Updated', 24, 'Winners', '4', 0, '0', '0', '0', '0', 'Changed winner:4', '::1()', '2021-12-21 22:04:18'),
(88, 1, 'Updated', 24, 'Winners', '4', 0, '0', '0', '0', '0', 'Changed winner:4', '::1()', '2021-12-21 22:05:13'),
(89, 1, 'Updated', 14, 'About Us', '54', 0, '0', '0', '0', '0', 'Changed winner:54', '::1()', '2021-12-27 15:06:55'),
(90, 1, 'Updated', 14, 'About Us', '54', 0, '0', '0', '0', '0', 'Changed winner:54', '::1()', '2021-12-27 15:07:07'),
(91, 1, 'Updated', 14, 'About Us', '54', 0, '0', '0', '0', '0', 'Changed winner:54', '::1()', '2021-12-27 15:07:14'),
(92, 1, 'Updated', 14, 'About Us', '49', 0, '0', '0', '0', '0', 'Changed winner:49', '::1()', '2021-12-27 15:07:26'),
(93, 1, 'Updated', 14, 'About Us', '49', 0, '0', '0', '0', '0', 'Changed winner:49', '::1()', '2021-12-27 15:07:36'),
(94, 1, 'Updated', 14, 'About Us', '54', 0, '0', '0', '0', '0', 'Changed winner:54', '::1()', '2021-12-27 15:08:40'),
(95, 1, 'update', 1, 'Sliders', '1', 0, '{\"id\":\"1\",\"thumb\":\"s1-thumb.jpg\",\"img\":\"explore-poster.jpg\",\"video\":\"explore.mp4\",\"first_title\":\"Dahabeya\",\"second_title\":\"Welcome To Queen-Farida\",\"rank\":\"1\",\"deleted\":\"0\"}', '{\"id\":\"1\",\"first_title\":\"Dahabeya\",\"second_title\":\"Welcome To Queen-Farida\",\"video\":\"4096.jpg\"}', '0', '0', 'updated slider: 1', '::1()', '2021-12-27 18:50:03'),
(96, 1, 'update', 1, 'Sliders', '1', 0, '{\"id\":\"1\",\"thumb\":\"Main Slider Background image\",\"img\":\"explore-poster.jpg\",\"video\":\"4096.jpg\",\"first_title\":\"Dahabeya\",\"second_title\":\"Welcome To Queen-Farida\",\"rank\":\"1\",\"deleted\":\"0\"}', '{\"id\":\"1\",\"first_title\":\"Scratch Game-Script Olympiad\",\"second_title\":\" Contestants will create NEW and original projects (story, animation, game, mixed, etc...) with the main subject: ... ! All projects must be written in Scratch 3.0 language (in your Scratch account on https:\\/\\/scratch.mit.edu) ! Having a nice intro, menus, storyline, animation, music and games inside will be a plus! This is an INDIVIDUAL competition, no TEAMS allowed!\"}', '0', '0', 'updated slider: 1', '::1()', '2021-12-27 19:56:09'),
(97, 1, 'update', 1, 'Sliders', '1', 0, '{\"id\":\"1\",\"thumb\":\"Main Slider Background image\",\"img\":\"explore-poster.jpg\",\"video\":\"4096.jpg\",\"first_title\":\"Scratch Game-Script Olympiad\",\"second_title\":\" Contestants will create NEW and original projects (story, animation, game, mixed, etc...) with the main subject: ... ! All projects must be written in Scratch 3.0 language (in your Scratch account on https:\\/\\/scratch.mit.edu) ! Having a nice intro, menus, storyline, animation, music and games inside will be a plus! This is an INDIVIDUAL competition, no TEAMS allowed!\",\"rank\":\"1\",\"deleted\":\"0\"}', '{\"id\":\"1\",\"first_title\":\"Scratch Game-Script Olympiad\",\"second_title\":\" Contestants will create NEW and original projects (story, animation, game, mixed, etc...) with the main subject: ... ! All projects must be written in Scratch 3.0 language (in your Scratch account on https:\\/\\/scratch.mit.edu) ! Having a nice intro, menus, storyline, animation, music and games inside will be a plus! This is an INDIVIDUAL competition, no TEAMS allowed!\",\"img\":\"0_Tm5YhkSDJAbXX6FL1.png\"}', '0', '0', 'updated slider: 1', '::1()', '2021-12-27 19:57:49'),
(98, 1, 'update', 1, 'Sliders', '2', 0, '{\"id\":\"2\",\"thumb\":\"Magnified Image\",\"img\":\"04-b_14.jpg\",\"video\":null,\"first_title\":\"Dahabeya\",\"second_title\":\"Welcome To Queen-Farida\",\"rank\":\"2\",\"deleted\":\"0\"}', '{\"id\":\"2\",\"img\":\"scratch-8815-71.jpg\"}', '0', '0', 'updated slider: 2', '::1()', '2021-12-27 19:59:05'),
(99, 1, 'update', 1, 'Sliders', '3', 0, '{\"id\":\"3\",\"thumb\":\"Inner Slider\",\"img\":\"04-b_08.jpg\",\"video\":null,\"first_title\":\"Dahabeya\",\"second_title\":\"Welcome To Queen-Farida\",\"rank\":\"3\",\"deleted\":\"0\"}', '{\"id\":\"3\",\"img\":\"Generic-Banner-07-Web-App-Developer.png\"}', '0', '0', 'updated slider: 3', '::1()', '2021-12-27 19:59:05'),
(100, 1, 'update', 1, 'Sliders', '3', 0, '{\"id\":\"3\",\"thumb\":\"Inner Slider\",\"img\":\"Generic-Banner-07-Web-App-Developer.png\",\"video\":null,\"first_title\":\"\",\"second_title\":\"\",\"rank\":\"3\",\"deleted\":\"0\"}', '{\"id\":\"3\",\"img\":\"C-7Wf0jXkAAANjQ1.jpg\"}', '0', '0', 'updated slider: 3', '::1()', '2021-12-27 20:10:59'),
(101, 1, 'update', 1, 'Sliders', '3', 0, '{\"id\":\"3\",\"thumb\":\"Inner Slider\",\"img\":\"C-7Wf0jXkAAANjQ1.jpg\",\"video\":null,\"first_title\":\"\",\"second_title\":\"\",\"rank\":\"3\",\"deleted\":\"0\"}', '{\"id\":\"3\",\"img\":\"Generic-Banner-07-Web-App-Developer1.png\"}', '0', '0', 'updated slider: 3', '::1()', '2021-12-27 20:11:14'),
(102, 1, 'Create', 3, 'partners', '57', 0, '0', '{\"title\":\"new partner\",\"img\":\"C-7Wf0jXkAAANjQ2.jpg\",\"meta\":\"partners\"}', '0', '0', 'added Info: 57', '::1()', '2021-12-27 21:01:56'),
(103, 1, 'Updated', 3, 'partners', '57', 0, '0', '0', '0', '0', 'Changed partner:57', '::1()', '2021-12-27 21:02:23'),
(104, 1, 'Updated', 3, 'partners', '57', 0, '0', '0', '0', '0', 'Changed partner:57', '::1()', '2021-12-27 21:03:10'),
(105, 1, 'Delete', 3, 'partners', '57', 0, '{\"id\":\"57\",\"header\":\"\",\"title\":\"partner\",\"paragraph\":\"\",\"points\":\"\",\"meta\":\"partners\",\"img\":\"4096.jpg\",\"link\":\"\",\"rank\":\"0\"}', '0', '0', '0', 'Deleted Info:57', '::1()', '2021-12-27 21:16:43');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(5) NOT NULL,
  `activity_id` int(5) NOT NULL,
  `hotel_id` int(5) NOT NULL,
  `sub_id` int(5) NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `activity_id`, `hotel_id`, `sub_id`, `rank`, `deleted`) VALUES
(1, 1, 1, 6, 1, 0),
(2, 3, 1, 7, 2, 0),
(3, 2, 1, 1, 3, 0),
(4, 2, 2, 1, 4, 0),
(5, 2, 2, 2, 5, 0),
(6, 2, 2, 3, 6, 0),
(7, 2, 2, 4, 7, 0),
(8, 2, 2, 5, 8, 0),
(9, 2, 3, 1, 9, 0),
(10, 2, 3, 2, 10, 0),
(11, 2, 3, 3, 11, 0),
(12, 2, 3, 4, 12, 0),
(13, 2, 3, 5, 13, 0),
(14, 2, 4, 1, 14, 0),
(15, 2, 4, 2, 15, 0),
(16, 2, 4, 3, 16, 0),
(17, 2, 4, 4, 17, 0),
(18, 2, 4, 5, 18, 0),
(19, 2, 5, 1, 19, 0),
(20, 2, 5, 2, 20, 0),
(21, 2, 5, 3, 21, 0),
(22, 2, 5, 4, 22, 0),
(23, 2, 5, 5, 23, 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creat` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `view` tinyint(1) NOT NULL,
  `remove` tinyint(1) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `creat`, `edit`, `view`, `remove`, `link`, `type`, `rank`, `deleted`) VALUES
(1, 'Sliders', 1, 1, 1, 1, '', 'site', 1, 0),
(2, 'Competitions', 1, 1, 1, 1, '', 'site', 2, 0),
(3, 'partners', 1, 1, 1, 1, '', 'site', 3, 0),
(4, 'Gallery', 1, 1, 1, 1, '', 'site', 4, 0),
(5, 'Projects', 1, 1, 1, 1, 'admin/site_manager/projects_manager', 'store', 5, 0),
(6, 'العملاء', 1, 1, 1, 1, 'admin/clients', 'store', 5, 0),
(7, 'المخزن', 1, 1, 1, 0, 'admin/items/groups', 'store', 4, 0),
(8, 'التقارير', 1, 1, 1, 1, 'reports', 'store', 6, 0),
(11, 'التسجيل', 1, 1, 1, 1, 'admin/log_activity', 'store', 6, 0),
(13, 'Site manager', 1, 1, 1, 1, 'admin/site_manager', 'store', 10, 0),
(14, 'About Us', 1, 1, 1, 1, '', 'site', 17, 0),
(15, 'Rules', 1, 1, 1, 1, 'admin/site_manager/rules_manager', 'store', 10, 0),
(17, 'portfolio', 1, 1, 1, 1, '', 'site', 12, 0),
(18, 'competitors', 1, 1, 1, 1, '', 'site', 13, 0),
(19, 'contact', 1, 1, 1, 1, '', 'site', 14, 0),
(20, 'Footer', 1, 1, 1, 1, '', 'site', 14, 0),
(21, 'Client Requests', 1, 1, 1, 1, '', 'site', 15, 0),
(22, 'gallery', 1, 1, 1, 1, '', 'site', 12, 0),
(23, 'team', 1, 1, 1, 1, '', 'site', 13, 0),
(24, 'Winners', 1, 1, 1, 1, '', 'site', 14, 0),
(25, 'language', 1, 1, 1, 1, '', 'site', 15, 0),
(26, 'voters', 1, 1, 1, 1, '', 'site', 16, 0),
(27, 'rules', 1, 1, 1, 1, '', 'site', 17, 0);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `to_branch_id` int(11) DEFAULT NULL,
  `to_dep_id` int(11) DEFAULT NULL,
  `readedby` int(11) DEFAULT NULL,
  `head` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(3) NOT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(5) NOT NULL,
  `activity_id` int(5) NOT NULL,
  `hotel_id` int(5) NOT NULL,
  `sub_id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `data` text NOT NULL,
  `image` varchar(150) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `special` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `activity_id`, `hotel_id`, `sub_id`, `name`, `data`, `image`, `price`, `currency`, `special`, `deleted`, `timestamp`) VALUES
(1, 1, 1, 6, 'Test', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(2, 3, 1, 7, 'Test 1', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(3, 2, 1, 1, 'Test 2', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(4, 2, 2, 1, 'Test 3', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(5, 2, 2, 2, 'Test 4', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(6, 2, 2, 3, 'Test 5', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(7, 2, 2, 4, 'Test 6', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(8, 2, 2, 5, 'Test 7', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(9, 2, 3, 1, 'Test 8', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(10, 2, 3, 2, 'Test 9', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(11, 2, 3, 3, 'Test 10', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(12, 2, 3, 4, 'Test 11', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(13, 2, 3, 5, 'Test 12', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(14, 2, 4, 1, 'Test 13', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(15, 2, 4, 2, 'Test 14', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(16, 2, 4, 3, 'Test 15', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(17, 2, 4, 4, 'Test 16', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(18, 2, 4, 5, 'Test 17', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(19, 2, 5, 1, 'Test 18', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(20, 2, 5, 2, 'Test 19', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(21, 2, 5, 3, 'Test 20', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(22, 2, 5, 4, 'Test 21', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(24, 1, 4, 6, 'test 23', 'Test Test Test Test Test55', 'IMG_3782.JPG', '10000', '$', 1, 0, '2020-01-02 14:01:53');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(5) NOT NULL,
  `boat_id` int(5) NOT NULL DEFAULT 1,
  `title` varchar(150) NOT NULL,
  `remarks` text NOT NULL,
  `time` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `location` varchar(250) NOT NULL,
  `price` double(15,2) NOT NULL,
  `status` int(5) NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `boat_id`, `title`, `remarks`, `time`, `date`, `image`, `location`, `price`, `status`, `rank`, `deleted`, `timestamp`) VALUES
(1, 1, 'Normal Package', 'Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo.', '09:00', '2021-08-01', '3.png', 'Aswan', 100.00, 2, 1, 0, '2021-07-09 11:50:37'),
(2, 1, 'Honeymooners Package', 'Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo.', '09:00', '2021-08-01', '1.png', 'Aswan', 200.00, 2, 2, 0, '2021-07-09 11:50:40'),
(3, 1, 'VIP Boat Package', 'Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo,Test Text For Demo.', '09:00', '2021-08-01', '2.png', 'Aswan', 500.00, 2, 3, 0, '2021-07-09 11:50:42');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `competition_id` int(11) DEFAULT NULL,
  `competitor_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `payment` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `project_link` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `votes` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `school` varchar(150) DEFAULT NULL,
  `trainer` varchar(50) DEFAULT NULL,
  `trainer_name` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `payment_attach` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `started_at` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `code`, `competition_id`, `competitor_id`, `title`, `description`, `payment`, `project_link`, `image`, `votes`, `active`, `deleted`, `timestamp`, `school`, `trainer`, `trainer_name`, `status`, `payment_attach`, `price`, `started_at`, `category`) VALUES
(5, 'PRO20215', 1, 2, 'CREATIVE project KALLYAS', ' Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings.', 'cash', 'https://www.ikcc.info/en/account/login_page', '40961.jpg', 1, 0, 0, '2021-12-10 22:49:17', 'sadasd', 'with trainer', NULL, 4, '40963.jpg', '0.00', '2021-11-20', 46),
(6, 'PRO20216', 1, 4, 'CREATIVE project KALLYAS', ' Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings.', 'cash', 'https://www.ikcc.info/en/account/login_page', 'animation_500_kw7wftcr.gif', 1, 0, 0, '2021-12-10 22:54:50', 'sadasd', 'with trainer', NULL, 4, '40963.jpg', '0.00', '2021-11-20', 47),
(7, 'PRO20217', 1, 3, 'CREATIVE project KALLYAS', ' Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings.', 'cash', 'https://www.ikcc.info/en/account/login_page', '0_Tm5YhkSDJAbXX6FL.png', 1, 0, 0, '2021-12-10 22:54:59', 'sadasd', 'with trainer', NULL, 4, '40963.jpg', '0.00', '2021-11-20', 48),
(9, 'PRO20219', 4, 1, 'CREATIVE project KALLYAS', ' Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings.', 'cash', 'https://www.ikcc.info/en/account/login_page', '40961.jpg', 20, 0, 0, '2021-12-15 01:16:58', 'sadasd', 'with trainer', NULL, 4, 'banner-competitions.png', '0.00', '2021-11-20', 46),
(10, 'PRO202110', 4, 1, 'CREATIVE project KALLYAS', ' Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings.', 'cash', 'https://www.ikcc.info/en/account/login_page', 'animation_500_kw7wftcr.gif', 15, 0, 0, '2021-12-10 22:58:21', 'sadasd', 'with trainer', NULL, 4, '40963.jpg', '0.00', '2021-11-20', 46),
(11, 'PRO202111', 4, 1, 'CREATIVE project KALLYAS', ' Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings. Currently one of the best selling items on ThemeForest, featured across the marketplace and awarded with thousands of 5 stars ratings.', 'cash', 'https://www.ikcc.info/en/account/login_page', '0_Tm5YhkSDJAbXX6FL.png', 17, 0, 0, '2021-12-15 00:44:32', 'sadasd', 'with trainer', NULL, 1, '40963.jpg', '0.00', '2021-11-20', 46),
(12, 'PRO202112', 2, 1, 'project', 'asdasdasd', 'Vodafone', 'https://www.ikcc.info/en/account/login_page', '0_Tm5YhkSDJAbXX6FL.png', 30, 0, 0, '2021-12-23 20:46:30', 'thanaweya askria', 'with trainer', 'test', 2, 'banner-competitions.png', '500.00', '2021-11-22', 46),
(15, 'PRO202113', 1, 1, '', '', '', '', '', 0, 0, 0, '2021-12-23 20:54:29', 'thanaweya askria', 'with trainer', 'coach name', 1, '', '0.00', '', 48);

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

CREATE TABLE `rules` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `video_link` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rank` tinyint(1) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rules`
--

INSERT INTO `rules` (`id`, `title`, `icon`, `content`, `file_name`, `video_link`, `rank`, `disabled`, `deleted`, `timestamp`) VALUES
(1, 'Sign Up', 'fas fa-address-card', 'Customer support is a range of customer services to assist customers in making cost effective and correct use of a product. It includes assistance in planning, installation, training, troubleshooting, maintenance, upgrading, and disposal of a product.                            ', 'What is ReadySteadyShare.docx', 'https://www.youtube.com/watch?v=oDrPsZxk-WM', 1, 0, 0, '2021-12-09 15:55:20'),
(2, 'Competition Registration', 'icon-process3', 'Customer support is a range of customer services to assist customers in making cost effective and correct use of a product. It includes assistance in planning, installation, training, troubleshooting, maintenance, upgrading, and disposal of a product.                                                        ', 'What is ReadySteadyShare.docx', '', 2, 0, 0, '2021-12-09 15:55:20'),
(3, 'Voters Registration', 'far fa-thumbs-up', 'Customer support is a range of customer services to assist customers in making cost effective and correct use of a product. It includes assistance in planning, installation, training, troubleshooting, maintenance, upgrading, and disposal of a product.                            ', 'What is ReadySteadyShare.docx', '', 3, 0, 0, '2021-12-09 15:55:20'),
(4, 'Competition Rules', 'fas fa-pencil-alt', 'Customer support is a range of customer services to assist customers in making cost effective and correct use of a product. It includes assistance in planning, installation, training, troubleshooting, maintenance, upgrading, and disposal of a product.                            ', 'What is ReadySteadyShare.docx', '', 4, 0, 0, '2021-12-09 15:55:20'),
(9, 'new competion rule', 'fas fa-pencil-alt', 'new competion rule new competion rule                                                                                                                ', 'shining-circle-purple-lighting-isolated-dark-background_1441-2396.jpg', 'test/test', 5, 0, 0, '2021-12-18 01:32:28');

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `id` int(11) NOT NULL,
  `school` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `site_files`
--

CREATE TABLE `site_files` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `form_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_files`
--

INSERT INTO `site_files` (`id`, `module_id`, `form_id`, `user_id`, `file_name`, `caption`, `timestamp`) VALUES
(3, 27, '2', 0, 'Screenshot 2021-12-10 014129.png', 'Sign Up', '2021-12-09 20:30:28'),
(4, 0, '6', 1, '4096.jpg', '', '2021-12-17 23:37:55'),
(5, 0, '6', 1, 'af8d63a477078732b79ff9d9fc60873f.jpg', '', '2021-12-17 23:38:21'),
(6, 0, '6', 1, '40961.jpg', '', '2021-12-17 23:43:20'),
(7, 15, '6', 1, 'af8d63a477078732b79ff9d9fc60873f1.jpg', '', '2021-12-17 23:47:09'),
(8, 15, '6', 1, 'banner-competitions.png', '', '2021-12-18 00:28:17'),
(9, 15, '6', 1, 'af8d63a477078732b79ff9d9fc60873f2.jpg', '', '2021-12-18 00:48:55'),
(10, 15, '6', 1, '40962.jpg', '', '2021-12-18 00:54:46'),
(11, 15, '7', 1, '40964.jpg', '', '2021-12-18 01:24:19'),
(12, 15, '7', 1, 'af8d63a477078732b79ff9d9fc60873f3.jpg', '', '2021-12-18 01:24:23'),
(13, 27, '8', 1, '40966.jpg', '', '2021-12-18 01:28:39'),
(14, 27, '8', 1, 'af8d63a477078732b79ff9d9fc60873f4.jpg', '', '2021-12-18 01:28:39'),
(15, 27, '9', 1, '4096.jpg', '', '2021-12-19 20:20:39'),
(16, 27, '9', 1, 'childrens-robotics-classes.webp', '', '2021-12-19 20:20:48'),
(17, 27, '9', 1, 'banner-competitions.png', '', '2021-12-19 20:20:48');

-- --------------------------------------------------------

--
-- Table structure for table `site_groups`
--

CREATE TABLE `site_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_groups`
--

INSERT INTO `site_groups` (`id`, `group_name`, `serial`, `img`, `rank`, `deleted`) VALUES
(1, 'HVAC ACCESSORIES', 'hva', 'hvac_accessories.jpg', 1, 0),
(2, 'VENTILATION FANS', 'ven', 'ventilation fans.jpg', 2, 0),
(3, 'ELECTRICAL MATERIALS', 'elec', 'ELECTRICAL MATERIALS.jpg', 3, 0),
(4, 'GRILLES & DIFFUSERS', 'gril', 'grilles diffusers.jpg', 4, 0),
(5, 'G.I. & ACCESSORIES', 'g.i.', 'G.I. & ACCESSORIES.jpg', 5, 0),
(6, 'P.I. & ACCESSORIES', 'p.i.', 'P.I. & ACCESSORIES.jpg', 6, 0),
(7, 'AIR CONDITION UNITS', 'air', 'AIR CONDITION UNITS.jpg', 7, 0),
(8, 'THERMOSTAT & CONTROL VALVES', 'ther', 'THERMOSTAT & CONTROL VALVES.jpg', 8, 0),
(9, 'AC FILTERS', 'ac', 'AC FILTERS.jpg', 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_group_items`
--

CREATE TABLE `site_group_items` (
  `id` int(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_group_items`
--

INSERT INTO `site_group_items` (`id`, `group_id`, `item_name`, `serial`, `company`, `description`, `img`, `file`, `rank`, `timestamp`, `deleted`) VALUES
(1, 1, 'وش مبنى 100 سم', '00133E8', '1', 'Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit.', '1.jpg', '1.pdf', 1, '2019-04-18 14:46:10', 0),
(2, 1, 'حديد 4 لنية ', '002225B', '4', 'حديد 4 لنية ', '', '', 2, '2019-04-24 14:50:10', 0),
(3, 1, 'test', '009B11E', '4', 'حديد 3 لنية', '', '', 3, '2019-04-24 15:22:01', 0),
(4, 3, 'حديد 4 لنية', '00269BB', '1', 'حديد 4 لنية', '', '', 0, '2019-04-24 15:23:21', 0),
(5, 3, 'حديد 4 لنية', '00F436C', '6', 'حديد 4 لنية', '', '', 0, '2019-04-24 15:24:17', 0),
(6, 3, 'حديد 2.5 لنية أطوال', '00A6820', '7', 'حديد 2.5 لنية', '', '', 0, '2019-04-24 15:26:18', 0),
(7, 3, 'حديد 2 لنية لفف', '003988F', '1', 'حديد 2 لنية لفف', '', '', 0, '2019-04-24 15:29:51', 0),
(8, 7, 'أسمنت الجيش رتبة 52.5 بورتلاندى', '0091BED', '13', 'أسمنت الجيش رتبة 52.5 بورتلاندى', '', '', 0, '2019-04-24 15:36:18', 0),
(9, 7, 'أسمنت السويدى', '0009D57', '12', 'أسمنت السويدى', '', '', 0, '2019-04-24 16:10:52', 0),
(10, 7, 'أسمنت المصرية', '00C27C9', '11', 'أسمنت المصرية', '', '', 0, '2019-04-24 16:11:38', 0),
(11, 7, 'أسمنت سويتر', '00D982E', '25', 'أسمنت سويتر', '', '', 0, '2019-04-24 16:15:22', 0),
(12, 7, 'أسمنت الجيش رتبة 42.5 بورتلاندى العريش', '00B3C18', '14', 'أسمنت الجيش رتبة 42.5 بورتلاندى العريش', '', '', 0, '2019-04-24 16:18:53', 0),
(14, 1, 'test2', '008C60E', 'test2', 'test2 test2 test2 test2', '556a0152-fab8-428f-9010-215a63d9fa00.png', '710b7b44-f1e3-4ecf-bf81-37ca8927664c1.png', 0, '2019-10-04 20:23:52', 0),
(15, 1, 'test2w', '00A31BA', 'test2w', 'test2wtest2wtest2w test2wtest2w', '556a0152-fab8-428f-9010-215a63d9fa00.png', 'Book121.csv', 1, '2019-10-04 20:27:44', 0),
(16, 2, 'test2ww', '00F9BB8', 'test2w', 'test 2uhsd', '6bc837d5-1df8-4527-8e1a-61ba7a17c5a8.png', '710b7b44-f1e3-4ecf-bf81-37ca8927664c.png', 5, '2019-10-04 22:38:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_meta_data`
--

CREATE TABLE `site_meta_data` (
  `id` int(11) NOT NULL,
  `header` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paragraph` text COLLATE utf8_unicode_ci NOT NULL,
  `points` text COLLATE utf8_unicode_ci NOT NULL,
  `meta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_meta_data`
--

INSERT INTO `site_meta_data` (`id`, `header`, `title`, `paragraph`, `points`, `meta`, `img`, `link`, `rank`) VALUES
(1, 'logo', ' ', '', '', 'site_logo', 'ISCPO-2021_logo_en_1.png', '', 1),
(2, 'slider', '', '', '', 'slider_image', '0_Tm5YhkSDJAbXX6FL.png', '', 1),
(3, 'slider', 'Scratch Game-Script Olympiad', 'Contestants will create NEW and original projects (story, animation, game, mixed, etc...) with the main subject: ... ! All projects must be written in Scratch 3.0 language (in your Scratch account on https://scratch.mit.edu) ! Having a nice intro, menus, storyline, animation, music and games inside will be a plus! This is an INDIVIDUAL competition, no TEAMS allowed!', '', 'slider_data', 'scratch-8815-7.jpg', '', 1),
(4, 'social', 'Facebook', 'https://web.facebook.com/hany.godzila/', '', 'social_data', 'fab fa-facebook-f', '', 1),
(5, 'social', 'Twitter', '', '', 'social_data', 'fab fa-twitter', '', 2),
(8, 'social', 'Google Plus', '', '', 'social_data', 'fab fa-google-plus-g', '', 3),
(9, 'contact', 'Phone', '+20 122 774 3419', '', 'call_us', 'phone-header fas fa-phone ml-5 visible-xs visible-sm visible-md', '', 1),
(10, 'about', 'Smart Goals Academy', 'At the initiative of NGO \"Asociatia SASORYCODE\" (non-governmental and non-profit organization) and with exceptional support of many partners and software companies, we proudly created this page to support children who love the world of programming and computers. IKCC is the starting point for these wonderful, passionate and inventive children.', '', 'about_us', '', '', 1),
(11, 'about', 'Location', 'Alkawthar disstrict، 147													Mohamed Saied st.(Metro st، Red Sea Governorate)', '', 'location', 'https://goo.gl/maps/i4Rr2VnTvW9MQgFu7', '', 1),
(12, 'contact', 'Email', 'info@smartgoals.academy', '', 'email_us', '', '', 1),
(13, 'Tel', '01002741794', '', '', 'footer_tel', '', '', 1),
(14, 'Email', 'info@divemore-group.com', '', '', 'footer_email', '', '', 1),
(15, 'Working Hours', '9am-5pm', '', '', 'footer_working', '', '', 1),
(16, 'Hurghada', 'Diving', '', 'Trips', 'gallery', '21.jpg', '', 1),
(17, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', '13620906_976161869163509_1200008273719622505_n.jpg', '', 1),
(18, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', '13731617_980013822111647_805224201259891546_n.jpg', '', 1),
(19, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', '14102607_1012532422193120_5040987907399577544_n.jpg', '', 2),
(20, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0027.JPG', '', 3),
(21, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0174.JPG', '', 4),
(22, 'Sent Successfully', 'Sent', '#000000', '', 'clients_req_status', '', '', 1),
(23, 'Failed to sent', 'Failed', '#bd1212', '', 'clients_req_status', '', '', 1),
(24, 'Waiting to send', 'Waiting', '#467b2b', '', 'clients_req_status', '', '', 1),
(25, 'Business City', '3.5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'cabo-pulmo-diving-11.jpg', '', 1),
(26, 'Business City', '4.5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'Discover-Scuba-Diving1.jpg', '', 3),
(28, 'Business City', '5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'scuba-diving2-960x11491.jpg', '', 2),
(29, 'Julien Miro', 'Software Developer', '', '', 'team', '11.jpg', '', 1),
(30, 'Joan Williams', 'Support Operator', '', '', 'team', '21.jpg', '', 2),
(32, 'Benedict Smith', 'Creative Director', '', '', 'team', '31.jpg', '', 3),
(33, 'Madlen Green', 'Sales Manager', '', '', 'team', '41.jpg', '', 4),
(34, 'Julien Miro', 'Julien Miro', '', '', 'team', 'Discover-Scuba-Diving.jpg', '', 5),
(35, 'Waiting payment', 'Waiting payment', '#bd1212', '', 'project_status', '', '2', 2),
(36, 'published project', 'published project', '#27a9e3', '', 'project_status', '', '4', 4),
(37, 'Payment Confirm', 'Payment Confirm', '#14d707', '', 'project_status', '', '3', 3),
(38, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0179.JPG', '', 5),
(39, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_0216.JPG', '', 6),
(40, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_1324.JPG', '', 7),
(41, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_1417.JPG', '', 8),
(42, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', 'IMG_1624.JPG', '', 2),
(43, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', 'IMG_1629.JPG', '', 3),
(44, 'Hurghada', 'Diving', '', 'Sea_Trip', 'gallery', 'IMG_1632.JPG', '', 4),
(45, 'Hurghada', 'Diving', '', 'Diving_Trip', 'gallery', 'IMG_1924.JPG', '', 9),
(46, 'Age Category less than 8', '< 8', 'less-than-8-category', '', 'age_category', '', '', 0),
(47, 'Age Category from 8 to 12', 'BETWEEN 8 AND 12', 'between-8-and-12-category', '', 'age_category', '', '', 0),
(48, 'Age Category more than 12', '> 12', 'more-than-12-category', '', 'age_category', '', '', 0),
(49, 'What is ReadySteadyShare? (Logo of RSS)', '', 'ReadySteadyShare is an international students coding competition organized by Smart Goals Academy for elementary and middle school students. RSS is uses Scratch as competitive programming, Scratch is a high-level block-based visual programming language and website targeted primarily at children 8–16 as an educational tool for programming.', '', 'about_us_what_is_readysteadyshare', '', '', 0),
(50, 'About us?', '', 'RSS is coding competition for School students,    we care about our children! We care about the future! And the future is digital! We support children to perform in software programming, advanced technologies and computer science.\r\nRSS operates on, is measured on and strives towards the following values:\r\nDIVERSITY, PASSION, INNOVATION.\r\n', '', 'about_us_rss', '', '', 0),
(51, 'Description of competition?', '', 'ReadySteadyShare is to develop children programming skills by creating games, stories or animations. Children will be attracted to these creative projects and will unleash their imagination.\r\nThe contests will help children develop their coding skills, meet new challenges and achieve the highest level of software programming performance.', '', 'about_us_competition_description', '', '', 0),
(52, 'Design', '', 'Add backdrop, sprite, diffculty, sound ,scoreThe contests will help children develop their coding skills, meet new challenges and achieve the highest level of software programming performance.', '', 'about_us_design', '', '', 0),
(53, 'Coding', '', 'Project will become a bit more specialized through the code, or, the set of instructions we provide in order for the game to carry out how we’d like it to', '', 'about_us_coding', '', '', 0),
(54, 'Voting', '', 'Submit and share your project to get more votes', '', 'about_us_voting', '', '', 0),
(55, 'smart goals', 'smart goals', '', '', 'partners', 'partner_1.png', '', 1),
(56, 'Just Created', 'Just Created', '#3e5569', '', 'project_status', '', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `thumb` varchar(150) NOT NULL,
  `img` varchar(150) NOT NULL,
  `video` varchar(150) DEFAULT NULL,
  `first_title` text NOT NULL,
  `second_title` text NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `thumb`, `img`, `video`, `first_title`, `second_title`, `rank`, `deleted`) VALUES
(1, 'Main Slider Background image', '0_Tm5YhkSDJAbXX6FL1.png', '4096.jpg', 'Scratch Game-Script Olympiad', ' Contestants will create NEW and original projects (story, animation, game, mixed, etc...) with the main subject: ... ! All projects must be written in Scratch 3.0 language (in your Scratch account on https://scratch.mit.edu) ! Having a nice intro, menus, storyline, animation, music and games inside will be a plus! This is an INDIVIDUAL competition, no TEAMS allowed!', 1, 0),
(2, 'Magnified Image', 'scratch-8815-71.jpg', NULL, '', '', 2, 0),
(3, 'Inner Slider', 'Generic-Banner-07-Web-App-Developer1.png', NULL, '', '', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `text_color` varchar(150) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `rank` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_name`, `status_color`, `text_color`, `rank`) VALUES
(1, 'Draft', '#000000', '#FFF', 2),
(2, 'Avalible', '#b88121', '#FFF', 4),
(3, 'Cancelled', '#0088cc', '#FFF', 6),
(4, 'Unavalible', '#1f8d5d', '#FFF', 8);

-- --------------------------------------------------------

--
-- Table structure for table `sub_activity`
--

CREATE TABLE `sub_activity` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_activity`
--

INSERT INTO `sub_activity` (`id`, `name`, `deleted`) VALUES
(1, 'Diving', 0),
(2, 'Courses', 0),
(3, 'Intro', 0),
(4, 'Daily Dive', 0),
(5, 'Speciality', 0),
(6, 'Aqua Center', 0),
(7, 'SPA', 0);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(150) NOT NULL,
  `possion` varchar(100) NOT NULL,
  `info` text NOT NULL,
  `contact` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `id` int(5) NOT NULL,
  `boat_id` int(5) NOT NULL,
  `title` varchar(150) NOT NULL,
  `remarks` text NOT NULL,
  `from_location` varchar(250) NOT NULL,
  `to_location` varchar(250) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `status` int(5) NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `role_id` int(5) NOT NULL,
  `department` int(11) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `permission` tinyint(1) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `fullname`, `email`, `role_id`, `department`, `mobile_no`, `password`, `is_admin`, `permission`, `disabled`, `last_ip`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'hany.hisham', 'admin', 'info@alamtc.com', 1, 1, '12345', '$2y$10$1jn497ev2euAuPFua8V8RunCu7kZlbH2I4GYNSxtnlLpQkpx57j1m', 1, 1, 0, '', '2021-07-04 00:00:00', '2021-07-04 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_branches`
--

CREATE TABLE `user_branches` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(6) NOT NULL,
  `role_id` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_branches`
--

INSERT INTO `user_branches` (`id`, `user_id`, `branch_id`, `role_id`) VALUES
(317, 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `permission`) VALUES
(1, 'Admin', 1),
(2, 'IT', 1),
(3, 'GMs', 0),
(4, 'Staff', 0),
(5, 'Managers', 0),
(6, 'test', 1),
(7, 'test2', 0),
(8, 'test22', 0),
(9, 'test221', 0),
(10, 'test22', 0),
(11, 'test23', 0),
(12, 'test22', 0),
(13, 'test23', 0),
(14, 'test23', 0),
(15, 'test221', 0),
(16, 'test221', 0),
(17, 'test221', 0),
(18, 'test22', 0),
(19, 'test', 0),
(20, 'test22', 0),
(21, 'test', 0),
(22, 'test2', 0),
(23, 'test22', 0),
(24, 'test2', 0),
(25, 'cost', 1),
(26, 'recieving', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups_permission`
--

CREATE TABLE `user_groups_permission` (
  `id` int(11) NOT NULL,
  `role_id` int(6) NOT NULL,
  `module_id` int(11) NOT NULL,
  `g_view` tinyint(1) NOT NULL,
  `creat` tinyint(2) NOT NULL,
  `edit` tinyint(2) NOT NULL,
  `view` tinyint(2) NOT NULL,
  `remove` tinyint(2) NOT NULL,
  `pr_edit` tinyint(1) NOT NULL,
  `pr_approve` tinyint(1) NOT NULL,
  `recieve` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups_permission`
--

INSERT INTO `user_groups_permission` (`id`, `role_id`, `module_id`, `g_view`, `creat`, `edit`, `view`, `remove`, `pr_edit`, `pr_approve`, `recieve`) VALUES
(1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(2, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0),
(3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(12, 0, 2, 1, 0, 0, 0, 1, 1, 0, 0),
(13, 0, 3, 1, 1, 1, 1, 1, 0, 0, 0),
(14, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(17, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(18, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(19, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(21, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0),
(22, 1, 2, 1, 1, 1, 1, 1, 1, 0, 0),
(23, 1, 3, 1, 0, 0, 0, 1, 0, 0, 0),
(24, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(27, 1, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(28, 1, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 1, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(31, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(32, 2, 2, 1, 0, 0, 0, 1, 1, 0, 0),
(33, 2, 3, 1, 0, 0, 0, 1, 0, 0, 0),
(34, 2, 4, 1, 0, 0, 0, 0, 0, 0, 0),
(35, 2, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(36, 2, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(37, 2, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(38, 2, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(39, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(40, 2, 10, 1, 1, 1, 1, 0, 0, 0, 0),
(41, 6, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(42, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0),
(43, 6, 3, 0, 1, 0, 0, 0, 0, 0, 0),
(44, 6, 4, 0, 0, 1, 0, 0, 0, 0, 0),
(45, 6, 5, 0, 1, 0, 0, 0, 0, 0, 0),
(46, 6, 6, 0, 1, 0, 1, 0, 0, 0, 0),
(47, 6, 7, 1, 1, 0, 0, 0, 0, 0, 0),
(48, 6, 8, 0, 1, 0, 1, 0, 0, 0, 0),
(49, 6, 9, 0, 1, 1, 0, 0, 0, 0, 0),
(50, 6, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(51, 25, 1, 1, 0, 0, 1, 0, 0, 0, 0),
(52, 25, 2, 1, 0, 0, 1, 0, 0, 0, 0),
(53, 25, 3, 1, 1, 0, 0, 0, 0, 0, 0),
(54, 25, 4, 1, 0, 1, 0, 0, 0, 0, 0),
(55, 26, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(56, 26, 2, 1, 1, 1, 1, 0, 1, 0, 1),
(57, 26, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(58, 26, 4, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE `user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `creat` tinyint(4) NOT NULL,
  `edit` tinyint(4) NOT NULL,
  `view` tinyint(4) NOT NULL,
  `remove` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`id`, `user_id`, `module_id`, `creat`, `edit`, `view`, `remove`, `timestamp`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(2, 1, 2, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(3, 1, 3, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(4, 1, 4, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(5, 1, 5, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(6, 1, 6, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(7, 1, 7, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(8, 1, 8, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(9, 1, 9, 1, 0, 0, 0, '2019-01-15 13:55:20'),
(10, 1, 11, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(11, 1, 12, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(12, 1, 13, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(13, 1, 14, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(14, 1, 15, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(15, 1, 16, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(16, 1, 17, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(17, 1, 18, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(18, 1, 19, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(19, 1, 20, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(20, 1, 21, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(21, 1, 22, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(22, 1, 23, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(23, 1, 24, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(24, 1, 25, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(25, 1, 26, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(26, 1, 27, 1, 1, 1, 1, '2019-01-15 13:55:20');

-- --------------------------------------------------------

--
-- Table structure for table `voters`
--

CREATE TABLE `voters` (
  `id` int(11) NOT NULL,
  `oauth_provider` enum('facebook','google','twitter','') COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fb_user_id` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fb_access_token` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `voters`
--

INSERT INTO `voters` (`id`, `oauth_provider`, `fb_user_id`, `first_name`, `last_name`, `email`, `gender`, `picture`, `fb_access_token`, `created_at`, `updated_at`) VALUES
(4, 'facebook', '4467280963321282', 'Mahmoud', 'Mohier', 'master_olympia2000@yahoo.', NULL, '{\"data\":{\"height\":50,\"is_silhouette\":false,\"url\":\"https:\\/\\/platform-lookaside.fbsbx.com\\/platform\\/profilepic\\/?asid=4467280963321282&height=50&width=50&ext=1640983941&hash=AeRi4IRXGlKOJyQTiKU\",\"width\":50}}', 'EAADwe2lXtU4BABx1aWae1jcQOaesVKI559W7FwMpbUmyEz82DhgLgk5TWiitZAZCWm7TTAdRaPtxsm7KL7ZAW70xmY7D0OV71sOaKqMZAhFBwlj6yphjcCUPyGVcBApbGFfbdPZAOYKLYU1R49smzU5Tn4Q97VL93EY04Xkn8KAuZBYcZC43n5UOgXp7ieVqgZC7Wmw3qT5q1kQAxNd9ZBWjkJJ4U7hg4Q8ZChz238TSeVEC3vUD82FR0R4YNcZC5rMVg4JHMwdqtep4QZDZD', '2021-12-01 22:56:20', '2021-12-11 00:44:49');

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE `votes` (
  `id` int(11) NOT NULL,
  `voter_id` int(11) NOT NULL,
  `competitor_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `vote` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `votes`
--

INSERT INTO `votes` (`id`, `voter_id`, `competitor_id`, `project_id`, `vote`) VALUES
(1, 4, 2, 5, 1),
(2, 4, 3, 7, 1),
(3, 4, 1, 8, 1),
(4, 4, 4, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `winners`
--

CREATE TABLE `winners` (
  `id` int(11) NOT NULL,
  `competitor_id` int(11) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `rank` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `winners`
--

INSERT INTO `winners` (`id`, `competitor_id`, `disabled`, `rank`, `timestamp`) VALUES
(1, 1, 0, 1, '2021-12-12 12:03:31'),
(2, 2, 0, 2, '2021-12-12 12:04:02'),
(3, 3, 0, 3, '2021-12-12 12:04:02'),
(4, 3, 0, 2, '2021-12-21 22:02:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boats`
--
ALTER TABLE `boats`
  ADD PRIMARY KEY (`boatid`),
  ADD KEY `country` (`country`),
  ADD KEY `company` (`boat_name`);

--
-- Indexes for table `boats_cabins`
--
ALTER TABLE `boats_cabins`
  ADD PRIMARY KEY (`cabinid`),
  ADD KEY `room_no` (`cabin_no`);

--
-- Indexes for table `boat_info`
--
ALTER TABLE `boat_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `cabins`
--
ALTER TABLE `cabins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `supp_name` (`client_name`);

--
-- Indexes for table `clients_mail_queue`
--
ALTER TABLE `clients_mail_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `competitions`
--
ALTER TABLE `competitions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `competitors`
--
ALTER TABLE `competitors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `font_icons`
--
ALTER TABLE `font_icons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallary`
--
ALTER TABLE `gallary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `rules`
--
ALTER TABLE `rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_files`
--
ALTER TABLE `site_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_groups`
--
ALTER TABLE `site_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_name` (`group_name`),
  ADD UNIQUE KEY `serial` (`serial`);

--
-- Indexes for table `site_group_items`
--
ALTER TABLE `site_group_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `serial` (`serial`);

--
-- Indexes for table `site_meta_data`
--
ALTER TABLE `site_meta_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_activity`
--
ALTER TABLE `sub_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_branches`
--
ALTER TABLE `user_branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_groups_permission`
--
ALTER TABLE `user_groups_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `voters`
--
ALTER TABLE `voters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `winners`
--
ALTER TABLE `winners`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `boats`
--
ALTER TABLE `boats`
  MODIFY `boatid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `boats_cabins`
--
ALTER TABLE `boats_cabins`
  MODIFY `cabinid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `boat_info`
--
ALTER TABLE `boat_info`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cabins`
--
ALTER TABLE `cabins`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clients_mail_queue`
--
ALTER TABLE `clients_mail_queue`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `competitions`
--
ALTER TABLE `competitions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `competitors`
--
ALTER TABLE `competitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `font_icons`
--
ALTER TABLE `font_icons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=595;

--
-- AUTO_INCREMENT for table `gallary`
--
ALTER TABLE `gallary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `rules`
--
ALTER TABLE `rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `site_files`
--
ALTER TABLE `site_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `site_groups`
--
ALTER TABLE `site_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `site_group_items`
--
ALTER TABLE `site_group_items`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `site_meta_data`
--
ALTER TABLE `site_meta_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sub_activity`
--
ALTER TABLE `sub_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trips`
--
ALTER TABLE `trips`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `user_branches`
--
ALTER TABLE `user_branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=318;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_groups_permission`
--
ALTER TABLE `user_groups_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `user_permissions`
--
ALTER TABLE `user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `voters`
--
ALTER TABLE `voters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `votes`
--
ALTER TABLE `votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `winners`
--
ALTER TABLE `winners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
