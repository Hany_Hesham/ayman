
function initDTableReports(tid,cols,furl,data='',ord='',reportData){
  var printCols = [];
  for (var i = 1; i < cols.length; i++) {
         printCols[i-1] = i;
     }
  var combdata = '';
  var myorder = '';
  if (data != '') { combdata = data}else{combdata = '{}'};
  if (ord != '') { myorder = ord}else{myorder = 0}
     $(document).ready(function() {
       let table = $('#'+tid+'').DataTable({
          dom:"<'row'<'col-sm-1'l><'col-sm-3 'B><'col-sm-8'f>>" +
              "<'row'<'col-sm-12'tr>>" +
              "<'row'<'col-sm-5'i><'col-sm-7'p>>", 
          buttons:[ 
                    {
                      extend: 'collection',
                      className: 'btn btn-outline-secondary btn-sm',
                      text: 'Export <i class="far fa-file-alt "></i>',
                      background:false,
                      buttons: [
                                { extend: 'copy', className: 'dropdown-item', init: removeButtonDesign(),},
                                { extend: 'csv', className: '', init: removeButtonDesign(),},
                                { extend: 'excel', className: '', init: removeButtonDesign(), },
                                { text: 'PDF', className: '', init: removeButtonDesign(), 
                                  action: function ( e, dt, button, config ) {
                                          extractReportData();
                                        }, 
                                },     
                               ],
                     },
                     {    
                      extend: 'colvis',
                      text: 'Visable',
                      background:false,
                      init: removeButtonDesign(),
                     },
                  ],
          "processing"  : true,
          "serverSide" : true,
          "pagingType": "full_numbers",
          "searching": true,
          "draw" :true,
          "pageLength" :10,
          "language": {
                        "lengthMenu":'<select class="form-control">'+
                          '<option value="10">10</option>'+
                          '<option value="20">20</option>'+
                          '<option value="30">30</option>'+
                          '<option value="40">40</option>'+
                          '<option value="50">50</option>'+
                          '<option value="-1">All</option>'+
                          '</select>'
                      },
          "responsive": true,
          "createdRow": function( row, data, dataIndex){
            //------ Recieving By Department styling---------------------
              if (reportData.reportName == 'Recieving By Department') {
                  if( data[1] ==  `<td><strong style="font-size:18px;">Department: </strong></td>`){
                      $(row).css( "background-color","#EEEEEE" );
                  }
                  if( data[1] ==  `<td><strong style="font-size:16px;">Item Group: </strong></td>`){
                      $(row).css( {"background-color":"#828282","color":"#FFFFFF"} );
                  }
                  if( data[1] ==  `<td><strong style="font-size:14px;">Total Item Group :</strong></td>`){
                      $(row).css( {"background-color":"#EEEEEE"} );
                  }
                  if( data[1] ==  `<td><strong style="font-size:18px;color:#da542e">Total Departments</strong></td>` 
                      || data[1] ==  `<td><strong style="font-size:18px;">Total Tax</strong></td>`
                      || data[1] ==  `<td><strong style="font-size:18px;">Total Report</strong></td>`){
                      $(row).css( "background-color","#EEEEEE" );
                  }
                }
             //------ Recieving By Period styling---------------------
              if (reportData.reportName == 'Recieving By Period') {
                  if( data[1] ==  `<strong style="font-size:18px;">Item Group</strong>`){
                      $(row).css( "background-color","#EEEEEE" );
                  }
                  if( data[1] ==  `<strong style="font-size:18px;">Total</strong>`){
                      $(row).css( {"background-color":"#828282","color":"#FFFFFF"} );
                  }
                  if( data[2] ==  `<td><strong style="font-size:18px;color:#da542e">Total Groups</strong></td>` 
                      || data[2] ==  `<td><strong style="font-size:18px;">Total Tax</strong></td>`
                      || data[2] ==  `<td><strong style="font-size:18px;">Total Report</strong></td>`){
                      $(row).css( "background-color","#EEEEEE" );
                  }
                }   
             //------ Recieving By Period styling---------------------
              if (reportData.reportName == 'Recieving Records') {
                  if( data[1] ==  `<strong style="font-size:18px;">Item</strong>`){
                      $(row).css( "background-color","#EEEEEE" );
                  }
                  if( data[1] ==  `<strong style="font-size:18px;">Total</strong>`){
                      $(row).css( {"background-color":"#828282","color":"#FFFFFF"} );
                  }
                  if( data[2] ==  `<td><strong style="font-size:18px;color:#da542e">Total Items</strong></td>` 
                      || data[2] ==  `<td><strong style="font-size:18px;">Total Tax</strong></td>`
                      || data[2] ==  `<td><strong style="font-size:18px;">Total Report</strong></td>`){
                      $(row).css( "background-color","#EEEEEE" );
                  }
                }  
             //------ item pricing differences styling---------------------
              if (reportData.reportName == 'Item Pricing Differences Report') {
                  if( data[1] ==  `<strong style="font-size:18px;">Item</strong>`){
                      $(row).css( "background-color","#EEEEEE" );
                  }
                  if( data[1] ==  `<strong style="font-size:18px;">Total</strong>`){
                      $(row).css( {"background-color":"#828282","color":"#FFFFFF"} );
                  }
                  if( data[2] ==  `<td><strong style="font-size:18px;color:#da542e">Total Items</strong></td>` 
                      || data[2] ==  `<td><strong style="font-size:18px;">Total Tax</strong></td>`
                      || data[2] ==  `<td><strong style="font-size:18px;">Total Report</strong></td>`){
                      $(row).css( "background-color","#EEEEEE" );
                  }
                }     
             //------ Recieving By Period styling---------------------
              if (reportData.reportName == 'Recieving By Supplier (Total)') {
                  if(data[1].match(/<strong style="font-size:20px;">Supplier:.*/)){
                      $(row).css( "background-color","#EEEEEE" );
                  }
                  if( data[1] ==  `<strong style="font-size:20px;">Total</strong>`){
                      $(row).css( {"background-color":"#828282","color":"#FFFFFF"} );
                  }
                  if( data[1] ==  `<strong style="font-size:20px;" class="text-danger">Total Report</strong>`){
                      $(row).css( "background-color","#EEEEEE" );
                  }
                }          
             //------ Recieving By Period styling---------------------
              if (reportData.reportName == 'Recieving By Supplier') {
                  if( data[2] ==  `<strong style="font-size:20px;backgroun-color:black;">Total</strong>`){
                      $(row).css( {"background-color":"#828282","color":"#FFFFFF"} );
                  }
                }             
            },
          "order": [[ myorder, "desc" ]],
          "columns": cols,                               
            "ajax": {
               'async': false,
                url  : furl,
                type : 'POST',
                data : combdata
                 },
           });
         
         table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
        });
     }     