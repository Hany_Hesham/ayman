/************************************************************************
*************************************************************************
@Name :       	jRating - jQuery Plugin
@Revison :    	3.0
@Date : 		28/01/2013 
@Author:     	 ALPIXEL - (www.myjqueryplugins.com - www.alpixel.fr) 
@License :		 Open Source - MIT License : http://www.opensource.org/licenses/mit-license.php
 
**************************************************************************
*************************************************************************/
$(document).ready(function(){
      $('.basic').jRating();
      
      $('.exemple2').jRating({
        type:'small',
        length : 40,
        decimalLength : 1,
      });
      
      $('.exemple3').jRating({
        step:true,
        length : 20 
      });
      
      $('.exemple4').jRating({
        isDisabled : true
      });
      
      $('.exemple5').jRating({
        length:10,
        decimalLength:1,
        onSuccess : function(){
          alert('Success : your rate has been saved :)');
        },
        onError : function(){
          alert('Error : please retry');
        }
      });
      
      $(".exemple6").jRating({
        length:10,
        decimalLength:1,
        showRateInfo:false
      });
      
      $('.exemple7').jRating({
        step:true,
        length : 20,
        canRateAgain : true,
        nbRates : 3
      });
    });